<?php
/**
* Template Name: Página Livros
*
* @package WordPress
* @author Mais Code Tecnologia
* @since First Version
*/ 
get_header(); 

?>
    
<style type="text/css">
  
    .sec-livros .pagination{
      display: block;
      margin-top: 50px;
    }
</style>
    
<div class="main-container">
    <section id="livros-pagina">

        <div class="container">
            <div class="row">
                <div class="col-md-9 mb-xs-24 sec-livros" id="conteudo_paginado">

                    <h1><?php $postType = get_queried_object(); echo esc_html($postType->labels->name); ?></h1>

                    <?php 

                        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                        $limit = 10;
                        $offset = $limit * ($paged - 1);                         

                        $args = array(
                            'post_type'      => 'livros',                            
                            'offset'         => $offset,
                            'orderby'        => 'title',
                            'order'          => 'ASC',
                            'nopaging' => false,
                        );

                          $postsList = new WP_Query($args);

                       ?>

                            <?php if ( $postsList->have_posts() ) :  ?>
                            
                             <?php while ( $postsList->have_posts() ) : $postsList->the_post(); ?>                             
                           
                            <?php  $image = wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() ));  ?>   

                             <div class="post-snippet container-livro" style="clear:both;">                                    
                                    <p>
                                        <?php if ($image != ""): ?>
                                            <div class="thumb_pequeno">
                                                <img class="mb24" alt="Post Image" src="<?php echo $image; ?>" width="100%">
                                            </div>
                                        <?php endif ?>
                                        <h4 class="inline-block"><?php the_title(); ?></h4>
                                        
                                        <?php the_excerpt(); ?>
                                        <span class="label"><?php the_field('data_livro'); ?></span>
                                    </p>
                                </div>
                                <hr>
                            
                            <?php endwhile ?>
                            
                    <?php endif ?>  
                    <?php paginacao($limit); ?>  
                </div>
                <div class="col-md-3 hidden-sm widget-methodus">
		               
                    <div class="busca-widget">
                        <form action="<?php bloginfo('siteurl'); ?>" id="searchform" method="get">
                            <div class="input-artigo">
                                <input type="hidden" name="tipo" value="livros" id="artigo">
                                <input type="text" name="s" value="<?php the_search_query(); ?>" />                                
                                <input type="submit" class="btn btn-input btn-sm" value="Buscar" id="searchsubmit" />
                                
                            </div>
                        </form>
                    </div>

                    <h6 class="title"><?php the_field('categorias_livros',340); ?></h6>		              
                     <?php
                          $args = array(
                           'taxonomy' => 'categoria_livros',
                           'orderby' => 'date',
                           'order'   => 'DESC',
                           'hide_empty' => false
                         );

                          $cats = get_terms($args);
                          $cont = 1;

                        ?>

                        <ul class="link-list">
                        <?php
                          foreach($cats as $cat) { 
                          ?>

                            <li><a href="<?php echo get_term_link($cat->term_id); ?>"><?php echo $cat->name; ?></a></li>

                          <?php 
                            }
                           ?>
                        </ul>

                        <h6 class="title"><?php the_field('texto_mais_acessados',326); ?></h6>
			                   
                        <ul class="link-list">



                        <?php 
                        $args = array(
                            'post_type' => 'artigos',
                            'post_status' => 'publish',
                            'posts_per_page' => '20',
                            'meta_key' => 'post_views_count',
                            'order' => 'DESC'
                        );
                        $my_posts = new WP_Query( $args );
       
                        if ( $my_posts->have_posts() ) : 
                        ?>
                            
                          <?php while ( $my_posts->have_posts() ) : $my_posts->the_post(); 

                          ?>

                            <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
     
                          <?php endwhile; ?>
                         <?php endif; ?>
         
                        </ul>
                </div>

            </div>
        </div>

    </section>
</div>

<?php get_footer(); ?>