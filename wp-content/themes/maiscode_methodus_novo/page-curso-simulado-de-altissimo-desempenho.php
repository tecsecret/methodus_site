<?php
/**
* Template Name: Curso Simulado de altissimo desempenho
* @package WordPress
* @author Mais Code Tecnologia
* @since First Version
*/
?>

<?php get_header(); ?>

<style type="text/css">
.courseColorBlue {
    color: #4682B4;
  }
  .courseColorRed {
    color: #B22222;
  }
  .courseColorGreen {
    color: #006400;
  }
  .courseBackgroundColor {
    background-color: #4682B4;
  }
  #secStoryModB .storyContent svg path {
    fill: #4682B4;
  }
  .sliderCard svg path {
    fill: #4682B4;
  }
  mark {
  background-color: yellow;
  color: black;
  }

  #secTopModB:before {
    content: '';
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    background: linear-gradient(270deg, rgb(0 69 160 / 70%) 23.16%, rgba(52, 50, 50, 0) 118.16%);
  }
  #secCallBannerModB .bgTransformation:before {
    content: '';
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    background: linear-gradient(179.64deg, rgba(96, 94, 94, 0) -36.93%, rgba(160, 0, 0, 0.6) 85.54%);
  }
  .loadd{
    display: none;
    opacity: .5;
    background: #000;
    width: 100%;
    position: absolute;
    top: 0px;
    height: 100vh;
    z-index: 10000000;
  }
 
  .sliderCardDepo .depo-prev
  {
    height: 36px;
    width: 220px;
    z-index: 100000;
    object-fit: contain;
  }
  .sliderCardDepo .depo-next{
    width: 235px;
    height: 36px;
    object-fit: contain;
  }

  #secStoryModB .storyContent {
      margin-left: 195px;
      margin-right: 195px;
      position: relative;
  }

  #secStoryModB .storyContent {
    margin: 0px; 
  }

  #secReminder  {
    padding-bottom: 0px;
  }


</style>

<section id="secTopModB" style="background-image: url('https://methodus.com.br/wp-content/uploads/2020/12/fea-prova-alunos_fto-cecilia-bastos-ribeiro_1.jpg')">
  <div class="container">
    <div class="row">
      <div class="col-md-7 col-md-offset-5 col-sm-7 col-sm-offset-5 topContent">
        <h2>COMO ATINGIR 90% DE ACERTOS EM CONCURSOS, VESTIBULARES OU ENEM.</h2>
        <p>Comprovado e real: apresentamos a você uma metodologia muito poderosa e eficiente para estar entre os primeiros colocados.</p>
        <img src="https://methodus.com.br/wp-content/uploads/2020/08/curso-de-oratoria-lp-1.png">
      </div>
    </div>
  </div>
</section>


<!-- BANNER CHAMADA -->
<section id="secCallBannerModB" class="courseBackgroundColor">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2>O SEGREDO REAL PARA PASSAR EM PROVAS E CONCURSOS.</h2>
      </div>
    </div>
  </div>
  </section>

<!-- O que é -->
<section id="secStoryModB">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="storyContent">
      </div>
      </div>
  </div>
    <div class="row">
      <div class="col-md-12">
        <h2 class="courseColorBlue">&#x25cf; O QUE É: &#x25cf;</h2>
        <div class="storyContent">
          <svg class="quoteStart" width="71" height="56" viewBox="0 0 71 56" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M16.2565 0L26.072 7.67124C21.4225 12.4444 18.3229 16.962 16.7731 21.2237C15.2232 25.3151 14.4483 29.4916 14.4483 33.7534L8.24908 29.4064H27.1052V56H0.5V37.3333C0.5 30.344 1.61931 23.8661 3.85793 17.8996C6.26876 11.7626 10.4016 5.79605 16.2565 0ZM59.6513 0L69.4668 7.67124C64.8173 12.4444 61.7177 16.962 60.1679 21.2237C58.6181 25.3151 57.8432 29.4916 57.8432 33.7534L51.6439 29.4064H70.5V56H43.8948V37.3333C43.8948 30.344 45.0141 23.8661 47.2528 17.8996C49.6636 11.7626 53.7964 5.79605 59.6513 0Z"/>
          </svg>
          <p style="font-size: 30px">“Simulados de Altíssimo Desempenho” é um MÉTODO de como atingir uma performance realmente alta na prova para a qual você estiver se preparando.</p>
<p>&nbsp;</p>
          <svg class="quoteEnd" width="73" height="58" viewBox="0 0 73 58" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M56.2934 0L46.1974 7.94521C50.9797 12.8889 54.1679 17.5677 55.762 21.9817C57.3561 26.2192 58.1531 30.5449 58.1531 34.9589L64.5295 30.4566H45.1347V58H72.5V38.6667C72.5 31.4277 71.3487 24.7184 69.0461 18.5388C66.5664 12.1827 62.3155 6.00305 56.2934 0ZM11.6587 0L1.56272 7.94521C6.34501 12.8889 9.5332 17.5677 11.1273 21.9817C12.7214 26.2192 13.5184 30.5449 13.5184 34.9589L19.8948 30.4566H0.5V58H27.8653V38.6667C27.8653 31.4277 26.714 24.7184 24.4114 18.5388C21.9317 12.1827 17.6808 6.00305 11.6587 0Z"/>
            </svg>
        </div>
      </div>
    </div>
  </div>
</section>

<!--Video-->
<section id="secStoryModB">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="storyContent">
        <iframe width="100%" height="660" src="https://www.youtube-nocookie.com/embed/vNlk4lVmKJs" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- Como funciona? -->
<section id="secStoryModB">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="storyContent">
      </div>
      </div>
  </div>
    <div class="row">
      <div class="col-md-12">
        <h2 class="courseColorBlue">&#x25cf; COMO FUNCIONA? &#x25cf;</h2>
        <div class="storyContent">
          <svg class="quoteStart" width="71" height="56" viewBox="0 0 71 56" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M16.2565 0L26.072 7.67124C21.4225 12.4444 18.3229 16.962 16.7731 21.2237C15.2232 25.3151 14.4483 29.4916 14.4483 33.7534L8.24908 29.4064H27.1052V56H0.5V37.3333C0.5 30.344 1.61931 23.8661 3.85793 17.8996C6.26876 11.7626 10.4016 5.79605 16.2565 0ZM59.6513 0L69.4668 7.67124C64.8173 12.4444 61.7177 16.962 60.1679 21.2237C58.6181 25.3151 57.8432 29.4916 57.8432 33.7534L51.6439 29.4064H70.5V56H43.8948V37.3333C43.8948 30.344 45.0141 23.8661 47.2528 17.8996C49.6636 11.7626 53.7964 5.79605 59.6513 0Z"/>
          </svg>
          <p style="font-size: 30px">São 3 Módulos de preparação. De acordo com o passo a passo de cada um dos módulos, você irá encaixar a matéria que estiver estudando e os simulados tradicionais que já faz, porém irá realiza-los de acordo com as estratégias e métodos CIENTIFICAMENTE COMPROVADOS para aumento da agilidade mental, percepção de dificuldades e concentração.</p>
<p>&nbsp;</p>
          <svg class="quoteEnd" width="73" height="58" viewBox="0 0 73 58" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M56.2934 0L46.1974 7.94521C50.9797 12.8889 54.1679 17.5677 55.762 21.9817C57.3561 26.2192 58.1531 30.5449 58.1531 34.9589L64.5295 30.4566H45.1347V58H72.5V38.6667C72.5 31.4277 71.3487 24.7184 69.0461 18.5388C66.5664 12.1827 62.3155 6.00305 56.2934 0ZM11.6587 0L1.56272 7.94521C6.34501 12.8889 9.5332 17.5677 11.1273 21.9817C12.7214 26.2192 13.5184 30.5449 13.5184 34.9589L19.8948 30.4566H0.5V58H27.8653V38.6667C27.8653 31.4277 26.714 24.7184 24.4114 18.5388C21.9317 12.1827 17.6808 6.00305 11.6587 0Z"/>
          </svg>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- Funciona comigo? -->
<section id="secStoryModB">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="storyContent">
      </div>
      </div>
  </div>
    <div class="row">
      <div class="col-md-12">
        <h2 class="courseColorBlue">&#x25cf; MAS FUNCIONA COMIGO? &#x25cf;</h2>
        <div class="storyContent">
          <svg class="quoteStart" width="71" height="56" viewBox="0 0 71 56" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M16.2565 0L26.072 7.67124C21.4225 12.4444 18.3229 16.962 16.7731 21.2237C15.2232 25.3151 14.4483 29.4916 14.4483 33.7534L8.24908 29.4064H27.1052V56H0.5V37.3333C0.5 30.344 1.61931 23.8661 3.85793 17.8996C6.26876 11.7626 10.4016 5.79605 16.2565 0ZM59.6513 0L69.4668 7.67124C64.8173 12.4444 61.7177 16.962 60.1679 21.2237C58.6181 25.3151 57.8432 29.4916 57.8432 33.7534L51.6439 29.4064H70.5V56H43.8948V37.3333C43.8948 30.344 45.0141 23.8661 47.2528 17.8996C49.6636 11.7626 53.7964 5.79605 59.6513 0Z"/>
          </svg>
          <p style="font-size: 30px">Sim! Ao se propor a realizar o passo a passo, você estará bem à frente de seus concorrentes na disputa. E com certeza, em um patamar tão superior, que será muito provável inclusive atingir os primeiros lugares.</p>
<p>&nbsp;</p>
          <svg class="quoteEnd" width="73" height="58" viewBox="0 0 73 58" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M56.2934 0L46.1974 7.94521C50.9797 12.8889 54.1679 17.5677 55.762 21.9817C57.3561 26.2192 58.1531 30.5449 58.1531 34.9589L64.5295 30.4566H45.1347V58H72.5V38.6667C72.5 31.4277 71.3487 24.7184 69.0461 18.5388C66.5664 12.1827 62.3155 6.00305 56.2934 0ZM11.6587 0L1.56272 7.94521C6.34501 12.8889 9.5332 17.5677 11.1273 21.9817C12.7214 26.2192 13.5184 30.5449 13.5184 34.9589L19.8948 30.4566H0.5V58H27.8653V38.6667C27.8653 31.4277 26.714 24.7184 24.4114 18.5388C21.9317 12.1827 17.6808 6.00305 11.6587 0Z"/>
          </svg>
        </div>
      </div>
    </div>
    <br><br> <!-- Espaçamento -->
  </div>
</section>

<section id="secReminder">
  <div class="reminderBanner courseBackgroundColor">
    <img class="small" src='https://methodus.com.br/wp-content/themes/maiscode_methodus_novo/img/small.png'>
    <div class="">
      <div class="row">
        <div class="col-md-12">
          <h2>Lembrete Methodus:</h2>
          <h3>Não perca tempo <br>Adquira logo os seus e-books!</h3>
          <a id="price_a" href="https://www.hotmart.com/product/simulados-de-altissimo-desempenho/B43678013O">Quero comprar!</a>
        </div>
      </div>
    </div>
    <img class="big" src='https://methodus.com.br/wp-content/themes/maiscode_methodus_novo/img/big.png'>
  </div>
</section>

<!-- QUERO MAIS DETALHES: -->
<section id="secStoryModB">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="storyContent">
      </div>
      </div>
  </div>
    <div class="row">
      <div class="col-md-12">
        <h2 class="courseColorBlue"> QUERO MAIS DETALHES:</h2>
        <div class="storyContent">
          <p style="font-size: 30px">Irei descrever abaixo a linha de raciocínio de cada um dos módulos para entendimento da metodologia.</p>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- MÓDULO 1 -->
<section id="secStoryModB">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="storyContent">
        </div>
      </div>
    </div>
      <div class="row">
        <div class="col-md-12">
          <h2 class="courseColorRed" style="font-size: 35px"> MÓDULO 1</h2>
          <h2 class="courseColorRed"> “O QUE NÃO FAZER E O QUE FAZER PARA PASSAR” </h2>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="col-md-6">
            <img src="https://methodus.com.br/wp-content/uploads/2020/12/modulo-1-a-scaled.jpg"/>
          </div>
          <div class="col-md-6">
            <div class="storyContent">
              <p style="font-size: 25px; color: #B22222">Você conhecerá os erros básicos cometidos por 70% a 90% dos candidatos na preparação para Concursos, Vestibulares e ENEM.</p>
              <br>
              <p style="font-size: 25px; color: #B22222">Saberá as 5 atitudes primordiais dos 10 primeiros colocados nas listas de aprovados.</p>
              <br>
              <p style="font-size: 25px; color: #B22222">Você entendera porque o segredo dos primeiros colocados não é o QI, mas atitudes que qualquer um pode ter desde que saiba como a mente humana aprende. Eu vou revelar isso a você!</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- MÓDULO 2 -->
<section id="secStoryModB">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="storyContent">
      </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <h2 class="courseColorBlue" style="font-size: 35px"> MÓDULO 2</h2>
        <h2 class="courseColorBlue"> “Seguindo o treinamento de cada um desses módulos, você irá passar com segurança.” </h2>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="storyContent">
          <p style="font-size: 25px; color:#4682B4">Você aprenderá OS 4 PRINCÍPIOS CIENTÍFICOS que deverão ser aprendidos e seguidos para que você tenha sucesso.</p>
          <br>
          <p style="font-size: 25px; color:#4682B4">Neste módulo você aprenderá o passo a passo detalhado explicativo de cada um dos 4 PRINCÍPIOS: Período, Quantidade, Dificuldade, Tempo.</p>
          <br>
          <p style="font-size: 25px; color:#4682B4">Seguindo este passo a passo, você entrará na prova muito seguro e terá disposição mental e física para pensar com clareza ao longo de toda a prova.</p>
          <br>
          <p style="font-size: 25px; color:#4682B4">E ATENÇÃO – MUITO IMPORTANTE: você terá energia e discernimento para resolver os 10% de questões difíceis nos últimos 50 minutos da prova. Não é o máximo?</p>
          <br>
          <p style="font-size: 25px; color:#4682B4">Seguindo o treinamento de cada um deles, você irá passar com segurança.</p>
          <br>
        </div>
      </div>
      <div class="col-md-6">
        <img src="https://methodus.com.br/wp-content/uploads/2020/12/modulo-2-a-scaled.jpg"/>
      </div>
    </div>
  </div>
</section>

<!-- MÓDULO 3 -->
<section id="secStoryModB">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="storyContent">
      </div>
      </div>
  </div>
    <div class="row">
      <div class="col-md-12">
        <h2 class="courseColorGreen" style="font-size: 35px"> MÓDULO 3</h2>
        <h2 class="courseColorGreen"> “VÉSPERA E DIA DAS PROVAS” </h2>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
         <img src="https://methodus.com.br/wp-content/uploads/2020/12/modulo-3-a-scaled.jpg"/>
      </div>
      <div class="col-md-6">
        <div class="storyContent">
          <p style="font-size: 30px; color:#228B22">Aqui, não só dicas para ir para a prova mais tranquilo, mas também o que fazer na Hora H.</p>
          <br>
          <p style="font-size: 35px; color:#228B22">Você chega na prova pronto para realizar o nocaute. É só uma questão de tempo até entregar a prova e derrubar o adversário.
Você chega ultra preparado e confiante. A vitória é certeira!</p>
        </div>
      </div>
    </div>
  </div>
</section>

<!--Imagem de todos os modulos juntos-->
<section>
  <div class="conteiner">
    <div class="row">
      <div class="col-md-12">
        <img src="https://methodus.com.br/wp-content/uploads/2020/12/todas-min-scaled.jpg"/>
      </div>
    </div>
  </div>
</section>

<!-- BANNER CHAMADA -->
<section id="secCallBannerModB" class="courseBackgroundColor">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2>A CHAVE DO SUCESSO! NO SEGUNDO MÓDULO, VOCÊ VAI APRENDER: </h2>
        <h2>Os 4 Princípios Científicos dos simulados de altíssimo desempenho.</h2>
      </div>
    </div>
  </div>
</section>

<!-- PREÇOS -->
<section id="secPrices" class="courseBackgroundColor" style="background-color: #F2F2F2; padding-bottom: 0px">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
          <br>
          <h3><b>COMPARAÇÕES DE INVESTIMENTO</b></h3>
          <br>
          <h3>Você já pensou quanto gastaria se tivesse que estudar mais um ano porque não passou?</h3>
          <br>
          <h3>1 ANO DE CURSINHO PREPARATÓRIO:</h3>
          <br>
          <h3>A partir de <h3 style="font-size: 35px">R$ 650 (online) até R$ 3.500 (presencial)</h3>
          <br>
          <h3 style="font-size: 35px; color:#4682B4"><b>E VOCÊ AINDA PERDE MAIS UM ANO ESTUDANDO!</b></h3>
          <br>
          <h3><mark><b>Já com os SIMULADOS DE ALTÍSSIMO DESEMPENHO</b></mark></h3>
          <br>
          <h3>Tiro certeiro para você passar!</h3>
          <br>
          <h1><b><del>DE: $549,00</del></b></h1>
          <br>
          <h1>Somente:
          <br> 
          <h1><b>12 x </b></h1>
          <h1 style="font-size:80px"><b>R$ 34,00 </b></h1>
          <br>
              </div>
            </div>
          </div>
      </div>
    </div>
  </div>
</section>

<!-- BOTÃO COMPRE AGORA -->
<section id="secPrices" class="courseBackgroundColor" style="padding-bottom: 0px">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="pricesTop featurePrice">
        <h2>Invista com sabedoria, invista na Methodos, invista em você! </h2>
        <br><br>
        <a id="price_a" href="https://www.hotmart.com/product/simulados-de-altissimo-desempenho/B43678013O">Quero comprar!</a>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- SOBRE A METHODUS -->
<section id="secStoryModB">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="storyContent">
      </div>
      </div>
  </div>
    <div class="row">
      <div class="col-md-12">
        <h2 class="courseColorBlue"> SOBRE A METHODUS </h2>

        <!-- FOTO PROFESSOR -->
<section id="secWhyMethodus">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="boxMethodusImg">
          <br><br>
          <img src="https://methodus.com.br/wp-content/uploads/2020/04/home.jpeg">
          <span class="courseColor">Prof Alcides Schotten, educador e filósofo.</span>
          <br><br>
        </div>
      </div>
    </div>
  </div>
</section>

        <div class="storyContent">
          <p style="font-size: 25px">Antes de você adquirir os 3 e-books, quero te contar quem sou:  Alcides Schotten, Professor e Filósofo. Lecionei em colégios e universidades e sempre percebi um grande
drama dos estudantes – não saber estudar. Por isso, não aprendem e apenas decoram os conteúdos das disciplinas para fazer as provas.
Educar deriva do latim “educare”, que significa “trazer para fora”. Sócrates, o maior dos filósofos da história, sabiamente praticava a genuína educação através da técnica da maiêutica. Concluí, então, que só tem um jeito de aprender – sendo autodidata.
</p>
          <br>
          <p style="font-size: 25px">E é por isso que ensino a você todas as técnicas para que ao estudar você tenha conhecimento científico de como seu cérebro funciona e porque você deve estudar desta ou daquela maneira.</p>
          <br>
          <p style="font-size: 25px">E garanto que décadas de ensinamentos e estudos estão aqui ao seu alcance de maneira fácil e num passo a passo que vai fazer você entender o porque de cada técnica que está fazendo e conseguir extrair o máximo dos seus estudos, se tornando uma verdadeira máquina de acertar questões.</p>
          <br>
          <p style="font-size: 25px">A Methodus tem sede na Av. Paulista desde 1998 – são mais de 20 anos treinando milhares de profissionais e estudantes, desenvolvendo-os em diversas áreas, e levando a upgrades de carreira, desenvolvimento pessoal e profissional.</p>
          <br><br>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- Links que rediricionam para outros cursos -->
<section id="secStoryModB" style="padding-top: 0px; background-color:#4682B4">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="storyContent">
      </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="storyContent">
          <br>
          <p style="font-size: 30px; color:#ffffff; font-weight:300">Conheça nossos outros cursos:</p>
          <br>
          <p style="font-size: 40px; color:#ffffff; font-weight:300"><u>Presenciais</u></p>
          <br>
          <a style="font-size: 30px; color:#ffffff; font-weight:300" href="https://methodus.com.br/o-curso-leitura-dinamica-e-memoria/">• Leitura dinâmica | Memória | Mind map | Concentração</a>
          <br><br>
          <a style="font-size: 30px; color:#ffffff; font-weight:300" href="https://methodus.com.br/cursos/administracao-do-tempo/">• Administração do Tempo - Hiperprodutividade - Saber Empreender</a>
          <br><br>
          <a style="font-size: 30px; color:#ffffff; font-weight:300" href="https://methodus.com.br/curso-de-oratoria/">• Oratória - Perder o Medo de Falar - A Arte de Comunicar Bem</a>
          <br><br>
          <a style="font-size: 30px; color:#ffffff; font-weight:300" href="https://methodus.com.br/cursos/mentoria-vestibulandos-e-concursandos/">• Mentorias para Estudantes/Vestibulandos/Concursandos</a>
          <br><br>
          <p style="font-size: 40px; color:#ffffff; font-weight:300"><u>Online</u></p>
          <br>
          <a style="font-size: 30px; color:#ffffff; font-weight:300" >• Simulados de Altíssimo Desempenho</a>
          <br><br>
          <a style="font-size: 30px; color:#ffffff; font-weight:300" href="https://methodus.com.br/cursos/mentoria-vestibulandos-e-concursandos/">• Mentorias para Estudantes/Vestibulandos/Concursandos</a>
          <br><br>
          <a style="font-size: 30px; color:#ffffff; font-weight:300" href="https://methodus.com.br/cursos/mentoria-oratoria-para-profissionais-e-academicos/">• Mentoria - Oratória para Profissionais e Acadêmicos</a>
          <br><br>
          <br>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- Fim
<section id="secPrices" style="padding-top: 40px">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <br>
        <h1 style="font-size: 55px"><b>methodus.com.br</b></h2>
        <br>
        <h3>AV. PAULISTA, 2.202 • CJ. 134 • METRÔ CONSOLAÇÃO – SP/SP</h3>
        <br>
        <h3>Tel: (11) 3288-2777  •  Whatsapp: (11) 97148-6385</h3>
      </div>
    </div>
  </div>
</section>
-->

<?php get_footer(); ?>