<?php
/**
* Template Name: Página Contrata
*
* @package WordPress
* @author Mais Code Tecnologia
* @since First Version
*/
get_header(); ?>

<section class="methodus-contrata">
    <div class="container">
        <div class="post-title">
            <h1 class="inline-block"><?php the_title(); ?></h1>
        </div>
        <hr>
        <?php the_field('sobre_contrata'); ?>
    </div>
</section>
<?php get_footer(); ?>