<?php 
/**
* Template Name: Página Links
*
* @package WordPress
* @author Mais Code Tecnologia
* @since First Version
*/ 
get_header(); ?>

<section id="artigos-pagina">
    <div class="container">
        <div class="row">
            <div class="col-md-9 mb-xs-24" id="conteudo_paginado">
                <?php           

                 $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                 $limit = 10;
                 $offset = $limit * ($paged - 1);                         

                        $args = array(
                            'post_type'      => 'links',                            
                            'offset'         => $offset,
                            'orderby'        => 'date',
                            'order'          => 'ASC',
                            'nopaging' => false,
                        );

                $postsList = new WP_Query($args);

                ?>

            	<?php if ($postsList->have_posts()): ?>
                    <?php while ($postsList->have_posts()) : $postsList->the_post(); 
                    	$endereco = get_field('link_link');
                    ?>
	                    <div class="post-snippet mb64" style="clear:both;">
	                    	<div class="post-title">
	                    		<h4 class="inline-block"><?php the_title(); ?></h4>
	                    		<span class="label">
	                    			<a href="<?php echo $endereco; ?>" target="_blank"><?php echo $endereco; ?></a>
	                    		</span>	
	                    	</div>
	                    	<hr>
	                    	<p>	
	                    		<?php the_excerpt(); ?>
	                    	</p>
	                    </div>
                    <?php endwhile ?>
                <?php endif ?>
                <div class="text-center">
                    <?php paginacao($limit); ?>    
                </div>
                
            </div>

            <div class="col-md-3 hidden-sm widget-methodus">
              <div class="busca-widget">
                <form action="<?php bloginfo('siteurl'); ?>" id="searchform" method="get">
                  <div class="input-artigo">
                    <input type="hidden" name="tipo" value="links" id="link">
                    <input placeholder="Pesquisar por" type="text" name="s" value="<?php the_search_query(); ?>" />
                    
                      <input type="submit" class="btn btn-input btn-sm" value="Buscar" id="searchsubmit" />
                  
              </div>
          </form>
              </div>
            </div>
        </div>
    </div>
</section>


<?php get_footer(); ?>