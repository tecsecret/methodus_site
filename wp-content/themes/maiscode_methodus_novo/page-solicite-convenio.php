<?php
/**
* Template Name: Página Solicite Convenio
*
* @package WordPress
* @author Mais Code Tecnologia
* @since First Version
*/
get_header();
    
$url_api  =  get_option('link_api'); 
$urlWP = get_template_directory_uri();

 ?>

<section>
    <div class="container">
        <div class="post-title">
            <h1 class="inline-block"><?php the_title(); ?></h1>
        </div>
        <hr>Algumas das instituições conveniadas:
        <br>
        <p align="center">
            <?php if (have_rows('convenios')): ?>
                <?php while(have_rows('convenios')):the_row(); ?>

                    <img style="max-width: 200px; height: auto;" src="<?php the_sub_field('logo_convenio'); ?>">

                <?php endwhile ?>
            <?php endif ?>
            
            <?php the_field('texto_convenio'); ?>
            <div class="convenio">

                <h2>Solicite Convênio</h2>

                <h4>Preencha aqui seus dados</h4>

                <script src="<?php echo $urlWP; ?>/incompany/scripts.js"></script>
                <form action="<?= $url_api; ?>/api/" method="post" class="form-email formAjax" data-validacao="checaConvenio" data-resultado="sucessoConvenio" id="generico_convenio">
                    <input type="hidden" name="a" value="convenios" />
                    <input type="hidden" name="tipo" value="Convênio" />

                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <input type="text" class="validate-required" name="razao" placeholder="Razão Social" lang="Razão Social" maxlength="150">
                        </div>
                        <div class="col-sm-6 col-md-6">
                            <input type="text" class="validate-required" name="nome" placeholder="Nome" lang="Nome" maxlength="150">
                        </div>
                        <div class="col-sm-6 col-md-6">
                            <input type="text" class="validate-required" name="cargo" placeholder="Cargo" lang="Cargo" maxlength="150">
                        </div>
                        <div class="col-sm-6 col-md-6">
                            <input type="text" class="validate-required" name="endereco" placeholder="Endereço" lang="Endereço" maxlength="150">
                        </div>
                        <div class="col-sm-6 col-md-6">
                            <input type="text" class="validate-required" name="cep" placeholder="CEP" lang="CEP" onKeyPress="mascara(this,mascaraCep);" maxlength="9">
                        </div>
                        <div class="col-sm-6 col-md-6">
                            <input type="text" class="validate-required" name="cidade" placeholder="Cidade" lang="Cidade" maxlength="150">
                        </div>
                        <div class="col-sm-6 col-md-6">
                            <select name="estado" class="form-control  required" lang="Estado">
                                <option value="">Estado</option>                 

                                <option value="AC">AC</option>
                                <option value="AL">AL</option>
                                <option value="AM">AM</option>
                                <option value="AP">AP</option>
                                <option value="BA">BA</option>
                                <option value="CE">CE</option>
                                <option value="DF">DF</option>
                                <option value="ES">ES</option>
                                <option value="GO">GO</option>
                                <option value="MA">MA</option>
                                <option value="MG">MG</option>
                                <option value="MS">MS</option>
                                <option value="MT">MT</option>
                                <option value="PA">PA</option>
                                <option value="PB">PB</option>
                                <option value="PE">PE</option>
                                <option value="PI">PI</option>
                                <option value="PR">PR</option>
                                <option value="RJ">RJ</option>
                                <option value="RN">RN</option>
                                <option value="RO">RO</option>
                                <option value="RR">RR</option>
                                <option value="RS">RS</option>
                                <option value="SC">SC</option>
                                <option value="SE">SE</option>
                                <option value="SP">SP</option>
                                <option value="TO">TO</option>

                            </select>
                        </div>
                        <div class="col-sm-6 col-md-6">
                            <input type="text" class="validate-required validate-email" name="email" placeholder="Email" lang="E-mail" maxlength="150">
                        </div>
                        <div class="col-sm-6 col-md-6">
                            <input type="text" class="validate-required" name="telefone" placeholder="Telefone" lang="Telefone" onKeyPress="mascara(this,mascaraTelefone);" maxlength="15">
                        </div>

                    </div>
                    <p><strong>Cursos de interesse:</strong></p>
                      
                    <?php
                    // Cursos
                    $xml_cursos     = executaPagina( 'api/', array( 'a'=>'cursos', 'metodo'=>'listar' ) );
                    $cursos         = lerXML( $xml_cursos );
                        
                    if ( $cursos->erro == 0 ){
                        $cursos = $cursos->cursos->curso;
                    }else{
                        exit("Erro ao acessar os dados dos cursos");        
                    }
                    
                    foreach( $cursos as $curso ){
                        echo '<label><input type="checkbox" name="cursos[]" value="'.$curso->titulo.'" /> '.$curso->titulo.'</label><BR />';
                    }
                    ?>
                    <br>
                    <button class="<?php echo $classe_curso; ?>" type="submit">Enviar</button>
                  </form>

            </div>
        </p>
    </div>
</section>

<script src="<?php bloginfo('template_url'); ?>/js/scripts_site/scriptsform.js"></script>

<?php get_footer(); ?>