<?php

get_header(); 


 global $wpdb;


$categoria = get_queried_object();



$term_slug = $categoria->slug;

?>



	<div class="main-container">
		<section>
			<div class="container">
				<div class="row">
					<div class="col-md-9 mb-xs-24" id="conteudo_paginado">

              <?php 
                  $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

                  $limit = 10;
                  $offset = $limit * ($paged - 1);                        
                  $args = array(
                      'post_type'      => 'artigos',
                      //'posts_per_page' => 10,
                      'offset'         => $offset,
                      'orderby'        => 'date',
                      'order'          => 'ASC',
                      'tax_query'         => array(
                        array(
                         'taxonomy'  => 'categoria_artigo',
                         'field'     => 'slug',
                         'terms'     => $term_slug
                       )
                      )
                  );

                  $postsList = new WP_Query($args);
              ?>


                  <?php if ( $postsList->have_posts() ) :  ?>
                  
                   <?php while ( $postsList->have_posts() ) : $postsList->the_post(); ?>

                      <?php 
                      
                      $image = wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() )); 

                      $sobre = get_field('sobre_artigo');
                      $btn = get_field('btn_mais');

                        
                      ?>

                          <div class="post-snippet mb64">
                              <div class="post-title">
                                  <span class="label"><?php the_time('d/m/Y'); ?></span>
                                  <a href="<?php the_permalink(); ?>">
                                      <h4 class="inline-block"><?php the_title(); ?></h4>
                                  </a>
                              </div>
                              <hr>
                              <p>
                                  <?php if ($image != ""): ?>
                                      <a href="<?php the_permalink(); ?>" class="thumb_pequeno">
                                          <img class="mb24" alt="Post Image" src="<?php echo $image; ?>" width="100%">
                                      </a>
                                  <?php endif ?>
                                  
                                  <?php the_excerpt(); ?>
                              </p>
                              <a class="btn btn-sm" href="<?php the_permalink(); ?>">
                                  <?php echo $btn; ?>
                              </a>
                          </div>

                      <?php endwhile ?>
                      
              <?php endif ?>    
              
              

              <div class="text-center">
                  
                  <?php paginacao($limit); ?>
              
              </div>
 

					</div>


      <div class="col-md-3 hidden-sm widget-methodus">

        <h6 class="title"><?php the_field('pesquisa_livros',340); ?></h6>
        <div class="busca-widget">
          <form action="<?php bloginfo('siteurl'); ?>" id="searchform" method="get">
            <div>
              <input placeholder="Pesquisar por" type="text" id="s" name="s" value="" />
              <div class="text-center">
                <input type="submit" class="btn btn-input btn-sm" value="Buscar" id="searchsubmit" />
              </div>
            </div>
          </form>
        </div>

        <h6 class="title"><?php the_field('texto_categoria_artigos',326); ?></h6>

        <?php
        $args = array(
         'taxonomy' => 'categoria_artigo',
         'orderby' => 'date',
         'order'   => 'DESC',
         'hide_empty' => false
       );

        $cats = get_terms($args);
        $cont = 1;

        ?>

        <ul class="link-list">
          <?php
          foreach($cats as $cat) { 
            ?>

            <li><a href="<?php echo get_term_link($cat->term_id); ?>"><?php echo $cat->name; ?></a></li>

            <?php 
          }
          ?>
        </ul>

        <h6 class="title"><?php the_field('texto_mais_acessados',326); ?></h6>
        <ul class="link-list">



          <?php 
          $args = array(
            'post_type' => 'artigos',
            'post_status' => 'publish',
            'posts_per_page' => '20',
            'meta_key' => 'post_views_count',
            'order' => 'DESC'
          );
          $my_posts = new WP_Query( $args );
          
          if ( $my_posts->have_posts() ) : 
            ?>
            
            <?php while ( $my_posts->have_posts() ) : $my_posts->the_post(); 

              ?>

              <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
              
            <?php endwhile; ?>
          <?php endif; ?>
          
        </ul>


        <?php //dynamic_sidebar ( 'sidebar-artigos' ) ; ?>


      </div>

    </div>
  </div>

</section>
</div>

<?php get_footer(); ?>