<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head>
    <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
    <meta name="robots" content="follow,all" />
    <meta http-equiv="Content-Language" content="pt-br" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Methodus - Leitura dinâmica, Administração do tempo e Oratória</title><META NAME="description" CONTENT="Curso de Oratória, Curso de Leitura Dinâmica e Memorização, Curso de Administração do Tempo. Conheça os cursos oferecidos pela Methodus, curso de orató"><META NAME="keywords" CONTENT="cursos,curso,oratória,leitura,dinâmica,memorização,administração,tempo"><meta property="og:type" content="website" />
    <meta property="og:site_name" content="Methodus - Cursos de Leitura dinâmica, Memorização, Administração do tempo e Oratória" />
    <meta property="og:title" content="Methodus - Leitura dinâmica, Administração do tempo e Oratória" />
    <meta property="og:url" content="https://www.methodus.com.br/home.html" />
    <meta property="og:image" content="https://www.methodus.com.br/site/img/redes_logo.png" />
    <meta property="og:description" content="Curso de Oratória, Curso de Leitura Dinâmica e Memorização, Curso de Administração do Tempo. Conheça os cursos oferecidos pela Methodus, curso de oratória, curso de leitura dinâmica, curso de memorização e curso de administração do tempo." />    
    <meta name="theme-color" content="#336699">

    <link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/favicon.ico" type="image/png"/>



    <link href="<?php bloginfo('template_url'); ?>/css/stylesheet2.css" rel="stylesheet" type="text/css" media="all" /> 
        <link href="<?php bloginfo('template_url'); ?>/css/css_site/et-line-icons.css" rel="stylesheet" type="text/css" media="all">
        <link href="<?php bloginfo('template_url'); ?>/css/css_site/flexslider.css" rel="stylesheet" type="text/css" media="all" />

        <link href="<?php bloginfo('template_url'); ?>/css/css_site/pe-icon-7-stroke.css" rel="stylesheet" type="text/css" media="all">
        <link href="<?php bloginfo('template_url'); ?>/css/css_site/font-awesome.min.css" rel="stylesheet" type="text/css" media="all">
        <script src="https://kit.fontawesome.com/bf4507744d.js" crossorigin="anonymous"></script>
        <link href="<?php bloginfo('template_url'); ?>/css/css_site/lightbox.min.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?php bloginfo('template_url'); ?>/css/css_site/themify-icons.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?php bloginfo('template_url'); ?>/css/css_site/bootstrap.css" rel="stylesheet" type="text/css" media="all" />


        <link href="<?php bloginfo('template_url'); ?>/css/css_site/theme-hyperblue.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?php bloginfo('template_url'); ?>/css/css_site/custom.css" rel="stylesheet" type="text/css" media="all" />

        <link href='//fonts.googleapis.com/css?family=Lato:300,400%7CRaleway:100,400,300,500,600,700%7COpen+Sans:400,500,600' rel='stylesheet' type='text/css'>

        <link href='//fonts.googleapis.com/css?family=Titillium+Web:300,400,600' rel='stylesheet' type='text/css'>

        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500&display=swap" rel="stylesheet">


        <link href="<?php bloginfo('template_url'); ?>/css/css_site/form_cadastro.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?php bloginfo('template_url'); ?>/css/css_site/estilos_pensa.css" rel="stylesheet" type="text/css" media="all" />


        <?php wp_head(); ?> 

        <script src="<?php bloginfo('template_url'); ?>/js/scripts_site/jquery.min.js"></script>

        <script src="<?php bloginfo('template_url'); ?>/js/scripts_site/scripts_pensa.js?ver=1"></script>
        <script src="<?php bloginfo('template_url'); ?>/js/scripts_site/formularios.js?ver=1"></script>

        <script>
             url_site = '<?php echo home_url(); ?>';
        </script>

        <?php echo get_option('tag_head'); ?>

 
    </head>

    <body class="scroll-assist"><?php //body_class(); ?> 

    
    <?php echo get_option('tag_body'); ?>

    <header>
        <div class="nav-container" style="min-height: 120px;">
            <nav class="nav-centered">
                <div class="nav-utility" style="margin: 40px 0;">
                    <div class="module left">
                        <a href="<?php echo esc_url( home_url() ); ?>">
                             <img class="logo logo-light" alt="Logo Methodus" src="<?php echo get_option('header_logo'); ?>">  
                              <img class="logo logo-dark" alt="Logo Methodus" src="<?php echo get_option('header_logo'); ?>"> 
                        </a>
                    </div>
                    <div class="module left">                        
                        <a class="position-relative" href="https://www.methodus.com.br/c/208-207/Localizacao.html">
                            <i class="fas fa-map-marker-alt" style="color: #252C80;"></i>
                            <span class="sub"><?php echo get_option('endereço'); ?></span>
                        </a>
                    </div>
                    <div class="module left">
                        <a href="https://www.methodus.com.br/contato.html">
                            <i class="ti-email" style="color: #252C80;">&nbsp;</i> 
                            <span class="sub"><?php echo get_option('email'); ?></span>
                        </a>
                    </div>
                    <div class="module left">
                        <a href="https://www.methodus.com.br/contato.html">
                            <i class="fa fa-whatsapp" style="color: #252C80;">&nbsp;</i> 
                            <span class="sub">
                                <?php echo get_option('whatsapp'); ?> <br>
                                <i class="fa fa-phone" style="color: #252C80;">&nbsp;</i>
                                <?php echo get_option('telefone'); ?>
                            </span>
                        </a>
                    </div>
                    <div class="module left"> </div>
                    <div class="module right"> 
                        <a class="btn btn-sm" href="<?php echo get_option('link_aluno'); ?>" style="margin-top: 5px;"><i class="far fa-user-circle"></i> Área do Aluno</a> 
                    </div>
                </div>
                <div class="nav-bar text-center">
                    <div class="module widget-handle mobile-toggle right visible-sm visible-xs"> 
                        <i class="ti-menu"></i> 
                    </div>
                    <div class="module-group text-left">
                        <div class="module left">

                         <?php
                         $defaults = array(
                            'theme_location'  => 'header-menu',
                            'container'       => false,
                            'menu_class'      => 'list-inline list-unstyled box-menu-header menu',
                            'menu_id'         => 'menu',
                            'echo'            => true
                        );
                         wp_nav_menu($defaults);
                         ?>

                     </div>
                 </div>
             </div>
         </nav>
     </div>



 </header>

<style type="text/css">
        .btn_top{
            color: #fff !important;
        }
        .btn_top:hover{
            background-color: #fff !important;
            color: <?php the_field('cor_curso'); ?> !important;
        }
        .btn_pg_curso{
            border: 2px solid <?php the_field('cor_curso'); ?> !important;;
        }
        .btn_pg_curso:hover{
            background-color: #fff !important;
            color: <?php the_field('cor_curso'); ?> !important;
        }
        .btn_chamada:hover{
            color: <?php the_field('cor_curso'); ?> !important;
        }
    </style>