<?php
/**
* Template Name: Página Localizacao
*
* @package WordPress
* @author Mais Code Tecnologia
* @since First Version
*/
get_header(); ?>

<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-7">
                <div class="post-title">
                    <h1 class="inline-block"><?php the_field('titulo_localizacao'); ?></h1>
                </div>
                <hr>
                <iframe frameborder="0" height="350" marginheight="0" marginwidth="0" scrolling="no" src="https://maps.google.com.br/maps?f=q&amp;source=s_q&amp;hl=pt-BR&amp;geocode=&amp;q=Av.+Paulista+2202,+Cj+134+Consola%C3%A7%C3%A3o+-+S%C3%A3o+Paulo+-SP&amp;sll=-14.179186,-50.449219&amp;sspn=108.909925,228.515625&amp;ie=UTF8&amp;hq=&amp;hnear=Av.+Paulista,+2202+-+Consola%C3%A7%C3%A3o,+S%C3%A3o+Paulo,+01310-300&amp;ll=-23.553995,-46.660051&amp;spn=0.013769,0.018239&amp;z=15&amp;iwloc=A&amp;output=embed" width="100%"></iframe>
                <br>
                <small><a href="https://maps.google.com.br/maps?f=q&amp;source=embed&amp;hl=pt-BR&amp;geocode=&amp;q=Av.+Paulista+2202,+Cj+134+Consola%C3%A7%C3%A3o+-+S%C3%A3o+Paulo+-SP&amp;sll=-14.179186,-50.449219&amp;sspn=108.909925,228.515625&amp;ie=UTF8&amp;hq=&amp;hnear=Av.+Paulista,+2202+-+Consola%C3%A7%C3%A3o,+S%C3%A3o+Paulo,+01310-300&amp;ll=-23.553995,-46.660051&amp;spn=0.013769,0.018239&amp;z=15&amp;iwloc=A" style="TEXT-ALIGN: left; COLOR: #0000ff"><?php the_field('btn_mapa'); ?></a></small>
                <br>
                <br>
                <?php if (have_rows('hoteis')): ?>
                    <p>
                        <strong><?php the_field('titulo_hoteisproximos'); ?></strong>
                    </p>
                    <?php while(have_rows('hoteis')): the_row(); ?>
                    
                        
                        <p>
                            <?php the_sub_field('nome_hotel'); ?>
                            <br>
                            <a href="<?php the_sub_field('link_hotel'); ?>" target="_blank"><?php the_sub_field('link_hotel'); ?></a>
                            <br> <?php the_sub_field('endereco_hotel'); ?>
                            <br> <?php the_sub_field('telefone_hotel'); ?>
                        </p>

                    <?php endwhile ?>
                <?php endif ?>
                    &nbsp;</p>
                <p>
                    &nbsp;</p>
                <?php if (have_rows('transportes')): ?>
                    <p>
                        <strong><?php the_field('titulo_transporte'); ?></strong>
                    </p>


                    <?php while(have_rows('transportes')) : the_row(); ?>
                        <p>
                            <strong>.&nbsp;<?php the_sub_field('tipo_transporte'); ?></strong>
                        </p>
                        <?php the_sub_field('descricao_transporte'); ?>
                    <?php endwhile ?>
                <?php endif ?>
                
                
            </div>

            <div class="col-sm-6 col-md-4 col-md-offset-1">            
                 <?php get_template_part( 'inc/contato' ); ?>
            </div>

        </div>
    </div>
</section>

<?php get_footer(); ?>