<?php
/**
* Template Name: Single Noticias
* Template Post Type: post, page, noticias
*
* @package WordPress
* @author Mais Code Tecnologia
* @since First Version
*/
get_header(); 
$image = wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() ));
?>

<section class="single-view">
    <div class="container">
        <div class="post-title">
            <h1 class="inline-block"><?php the_title(); ?></h1>
        </div>
        <hr>
        <img src="<?php echo $image; ?>" width="40%" class="foto_conteudo">
        <?php the_content(); ?>

    </div>
</section>


<?php get_footer(); ?>