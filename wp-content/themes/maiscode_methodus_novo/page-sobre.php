<?php
/**
* Template Name: Página Sobre
*
* @package WordPress
* @author Mais Code Tecnologia
* @since First Version
*/
get_header(); ?>

<section>
    <div class="container">     
        <div class="row">
            <div class="col-sm-6 col-md-7 sobre-methodus">
                <div class="post-title">
                    <h1 class="inline-block"><?php the_title(); ?></h1>
                </div>
                <hr>
                <p>
                    <font color="#000080"><font size="2"><strong><font color="#3366ff"><?php the_field('titulo_missao'); ?></font></strong></font></font>
                    <?php the_field('texto_missao'); ?>
                </p>
                <br>
                <p>
                    <font color="#3366ff" size="2">
                        <strong><?php the_field('nome_cargo'); ?></strong>
                    </font>                    
                    <img align="left" alt="" height="116" hspace="10" src="<?php the_field('imagem_responsavel'); ?>" vspace="5" width="113">
                </p>
                <br>
                <p>
                    <strong><?php the_field('nome_responsavel'); ?></strong><br>
                    <span><?php the_field('formacao_responsavel'); ?></span><br>
                    <br>
                    <?php the_field('texto_responsavel'); ?><br>
                    <br>
                    <font size="2"><strong><font color="#3366ff"><?php the_field('titulo_estrutura'); ?></font></strong></font>
                    <?php the_field('texto_estrutura'); ?>
                </p>
                <br>
                <p>
                    <strong><font size="2"><font color="#3366ff"><?php the_field('titulo_material'); ?></font></font></strong><br>
                    <?php the_field('texto_material'); ?>
                </p>
                <br>
                <p>
                    <strong><font size="2"><font color="#3366ff"><?php the_field('titulo_beneficos'); ?></font></font></strong><br>
                    <?php the_field('texto_beneficios'); ?>
                </p>
                <br>
                <p>
                    <strong><font color="#3366ff" size="2"><?php the_field('titulo_principais_clientes'); ?></font></strong><br>
                    <?php the_field('texto_principais_clientes'); ?>
                </p>
            </div>
            <div class="col-sm-6 col-md-4 col-md-offset-1">
               <?php get_template_part( 'inc/contato' ); ?>
            </div>
        </div>      
    </div>
</section>

<?php get_footer(); ?>