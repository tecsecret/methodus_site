<?php 
/**
* Template Name: Pagina Artigo Curso
* Template Post Type: post, page, cursos
*
* @package WordPress
* @author Mais Code Tecnologia
* @since First Version
*/

get_header(); 



$ID = get_field('id_do_curso');

$url_api  =  get_option('link_api'); 


?>


<section>
    <div class="container">
        <div class="row">
            <?php if ( have_posts() ) : the_post();
                $image = wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() ));

             ?>
            <div class="col-sm-6 col-md-7">
                <div class="post-title">
                    <h1 class="inline-block"><?php the_title(); ?></h1>
                </div>
                <hr><img src="<?php echo $image; ?>" width="100%">
                <?php the_field('conteudo_artigo'); ?>
                <?php the_content(); ?>
            </div>
                       

              
           
        <?php endif; ?>
    </div>
</section>

<?php get_footer(); ?>