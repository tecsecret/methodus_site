<?php
/**
* Template Name: Página Video Single
*
* @package WordPress
* @author Mais Code Tecnologia
* @since First Version
*/
get_header(); 

?>
<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-7">
                <div class="post-title">
                    1231465456789
                    <h1 class="inline-block">Liderança para os novos tempos</h1>
                </div>
                <hr>
                <div class="video">
                    <link href="https://www.methodus.com.br/site/../asa/_base/videojs/video-js.css" rel="stylesheet">
                    <script src="https://www.methodus.com.br/site/../asa/_base/videojs/video.js"></script>

                    <div autoplay="true" data-setup="{}" preload="auto" class="video-js vjs-default-skin vjs-paused vjs-error vjs-controls-enabled vjs-user-inactive" id="MY_VIDEO_1" style="width: 100%; height: 100%;">
                        <video id="MY_VIDEO_1_html5_api" class="vjs-tech" preload="auto" data-setup="{}" autoplay="autoplay">
                            <source src="https://www.methodus.com.br/site/../facix2/ftp/Uploads/videos/Lideranca para os novos tempos 36.flv" type="video/flv">
                            <p class="vjs-no-js">Para ver esse vídeo, habilite o javascript e considere atualizar seu navegador.</p>
                        </video>
                        <div></div>
                        <div class="vjs-poster" tabindex="-1" style="display: none;"></div>
                        <div class="vjs-text-track-display"></div>
                        <div class="vjs-loading-spinner"></div>
                        <div class="vjs-big-play-button" role="button" aria-live="polite" tabindex="0" aria-label="play video"><span aria-hidden="true"></span></div>
                        <div class="vjs-control-bar">
                            <div class="vjs-play-control vjs-control " role="button" aria-live="polite" tabindex="0">
                                <div class="vjs-control-content"><span class="vjs-control-text">Play</span></div>
                            </div>
                            <div class="vjs-current-time vjs-time-controls vjs-control">
                                <div class="vjs-current-time-display" aria-live="off"><span class="vjs-control-text">Current Time </span>0:00</div>
                            </div>
                            <div class="vjs-time-divider">
                                <div><span>/</span></div>
                            </div>
                            <div class="vjs-duration vjs-time-controls vjs-control">
                                <div class="vjs-duration-display" aria-live="off"><span class="vjs-control-text">Duration Time</span> 0:00</div>
                            </div>
                            <div class="vjs-remaining-time vjs-time-controls vjs-control">
                                <div class="vjs-remaining-time-display" aria-live="off"><span class="vjs-control-text">Remaining Time</span> -0:00</div>
                            </div>
                            <div class="vjs-live-controls vjs-control">
                                <div class="vjs-live-display" aria-live="off"><span class="vjs-control-text">Stream Type</span>LIVE</div>
                            </div>
                            <div class="vjs-progress-control vjs-control">
                                <div role="slider" aria-valuenow="NaN" aria-valuemin="0" aria-valuemax="100" tabindex="0" class="vjs-progress-holder vjs-slider" aria-label="video progress bar" aria-valuetext="0:00">
                                    <div class="vjs-load-progress"><span class="vjs-control-text"><span>Loaded</span>: 0%</span>
                                    </div>
                                    <div class="vjs-play-progress"><span class="vjs-control-text"><span>Progress</span>: 0%</span>
                                    </div>
                                    <div class="vjs-seek-handle vjs-slider-handle" aria-live="off"><span class="vjs-control-text">00:00</span></div>
                                </div>
                            </div>
                            <div class="vjs-fullscreen-control vjs-control " role="button" aria-live="polite" tabindex="0">
                                <div class="vjs-control-content"><span class="vjs-control-text">Fullscreen</span></div>
                            </div>
                            <div class="vjs-volume-control vjs-control">
                                <div role="slider" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" tabindex="0" class="vjs-volume-bar vjs-slider" aria-label="volume level" aria-valuetext="100%">
                                    <div class="vjs-volume-level"><span class="vjs-control-text"></span></div>
                                    <div class="vjs-volume-handle vjs-slider-handle"><span class="vjs-control-text">00:00</span></div>
                                </div>
                            </div>
                            <div class="vjs-mute-control vjs-control" role="button" aria-live="polite" tabindex="0">
                                <div><span class="vjs-control-text">Mute</span></div>
                            </div>
                            <div class="vjs-playback-rate vjs-menu-button vjs-control vjs-hidden" aria-haspopup="true" role="button">
                                <div class="vjs-control-content"><span class="vjs-control-text">Playback Rate</span></div>
                                <div class="vjs-playback-rate-value">1</div>
                                <div class="vjs-menu">
                                    <ul class="vjs-menu-content"></ul>
                                </div>
                            </div>
                            <div class="vjs-subtitles-button vjs-menu-button vjs-control " role="button" aria-live="polite" tabindex="0" aria-haspopup="true" aria-label="Subtitles Menu" style="display: none;">
                                <div class="vjs-control-content"><span class="vjs-control-text">Subtitles</span>
                                    <div class="vjs-menu">
                                        <ul class="vjs-menu-content">
                                            <li class="vjs-menu-item vjs-selected" role="button" aria-live="polite" tabindex="0" aria-selected="true">subtitles off</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="vjs-captions-button vjs-menu-button vjs-control " role="button" aria-live="polite" tabindex="0" aria-haspopup="true" aria-label="Captions Menu" style="display: none;">
                                <div class="vjs-control-content"><span class="vjs-control-text">Captions</span>
                                    <div class="vjs-menu">
                                        <ul class="vjs-menu-content">
                                            <li class="vjs-menu-item vjs-selected" role="button" aria-live="polite" tabindex="0" aria-selected="true">captions off</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="vjs-chapters-button vjs-menu-button vjs-control " role="button" aria-live="polite" tabindex="0" aria-haspopup="true" aria-label="Chapters Menu" style="display: none;">
                                <div class="vjs-control-content"><span class="vjs-control-text">Chapters</span>
                                    <div class="vjs-menu">
                                        <ul class="vjs-menu-content">
                                            <li class="vjs-menu-title">Chapters</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="vjs-error-display">
                            <div>No compatible source was found for this video.</div>
                        </div>
                    </div>
                </div>
                <br>
            </div>
            <div class="col-sm-6 col-md-4 col-md-offset-1">
                <div class="box-contato" id="investir">
                    <h2 class="titulo-box vermelho">Oratória</h2>
                    <div class="interno-box">
                        <span class="texto-escuro"> Próximas Turmas</span>
                        <br>
                        <p>Grupos com 15 alunos</p>
                        <p><span class="texto-vermelho"><b>18/11/2019 à 29/11/2019 </b></span>
                            <br>
                            <span class="texto-escuro">(9 noites - 2ª à 6ª - 19h às 22h20min)</span></p>
                    </div>

                    <h4 class="dados-box vermelho"><strong>Preencha aqui seus dados</strong></h4>
                    <div class="interno-box">
                        <script src="https://www.methodus.com.br/site/cursos/investimento.js"></script>
                        <form action="https://www.methodus.com.br/api/" method="post" class="form-email" id="investimento" data-validacao="checaCurso" data-resultado="sucessoCurso" data-curso="5">
                            <input type="hidden" name="a" value="aluno">
                            <input type="hidden" name="metodo" value="cadastro">
                            <input type="hidden" name="origem" value="Investimento">
                            <input type="hidden" name="curso" value="5">
                            <input type="text" class="validate-required" name="nome" placeholder="Nome" lang="Nome">
                            <input type="text" class="validate-required validate-email" name="email" placeholder="Email" lang="E-mail">
                            <!-- <p class="pl20"><strong>Quem é você?</strong></p> -->

                            <br>
                            <button class="vermelho" type="submit">Veja o investimento e calendário</button>
                            <!-- <button class="vermelho" type="submit">
        Realizar Inscrição Online        </button> -->
                        </form>

                        <div class="fb-page fb_iframe_widget" data-href="https://www.facebook.com/cursomethodus/" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-height="720" fb-xfbml-state="rendered" fb-iframe-plugin-query="adapt_container_width=true&amp;app_id=1302272369795939&amp;container_width=340&amp;height=720&amp;hide_cover=false&amp;href=https%3A%2F%2Fwww.facebook.com%2Fcursomethodus%2F&amp;locale=pt_BR&amp;sdk=joey&amp;show_facepile=true&amp;small_header=false&amp;tabs=timeline"><span style="vertical-align: bottom; width: 340px; height: 720px;"><iframe name="f150fcec9eec0bc" width="1000px" height="720px" title="fb:page Facebook Social Plugin" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" allow="encrypted-media" src="https://www.facebook.com/v2.11/plugins/page.php?adapt_container_width=true&amp;app_id=1302272369795939&amp;channel=https%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter.php%3Fversion%3D44%23cb%3Df1101b7277ac3d8%26domain%3Dwww.methodus.com.br%26origin%3Dhttps%253A%252F%252Fwww.methodus.com.br%252Ff1475ed1b8fc558%26relation%3Dparent.parent&amp;container_width=340&amp;height=720&amp;hide_cover=false&amp;href=https%3A%2F%2Fwww.facebook.com%2Fcursomethodus%2F&amp;locale=pt_BR&amp;sdk=joey&amp;show_facepile=true&amp;small_header=false&amp;tabs=timeline" style="border: none; visibility: visible; width: 340px; height: 720px;" class=""></iframe></span></div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>