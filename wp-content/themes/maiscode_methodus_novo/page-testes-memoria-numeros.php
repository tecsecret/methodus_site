<?php
/**
* Template Name: Página Teste Memoria Numeros
* @package WordPress
* @author Mais Code Tecnologia
* @since First Version
*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Teste</title>
</head>

<body>



<?php
	$lista_palavras 	= "1,6,8,4,4,3,0,6,3,1,1,2,1,4,0,0,7,4,9,2"	;

	$lista_palavras 	= explode(",",$lista_palavras);
?>


<script type="text/javascript">
//#########################################################//
//                         TESTES                          //
//#########################################################//


function iniciar()
{
	
	document.getElementById( "camada_01" ).style.display='none'; 
	document.getElementById( "camada_02" ).style.display='block'; 
	
	document.getElementById( "palavras_02" ).style.display='none';
	document.getElementById( "palavras_01" ).style.display='block';
	
	relogio();
}

relogio_i = 60;
function relogio()
{

	if (relogio_i >= 0)
	{
	
		div_relogio = document.getElementById( "tempo" );
		div_relogio.innerHTML = "<b>Tempo:</b> "+ relogio_i;
		relogio_i = relogio_i - 1
		setTimeout("relogio();", 1000);
	
	}
	else
	{
		
		termina();
		document.getElementById( "botao_finaliza" ).style.display='block';
		
	}
	
}

function termina()
{
	document.getElementById( "palavras_01" ).style.display='none';
	document.getElementById( "palavras_02" ).style.display='block';
	document.getElementById( "tempo" ).style.display='none';
}

acertos = 0;
function finaliza()
{
	relogio_i = 0;
	
	document.getElementById( "palavras_01" ).style.display='block';
	document.getElementById( "palavras_02" ).style.display='block';
	
	for (i=0; i<20; i++)
	{
		controle_registro = document.getElementById( "controle_"+i ).value;
		resposta_registro = document.getElementById( "resposta_"+i ).value;
		
		if ( controle_registro == resposta_registro )
		{
			acertos = acertos + 1;
			document.getElementById( "resposta_"+i ).style.backgroundColor = "#CCFF99"; 
		}else
		{
			document.getElementById( "resposta_"+i ).style.backgroundColor = "#FFEEEE"; 
		}
		
		//document.getElementById( "resposta_"+i ).readonly = true;
		
	}
	
	
	div_resultado = document.getElementById( "resultado" );
	div_resultado.style.display = "block";
	div_resultado.innerHTML = "<b>Resultado:</b> "+ acertos +"<BR /><BR />";
	
	document.getElementById( "avaliacao" ).style.display='block';
	document.getElementById( "tempo" ).style.display='none';
	document.getElementById( "botao_finaliza" ).style.display='none';
	
}

	
</script>



<table border="0" cellpadding="0" cellspacing="0" width="100%">

<TR id="camada_01" style="display:block">
	<TD>
    	Clique em iniciar para começar a contagem de 1 minuto e memorizar os números em sequência. Ao termino do tempo, preencha o formulário corretamente e clique em "finalizar" para ver os resultados.
    	<BR /><BR />
    	<center>
        	<input type="button" value="Iniciar" class="input_botao_01" style="width:250px;" onclick="iniciar();" />
    	</center>
    </TD>
</TR>

<TR id="camada_02" style="display:none;">

	<TD>
    
        
        <table border="0" cellpadding="10" cellspacing="0" align="center">
        <form name="teste">
            <TR>
                <TD valign="top" id="palavras_01">
                
                	<table border="0" cellpadding="0" cellspacing="0"><TR>
                	
                    <?php for ($i=0; $i<count($lista_palavras); $i++){ ?>

                        <TD><input type="text" name="controle_<?php echo $i ?>" id="controle_<?php echo $i ?>" readonly="readonly" style="width:15px;" value="<?php echo $lista_palavras[$i] ?>" class="campo_form" /></TD>

                    <?php } ?>
                    
                    </TR></table>
                    
                </TD>
            </TR>
            <TR>            
                <TD valign="top" id="palavras_02">
                
                	<table border="0" cellpadding="0" cellspacing="0"><TR>
                
                	<?php for ($i=0; $i<count($lista_palavras); $i++){ ?>
                    
                        <TD><input type="text" name="resposta_<?php echo $i ?>" id="resposta_<?php echo $i ?>" style="width:15px;" maxlength="1" onKeyUp="return autoTab(this, 1, event);" class="campo_form" /></TD>
					
                    <?php } ?>
                    
                    </TR></table>
                
                </TD>
                
            </TR>
            <TR>
            	<TD valign="top" width="100">
                
                	<div id="tempo"><b>Tempo:</b> 60</div>
                    
                    <div id="resultado" style="display:none;"> <b>Resultado:</b> --<BR /></div>
                    
                    <div id="avaliacao" style="display:none;">
                        <b>Avaliação:</b><BR />
                        Bom - 18 a 20<BR />
                        Médio - 13 a 17<BR />
                        Ruim - 6 a 12
                    </div>
                    
        			<input id="botao_finaliza" type="button" value="Finalizar" class="input_botao_01" onclick="finaliza()" style="display:none;" />
                    
                </TD>
            </TR>
        </form>
        </table>
        
	</TD>
</TR>

</table>

</body>
</html>