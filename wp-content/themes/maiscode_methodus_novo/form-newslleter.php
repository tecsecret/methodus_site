<?php
if ( isset($identificador_form_generico) == false ){
    $identificador_form_generico = 'popup'; 
}

  $url_api          = 'https://www.methodus.com.br/api/';


?>
<section id="secao_generico_<?php echo $identificador_form_generico; ?>">
  <div class="col-sm-2"></div>
  <div class="col-sm-8 text-center">
    <ul class="accordion accordion-1 mb0">
      <li class="active">
        <div class="title"> <span style="font-size: 24px">Quero manter-me atualizado</span> </div>
        <div class="content">
          <div id="bio_ep_content">
            <form action="<?= $url_api; ?>" id="generico_<?php echo $identificador_form_generico; ?>" method="post" class="formAjax" data-validacao="checaGenerico" data-resultado="sucessoGenerico">
            <input type="hidden" name="a" value="aluno" />
            <input type="hidden" name="metodo" value="cadastro" />
            <input type="hidden" name="origem" value="Newsletter" />
            <div id="leftpopup">
              <input type="text" name="nome" placeholder="Nome" lang="Nome">
              <input type="text" name="email" placeholder="Email" lang="E-mail">
            </div>
            <div id="rightpopup">
              <!-- <select name="curso" class="form-control" onchange="mudaPerfis('<?php echo $identificador_form_generico; ?>', this)" lang="Área de interesse">
                <option value="">Selecione Área de Interesse</option>
                <?php
                /*$xml_cursos   = executaPagina( 'api/', array( 'a'=>'cursos', 'metodo'=>'listar' ) );
                $lista_cursos   = lerXML( $xml_cursos );
                
                if ( $lista_cursos->erro == 0 ){
                    $lista_cursos = $lista_cursos->cursos->curso;
                    foreach ($lista_cursos as $lista_curso){
                        echo '<option value="'.$lista_curso['codigo'].'">'.$lista_curso->titulo.'</option>';
                    }
                }*/
                ?>
              </select> -->
              <!-- <select name="perfil" class="form-control" id="generico_perfil" lang="Perfil">
                <option value="">Selecione seu perfil</option>
              </select> -->
              
              <?php
                $xml_cursos     = executaPagina( 'api/', array( 'a'=>'cursos', 'metodo'=>'listar' ) );
                $lista_cursos   = lerXML( $xml_cursos );
                
                if ( $lista_cursos->erro == 0 ){
                    $lista_cursos = $lista_cursos->cursos->curso;
                    foreach ($lista_cursos as $lista_curso){
                        echo    '<div class="radio_cursos">
                                    <label class="radio"><input type="radio" name="curso" value="'.$lista_curso['codigo'].'" id="radio_curso_'.$identificador_form_generico.'_'.$lista_curso['codigo'].'" /><span></span></label> 
                                    <label for="radio_curso_'.$identificador_form_generico.'_'.$lista_curso['codigo'].'" style="cursor:pointer;">'.$lista_curso->titulo.'</label>
                                </div>';
                    }
                }
                ?>
            </div>
            <!--<a href="#YOURURLHERE" class="bio_btn" style="margin-bottom: 20px; margin-top: 0px">QUERO MANTER-ME ATUALIZADO</a> -->
            <input type="button" class="bio_btn" style="margin-bottom: 20px; margin-top: 0px" value="QUERO MANTER-ME ATUALIZADO" onclick="$('#generico_<?php echo $identificador_form_generico; ?>').submit();" />
            </form>
          </div>
        </div>
      </li>
    </ul>
  </div>
</section>