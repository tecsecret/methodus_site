<?php

get_header(); 
setPostViews(get_the_ID());
$image = wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() ));

?>

<section class="single-view">
    <div class="container">
        <div class="row">
            <div class="col-md-9 mb-xs-24" id="conteudo_paginado">
                <div class="post-title">
                    <h1 class="inline-block"><?php the_title(); ?></h1>
                </div>
                <hr>
                <div class="post-snippet mb64" style="clear:both;">
                    <div class="post-title">
                        <span class="label">
                            <a href="<?php the_field('site_empresa'); ?>" target="_blank"><?php the_field('site_empresa'); ?></a>
                        </span>	
                    </div>
                    <hr>
                    <p>	
                        <img class="mb24 thumb_pequeno" src="<?php echo $image; ?>"><?php the_field('breve_descricao_da_empresa'); ?><br><?php the_field('telefone_empresa'); ?><br>
                        <a href="mailto:contato@pensadigital.com.br"><?php the_field('e-mail_empresa'); ?></a>
                    </p>
                </div>                
            </div>


            
            <div class="col-sm-6 col-md-4 col-md-offset-1">
                <h6 class="title"><?php the_field('pesquisar_empresas',401); ?></h6>
                <div class="busca-widget">
                    <form action="<?php bloginfo('siteurl'); ?>" id="searchform" method="get">
                        <div>
                            <input placeholder="Pesquisar por" type="text" id="s" name="s" value="" />
                            <div class="text-center">
                                <input type="submit" class="btn btn-input btn-sm" value="Buscar" id="searchsubmit" />
                            </div>
                        </div>
                    </form>
                </div>

                <h6 class="title"><?php the_field('texto_categoria_artigos',326); ?></h6>

                <?php
                $args = array(
                    'taxonomy' => 'categoria_empresa',
                 'orderby' => 'date',
                 'order'   => 'DESC',
                 'hide_empty' => false
             );

                $cats = get_terms($args);
                $cont = 1;

                ?>

                <ul class="link-list">
                    <?php
                    foreach($cats as $cat) { 
                      ?>

                      <li><a href="<?php echo get_term_link($cat->term_id); ?>"><?php echo $cat->name; ?></a></li>

                      <?php 
                  }
                  ?>
              </ul>

              <h6 class="title"><?php the_field('texto_mais_acessados',326); ?></h6>
              <ul class="link-list">



                <?php 
                $args = array(
                    'post_type' => 'empresas',
                    'post_status' => 'publish',
                    'posts_per_page' => '20',
                    'meta_key' => 'post_views_count',
                    'order' => 'DESC'
                );
                $my_posts = new WP_Query( $args );

                if ( $my_posts->have_posts() ) : 
                    ?>

                    <?php while ( $my_posts->have_posts() ) : $my_posts->the_post(); 

                      ?>

                      <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>

                  <?php endwhile; ?>
              <?php endif; ?>

          </ul>


          <?php //dynamic_sidebar ( 'sidebar-artigos' ) ; ?>



      </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>