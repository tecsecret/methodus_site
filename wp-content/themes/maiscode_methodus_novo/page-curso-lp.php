<?php
/**
* Template Name: Pagina Curso LP
* Template Post Type: post, page, cursos
*
* @package WordPress
* @author Mais Code Tecnologia
* @since First Version
*/

get_header(); 

switch (get_field('modelo_pagina')) {
  case 'modelo-a':
    $modelo_curso = 'a';
    break;
  case 'modelo-b':
    $modelo_curso = 'b';
    break;  
  default:
    $modelo_curso = 'a';
    break;
}

get_template_part( 'templates/curso', $modelo_curso );

get_footer(); ?>