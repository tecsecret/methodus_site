﻿<?php
/**
* Template Name: Curso Leitura Dinamica e Memoria
* @package WordPress
* @author Mais Code Tecnologia
* @since First Version
*/


$url_site = get_option('link_site'); 
$url_api  =  get_option('link_api'); 
$url_amigavel_wp = home_url(); 
// URLs amigáveis
$url_amigavel_on  = 1;
$url_amigavel   =  get_option('link_site_amigavel');

$pagina_curso = 1;

$urlWP = get_template_directory_uri();
// $urlWP = site_url();
// $urlWP = 'https://www.methodus.com.br/';
  
// Verificar
$ID = get_field('id_do_curso');
$ID_Depoimento_Curso = get_field('id_depoimento_curso');
$ebookWp = get_field('ebook');
$xml_curso  = executaPagina( 'api/', array( 'a'=>'cursos', 'metodo'=>'ver', 'ID'=>$ID ) );
$curso      = lerXML( $xml_curso );
    

// var_dump($curso);
    
if ( $curso->erro == 0 ){
    
    $curso              = $curso->curso;
    $status             = $curso->status;
    $titulo_curso       = $curso->titulo;
    $url_curso          = $curso->url;
    $distancia_curso    = $curso->distancia;
    $banner_curso       = $curso->banner;
    $resumo_curso       = $curso->resumo;
    $descricao_curso    = $curso->descricao;
    $variaveis_curso    = $curso->variaveis; 
    $planos_curso       = $curso->planos;
    $turmas_curso       = $curso->turmas->turma;
    $perfis_curso       = $curso->perfis->perfil;
    $ID_tema            = $curso->tema;
    
    $seo_metatags       = $curso->metatags;
    $seo_title          = $titulo_curso.' - Methodus';
    $seo_imagem         = 'https://'.$_SERVER['HTTP_HOST'].''.$banner_curso;
    
    if ( isset( $variaveis_curso->classe ) ){
        $classe_curso = $variaveis_curso->classe;
    }else{
        $classe_curso = 'vermelho'; 
    }
    
    if ( isset( $curso->fotos ) ){
        $fotos_curso = $curso->fotos->foto;
    }

    if ( $status != 1 ){
        exit($seo_imagem );
    }
    
}else{
    exit("Erro ao acessar os dados do curso");      
}

?>


<?php get_header(); ?>


<style type="text/css">
	  .caixa_ebook img{
    width: 170px;
  }
        .btn_top{
            color: #fff !important;
        }
        .btn_top:hover{
            background-color: #fff !important;
            color: #214822 !important;
        }
        .btn_pg_curso{
            border: 2px solid #214822 !important;;
        }
        .btn_pg_curso:hover{
            background-color: #fff !important;
            color: #214822 !important;
        }
        .btn_chamada:hover{
            color: #214822 !important;
        }
    </style><!-- TOPO -->

<style type="text/css">
  .courseColor {
    color: #214822;
  }
  .courseBackgroundColor {
    background-color: #214822;
  }
  #secStoryModB .storyContent svg path {
    fill: #214822;
  }
  .sliderCard svg path {
    fill: #214822;
  }
  #secTopModB:before {
    content: '';
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    background: linear-gradient(270deg, rgba(33, 72, 34, 0.7) 23.16%, rgba(52, 50, 50, 0) 118.16%);
  }
  #secCallBannerModB .bgTransformation:before {
    content: '';
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    background: linear-gradient(179.64deg, rgba(96, 94, 94, 0) -36.93%, rgba(33, 72, 34, 0.6) 85.54%);;
  }
  .loadd{
    display: none;
    opacity: .5;
    background: #000;
    width: 100%;
    position: absolute;
    top: 0px;
    height: 100vh;
    z-index: 10000000;
  }
  
  .sliderCardDepo .depo-prev
  {
    height: 36px;
    width: 220px;
    z-index: 100000;
    object-fit: contain;
  }
  .sliderCardDepo .depo-next{
    width: 235px;
    height: 36px;
    object-fit: contain;
  }
</style>

<section id="secTopModB" style="background-image: url('https://methodus.com.br/wp-content/uploads/2020/10/curso-de-leitura-dinamica-e-memoria-lp.jpg')">
  <div class="container">
    <div class="row">
      <div class="col-md-7 col-md-offset-5 col-sm-7 col-sm-offset-5 topContent">
        <h2>Leia facilmente 1 livro no tempo de um filme:</h2>
        <p>200 páginas em 2 horas.</p>
        <img src="https://methodus.com.br/wp-content/uploads/2020/08/curso-de-oratoria-lp-1.png">
      </div>
    </div>
  </div>
</section>

<!-- VIDEO -->
<?php
    if ( isset( $variaveis_curso->ebook_download ) ){
        $temp_ebook = explode( '|', $variaveis_curso->ebook_download );

        if ( count( $temp_ebook ) >= 4 ){

            $titulo_ebook           = $temp_ebook[0];
            $descricao_ebook        = $temp_ebook[1];
            $destino_ebook          = $temp_ebook[2];
            $identificador_ebook    = $temp_ebook[3];
            $urlWP = get_template_directory_uri();
            ?>


            <section class="caixa_ebook ">
                <div class="container-fluid bg_ebook">
                    <div class="container">                       
                 
                        <script src="<?php echo $urlWP; ?>/js/scripts_site/ebook.js?ver=1"></script>
                        <form action="<?php echo $url_api; ?>/api/" method="post" class="form-email formAjax" id="form_ebook" data-validacao="checaEbook" data-resultado="sucessoEbook">
                            <input type="hidden" name="destino" value="<?php echo $ebookWp ?>" />
                            <input type="hidden" name="a" value="aluno" />
                            <input type="hidden" name="metodo" value="cadastro" />
                            <input type="hidden" name="origem" value="Ebook_<?php echo $identificador_ebook; ?>" />
                            <input type="hidden" name="curso" value="<?php echo $ID; ?>" />
                    
                            <div class="row">
                                <div class="col-xs-12 col-md-8">
                                    <img src="<?php echo $urlWP ?>/img/ebook_<?php echo $classe_curso; ?>.png" width="80" />
                                    <div class="chamada-ebook"><?php echo $descricao_ebook ?></div>
                                </div>
                                <div class="col-xs-12 col-md-4">
                                    <h5 class="mb0 inline-block p0-xs">
                                        <?php echo $titulo_ebook ?>
                                    </h5>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <input type="text" class="validate-required cada-input" name="nome" placeholder="Nome" lang="Nome">
                                        </div>
                                        <div class="col-sm-12 form-ebook">
                                            <input type="text" class="validate-required cada-input" name="email" placeholder="E-mail" lang="E-mail">
                                            <?php
                                            $i = 0;
                                            if( have_rows('formulario') ):
                                                while ( have_rows('formulario') ) : the_row(); $i++?> 
                                                    <div class="radio_cursos">  
                                                        <label for="ebook-<?php echo $i; ?>" class="radio">
                                                            <input type="radio" class="validate-required" id="ebook-<?php echo $i; ?>" name="conheceu" value="<?php the_sub_field('opcoes'); ?>" lang="opções">
                                                            <span></span>
                                                        </label>          
                                                        <label for="ebook-<?php echo $i; ?>"><?php the_sub_field('opcoes'); ?></label><br>
                                                    </div>
                                                <?php endwhile;
                                            endif;
                                            ?>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <button class="btn <?php echo $classe_curso; ?>" type="submit">Download</button>
                                        </div>
                                    </div>
                                </div>
                            </div>                            
                        </form>
                    </div>
                </div>
            </section>
        <?php
        }
    }
    ?>

<!-- BANNER CHAMADA -->
<section id="secCallBannerModB" class="courseBackgroundColor">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2>Sabe aquela expressão: “Xiiiii não vou ler tudo isso, vou esperar sair o filme” ? Pois é, agora ela não vale mais!</h2>
                <p></p>
      </div>
    </div>
  </div>
  </section>

        

<!-- HISTORIA -->
<section id="secStoryModB">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="storyContent">
        <p>VOCÊ DUVIDA? VEJA ABAIXO DEPOIMENTO DO ANDRÉ<br />
Tempo de Leitura deste texto: 5 minutos<br />
Tempo para quem fez o curso Methodus: 1 minuto</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
      </div>
      </div>
  </div>
    <div class="row">
      <div class="col-md-12">
        <h2 class="courseColor">&#x25cf; Eu fiz o curso da Methodus &#x25cf;</h2>
        <div class="storyContent">
          <svg class="quoteStart" width="71" height="56" viewBox="0 0 71 56" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M16.2565 0L26.072 7.67124C21.4225 12.4444 18.3229 16.962 16.7731 21.2237C15.2232 25.3151 14.4483 29.4916 14.4483 33.7534L8.24908 29.4064H27.1052V56H0.5V37.3333C0.5 30.344 1.61931 23.8661 3.85793 17.8996C6.26876 11.7626 10.4016 5.79605 16.2565 0ZM59.6513 0L69.4668 7.67124C64.8173 12.4444 61.7177 16.962 60.1679 21.2237C58.6181 25.3151 57.8432 29.4916 57.8432 33.7534L51.6439 29.4064H70.5V56H43.8948V37.3333C43.8948 30.344 45.0141 23.8661 47.2528 17.8996C49.6636 11.7626 53.7964 5.79605 59.6513 0Z"/>
          </svg>
                                    <p>A Methodus nasceu em 1998, quando a internet ainda era uma desconhecida. O hábito de ler jornais, revistas e livros físicos fazia parte da vida de todas as pessoas. Da minha inclusive. Eu estava na faculdade quando fiz meu primeiro curso na Methodus, em 1998. Eu me lembro bem, pois foi durante a Copa do Mundo – me recordo de ter assistido a um jogo do Brasil contra a Holanda e ter ido correndo para o curso para não perder a aula. Era o curso de Leitura Dinâmica e Memorização.</p>
<p>&nbsp;</p>
<p>Cheguei ao curso um pouco receoso, pensando se valeria a pena. Será que funciona mesmo, eu pensava. Será que não é enrolação?</p>
<p>&nbsp;</p>
<p>E aí, já na primeira aula, me lembro com muita clareza, como se fosse hoje: o Professor Alcides nos fez ler um livro de frente a um colega enquanto ele observava nossos olhos. E depois trocávamos. Era para lermos o mais rápido possível.</p>
<p>&nbsp;</p>
<p>E foi impressionante: por mais que lêssemos rapidamente, os olhos paravam em cada sílaba ou cada palavra, mesmo que por um milésimo de segundo – e é marcante ver os olhos parando diversas vezes até chegar ao final da linha e recomeçar o “anda e para” na linha de baixo.</p>
<p>&nbsp;</p>
<p>E foi aí, explicando porque lemos assim pausadamente, e explicando a teoria por trás do aprendizado que temos quando criança e como deveríamos treinar e reprogramar nosso tipo de leitura, que eu fiquei fascinado e acabei com qualquer dúvida que pairava sobre mim se valeria a pena. E valeu muito a pena!</p>
<p>&nbsp;</p>

                              <div class="boxContrast">
                                      <span class="courseColor">No final de 10 aulas, eu tinha aprendido as técnicas e treinado da maneira correta e:</span>
                                    <h2>Eu estava lendo 5 vezes mais rápido do que no dia em que iniciei o curso na Methodus. </h2>
                </div>
                                        <p><strong> <em>E comprovei isso na sala de aula da faculdade. </em> </strong><br />
Quando havia algum texto para ser lido em sala, eu terminava e levantava a cabeça – e via todos os meus colegas na sala ainda com a cabeça abaixada lendo.<br />
E tudo isso com compreensão de texto.<br />
Além de tudo isso, um Bônus: o curso de memória me ajudou muito durante a faculdade.</p>
<p>&nbsp;</p>

                              <div class="boxContrast">
                                    <h2>Para estudantes, vestibulandos, concurseiros e pessoas que precisam reter muitas informações no trabalho, as técnicas de mind map melhoram e muito a memorização das informações. É algo que se aprende e nunca mais esquece!</h2>
                </div>
                                        <p>E tem uma coisa que foi o diferencial pra mim: no curso de Leitura Dinâmica, a evolução é medida com testes em todas as aulas: não só de velocidade de leitura, mas também de compreensão dos textos, com perguntas sobre o que lemos. Ou seja, a cada aula, eu saía de lá sabendo que estava funcionando!</p>

                              <div class="boxContrast">
                                    <h2>Foi uma transformação impressionante!</h2>
                </div>
                                        <p><em>André – Adm de Empresas – Empreendedor, Sócio de uma Agência de Publicidade.</em></p>

                                              <svg class="quoteEnd" width="73" height="58" viewBox="0 0 73 58" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M56.2934 0L46.1974 7.94521C50.9797 12.8889 54.1679 17.5677 55.762 21.9817C57.3561 26.2192 58.1531 30.5449 58.1531 34.9589L64.5295 30.4566H45.1347V58H72.5V38.6667C72.5 31.4277 71.3487 24.7184 69.0461 18.5388C66.5664 12.1827 62.3155 6.00305 56.2934 0ZM11.6587 0L1.56272 7.94521C6.34501 12.8889 9.5332 17.5677 11.1273 21.9817C12.7214 26.2192 13.5184 30.5449 13.5184 34.9589L19.8948 30.4566H0.5V58H27.8653V38.6667C27.8653 31.4277 26.714 24.7184 24.4114 18.5388C21.9317 12.1827 17.6808 6.00305 11.6587 0Z"/>
          </svg>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="secReminder">
  <div class="reminderBanner courseBackgroundColor">
    <img class="small" src='https://methodus.com.br/wp-content/themes/maiscode_methodus_novo/img/small.png'>
    <div class="">
      <div class="row">
        <div class="col-md-12">
          <h2>Lembrete Methodus:</h2>
          <h3>Como são no máximo 15 alunos por turma, garanta já a sua vaga, <br>para não ter que esperar por outra turma depois!</h3>
          <a data-toggle="modal" data-target="#cursoVejaInvestimento" href="#">Clique aqui agora para garantir a sua vaga!</a>
        </div>
      </div>
    </div>
    <img class="big" src='https://methodus.com.br/wp-content/themes/maiscode_methodus_novo/img/big.png'>
  </div>
</section>

<!-- POR QUE A METHODUS -->
<section id="secWhyMethodus">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2 class="courseColor">DESCUBRA SE O CURSO É PARA VOCÊ</h2>
        <div class="methodusContent">
                                                    <h3 class="courseColor">&#x25cf; O Curso de Leitura Dinâmica e Memória Methodus é perfeito para quem: &#x25cf;</h3>
                              <ul>
<li style="list-style-type: none;">
<ul>
<li style="list-style: initial;"><strong>Possui Leitura Lenta e PRINCIPALMENTE Dispersiva (ou seja, você começa a ler e pronto, já tá “viajando” pensando em outra coisa. O foco e velocidade da Leitura Dinâmica não deixam a mente sair dali e nem “viajar”. É impressionante!</strong></li>
</ul>
</li>
</ul>
<p>&nbsp;</p>
<ul>
<li style="list-style-type: none;">
<ul>
<li style="list-style: initial;"><strong>É para quem tem falta de foco ao ler ou estudar – você aprenderá técnicas de como estudar e focar.</strong></li>
</ul>
</li>
</ul>
<p>&nbsp;</p>
<ul>
<li style="list-style-type: none;">
<ul>
<li style="list-style: initial;"><strong>É para que tem um monte de matéria e informação para estudar (seja estudante ou no trabalho) e não sabe por onde começar.</strong></li>
</ul>
</li>
</ul>
<p>&nbsp;</p>
<ul>
<li style="list-style-type: none;">
<ul>
<li style="list-style: initial;"><strong>É Profissional e que identifica a necessidade de retomar estudos e quer fazer uma pós, especializações, doutorado, MBA ou simplesmente retomar o hábito de ler.</strong></li>
</ul>
</li>
</ul>
<p>&nbsp;</p>
<ul>
<li style="list-style-type: none;">
<ul>
<li style="list-style: initial;"><strong>Deseja passar em Concursos Públicos, mesmo trabalhando enquanto estuda.</strong></li>
</ul>
</li>
</ul>
<p>&nbsp;</p>
<ul>
<li style="list-style-type: none;">
<ul>
<li style="list-style: initial;"><strong>Estudantes do 3ºano do Ensino Médio, já realizando cursinhos para Vestibular</strong></li>
</ul>
</li>
</ul>
<p>&nbsp;</p>
<ul>
<li style="list-style: initial;"><strong>Estudantes de Medicina e Residentes que têm um volume muito grande para reter.</strong></li>
</ul>
                                                        <h3 class="courseColor">E temos uma pergunta: Por que não pode ser você?</h3>
<p><strong>É claro que pode.</strong> A habilidade da leitura dinâmica vem com o treinamento. E é possível a qualquer pessoa.</p>
                                                        <h3 class="courseColor">&#x25cf; E por que o Curso é junto com o de Memória e Técnicas Concentração? &#x25cf;</h3>
                              <p>A gente explica cientificamente para você.</p>
<p>&nbsp;</p>
<p>Durante o curso, você terá de maneira aprofundada a explicação abaixo. Mas para entender de maneira simples e objetiva, o motivo dos cursos serem juntos é que cada um trabalha um aspecto cerebral que complementa o outro. As aulas são permeadas por exercícios de leitura, de memória e concentração no mesmo dia.</p>
<p>&nbsp;</p>
<p>Um dos módulos do Curso de Memória que é exclusivo e desenvolvido pela Methodus, expande muito a capacidade do lado direito do cérebro, que é a parte responsável pelas imagens e criatividade do cérebro.</p>
<p>&nbsp;</p>
<p>Desta forma, quando vamos ler dinamicamente, temos que ver as palavras como um todo e não como sílabas. Então, quando temos essa capacidade cerebral aumentada, é como se estivéssemos turbinando o cérebro para conseguir assimilar as aulas de leitura com muito mais eficiência.</p>
<p>&nbsp;</p>
<p>E do lado oposto, o foco necessário e a concentração para se ler dinamicamente expandem o desenvolvimento do cérebro para que as informações sejam analisadas e posteriormente, guardadas com muito mais eficiência na memória.</p>
<p>&nbsp;</p>
<p>Ou seja, além de sair muito mais preparado, você sai com o cérebro muito mais desenvolvido.</p>
                                                        <h3 class="courseColor">&#x25cf; Nota Methodus: &#x25cf;</h3>
                              <p>O Curso de leitura Dinâmica + Memória da Methodus é exclusivo e baseado em estudos de neurociência. E foram desenvolvidos pelo fundador da Methodus, o Professor, Filósofo e Pedagogo Alcides Schotten.</p>
<p>&nbsp;</p>
<p>Mais abaixo veremos a biografia dele para você entender melhor.</p>
<p>Como funciona o Treinamento:</p>
<p><strong style="font-size: 18px;">Aspecto Físico</strong></p>
<p>Globos Oculares – Treinamento de agilidade, captação, campo de visão e percepção.</p>
<p><strong style="font-size: 18px;">Aspecto Mental</strong></p>
<p>Técnicas Exclusivas Methodus que unem a expansão do lado direito do cérebro com a capacidade de foco e concentração (inclusive com técnicas de relaxamento do cérebro para maior retenção de informações).<strong> </strong></p>
<p>&nbsp;</p>
<p><strong>Para entender estes diferenciais, que chamamos de Método Methodus, primeiro é preciso que você conheça quem é o Prof. Alcides Schotten, professor, filósofo, pedagogo e criador e desenvolvedor do método.</strong></p>
<p>&nbsp;</p>
<p><strong>Leia o texto abaixo:</strong></p>
                                                        <h3 class="courseColor">&#x25cf; Por que a Methodus funciona melhor para você &#x25cf;</h3>
                              <p>Para você entender quais os diferencias reais, vamos falar primeiro sobre o criador da Methodus, o professor Alcides Schotten.</p>
<p>O professor Alcides é PROFESSOR E FILÓSOFO, ou seja, ele vai além do básico e aprofunda-se realmente na mente humana.</p>
<p>&nbsp;</p>
<p>Estudioso por natureza, aprimora-se todos os anos, buscando na neurociência, o entendimento de como o cérebro funciona e reage. E assim, usa toda sua base e conhecimento de professor e filósofo, para criar os estímulos perfeitos para que o cérebro aprenda de verdade. Não é simplesmente aprender por uns dias. A metodologia é feita para ficar. E de maneira fácil.</p>
                                                        <h3 class="courseColor">&#x25cf; NOSSA MOTIVAÇÃO &#x25cf;</h3>
                              <p><em>O Alcides Schotten, fundador da Methodus, sempre foi professor, sempre foi um mestre, e por isso, ensinar é a motivação de vida que o move a querer sempre ver seus alunos conquistando seus sonhos! E isso é verdade. Basta ver a felicidade estampada no rosto de quem completa os cursos.</em></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
                              <div class="boxMethodusImg">
                  <img src="https://methodus.com.br/wp-content/uploads/2020/04/home.jpeg">
                  <span class="courseColor"></span>
                </div>
                                            </div>
        <div class="differenceTitle">
          <h2 class="courseColor">Como fazemos para você aprender? </h2>
          <h3 class="courseColor">A gente explica!</h3>
        </div>
        <div class="differenceContent">
                                    <h3><u>METODOLOGIA: <strong>TEM QUE SER FÁCIL.</strong></u></h3>
<p>ALGUMA COISA DIFÍCIL DE APRENDER NA ESCOLA VOCÊ LEMBRA ATÉ HOJE? NÃO, NÉ?</p>
<p>&nbsp;</p>
<p>Por isso, a gente sempre pensa em jeitos fáceis de você aprender. Porque senão, você esquece. O método utilizado nos cursos e treinamentos oferecidos pela Methodus foi desenvolvido pelo professor e filósofo Alcides Schotten, que procurou em todo o conhecimento humano – desde Platão e Aristóteles, na Grécia Antiga, até os pensadores mais modernos – um ponto de equilíbrio e o segredo do aperfeiçoamento de talentos naturais.</p>
<p>&nbsp;</p>
<p>Através do método usado nos cursos da Methodus, o aluno desenvolverá de forma prática, gradual e relativa às suas necessidades, habilidades de otimização pessoal. Ou seja, não é uma fórmula que pode funcionar para um e não para o outro. Existe uma fórmula e dosagem certa para você.</p>
<p>&nbsp;</p>
<p>Por isso é fácil e duradouro.</p>
                                        <h3><strong><u>E O QUE VOU CONSEGUIR FAZENDO O CURSO?</u></strong></h3>
                                                <ul>
                                          <li><span>Ler de 5 a 8 vezes mais rápido</span></li>
                                          <li><span>Ter compreensão do que leu 5 a 8 vezes mais rápido – baseado em testes reais feitos na primeira e última aulas. E você fará os seus também.</span></li>
                                          <li><span>Acabar com a Frustração de não conseguir terminar os livros.</span></li>
                                          <li><span>Acabar com o medo de achar que não dará tempo de ler o que você precisa.</span></li>
                                          <li><span>Aprender técnicas de retenção de conteúdo – memória – para o que você precisar – desde números a conteúdos densos de estudo ou trabalho.</span></li>
                                          <li><span>Criar Mind Maps de Estudo.</span></li>
                                          <li><span>Aprender técnicas poderosas de concentração e foco.</span></li>
                                          <li><span>Entender a metodologia e como o cérebro aprende – para entender que sim é metodologia científica e que sim, funciona de verdade!</span></li>
                                      </ul>
                                                        <h3><u>E COMO FUNCIONA O CURSO?</u></h3>
                                                <ul>
                                          <li><span>10 Aulas de 3 horas (2x por semana) ou 5 Aulas (aos sábados)</span></li>
                                          <li><span>Testes de Velocidade de Leitura em todas as aulas</span></li>
                                          <li><span>Intercalar Leitura com Memória para trabalhar os dois lados do cérebro e solidificar o aprendizado.</span></li>
                                          <li><span>Treinos no telão de aumento de campo de visão</span></li>
                                          <li><span>Treinos no telão de Velocidade de observação</span></li>
                                          <li><span>Treinos no Telão de compreensão de textos</span></li>
                                          <li><span>Treinos no Telão de Agilidade Ocular</span></li>
                                          <li><span>Interação entre os alunos para ver os olhos em funcionamento</span></li>
                                          <li><span>Criação de Mind Maps de Matérias Selecionadas</span></li>
                                          <li><span>Aprendizado de técnicas de relaxamento que deixam o cérebro descansado e pronto para aprender e reter mais fácil</span></li>
                                          <li><span>Máximo de 15 alunos por turma – para acompanhamento de perto de cada aluno.</span></li>
                                      </ul>
                                                            </div>
      </div>
    </div>
  </div>
</section>

<!-- VANTAGENS -->
<section id="secAdvantages">
  <div class="bannerSatisfaction courseBackgroundColor">
    <h2>AGORA VEJA A SATISFAÇÃO REAL DE QUEM JÁ FEZ O NOSSO CURSO:</h2>
    <h3>FUNCIONA PARA TODOS OS TIPOS DE PESSOAS!</h3>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
                                <div class="advantagesBox">
                  </div>
      </div>
    </div>

<section id="secTestimony">
    <div class="row">
      <div class="col-md-12">
                    <div class="row sliderCardDepo"  style="    max-height: 544px;">
                              <div class="col-md-12">
                  <div class="boxTestimony">
                    <img class="avatarImg" src="https://methodus.com.br/wp-content/uploads/2020/10/curso-de-leitura-dinamica-e-memoria-lp-4.jpg">
                    <h3 class="courseColor">Alexandre Silva Ciconato</h3>
                    <span>Bacharel em Administração de Empresas / Pósgraduando em Filosofia e Autoconhecimento Pessoal e Profissional, Esplendor Tratamento de Superfície </span>
                    <div class="testimonyText">
                      <svg class="quoteSlickStart" width="39" height="33" viewBox="0 0 39 33" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M9.0535 0.201172L14.3819 4.58474C11.8579 7.31229 10.1753 9.89371 9.33395 12.329C8.49262 14.6669 8.07196 17.0535 8.07196 19.4888L4.70664 17.0048H14.9428V32.2012H0.5V21.5345C0.5 17.5406 1.10763 13.8389 2.32288 10.4295C3.63161 6.92264 5.87515 3.5132 9.0535 0.201172ZM32.6107 0.201172L37.9391 4.58474C35.4151 7.31229 33.7325 9.89371 32.8911 12.329C32.0498 14.6669 31.6292 17.0535 31.6292 19.4888L28.2638 17.0048H38.5V32.2012H24.0572V21.5345C24.0572 17.5406 24.6648 13.8389 25.8801 10.4295C27.1888 6.92264 29.4324 3.5132 32.6107 0.201172Z"/>
                      </svg>
                      <p><strong>Melhorou minha leitura muito além do que esperava</strong></p>
<p>&#8220;O curso conseguiu melhorar a minha leitura muito além do que esperava. Os mind maps vão me ajudar muito nos meus estudos de atualização acadêmica e profissional. Concluo ser um curso bem dinâmico no qual realmente aprenderemos e conseguiremos o que estávamos buscando&#8221;</p>
                      <svg class="quoteSlickEnd" width="39" height="31" viewBox="0 0 39 31" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M29.9465 0.201172L24.6181 4.31076C27.1421 6.86784 28.8247 9.28793 29.6661 11.571C30.5074 13.7628 30.928 16.0003 30.928 18.2834L34.2934 15.9546H24.0572V30.2012H38.5V20.2012C38.5 16.4569 37.8924 12.9866 36.6771 9.79022C35.3684 6.50255 33.1248 3.3062 29.9465 0.201172ZM6.3893 0.201172L1.06088 4.31076C3.58487 6.86784 5.26752 9.28793 6.10885 11.571C6.95018 13.7628 7.37085 16.0003 7.37085 18.2834L10.7362 15.9546H0.5V30.2012H14.9428V20.2012C14.9428 16.4569 14.3352 12.9866 13.1199 9.79022C11.8112 6.50255 9.56765 3.3062 6.3893 0.201172Z"/>
                      </svg>
                    </div>
                  </div>
                </div>
                              <div class="col-md-12">
                  <div class="boxTestimony">
                    <img class="avatarImg" src="https://methodus.com.br/wp-content/uploads/2020/10/curso-de-leitura-dinamica-e-memoria-lp-5.jpg">
                    <h3 class="courseColor">Klaus Dieter Bernhard Von Bartels</h3>
                    <span>Superior, Volkswagen do Brasil</span>
                    <div class="testimonyText">
                      <svg class="quoteSlickStart" width="39" height="33" viewBox="0 0 39 33" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M9.0535 0.201172L14.3819 4.58474C11.8579 7.31229 10.1753 9.89371 9.33395 12.329C8.49262 14.6669 8.07196 17.0535 8.07196 19.4888L4.70664 17.0048H14.9428V32.2012H0.5V21.5345C0.5 17.5406 1.10763 13.8389 2.32288 10.4295C3.63161 6.92264 5.87515 3.5132 9.0535 0.201172ZM32.6107 0.201172L37.9391 4.58474C35.4151 7.31229 33.7325 9.89371 32.8911 12.329C32.0498 14.6669 31.6292 17.0535 31.6292 19.4888L28.2638 17.0048H38.5V32.2012H24.0572V21.5345C24.0572 17.5406 24.6648 13.8389 25.8801 10.4295C27.1888 6.92264 29.4324 3.5132 32.6107 0.201172Z"/>
                      </svg>
                      <p>&#8220;Posso falar claramente que foi um dos cursos mais válidos da minha vida. Diferente de todos anteriormente feitos não seguia um tópico de aprendizado de cada vez, na sequência, mas todos juntos todos os dias. Não era um &#8220;passo grande&#8221; mas &#8220;pequenos passos contínuos&#8221;. Gostei muito da parte de introspecção. E também o ensinamento que uma mente sã depende de um corpo são. E também que um grande esforço hiper enorme faz o grande esforço ficar mais fácil. Grato!</p>
                      <svg class="quoteSlickEnd" width="39" height="31" viewBox="0 0 39 31" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M29.9465 0.201172L24.6181 4.31076C27.1421 6.86784 28.8247 9.28793 29.6661 11.571C30.5074 13.7628 30.928 16.0003 30.928 18.2834L34.2934 15.9546H24.0572V30.2012H38.5V20.2012C38.5 16.4569 37.8924 12.9866 36.6771 9.79022C35.3684 6.50255 33.1248 3.3062 29.9465 0.201172ZM6.3893 0.201172L1.06088 4.31076C3.58487 6.86784 5.26752 9.28793 6.10885 11.571C6.95018 13.7628 7.37085 16.0003 7.37085 18.2834L10.7362 15.9546H0.5V30.2012H14.9428V20.2012C14.9428 16.4569 14.3352 12.9866 13.1199 9.79022C11.8112 6.50255 9.56765 3.3062 6.3893 0.201172Z"/>
                      </svg>
                    </div>
                  </div>
                </div>
                              <div class="col-md-12">
                  <div class="boxTestimony">
                    <img class="avatarImg" src="https://methodus.com.br/wp-content/uploads/2020/10/curso-de-leitura-dinamica-e-memoria-lp-6.jpg">
                    <h3 class="courseColor">Amanda Prado Mascaria</h3>
                    <span>Ensino Médio</span>
                    <div class="testimonyText">
                      <svg class="quoteSlickStart" width="39" height="33" viewBox="0 0 39 33" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M9.0535 0.201172L14.3819 4.58474C11.8579 7.31229 10.1753 9.89371 9.33395 12.329C8.49262 14.6669 8.07196 17.0535 8.07196 19.4888L4.70664 17.0048H14.9428V32.2012H0.5V21.5345C0.5 17.5406 1.10763 13.8389 2.32288 10.4295C3.63161 6.92264 5.87515 3.5132 9.0535 0.201172ZM32.6107 0.201172L37.9391 4.58474C35.4151 7.31229 33.7325 9.89371 32.8911 12.329C32.0498 14.6669 31.6292 17.0535 31.6292 19.4888L28.2638 17.0048H38.5V32.2012H24.0572V21.5345C24.0572 17.5406 24.6648 13.8389 25.8801 10.4295C27.1888 6.92264 29.4324 3.5132 32.6107 0.201172Z"/>
                      </svg>
                      <p><strong>Melhorei 23,5 vezes em leitura</strong></p>
<p>&#8220;O methodus não só me ajudou a ler melhor ou a memorizar, mas antes de mais nada me ensinou a ser diferente. Hoje leio uma notícia de jornal e fico feliz ao constatar que não preciso do tempo que antes precisava para ler e compreender o texto. Em nossa última avaliação pude perceber que melhorei 23,5 vezes em leitura desde que entrei no curso e nos testes de memorização cheguei a atingir 100%. O curso acabou, fico triste por isso, mas me alegro pelos resultados.</p>
                      <svg class="quoteSlickEnd" width="39" height="31" viewBox="0 0 39 31" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M29.9465 0.201172L24.6181 4.31076C27.1421 6.86784 28.8247 9.28793 29.6661 11.571C30.5074 13.7628 30.928 16.0003 30.928 18.2834L34.2934 15.9546H24.0572V30.2012H38.5V20.2012C38.5 16.4569 37.8924 12.9866 36.6771 9.79022C35.3684 6.50255 33.1248 3.3062 29.9465 0.201172ZM6.3893 0.201172L1.06088 4.31076C3.58487 6.86784 5.26752 9.28793 6.10885 11.571C6.95018 13.7628 7.37085 16.0003 7.37085 18.2834L10.7362 15.9546H0.5V30.2012H14.9428V20.2012C14.9428 16.4569 14.3352 12.9866 13.1199 9.79022C11.8112 6.50255 9.56765 3.3062 6.3893 0.201172Z"/>
                      </svg>
                    </div>
                  </div>
                </div>
                          </div>

              </div>
    </div>
  </section>
  </div>
</section>

<section id="secBannerAdvantages" class="courseBackgroundColor">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2>O Curso Methodus possui inúmeros diferenciais que são exclusivos e você não encontra em outros cursos. Por isso nós nem comparamos com outros cursos.  E por isso não chamamos de investimento nem custo, mas sim de VALOR, pois você passa a valer mais após o curso. E definimos nosso VALOR baseado em POTENCIAL e SATISFAÇÃO:</h2>
        <div class="advantagesBox">
                                    <h3>Potencial de conseguir um emprego novo.</h3>
                          <h3>Potencial de conseguir uma promoção no trabalho e ganhar muito mais.</h3>
                          <h3>Potencial de conseguir passar no vestibular que você quer.</h3>
                          <h3>Potencial de conseguir passar no concurso que você sonha.</h3>
                          <h3>Potencial de conseguir a melhor residência médica.</h3>
                          <h3>Potencial de concluir a pós-graduação que você deseja.</h3>
                          <h3>Satisfação de terminar os livros que começou e parou.</h3>
                          <h3>Satisfação de ler mais livros por ano (5 a 8 vezes mais).</h3>
                          <h3>Satisfação de passar no concurso ou vestibular ou pós e orgulhar a família.</h3>
                          <h3>Satisfação pessoal de se desenvolver intelectualmente.</h3>
                          <h3>Satisfação de ter um Certificado de Conclusão que pode ser colocado no currículo.</h3>
                          <h3>Satisfação de poder refazer o curso novamente para treinar, de graça, para sempre!</h3>
                          <a data-toggle="modal" data-target="#cursoVejaInvestimento" href="#">Clique agora para garantir sua vaga!</a>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- Modal Cupom 
<div class="modal fade" id="cupomModal" tabindex="-1" role="dialog" aria-labelledby="cupomModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div role="form" class="wpcf7" id="wpcf7-f6273-o1" lang="pt-BR" dir="ltr">
<div class="screen-reader-response"><p role="status" aria-live="polite" aria-atomic="true"></p> <ul></ul></div>

<form action="/cursos/leitura-dinamica-e-memoria/#wpcf7-f6273-o1" method="post" class="wpcf7-form init" novalidate="novalidate" data-status="init">
<div style="display: none;">
<input type="hidden" name="_wpcf7" value="6273" />
<input type="hidden" name="_wpcf7_version" value="5.3" />
<input type="hidden" name="_wpcf7_locale" value="pt_BR" />
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f6273-o1" />
<input type="hidden" name="_wpcf7_container_post" value="0" />
<input type="hidden" name="_wpcf7_posted_data_hash" value="" />
</div>
<div class="box-contato" id="investir">
<h2 class="titulo-box vermelho">Preencha abaixo e receba um cupom de desconto de 10% no seu e-mail!</h2>
<div class="interno-box">
<div class="inputCupomForm">
<span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Primeiro nome" /></span><br />
<span class="wpcf7-form-control-wrap email"><input type="email" name="email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="E-mail" /></span>
</div>
<p><label>Curso de Preferência</label><br />
<span class="wpcf7-form-control-wrap menu-140"><select name="menu-140" class="wpcf7-form-control wpcf7-select" id="coursePref" aria-invalid="false"><option value="">---</option><option value="Oratória">Oratória</option><option value="Leitura Dinâmica / Memória">Leitura Dinâmica / Memória</option><option value="Administração do Tempo">Administração do Tempo</option></select></span></p>
<div class="oratory radioCupom" style="display: none">
<p>Eu sou: Oratória</p>
<p><span class="wpcf7-form-control-wrap radio-972"><span class="wpcf7-form-control wpcf7-radio"><span class="wpcf7-list-item first"><input type="radio" name="radio-972" value="Sou profissional / apresentações / treinamentos" /><span class="wpcf7-list-item-label">Sou profissional / apresentações / treinamentos</span></span><span class="wpcf7-list-item"><input type="radio" name="radio-972" value="Sou profissional vendas / líder religioso / político / vídeos" /><span class="wpcf7-list-item-label">Sou profissional vendas / líder religioso / político / vídeos</span></span><span class="wpcf7-list-item"><input type="radio" name="radio-972" value="Sou concursando e me preparo para prova oral" /><span class="wpcf7-list-item-label">Sou concursando e me preparo para prova oral</span></span><span class="wpcf7-list-item last"><input type="radio" name="radio-972" value="Sou universitário / apresento seminários / processo seletivo" /><span class="wpcf7-list-item-label">Sou universitário / apresento seminários / processo seletivo</span></span></span></span>
</p></div>
<div class="memory radioCupom" style="display: none">
<p>Eu sou: Leitura Dinâmica / Memória</p>
<p><span class="wpcf7-form-control-wrap radio-429"><span class="wpcf7-form-control wpcf7-radio"><span class="wpcf7-list-item first"><input type="radio" name="radio-429" value="Vestibular / concursos / ensino médio" /><span class="wpcf7-list-item-label">Vestibular / concursos / ensino médio</span></span><span class="wpcf7-list-item"><input type="radio" name="radio-429" value="Cursando ensino superior / estagiário / trainee" /><span class="wpcf7-list-item-label">Cursando ensino superior / estagiário / trainee</span></span><span class="wpcf7-list-item last"><input type="radio" name="radio-429" value="Sou profissional / faço pós e especializações" /><span class="wpcf7-list-item-label">Sou profissional / faço pós e especializações</span></span></span></span>
</p></div>
<div class="time radioCupom" style="display: none">
<p>Eu sou: Administração do Tempo</p>
<p><span class="wpcf7-form-control-wrap radio-429"><span class="wpcf7-form-control wpcf7-radio"><span class="wpcf7-list-item first"><input type="radio" name="radio-429" value="Sou empreendedor / tenho sonhos a realizar" /><span class="wpcf7-list-item-label">Sou empreendedor / tenho sonhos a realizar</span></span><span class="wpcf7-list-item"><input type="radio" name="radio-429" value="Sou profissional formado / preciso administrar o tempo / procrastino" /><span class="wpcf7-list-item-label">Sou profissional formado / preciso administrar o tempo / procrastino</span></span><span class="wpcf7-list-item last"><input type="radio" name="radio-429" value="Estou empregado / não tenho curso superior / preciso me organizar" /><span class="wpcf7-list-item-label">Estou empregado / não tenho curso superior / preciso me organizar</span></span></span></span>
</p></div>
<p><input type="submit" value="Enviar" class="wpcf7-form-control wpcf7-submit" />
</p></div>
</div>
</div>
<div class="wpcf7-response-output" aria-hidden="true"></div></form></div>      </div>
    </div>
  </div>
</div>
-->

<!-- PREÇOS -->
<section id="secPrices" class="courseBackgroundColor" style="background-color: #ffffff">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="pricesTop featurePrice">
         <h3>QUAL VALOR DE TUDO ISSO PARA VOCÊ?</h3>
<h3>O Valor do Curso Methodus de Leitura Dinâmica e Memória é:</h3>
<h1><strong><del>R$2.390,00</del></strong></h1>
<h3><span style="color: #339966; display: inline; font-size: 32px;">Esta promoção especial que fazemos agora, é por tempo indeterminado – ou seja, não temos a data certa de quando ela sairá do ar</span>.</h3>
<h3>O curso, com toda a metodologia exclusiva Methodus e que vai ensinar você de verdade a sair lendo até 8x mais rápido, cabe no seu bolso:</h3>

   <a data-toggle="modal" data-target="#cursoVejaInvestimento" href="#">Quero ver investimentos e formas de pagamento!</a>

<br/>
<br/>
<h2 style="color: #339966; display: inline;">BÔNUS PERMANENTE:</h2>
<h3>RECICLAGEM PERMANENTE – VOCÊ PODE FAZER, DE GRAÇA, O CURSO QUANTAS VEZES QUISER NOVAMENTE.</h3>
<h3>ISSO NÃO TEM PREÇO!</h3>
<h2 style="color: #339966; display: inline;">BÔNUS EXTRA:</h2>
<h3>Para os inscritos no curso Presencial – um Bônus EXTRA mais que especial:</h3>
<h3>Mentoria de 1 hora presencial ou online com o Prof. Alcides Schotten.</h3>
<h3></h3>
<h3></h3>
        </div>
        <a data-toggle="modal" data-target="#cursoVejaInvestimento" href="#">Clique agora para garantir sua vaga!</a>
      </div>
    </div>
  </div>
</section>
<!-- proximas turmas -->
<section id="secPrices" class="courseBackgroundColor">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
    <span>Com um pequeno aumento no seu salário ou um novo emprego, isso sai de graça.</span>
        <div class="boxWorkload">
          <p><p>Carga horária: 10 aulas de 3 horas<br />
Participantes: limite de 15 por turma</p>
</p>
        </div>
        <h2>PRÓXIMAS TURMAS</h2>
                              <div class="col-md-6 col-sm-12">
              <div class="classesDate">
                <p>26/06/2021 à 01/08/2021</p>
                <small>(5 sáb. e 5 dom. - 14h às 17h)</small>
              </div>
            </div>
                      <div class="col-md-6 col-sm-12">
              <div class="classesDate">
                <p>27/05/2021 à 01/07/2021</p>
                <small>(10 noites – 3ª e 5ª – das 19h às 22h)</small>
              </div>
            </div>
            <!-- DATAS
                      <div class="col-md-6 col-sm-12">
              <div class="classesDate">
                <p> 11/01/2021 à 22/01/2021</p>
                <small>(10 tardes - 2ª à 6ª - 14h às 17h)</small>
              </div>
            </div>
                      <div class="col-md-6 col-sm-12">
              <div class="classesDate">
                <p>26/01/2021 à 05/02/2021</p>
                <small>(9 manhãs - 2ª à 6ª - 9h às 12h20)</small>
              </div>
            </div>
            -->
      </div>
    </div>
  <div class="row">
    <div class="col-md-12">
              <div>
              <a data-toggle="modal" data-target="#cursoVejaInvestimento" href="#">Clique aqui e mude seu futuro profissional!</a>
        </div>
    </div>
  </div>
  </div>
</section>

<section id="secTestimony">
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1 col-sm-12">
        <h2 class="courseColor">Veja mais depoimentos SUPER ATUAIS de quem fez o curso:</h2>
                  
            <div class="row sliderCard"  style="    max-height: 544px;">
                              <div class="col-md-12">
                  <div class="boxTestimony">
                    <img class="avatarImg" src="https://methodus.com.br/wp-content/uploads/2020/10/curso-de-leitura-dinamica-e-memoria-lp-7.jpg">
                    <h3 class="courseColor">Gabriela Ravelli Ratto Martins</h3>
                    <span>Bacharel em Psicologia</span>
                    <div class="testimonyText">
                      <svg class="quoteSlickStart" width="39" height="33" viewBox="0 0 39 33" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M9.0535 0.201172L14.3819 4.58474C11.8579 7.31229 10.1753 9.89371 9.33395 12.329C8.49262 14.6669 8.07196 17.0535 8.07196 19.4888L4.70664 17.0048H14.9428V32.2012H0.5V21.5345C0.5 17.5406 1.10763 13.8389 2.32288 10.4295C3.63161 6.92264 5.87515 3.5132 9.0535 0.201172ZM32.6107 0.201172L37.9391 4.58474C35.4151 7.31229 33.7325 9.89371 32.8911 12.329C32.0498 14.6669 31.6292 17.0535 31.6292 19.4888L28.2638 17.0048H38.5V32.2012H24.0572V21.5345C24.0572 17.5406 24.6648 13.8389 25.8801 10.4295C27.1888 6.92264 29.4324 3.5132 32.6107 0.201172Z"/>
                      </svg>
                      <p>O curso atendeu muito bem as minhas expectativas. Mesmo não tendo muito tempo para praticar em casa é dado de uma maneira que já nos faz treinar e conseguir atingir os resultados esperados. Meu dia a dia já não é mais o mesmo. Espero não parar e colocar a leitura dinâmica e memorização sempre como recurso para me auxiliar na rapidez desse mundo &#8220;novo&#8221;. Indico muito o curso. Para todas as idades e consigo enxergar até a aplicabilidade das técnicas em escolas, alunos se sentiriam mais motivados. A Methodus é realmente focada no aluno.</p>
                      <svg class="quoteSlickEnd" width="39" height="31" viewBox="0 0 39 31" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M29.9465 0.201172L24.6181 4.31076C27.1421 6.86784 28.8247 9.28793 29.6661 11.571C30.5074 13.7628 30.928 16.0003 30.928 18.2834L34.2934 15.9546H24.0572V30.2012H38.5V20.2012C38.5 16.4569 37.8924 12.9866 36.6771 9.79022C35.3684 6.50255 33.1248 3.3062 29.9465 0.201172ZM6.3893 0.201172L1.06088 4.31076C3.58487 6.86784 5.26752 9.28793 6.10885 11.571C6.95018 13.7628 7.37085 16.0003 7.37085 18.2834L10.7362 15.9546H0.5V30.2012H14.9428V20.2012C14.9428 16.4569 14.3352 12.9866 13.1199 9.79022C11.8112 6.50255 9.56765 3.3062 6.3893 0.201172Z"/>
                      </svg>
                    </div>
                  </div>
                </div>
                              <div class="col-md-12">
                  <div class="boxTestimony">
                    <img class="avatarImg" src="https://methodus.com.br/wp-content/uploads/2020/10/curso-de-leitura-dinamica-e-memoria-lp-8.jpg">
                    <h3 class="courseColor">Aline Fernanda Gindro Labanca</h3>
                    <span>Bacharel em Direito/Pós Difusos e Coletivos</span>
                    <div class="testimonyText">
                      <svg class="quoteSlickStart" width="39" height="33" viewBox="0 0 39 33" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M9.0535 0.201172L14.3819 4.58474C11.8579 7.31229 10.1753 9.89371 9.33395 12.329C8.49262 14.6669 8.07196 17.0535 8.07196 19.4888L4.70664 17.0048H14.9428V32.2012H0.5V21.5345C0.5 17.5406 1.10763 13.8389 2.32288 10.4295C3.63161 6.92264 5.87515 3.5132 9.0535 0.201172ZM32.6107 0.201172L37.9391 4.58474C35.4151 7.31229 33.7325 9.89371 32.8911 12.329C32.0498 14.6669 31.6292 17.0535 31.6292 19.4888L28.2638 17.0048H38.5V32.2012H24.0572V21.5345C24.0572 17.5406 24.6648 13.8389 25.8801 10.4295C27.1888 6.92264 29.4324 3.5132 32.6107 0.201172Z"/>
                      </svg>
                      <p>Sim, atingiu minhas expectativas. Eu aprendi a fazer mind maps; otimizar a leitura sem tentar decorar na primeira vez; o curso ainda me ajudou a entender que revisões são importantes e ainda que estudar é uma estratégia; o que fez eu ficar animada e motivada para estudar para concurso do Ministério Público; o curso ainda trouxe a importância de ter uma rotina disciplinada; é um curso muito completo e motivador.</p>
                      <svg class="quoteSlickEnd" width="39" height="31" viewBox="0 0 39 31" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M29.9465 0.201172L24.6181 4.31076C27.1421 6.86784 28.8247 9.28793 29.6661 11.571C30.5074 13.7628 30.928 16.0003 30.928 18.2834L34.2934 15.9546H24.0572V30.2012H38.5V20.2012C38.5 16.4569 37.8924 12.9866 36.6771 9.79022C35.3684 6.50255 33.1248 3.3062 29.9465 0.201172ZM6.3893 0.201172L1.06088 4.31076C3.58487 6.86784 5.26752 9.28793 6.10885 11.571C6.95018 13.7628 7.37085 16.0003 7.37085 18.2834L10.7362 15.9546H0.5V30.2012H14.9428V20.2012C14.9428 16.4569 14.3352 12.9866 13.1199 9.79022C11.8112 6.50255 9.56765 3.3062 6.3893 0.201172Z"/>
                      </svg>
                    </div>
                  </div>
                </div>
                              <div class="col-md-12">
                  <div class="boxTestimony">
                    <img class="avatarImg" src="https://methodus.com.br/wp-content/uploads/2020/10/curso-de-leitura-dinamica-e-memoria-lp-9.jpg">
                    <h3 class="courseColor">Ismael Vieira de Cristo Constantino</h3>
                    <span>Mestrado Direito Relacionamento Sociais</span>
                    <div class="testimonyText">
                      <svg class="quoteSlickStart" width="39" height="33" viewBox="0 0 39 33" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M9.0535 0.201172L14.3819 4.58474C11.8579 7.31229 10.1753 9.89371 9.33395 12.329C8.49262 14.6669 8.07196 17.0535 8.07196 19.4888L4.70664 17.0048H14.9428V32.2012H0.5V21.5345C0.5 17.5406 1.10763 13.8389 2.32288 10.4295C3.63161 6.92264 5.87515 3.5132 9.0535 0.201172ZM32.6107 0.201172L37.9391 4.58474C35.4151 7.31229 33.7325 9.89371 32.8911 12.329C32.0498 14.6669 31.6292 17.0535 31.6292 19.4888L28.2638 17.0048H38.5V32.2012H24.0572V21.5345C24.0572 17.5406 24.6648 13.8389 25.8801 10.4295C27.1888 6.92264 29.4324 3.5132 32.6107 0.201172Z"/>
                      </svg>
                      <p>O curso superou minhas expectativas porque se baseou em um contexto extremamente atual, com exemplos, teoria, exercícios e aplicações para situações atuais e futuras. Recomendo com ênfase o curso porque nossa formação acadêmica tradicional está totalmente desvinculada da atualidade e este curso é atualidade pura.</p>
                      <svg class="quoteSlickEnd" width="39" height="31" viewBox="0 0 39 31" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M29.9465 0.201172L24.6181 4.31076C27.1421 6.86784 28.8247 9.28793 29.6661 11.571C30.5074 13.7628 30.928 16.0003 30.928 18.2834L34.2934 15.9546H24.0572V30.2012H38.5V20.2012C38.5 16.4569 37.8924 12.9866 36.6771 9.79022C35.3684 6.50255 33.1248 3.3062 29.9465 0.201172ZM6.3893 0.201172L1.06088 4.31076C3.58487 6.86784 5.26752 9.28793 6.10885 11.571C6.95018 13.7628 7.37085 16.0003 7.37085 18.2834L10.7362 15.9546H0.5V30.2012H14.9428V20.2012C14.9428 16.4569 14.3352 12.9866 13.1199 9.79022C11.8112 6.50255 9.56765 3.3062 6.3893 0.201172Z"/>
                      </svg>
                    </div>
                  </div>
                </div>
                          </div>
          
              </div>
    </div>
  </div>
</section>

<section id="secFeaturedBanner" class="courseBackgroundColor">
  <div class="row">
    <div class="col-md-12">
      <div class="featuredList">
        <h2></h2>
              </div>
      <div class="featuredCta">
        <h2>AGORA, É A SUA VEZ! </h2>
        <h3>CLIQUE AQUI E GARANTA UMA DAS POUCAS VAGAS POR TURMA PRESENCIAL: </h3>
        <div id="formRegistration" class="row">
          <div class="col-md-8 col-md-offset-2">
            <form>
              <input type="hidden" name="curso" value="" />
              <div class="btt2linhas">
              <a data-toggle="modal" data-target="#cursoVejaInvestimento" href="#">
                Quero fazer o curso de Leitura Dinâmica e Memória da Methodus                </a>
                </div>
            </form>
          </div>
        </div>
              </div>
    </div>
  </div>
</section>



<!-- Modal  veja investimento -->
<div id="cursoVejaInvestimento" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <?php echo do_shortcode( '[contact-form-7 id="6301" title="VEJA INVESTIMENTOS E PRÓXIMOS GRUPOS! VERDE"]'); ?>

      </div>
    </div>

  </div>
</div>

<



  
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script> 
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>

<script type="text/javascript">
$(document).ready(function(){
  $('.sliderCard').slick({
    dots: false,
    slidesToShow: 1,
    infinite: false,
    autoplay: false,
    fade: true,
    arrows: true,
    prevArrow: "<img class='a-left control-c prev slick-prev' src='https://methodus.com.br/wp-content/themes/maiscode_methodus_novo/img/arrowPrev.png'>",
    nextArrow: "<img class='a-right control-c next slick-next' src='https://methodus.com.br/wp-content/themes/maiscode_methodus_novo/img/arrowNext.png'>",
    responsive: [
      {
        breakpoint: 780,
        settings: {
          slidesToShow: 1,
          arrows: false,
        }
      },        
    ]
  });
  $('.sliderCardDepo').slick({
    dots: false,
    slidesToShow: 1,
    infinite: true,
    autoplay: true,
    fade: true,
    arrows: true,
    prevArrow: "<img class='a-left control-c prev slick-prev depo-prev' src='https://methodus.com.br/wp-content/themes/maiscode_methodus_novo/img/arrowPrev.png'>",
    nextArrow: "<img class='a-right control-c next slick-next depo-next' src='https://methodus.com.br/wp-content/themes/maiscode_methodus_novo/img/arrowNext.png'>",
    responsive: [
      {
        breakpoint: 780,
        settings: {
          slidesToShow: 1,
          arrows: false,
        }
      },        
    ]
  });
 });


$(document).ready(function () {
  $("#coursePref").change(function () {
    var val = $(this).val();
    if (val == "Oratória") {
      $( ".oratory" ).show();
      $( ".memory" ).hide();
      $( ".time" ).hide();
    } else if (val == "Leitura Dinâmica / Memória") {
      $( ".memory" ).show();
      $( ".oratory" ).hide();
      $( ".time" ).hide();
    } else if (val == "Administração do Tempo") {
      $( ".time" ).show();
      $( ".oratory" ).hide();
      $( ".memory" ).hide();
    }
  });
});

// banner reminder
$(document).ready(function (){
  $("#reminder").click(function (){
      $('html, body').animate({
        scrollTop: $("#formRegistration").offset().top
      }, 1500);
  });
});
$(document).ready(function (){
  $("#advantage").click(function (){
      $('html, body').animate({
        scrollTop: $("#formRegistration").offset().top
      }, 1500);
  });
});
$(document).ready(function (){
  $("#price_a").click(function (){
      $('html, body').animate({
        scrollTop: $("#formRegistration").offset().top
      }, 1500);
  });
});
$(document).ready(function (){
  $("#price_b").click(function (){
      $('html, body').animate({
        scrollTop: $("#formRegistration").offset().top
      }, 1500);
  });
});
</script>
      


<?php get_footer(); ?>