<?php
/**
* Template Name: Leitura Dinâmica Online
* @package WordPress
* @author Mais Code Tecnologia
* @since First Version
*/

$url_site = get_option('link_site'); 
$url_api  =  get_option('link_api'); 
$url_amigavel_wp = home_url(); 
// URLs amigáveis
$url_amigavel_on  = 1;
$url_amigavel   =  get_option('link_site_amigavel');

$pagina_curso = 1;

$urlWP = get_template_directory_uri();
// $urlWP = site_url();
// $urlWP = 'https://www.methodus.com.br/';
  
// Verificar
$ID = get_field('id_do_curso');
$ID_Depoimento_Curso = get_field('id_depoimento_curso');
$ebookWp = get_field('ebook');
$xml_curso  = executaPagina( 'api/', array( 'a'=>'cursos', 'metodo'=>'ver', 'ID'=>$ID ) );
$curso      = lerXML( $xml_curso );
    

// var_dump($curso);
    
if ( $curso->erro == 0 ){
    
    $curso              = $curso->curso;
    $status             = $curso->status;
    $titulo_curso       = $curso->titulo;
    $url_curso          = $curso->url;
    $distancia_curso    = $curso->distancia;
    $banner_curso       = $curso->banner;
    $resumo_curso       = $curso->resumo;
    $descricao_curso    = $curso->descricao;
    $variaveis_curso    = $curso->variaveis; 
    $planos_curso       = $curso->planos;
    $turmas_curso       = $curso->turmas->turma;
    $perfis_curso       = $curso->perfis->perfil;
    $ID_tema            = $curso->tema;
    
    $seo_metatags       = $curso->metatags;
    $seo_title          = $titulo_curso.' - Methodus';
    $seo_imagem         = 'https://'.$_SERVER['HTTP_HOST'].''.$banner_curso;
    
    if ( isset( $variaveis_curso->classe ) ){
        $classe_curso = $variaveis_curso->classe;
    }else{
        $classe_curso = 'azul'; 
    }
    
    if ( isset( $curso->fotos ) ){
        $fotos_curso = $curso->fotos->foto;
    }

    if ( $status != 1 ){
        exit("Erro ao acessar os dados do curso 1");
    }
    
}else{
    exit("Erro ao acessar os dados do curso");      
}

?>


<?php get_header(); ?>

<style type="text/css">
    .caixa_ebook img{
    width: 170px;
  }
        .btn_top{
            color: #fff !important;
        }
        .btn_top:hover{
            background-color: #fff !important;
            color: #000080 !important;
        }
        .btn_pg_curso{
            border: 2px solid #000080 !important;;
        }
        .btn_pg_curso:hover{
            background-color: #fff !important;
            color: #000080 !important;
        }
        .btn_chamada:hover{
            color: #000080 !important;
        }
    </style><!-- TOPO -->

<style type="text/css">
  .courseColor {
    color: #000080;
  }
  .courseBackgroundColor {
    background-color: #000080
  }
  #secStoryModB .storyContent svg path {
    fill: #000080;
  }
  .sliderCard svg path {
    fill: #000080;
  }
  #secTopModB:before {
    content: '';
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    background: linear-gradient(270deg, rgba(59,89,152, 0.7) 23.16%, rgba(52, 50, 50, 0) 118.16%);
  }
  #secCallBannerModB .bgTransformation:before {
    content: '';
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    background: linear-gradient(179.64deg, rgba(96, 94, 94, 0) -36.93%, rgba(160, 0, 0, 0.6) 85.54%);
  }
  .loadd{
    display: none;
    opacity: .5;
    background: #000;
    width: 100%;
    position: absolute;
    top: 0px;
    height: 100vh;
    z-index: 10000000;
  }
 
  .sliderCardDepo .depo-prev
  {
    height: 36px;
    width: 220px;
    z-index: 100000;
    object-fit: contain;
  }
  .sliderCardDepo .depo-next{
    width: 235px;
    height: 36px;
    object-fit: contain;
  }
</style>



<section id="secTopModB" style="background-image: url('https://methodus.com.br/wp-content/uploads/2021/03/imagem-1.jpeg">
  <div class="container">
    <div class="row">
      <div class="col-md-7 col-md-offset-5 col-sm-7 col-sm-offset-5 topContent">
        <h2>Leitura Dinâmica 4.0 Online</h2>
        <p></p>
        <img src="https://methodus.com.br/wp-content/uploads/2020/08/curso-de-oratoria-lp-1.png">
      </div>
    </div>
  </div>
</section>

        <?php
    if ( isset( $variaveis_curso->ebook_download ) ){
        $temp_ebook = explode( '|', $variaveis_curso->ebook_download );

        if ( count( $temp_ebook ) >= 4 ){

            $titulo_ebook           = $temp_ebook[0];
            $descricao_ebook        = $temp_ebook[1];
            $destino_ebook          = $temp_ebook[2];
            $identificador_ebook    = $temp_ebook[3];
            $urlWP = get_template_directory_uri();
            ?>


            <section class="caixa_ebook ">
                <div class="container-fluid bg_ebook">
                    <div class="container">                       
                 
                        <script src="<?php echo $urlWP; ?>/js/scripts_site/ebook.js?ver=1"></script>
                        <form action="<?php echo $url_api; ?>/api/" method="post" class="form-email formAjax" id="form_ebook" data-validacao="checaEbook" data-resultado="sucessoEbook">
                            <input type="hidden" name="destino" value="<?php echo $ebookWp ?>" />
                            <input type="hidden" name="a" value="aluno" />
                            <input type="hidden" name="metodo" value="cadastro" />
                            <input type="hidden" name="origem" value="Ebook_<?php echo $identificador_ebook; ?>" />
                            <input type="hidden" name="curso" value="<?php echo $ID; ?>" />
                    
                            <div class="row">
                                <div class="col-xs-12 col-md-8">
                                    <img src="<?php echo $urlWP ?>/img/ebook_<?php echo $classe_curso; ?>.png" width="80" />
                                    <div class="chamada-ebook"><?php echo $descricao_ebook ?></div>
                                </div>
                                <div class="col-xs-12 col-md-4">
                                    <h5 class="mb0 inline-block p0-xs">
                                        <?php echo $titulo_ebook ?>
                                    </h5>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <input type="text" class="validate-required cada-input" name="nome" placeholder="Nome" lang="Nome">
                                        </div>
                                        <div class="col-sm-12 form-ebook">
                                            <input type="text" class="validate-required cada-input" name="email" placeholder="E-mail" lang="E-mail">
                                            <?php
                                            $i = 0;
                                            if( have_rows('formulario') ):
                                                while ( have_rows('formulario') ) : the_row(); $i++?> 
                                                    <div class="radio_cursos">  
                                                        <label for="ebook-<?php echo $i; ?>" class="radio">
                                                            <input type="radio" class="validate-required" id="ebook-<?php echo $i; ?>" name="conheceu" value="<?php the_sub_field('opcoes'); ?>" lang="opções">
                                                            <span></span>
                                                        </label>          
                                                        <label for="ebook-<?php echo $i; ?>"><?php the_sub_field('opcoes'); ?></label><br>
                                                    </div>
                                                <?php endwhile;
                                            endif;
                                            ?>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <button class="btn <?php echo $classe_curso; ?>" type="submit">Download</button>
                                        </div>
                                    </div>
                                </div>
                            </div>                            
                        </form>
                    </div>
                </div>
            </section>
        <?php
        }
    }
    ?>

<!-- VIDEO -->
<section id="secVideoModB">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h5 style="" class="courseColor">Com este método de <span style="text-color: #FF0000";>Leitura Dinâmica</span>, já são mais de 6 mil pessoas que leem</h5>
		<h5 class="courseColor">em um dia, os textos que você demora 1 semana.</h5>
        <h5 class="courseColor">Já imaginou quanto tempo livre a mais você teria?</h5>
            <h5 class="courseColor" style="text-transform:uppercase">Assista ao vídeo: Leitura silábica x leitura dinâmica</h5>
			<div class="col-12">
				<div class="" style="text-align:center">
				<div class="background-image-holder" style="background: url(&quot;https://methodus.com.br/wp-content/uploads/2020/08/curso-de-oratoria-lp-2.png&quot;); "> 
				
            </div>
            <video controls="">
               <source src="https://methodus.com.br/wp-content/uploads/2021/05/leitura_silabica_instagram.mp4" type="video/mp4">                                
              </video> 
              <div class="play-button" style="opacity: 0;"></div>
            </div>
            </div>
          </div>

            </div>      </div>
  </div>
</section> 
<!-- BANNER CHAMADA -->
<section id="secCallBannerModB" class="courseBackgroundColor">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2>Se você duvida, veja abaixo Depoimento do Paulo, ex-aluno da Methodus.</h2>
      </div>
    </div>
  </div>
    
</section>

<!-- HISTORIA -->
<section id="secStoryModB">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="storyContent">
              </div>
      </div>
  </div>
    <div class="row">
      <div class="col-md-12">
        <h2 class="courseColor">&#x25cf; Ele fez o curso de leitura dinâmica presencial. E hoje, em 2021, lançamos o curso de leitura dinâmica online – para fazer quando e onde quiser.
 &#x25cf;</h2>
   <div class="storyContent">
        <p>
          Tempo de Leitura deste texto: 5 minutos
        <br>
          Tempo para quem realizou o curso de Leitura Dinâmica Methodus: 1 minuto
    </div>
        <div class="storyContent">
          <svg class="quoteStart" width="71" height="56" viewBox="0 0 71 56" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M16.2565 0L26.072 7.67124C21.4225 12.4444 18.3229 16.962 16.7731 21.2237C15.2232 25.3151 14.4483 29.4916 14.4483 33.7534L8.24908 29.4064H27.1052V56H0.5V37.3333C0.5 30.344 1.61931 23.8661 3.85793 17.8996C6.26876 11.7626 10.4016 5.79605 16.2565 0ZM59.6513 0L69.4668 7.67124C64.8173 12.4444 61.7177 16.962 60.1679 21.2237C58.6181 25.3151 57.8432 29.4916 57.8432 33.7534L51.6439 29.4064H70.5V56H43.8948V37.3333C43.8948 30.344 45.0141 23.8661 47.2528 17.8996C49.6636 11.7626 53.7964 5.79605 59.6513 0Z"/>
          </svg>
                                    <p><span class="h3";>MUDEI A MINHA VIDA. NEM EU ACREDITEI!</span>

<br>

Foram 4 anos tentando passar num concurso público. O sonho da minha vida era ser promotor. Sim! 4 anos... Era muita matéria para ler. Conteúdo e mais conteúdo. E fora isso, a ansiedade pré-estudo. Só de PENSAR que teria que ler tanta coisa, já me desanimada, me frustrava. Eu pensava: nossa! Passarei a semana sem um segundo de folga. E já sofria por antecipação. E aí, a ansiedade gera mais ansiedade. E desconta na comida. E desconta com mau humor com a família. E o cérebro não descansa, aí não aprende com eficiência porque não absorve o que precisa. E foi aí que, meio relutante, resolvi fazer o Curso de Leitura Dinâmica da Methodus. Pensava se valia a pena o investimento. Pensava se funcionaria de verdade. Mas fui em frente! Já tinha tentado passar na prova 3 vezes e nada... eu estava apelando até para reza braba. E foi uma coisa incrível ENTENDER como o cérebro funciona e como ele tem um poder muito maior do que os limites que a gente aprende na escola.</p>

                              <div class="boxContrast">
                                      <span class="courseColor">Nosso cérebro tem o motor de um Fórmula 1, mas na escola eles só usam até a terceira marcha. E ficamos ali, presos, sem saber que podemos mais!</span>
                                   
                </div>
                                        
                                        <p><em><strong> </strong></em>Fiz o curso e tudo mudou.

&nbsp;

Primeiro porque eu comecei a ler quase 6 vezes mais rápido – e com compreensão. E aí, sobrava tempo para: ler tudo, não estar cansado para quando fosse estudar de novo, e poder ter momentos de lazer.

&nbsp;</p>

                              <div class="boxContrast">
                                    <span class="courseColor">E tudo isso, no final das contas, diminui muito a minha ansiedade.</span>
                </div>
				<p><em><strong> </strong></em>E ajudou com certeza absoluta a ter mais tranquilidade tanto para estudar, quanto na hora da prova.

&nbsp;

Eu recomendo a todo mundo, sem dúvida nenhuma!

&nbsp;</p>
<br>
<p>Paulo Ramires – Promotor * (nome fictício utilizado)</p>

                                              <svg class="quoteEnd" width="73" height="58" viewBox="0 0 73 58" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M56.2934 0L46.1974 7.94521C50.9797 12.8889 54.1679 17.5677 55.762 21.9817C57.3561 26.2192 58.1531 30.5449 58.1531 34.9589L64.5295 30.4566H45.1347V58H72.5V38.6667C72.5 31.4277 71.3487 24.7184 69.0461 18.5388C66.5664 12.1827 62.3155 6.00305 56.2934 0ZM11.6587 0L1.56272 7.94521C6.34501 12.8889 9.5332 17.5677 11.1273 21.9817C12.7214 26.2192 13.5184 30.5449 13.5184 34.9589L19.8948 30.4566H0.5V58H27.8653V38.6667C27.8653 31.4277 26.714 24.7184 24.4114 18.5388C21.9317 12.1827 17.6808 6.00305 11.6587 0Z"/>
          </svg>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- POR QUE A METHODUS -->

<section id="secLogistic">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2 class="courseColor">DESCUBRA SE O CURSO É PARA VOCÊ</h2>
        <h3 class="courseColor">&#x25cf; O Curso de Leitura Dinâmica e Memória Methodus é perfeito para quem: &#x25cf;</h3>
        <div class="boxLogistic">
                      <ul>
                              <li><span>Possui Leitura Lenta e PRINCIPALMENTE Dispersiva (ou seja, você começa a ler e pronto, já tá “viajando” pensando em outra coisa. O foco e velocidade da Leitura Dinâmica não deixam a mente sair dali e nem “viajar”. É impressionante!</span></li>
                              <li><span>É para quem tem falta de foco ao ler ou estudar – você aprenderá técnicas de como estudar e focar.</span></li>
                              <li><span>É para que tem um monte de matéria e informação para estudar (seja estudante ou no trabalho) e não sabe por onde começar.</span></li>
                              <li><span>É Profissional e que identifica a necessidade de retomar estudos e quer fazer uma pós, especializações, doutorado, MBA ou simplesmente retomar o hábito de ler e ler rápido com compreensão e retenção. </span></li>
                              <li><span>Deseja passar em Concursos Públicos, mesmo trabalhando enquanto estuda.</span></li>
                              <li><span>Estudantes do 3ºano do Ensino Médio, já realizando cursinhos para Vestibular.</span></li>
							  <li><span>Estudantes de Medicina e Residentes que têm um volume muito grande para reter.</span></li>
                          </ul>
                  </div>
		<h2 style="padding: 35px; !important" class="courseColor">E temos uma pergunta: <br>Por que não pode ser você?</h2>
		<h3 class="courseColor">É claro que pode. <span>A habilidade da leitura dinâmica vem com o treinamento.</span> <br>E é possível a qualquer pessoa.</h3>
		<div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
		        <img class="align-center wp-image-6362" src="https://methodus.com.br/wp-content/uploads/2021/01/mulher_lendo.jpeg" alt="" width="auto" height="100%" />
      </div>
	  </div>
	  </div>
	  </div>
	  
    </div>
	
  </div>
</section>
                                            </div>

<!-- VANTAGENS -->
<section id="secAdvantages">
  <div class="bannerSatisfaction courseBackgroundColor">
    <h2>AGORA VEJA A SATISFAÇÃO REAL DE QUEM JÁ FEZ O NOSSO CURSO:</h2>
    <h3>FUNCIONA PARA TODOS OS TIPOS DE PESSOAS!</h3>
  </div>
</section>

<section id="secLogistic">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2 class="courseColor" style="padding-top:0">Como funciona o Treinamento</h2>
		<br>
        <div class="boxLogistic">
						<ul>
                              <li><span>Aspecto Físico <br>Globos Oculares – Treinamento de agilidade, captação, campo de visão e percepção.</span></li>
                              <li><span>Aspecto Mental <br>Técnicas Exclusivas Methodus que unem a expansão do lado direito do cérebro com a capacidade de foco e concentração (inclusive com técnicas de relaxamento do cérebro para maior retenção de informações).</span></li>
                        </ul>
					<div class="boxContrast">
                                      <span class="courseColor">Para entender estes diferenciais, que chamamos de Método Methodus, primeiro é preciso que você conheça quem é o Prof. Alcides Schotten, professor, filósofo, pedagogo e criador e desenvolvedor do método.</span>
                                   
					</div>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- POR QUE A METHODUS? -->
<section id="secStoryModB">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2 class="courseColor">&#x25cf; Por que a Methodus funciona <br>melhor para você: &#x25cf;</h2>
   <div class="storyContent">
        <p>Para você entender quais os diferencias reais, vamos falar primeiro sobre o criador da Methodus, o professor Alcides Schotten.
        <br>Alcides Schotten é PROFESSOR E FILÓSOFO, ou seja, ele vai além do básico e aprofunda-se realmente na mente humana.</p>
		<br><p>Estudioso por natureza, aprimora-se todos os anos, buscando na neurociência, o entendimento de como o cérebro funciona e reage. E assim, usa toda sua base e conhecimento de professor e filósofo, para criar os estímulos perfeitos para que o cérebro aprenda de verdade. Não é simplesmente aprender por uns dias. A metodologia é feita para ficar. E de maneira fácil.</p>
		<br>
    </div>
	<div class="container">
	<div class="row">
	<div class="col-sm-12 col-md-10 col-md-offset-1">
	<img class="wp-image-6367" style="" src="https://methodus.com.br/wp-content/uploads/2019/11/curso-de-administracao-do-tempo-8-1024x682.jpg" alt="" width="auto" height="100%" />
	</div></div></div>
        <div class="storyContent">
		<br>
		<br>
                                    <h2>NOSSA MOTIVAÇÃO:</h2>
									<p>
<br>
O Alcides Schotten, fundador da Methodus, sempre foi professor, sempre foi um mestre, e por isso, ensinar é a motivação de vida que o move a querer sempre ver seus alunos conquistando seus sonhos! E isso é verdade. Basta ver a felicidade estampada no rosto de quem completa os cursos.</p>
								<div class="boxContrast">
                                      <span class="courseColor">Como fazemos para você aprender? A gente explica!</span>                                  
								</div>
                                        
                                        <p><em><strong>METODOLOGIA: TEM QUE SER FÁCIL.</strong></em> &nbsp;

<p><em><strong>ALGUMA COISA DIFÍCIL DE APRENDER NA ESCOLA VOCÊ LEMBRA ATÉ HOJE? NÃO, NÉ?</strong></em>
&nbsp;</p>
<br>
				<p><em><strong></strong></em>Por isso, a gente sempre pensa em jeitos fáceis de você aprender. Porque senão, você esquece. O método utilizado nos cursos e treinamentos oferecidos pela Methodus foi desenvolvido pelo professor e filósofo Alcides Schotten, que procurou em todo o conhecimento humano – desde Platão e Aristóteles, na Grécia Antiga, até os pensadores mais modernos – um ponto de equilíbrio e o segredo do aperfeiçoamento de talentos naturais.
<p>Através do método usado nos cursos da Methodus, o aluno desenvolverá de forma prática, gradual e relativa às suas necessidades, habilidades de otimização pessoal. Ou seja, não é uma fórmula que pode funcionar para um e não para o outro. Existe uma fórmula e dosagem certa para você.</p>
<p>Por isso é fácil e duradouro.</p>
&nbsp;</p>
<br>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- COMO FUNCIONA O CURSO -->
<section id="secLogistic">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2 class="courseColor" style="padding-top:0 !important">E COMO FUNCIONA O CURSO?</h2>
		<br>
        <div class="boxLogistic">
                      <ul>
                              <li><span>MÓDULO BÁSICO – Mudando os Paradigmas da Leitura Silábica/Fonética.<br>
Entender os fundamentos teóricos científicos apoiados pelas novas descobertas da Neurociência sobre a capacidade da mente ao identificar, entender e reter as informações. Prática de 13 exercícios para ensinar a mente a fazer a mudança dos paradigmas da leitura silábica/fonética para os novos paradigmas da leitura dinâmica. Agilidade nos movimentos oculares e treino para sensibilizar a nitidez de captação do campo visual central e periférico.
</span></li>
                              <li><span>MÓDULO INTERMEDIÁRIO – Sedimentando os Paradigmas da Leitura Dinâmica.<br>
Aplicando os novos paradigmas da leitura nos textos. A leitura silábica/fonética divide a palavra em sílabas, sonoriza as sílabas, juntas os sons e forma as palavras; usa portanto o hemisfério esquerdo para ler, cuja função primordial é dividir para entender; com os novos paradigmas da leitura dinâmica você identifica raciocínios, exatamente como faz quando ouve um texto. 
</span></li>
                              <li><span>MÓDULO AVANÇADO – Automatizando os Paradigmas da Leitura Dinâmica.<br>
Os exercícios desse módulo tornam possível a você ler um livro de 300 páginas em 3 horas. Sua velocidade média de leitura será de 800 palavras por minuto com uma compreensão de 80%. Portanto 5 a 8 vezes com mais qualidade em relação ao teste inicial realizado ao iniciar o curso, cuja velocidade padrão é 110 palavras por minuto com uma compreensão em torno de 60%. A prática dos exercícios propostos nesse módulo automatizam os novos paradigmas da leitura dinâmica, isto é, você não faz mais esforço para ler dinamicamente.
</span></li>
                              <li><span>Testes de Velocidade de Leitura
                              <li><span>Treinos de Aumento de Campo de Visão</span></li>
                              <li><span>Treinos de Velocidade de Observação</span></li>
							  <li><span>Treinos de Compreensão de Textos</span></li>
							  <li><span>Treinos de Agilidade Ocular</span></li>
                          </ul>
		<h2 class="courseColor" style="padding-top:0 !important">E O QUE VOU CONSEGUIR FAZENDO O CURSO?</h2><br>
						<ul>
                              <li><span>Ler de 5 a 8 vezes mais rápido.
                              <li><span>Ter compreensão do que leu 5 a 8 vezes mais rápido – baseado em testes reais feitos por você mesmo para comparar sua velocidade no início e no final.</span></li>
                              <li><span>Acabar com a Frustração de não conseguir terminar os livros.</span></li>
							  <li><span>Acabar com o medo de achar que não dará tempo de ler o que você precisa.</span></li>
							  <li><span>Entender a metodologia e como o cérebro aprende – para entender que sim é metodologia científica e que sim, funciona de verdade!</span></li>
                          </ul>
						  
                  </div>
      </div>
    </div>
  </div>
						<section id="secBannerAdvantages">
							<div class="container">
								<div class="row">
									<div class="col-md-12">
										<div class="advantagesBox">
											<a data-toggle="modal" data-target="#cursoVejaInvestimento" href=#>CLIQUE AQUI E INSCREVA-SE AGORA!</a>
										</div>
									</div>
								</div>
							</div>
						</section>
</section>

<!-- depoimentos -->

<!-- VANTAGENS
<section id="secAdvantages">
  <div class="bannerSatisfaction courseBackgroundColor">
    <h2>AGORA VEJA A SATISFAÇÃO REAL DE QUEM JÁ FEZ O NOSSO CURSO!</h2>
  </div>
</section>
 -->

<section id="secTestimony" style="background-color:#F2F2F2;">
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1 col-sm-12">
        <h2 class="courseColor" style="padding-top:0 !important">AGORA VEJA A SATISFAÇÃO REAL DE QUEM JÁ FEZ O NOSSO CURSO!</h2>
                  
            <div class="row sliderCard">
                <div class="col-md-12">
                  <div class="boxTestimony">
                    <img class="avatarImg" src="https://methodus.com.br/wp-content/uploads/2021/04/luciana.jpg">
                    <h3 class="courseColor">Luciana Patrícia Rodrigues</h3>
                    <span>Bacharel em Adminisrtação de Empresas, Autônoma</span>
                    <div class="testimonyText">
                      <svg class="quoteSlickStart" width="39" height="33" viewBox="0 0 39 33" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M9.0535 0.201172L14.3819 4.58474C11.8579 7.31229 10.1753 9.89371 9.33395 12.329C8.49262 14.6669 8.07196 17.0535 8.07196 19.4888L4.70664 17.0048H14.9428V32.2012H0.5V21.5345C0.5 17.5406 1.10763 13.8389 2.32288 10.4295C3.63161 6.92264 5.87515 3.5132 9.0535 0.201172ZM32.6107 0.201172L37.9391 4.58474C35.4151 7.31229 33.7325 9.89371 32.8911 12.329C32.0498 14.6669 31.6292 17.0535 31.6292 19.4888L28.2638 17.0048H38.5V32.2012H24.0572V21.5345C24.0572 17.5406 24.6648 13.8389 25.8801 10.4295C27.1888 6.92264 29.4324 3.5132 32.6107 0.201172Z"/>
                      </svg>
                      <p><strong>O Curso superou minhas expectativas.</strong><br />
						O curso surpreendeu minhas expectativas, pois buscava aprimorar minha leitura devido minha necessidade de absorver novos conteúdos. o curso de leitura dinâmica foi além trazendo novos conhecimentos em estratégia de estudo, memória, concentração, relaxamento e desenvolvendo a habilidade de ser autodidata. Esses novos aprendizados com toda certeza me ajudará a elaborar estratégias adequadas a minha realidade cotidiana.</p>
                      <svg class="quoteSlickEnd" width="39" height="31" viewBox="0 0 39 31" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M29.9465 0.201172L24.6181 4.31076C27.1421 6.86784 28.8247 9.28793 29.6661 11.571C30.5074 13.7628 30.928 16.0003 30.928 18.2834L34.2934 15.9546H24.0572V30.2012H38.5V20.2012C38.5 16.4569 37.8924 12.9866 36.6771 9.79022C35.3684 6.50255 33.1248 3.3062 29.9465 0.201172ZM6.3893 0.201172L1.06088 4.31076C3.58487 6.86784 5.26752 9.28793 6.10885 11.571C6.95018 13.7628 7.37085 16.0003 7.37085 18.2834L10.7362 15.9546H0.5V30.2012H14.9428V20.2012C14.9428 16.4569 14.3352 12.9866 13.1199 9.79022C11.8112 6.50255 9.56765 3.3062 6.3893 0.201172Z"/>
                      </svg>
                    </div>
                  </div>
                </div>
                              <div class="col-md-12">
                  <div class="boxTestimony">
                    <img class="avatarImg" src="https://methodus.com.br/wp-content/uploads/2021/04/angelo.jpg">
                    <h3 class="courseColor">Angelo Carisio Nasciutti</h3>
                    <span>Tecnico em Administração/MBA Gestão executiva, Atex do Brasil</span>
                    <div class="testimonyText">
                      <svg class="quoteSlickStart" width="39" height="33" viewBox="0 0 39 33" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M9.0535 0.201172L14.3819 4.58474C11.8579 7.31229 10.1753 9.89371 9.33395 12.329C8.49262 14.6669 8.07196 17.0535 8.07196 19.4888L4.70664 17.0048H14.9428V32.2012H0.5V21.5345C0.5 17.5406 1.10763 13.8389 2.32288 10.4295C3.63161 6.92264 5.87515 3.5132 9.0535 0.201172ZM32.6107 0.201172L37.9391 4.58474C35.4151 7.31229 33.7325 9.89371 32.8911 12.329C32.0498 14.6669 31.6292 17.0535 31.6292 19.4888L28.2638 17.0048H38.5V32.2012H24.0572V21.5345C24.0572 17.5406 24.6648 13.8389 25.8801 10.4295C27.1888 6.92264 29.4324 3.5132 32.6107 0.201172Z"/>
                      </svg>
                      <p><strong>O Curso entrega o que promete.</strong><br />
						Aumentei minha velocidade de leitura sem perder qualidade de registro. Superou as expectativas pois as técnicas de estudo e memorização potencializaram meu desempenho profissional; também sou treinador e me ajudaram a estruturar o meu conhecimento pra replicação aos meus liderados. Sim, recomendo o curso porque ele entrega o que promete.</p>
                      <svg class="quoteSlickEnd" width="39" height="31" viewBox="0 0 39 31" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M29.9465 0.201172L24.6181 4.31076C27.1421 6.86784 28.8247 9.28793 29.6661 11.571C30.5074 13.7628 30.928 16.0003 30.928 18.2834L34.2934 15.9546H24.0572V30.2012H38.5V20.2012C38.5 16.4569 37.8924 12.9866 36.6771 9.79022C35.3684 6.50255 33.1248 3.3062 29.9465 0.201172ZM6.3893 0.201172L1.06088 4.31076C3.58487 6.86784 5.26752 9.28793 6.10885 11.571C6.95018 13.7628 7.37085 16.0003 7.37085 18.2834L10.7362 15.9546H0.5V30.2012H14.9428V20.2012C14.9428 16.4569 14.3352 12.9866 13.1199 9.79022C11.8112 6.50255 9.56765 3.3062 6.3893 0.201172Z"/>
                      </svg>
                    </div>
                  </div>
                </div>
                          </div>
              </div>
    </div>
  </div>
</section>

<section id="secAdvantages">
  <div class="bannerSatisfaction courseBackgroundColor">
    <h2>FUNCIONA PARA TODOS OS TIPOS DE PESSOAS!</h2>
	<h3>ASSIM COMO TODOS APRENDERAM A LER, TODOS PODEM APRENDER A LER DINAMICAMENTE!</h3>
	<h2>SÓ DEPENDE DE VOCÊ.</h2>
  </div>
</section>
<!-- POR QUE A METHODUS? -->
<section id="secStoryModB" style="padding-top:0;">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2 class="courseColor" style="padding-top:0;">&#x25cf; MAS, E QUAL O VALOR DO CURSO? &#x25cf;</h2>
			<div class="storyContent">
				<p>O Curso de Leitura Dinâmica Methodus possui inúmeros diferenciais que são exclusivos e você não encontra em outros cursos. Por isso nós nem comparamos com outros cursos.
				<br>E por isso não chamamos de investimento nem custo, mas sim de VALOR, pois você passa a valer mais após o curso.
				<br>Nós entendemos que você vai ter um desenvolvimento intelectual muito acima da média.
				<br>E por isso, definimos nosso VALOR baseado em POTENCIAL e SATISFAÇÃO!</p>
			</div>
      </div>
    </div>
  </div>
</section>

<section id="secLogistic">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
	        <h2 class="courseColor" style="padding-top:0 !important">E COMO FUNCIONA O CURSO?</h2>
			<br>
        <div class="boxLogistic">
                      <ul>
                              <span><li>Potencial de conseguir um emprego novo</li></span>
                              <span><li>Potencial de conseguir uma promoção no trabalho e ganhar muito mais</li></span>
                              <span><li>Potencial de conseguir passar no vestibular que você quer</li></span>
							  <span><li>Potencial de conseguir passar no concurso que você sonha</li></span>
							  <span><li>Potencial de conseguir a melhor residência médica</li></span>
							  <span><li>Potencial de concluir a pós-graduação que você deseja</li></span>
                              <span><li>Satisfação de terminar os livros que começou e parou</li></span>
                              <span><li>Satisfação de ler mais livros por ano (5 a 8 vezes mais)</li></span>
							  <span><li>Satisfação de ler tudo que precisa da sua faculdade, pós ou emprego em um dia, o que levaria uma semana</li></span>
							  <span><li>Satisfação de passar no concurso ou vestibular ou pós e orgulhar a família</li></span>
							  <span><li>Satisfação pessoal de se desenvolver intelectualmente</li></span>
                              <span><li>Satisfação de ter um Certificado de Conclusão que pode ser colocado no currículo</li></span>
                              <span><li>Satisfação de poder refazer o curso novamente para treinar, de graça, para sempre!</li></span>
                          </ul>
        </div>
      </div>
    </div>
  </div>
</section>

<!--
<section id="secLogistic">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2 class="courseColor">LOGÍSTICA DAS AULAS</h2>
        <h3 class="courseColor">&#x25cf; O MÉTODO METHODUS, QUE É O DIFERENCIAL &#x25cf;</h3>
        <div class="boxLogistic">
                      <ul>
                              <li><span>Apenas 10 a 15 participantes por grupo.</span></li>
                              <li><span>Em todas as aulas os participantes falarão pelo menos duas vezes.</span></li>
                              <li><span>Apresentações filmadas e analisadas com o professor.</span></li>
                              <li><span>Professor preparado para atender participantes em suas dúvidas.</span></li>
                              <li><span>Manual contendo a teoria abordada.</span></li>
                              <li><span>Gravações das suas apresentações para você baixar.</span></li>
                          </ul>
                    <h3 class="courseColor">OK, MAS E QUAL O VALOR DO INVESTIMENTO?</h3>
                      <div class="boxInvestiment">
              <ul>
                                  <li><span>Quanto vale colocar isso no seu currículo?</span></li>
                                  <li><span>Quanto vale ser promovido (quanto você ganhará a mais de salário)?</span></li>
                                  <li><span>Quanto vale conseguir ir bem numa entrevista de emprego?</span></li>
                                  <li><span>Quanto custam todas as mensalidades de uma pós-graduação (que demora e pode não ser decisiva para ser promovido)?</span></li>
                                  <li><span>Qual o preço de fazer outro curso e não ficar satisfeito (jogar dinheiro fora)?</span></li>								 
                              </ul>
					        <a data-toggle="modal" data-target="#cursoVejaInvestimento" href="#">CLIQUE AQUI E INSCREVA-SE AGORA!</a>
            </div>
                  </div>
      </div>
    </div>
  </div>
</section>
-->



<!-- Modal Cupom 
<div class="modal fade" id="cupomModal" tabindex="-1" role="dialog" aria-labelledby="cupomModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div role="form" class="wpcf7" id="wpcf7-f6273-o1" lang="pt-BR" dir="ltr">
<div class="screen-reader-response"><p role="status" aria-live="polite" aria-atomic="true"></p> <ul></ul></div>
<form action="/cursos/curso-de-oratoria/#wpcf7-f6273-o1" method="post" class="wpcf7-form init" novalidate="novalidate" data-status="init">
<div style="display: none;">
<input type="hidden" name="_wpcf7" value="6273" />
<input type="hidden" name="_wpcf7_version" value="5.3" />
<input type="hidden" name="_wpcf7_locale" value="pt_BR" />
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f6273-o1" />
<input type="hidden" name="_wpcf7_container_post" value="0" />
<input type="hidden" name="_wpcf7_posted_data_hash" value="" />
</div>
<div class="box-contato" id="investir">
<h2 class="titulo-box vermelho">Preencha abaixo e receba um cupom de desconto de 10% no seu e-mail!</h2>
<div class="interno-box">
<div class="inputCupomForm">
<span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Primeiro nome" /></span><br />
<span class="wpcf7-form-control-wrap email"><input type="email" name="email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="E-mail" /></span>
</div>
<p><label>Curso de Preferência</label><br />
<span class="wpcf7-form-control-wrap menu-140"><select name="menu-140" class="wpcf7-form-control wpcf7-select" id="coursePref" aria-invalid="false"><option value="">---</option><option value="Administração do Tempo">Administração do Tempo</option><option value="Leitura Dinâmica / Memória">Leitura Dinâmica / Memória</option><option value="Administração do Tempo">Administração do Tempo</option></select></span></p>
<div class="oratory radioCupom" style="display: none">
<p>Eu sou: Administração do Tempo</p>
<p><span class="wpcf7-form-control-wrap radio-972"><span class="wpcf7-form-control wpcf7-radio"><span class="wpcf7-list-item first"><input type="radio" name="radio-972" value="Sou profissional / apresentações / treinamentos" /><span class="wpcf7-list-item-label">Sou profissional / apresentações / treinamentos</span></span><span class="wpcf7-list-item"><input type="radio" name="radio-972" value="Sou profissional vendas / líder religioso / político / vídeos" /><span class="wpcf7-list-item-label">Sou profissional vendas / líder religioso / político / vídeos</span></span><span class="wpcf7-list-item"><input type="radio" name="radio-972" value="Sou concursando e me preparo para prova oral" /><span class="wpcf7-list-item-label">Sou concursando e me preparo para prova oral</span></span><span class="wpcf7-list-item last"><input type="radio" name="radio-972" value="Sou universitário / apresento seminários / processo seletivo" /><span class="wpcf7-list-item-label">Sou universitário / apresento seminários / processo seletivo</span></span></span></span>
</p></div>
<div class="memory radioCupom" style="display: none">
<p>Eu sou: Leitura Dinâmica / Memória</p>
<p><span class="wpcf7-form-control-wrap radio-429"><span class="wpcf7-form-control wpcf7-radio"><span class="wpcf7-list-item first"><input type="radio" name="radio-429" value="Vestibular / concursos / ensino médio" /><span class="wpcf7-list-item-label">Vestibular / concursos / ensino médio</span></span><span class="wpcf7-list-item"><input type="radio" name="radio-429" value="Cursando ensino superior / estagiário / trainee" /><span class="wpcf7-list-item-label">Cursando ensino superior / estagiário / trainee</span></span><span class="wpcf7-list-item last"><input type="radio" name="radio-429" value="Sou profissional / faço pós e especializações" /><span class="wpcf7-list-item-label">Sou profissional / faço pós e especializações</span></span></span></span>
</p></div>
<div class="time radioCupom" style="display: none">
<p>Eu sou: Administração do Tempo</p>
<p><span class="wpcf7-form-control-wrap radio-429"><span class="wpcf7-form-control wpcf7-radio"><span class="wpcf7-list-item first"><input type="radio" name="radio-429" value="Sou empreendedor / tenho sonhos a realizar" /><span class="wpcf7-list-item-label">Sou empreendedor / tenho sonhos a realizar</span></span><span class="wpcf7-list-item"><input type="radio" name="radio-429" value="Sou profissional formado / preciso administrar o tempo / procrastino" /><span class="wpcf7-list-item-label">Sou profissional formado / preciso administrar o tempo / procrastino</span></span><span class="wpcf7-list-item last"><input type="radio" name="radio-429" value="Estou empregado / não tenho curso superior / preciso me organizar" /><span class="wpcf7-list-item-label">Estou empregado / não tenho curso superior / preciso me organizar</span></span></span></span>
</p></div>
<p><input type="submit" value="Enviar" class="wpcf7-form-control wpcf7-submit" />
</p></div>
</div>
</div>
<div class="wpcf7-response-output" aria-hidden="true"></div></form></div>      </div>
    </div>
  </div>
</div>
-->
<!-- PREÇOS -->
<section id="secPrices" class="courseBackgroundColor" style="background-color: #ffffff">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="pricesTop featurePrice">
         <h3>QUAL VALOR DE TUDO ISSO PARA VOCÊ?</h3>
<h3>O Valor do Curso Methodus de Leitura Dinâmica e Memória é:</h3>
<h1><strong><del>R$2.390,00</del></strong></h1>
<h3><span style="color: #339966; display: inline; font-size: 32px;">Profissional – deixa de ganhar uma promoção para ganhar 22 vezes mais de salário. Quantos mil reais a mais por ano você ganharia?</span>.</h3>
<h3>E valor de um ano de cursinho para quem é estudante:</h3>
<h1><strong><del>R$ 19.750,00</del></strong></h1>
<h3>Esta promoção especial que fazemos agora, é por tempo indeterminado – ou seja, não temos a data certa de quando ela sairá do ar.</h3>
<h1><strong>Sem custo de deslocamento – na hora que você puder fazer</strong></h1>
<h3>O curso Leitura Dinâmica Online, com toda a metodologia exclusiva Methodus e que vai ensinar você de verdade a sair lendo até 8x mais rápido, sai por apenas:</h3>

<br>
   <a data-toggle="modal" data-target="#cursoVejaInvestimento" href=#>Clique aqui para ver o investimento</a>

<br/>
<br/>
<h2 style="color: #339966; display: inline;">BÔNUS PERMANENTE:</h2>
<h3>ACESSO VITALÍCIO AO CURSO!</h3>
<h3>ISSO NÃO TEM PREÇO!</h3>
<h2 style="color: #339966; display: inline;">BÔNUS EXTRA - Por tempo indeterminado:</h2>
<h3>(Não sabemos até quando este Bônus ficará no ar)</h3>
<h3>Mentoria Individual com criador e idealizador do Curso Prof. Alcides Schotten.</h3>
		<div class="boxWorkload">
			<h3 style="color: #339966;">GARANTIA 7 DIAS</h3>
			<h5>VOCÊ PODE COMPRAR O CURSO E SE NÃO GOSTAR, DEVOLVEMOS INTEGRALMENTE O VALOR EM 7 DIAS! SEM PERGUNTAS OU MOTIVOS!</h5>
        </div
<br>
   <a data-toggle="modal" data-target="#cursoVejaInvestimento" href=#>Clique aqui agora para começar a ler 5 a 8 x mais rápido!</a>

        </div>
      </div>
    </div>
  </div>
</section>

<section id="secTestimony">
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1 col-sm-12">
        <h2 class="courseColor" style="margin-top:0 !important">Veja mais depoimentos SUPER ATUAIS de quem fez o curso:</h2>
                  
            <div class="row sliderCard">
                              <div class="col-md-12">
                  <div class="boxTestimony">
                    <img class="avatarImg" src="https://methodus.com.br/wp-content/uploads/2021/04/thais.jpg">
                    <h3 class="courseColor">Thais Cristine Santo</h3>
                    <span>Bacharel em Administração de Empresas, Banco Votorantim</span>
                    <div class="testimonyText">
                      <svg class="quoteSlickStart" width="39" height="33" viewBox="0 0 39 33" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M9.0535 0.201172L14.3819 4.58474C11.8579 7.31229 10.1753 9.89371 9.33395 12.329C8.49262 14.6669 8.07196 17.0535 8.07196 19.4888L4.70664 17.0048H14.9428V32.2012H0.5V21.5345C0.5 17.5406 1.10763 13.8389 2.32288 10.4295C3.63161 6.92264 5.87515 3.5132 9.0535 0.201172ZM32.6107 0.201172L37.9391 4.58474C35.4151 7.31229 33.7325 9.89371 32.8911 12.329C32.0498 14.6669 31.6292 17.0535 31.6292 19.4888L28.2638 17.0048H38.5V32.2012H24.0572V21.5345C24.0572 17.5406 24.6648 13.8389 25.8801 10.4295C27.1888 6.92264 29.4324 3.5132 32.6107 0.201172Z"/>
                      </svg>
                      <p><strong>Qualidade na absorção e retenção ao ler.</strong><br />
Qualidade no entendimento e possibilidades de estudar de 
maneira autônoma. Ampliação na capacidade de leitura e 
absorção/retenção foram essenciais para mim. Todos somos 
capazes de nos auto-aprimorar, gerir e nos desenvolver, porém 
é fundamental a disciplina, envolvimento, foco e determinação.</p>
                      <svg class="quoteSlickEnd" width="39" height="31" viewBox="0 0 39 31" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M29.9465 0.201172L24.6181 4.31076C27.1421 6.86784 28.8247 9.28793 29.6661 11.571C30.5074 13.7628 30.928 16.0003 30.928 18.2834L34.2934 15.9546H24.0572V30.2012H38.5V20.2012C38.5 16.4569 37.8924 12.9866 36.6771 9.79022C35.3684 6.50255 33.1248 3.3062 29.9465 0.201172ZM6.3893 0.201172L1.06088 4.31076C3.58487 6.86784 5.26752 9.28793 6.10885 11.571C6.95018 13.7628 7.37085 16.0003 7.37085 18.2834L10.7362 15.9546H0.5V30.2012H14.9428V20.2012C14.9428 16.4569 14.3352 12.9866 13.1199 9.79022C11.8112 6.50255 9.56765 3.3062 6.3893 0.201172Z"/>
                      </svg>
                    </div>
                  </div>
                </div>
                              <div class="col-md-12">
                  <div class="boxTestimony">
                    <img class="avatarImg" src="https://methodus.com.br/wp-content/uploads/2021/04/ismael.jpg">
                    <h3 class="courseColor">Ismael Vieira de Cristo Constantino</h3>
                    <span>Bacharel e Mestrado em Direito; Relacionamentos Sociais, Autônomo</span>
                    <div class="testimonyText">
                      <svg class="quoteSlickStart" width="39" height="33" viewBox="0 0 39 33" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M9.0535 0.201172L14.3819 4.58474C11.8579 7.31229 10.1753 9.89371 9.33395 12.329C8.49262 14.6669 8.07196 17.0535 8.07196 19.4888L4.70664 17.0048H14.9428V32.2012H0.5V21.5345C0.5 17.5406 1.10763 13.8389 2.32288 10.4295C3.63161 6.92264 5.87515 3.5132 9.0535 0.201172ZM32.6107 0.201172L37.9391 4.58474C35.4151 7.31229 33.7325 9.89371 32.8911 12.329C32.0498 14.6669 31.6292 17.0535 31.6292 19.4888L28.2638 17.0048H38.5V32.2012H24.0572V21.5345C24.0572 17.5406 24.6648 13.8389 25.8801 10.4295C27.1888 6.92264 29.4324 3.5132 32.6107 0.201172Z"/>
                      </svg>
                      <p><strong>Agilidade mental e leitura rápida!</strong><br />
O curso superou minhas expectativas porque se baseou em um 
contexto extremamente atual, com exemplos, teoria, exercícios 
e aplicações para situações atuais e futuras. Recomendo com 
ênfase o curso de Leitura Dinâmica da Methodus porque nossa 
formação acadêmica tradicional está totalmente desvinculada 
da atualidade e este curso nos ensina e motiva a ler com 
excelente aproveitamento.</p>
                      <svg class="quoteSlickEnd" width="39" height="31" viewBox="0 0 39 31" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M29.9465 0.201172L24.6181 4.31076C27.1421 6.86784 28.8247 9.28793 29.6661 11.571C30.5074 13.7628 30.928 16.0003 30.928 18.2834L34.2934 15.9546H24.0572V30.2012H38.5V20.2012C38.5 16.4569 37.8924 12.9866 36.6771 9.79022C35.3684 6.50255 33.1248 3.3062 29.9465 0.201172ZM6.3893 0.201172L1.06088 4.31076C3.58487 6.86784 5.26752 9.28793 6.10885 11.571C6.95018 13.7628 7.37085 16.0003 7.37085 18.2834L10.7362 15.9546H0.5V30.2012H14.9428V20.2012C14.9428 16.4569 14.3352 12.9866 13.1199 9.79022C11.8112 6.50255 9.56765 3.3062 6.3893 0.201172Z"/>
                      </svg>
                    </div>
                  </div>
                </div>
                </div>
				<h2>AGORA É SUA VEZ!</h2>
                          </div>
          
              </div>
    </div>
  </div>
</section>
<!-- Fora de uso
<section id="secFeaturedBanner" class="courseBackgroundColor">
  <div class="row">
    <div class="col-md-12">
      <div class="featuredList">
        <h2>Olhe o texto abaixo.<br> Agora, pare e imagine acontecendo com você.</h2>
                  <ul>
                          <li><span>Falando sem medo, sem inibição, sem sentir vergonha e com naturalidade.</span></li>
                          <li><span>Expressando ideias com desenvoltura, convicção e dinamismo.</span></li>
                          <li><span>Fazendo apresentações objetivas sem errar.</span></li>
                          <li><span>Sabendo segredos para convencer públicos difíceis.</span></li>
                          <li><span>Não falando mais “ééé” ou “né?, né?” nem outros cacoetes.</span></li>
                          <li><span>Usando projetor, flip chart ou quadro branco e explicando algo.</span></li>
                          <li><span>Conversando tanto na vida pessoal quanto profissional com desconhecidos.</span></li>
                          <li><span>Falando elegantemente com gestos.</span></li>
                          <li><span>Falando com olhar impactante e seguro.</span></li>
                          <li><span>Desenvolvendo habilidade de liderar (ser ouvido com razão pelos colegas). </span></li>
                          <li><span>Falando naturalmente em pé ou sentado.</span></li>
                          <li><span>Sabendo iniciar, desenvolver e concluir palestras.</span></li>
                          <li><span>Ampliando o vocabulário para diversas situações.</span></li>
                      </ul>
              </div>
      <div class="featuredCta">
        <h2>E aí? Vai ver todo mundo conseguindo e você não?</h2>
        <h3>Faça isso AGORA! Aproveite as últimas vagas da próxima turma com desconto especial!</h3>
        <div id="formRegistration" class="row">
          <div class="col-md-8 col-md-offset-2">
            <form>
              <input type="hidden" name="curso" value="" />
              <div class="btt2linhas">
              <a data-toggle="modal" data-target="#cursoVejaInvestimento" href="#">
                Quero me inscrever com o desconto agora!                </a>
                </div>
            </form>
          </div>
        </div>
              </div>
    </div>
  </div>
</section>
-->

<!-- Modal  veja investimento -->
<div id="cursoVejaInvestimento" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <?php echo do_shortcode( '[contact-form-7 id="6431" title="VEJA INVESTIMENTOS E PRÓXIMOS GRUPOS!"]'); ?>

      </div>
    </div>

  </div>
</div>


  <script type='text/javascript' async src='https://d335luupugsy2.cloudfront.net/js/loader-scripts/67cc1fec-89be-4715-b86e-5eda535a7d57-loader.js'></script><script type='text/javascript' src='https://methodus.com.br/wp-content/themes/maiscode_methodus_novo/js/main.js?ver=1.0.0' id='jquery-js'></script>
<script type='text/javascript' id='contact-form-7-js-extra'>
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/methodus.com.br\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script type='text/javascript' src='https://methodus.com.br/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.3' id='contact-form-7-js'></script>
<script type='text/javascript' src='https://methodus.com.br/wp-includes/js/wp-embed.min.js?ver=5.5.3' id='wp-embed-js'></script>

<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script> 
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>

<script type="text/javascript">
$(document).ready(function(){

  $('.sliderCard').slick({
    dots: false,
    slidesToShow: 1,
    infinite: false,
    autoplay: false,
    fade: true,
    arrows: true,
    prevArrow: "<img class='a-left control-c prev slick-prev' src='https://methodus.com.br/wp-content/themes/maiscode_methodus_novo/img/arrowPrev.png'>",
    nextArrow: "<img class='a-right control-c next slick-next' src='https://methodus.com.br/wp-content/themes/maiscode_methodus_novo/img/arrowNext.png'>",
    responsive: [
      {
        breakpoint: 780,
        settings: {
          slidesToShow: 1,
          arrows: false,
        }
      },        
    ]
  });

  $('.sliderCardDepo').slick({
    dots: false,
    slidesToShow: 1,
    infinite: true,
    autoplay: true,
    fade: true,
    arrows: true,
    prevArrow: "<img class='a-left control-c prev slick-prev depo-prev' src='https://methodus.com.br/wp-content/themes/maiscode_methodus_novo/img/arrowPrev.png'>",
    nextArrow: "<img class='a-right control-c next slick-next depo-next' src='https://methodus.com.br/wp-content/themes/maiscode_methodus_novo/img/arrowNext.png'>",
    responsive: [
      {
        breakpoint: 780,
        settings: {
          slidesToShow: 1,
          arrows: false,
        }
      },        
    ]
  });


 });


</script>

<?php get_footer(); ?>