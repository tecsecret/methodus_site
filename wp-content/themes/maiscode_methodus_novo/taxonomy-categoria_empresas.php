<?php

get_header(); 


 global $wpdb;


$categoria = get_queried_object();


$term_id = $categoria->term_id;
$term_slug = $categoria->slug;
// $term_name = $categoria->name;
// $term_description = $categoria->description; 

?>

<h1>aqui</h1>

<?php 
$args = array(
	'post_type' => 'empresas',
	'posts_per_page' => 3,
	'tax_query'         => array(
		array(
			'taxonomy'  => 'categoria_empresas',
			'field'     => 'slug',
			'terms'     => $term_slug
		)
	)
);
$my_posts = new WP_Query( $args );
if ( $my_posts->have_posts() ) : 
	?>
	<section>
		<div class="container">
			<div class="row">
				<div class="col-12 col-md-9 mb-xs-24" id="conteudo_paginado">
					<?php while ( $my_posts->have_posts() ) : $my_posts->the_post(); 
						$image = wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() )); 
						$site = get_field('site_empresa');
						$sobre = get_field('breve_descricao_da_empresa');
						$telefone = get_field('telefone_empresa');
						$email = get_field('e-mail_empresa');
						?>
						<div class="post-snippet mb64" style="clear:both;">
							<div class="post-title">
								<h4 class="inline-block"><?php the_title(); ?></h4>
								<span class="label">
									<a href="<?php echo $site; ?>" target="_blank"><?php echo $site; ?></a>
								</span>	
							</div>
							<hr>
							<p>	
								<img class="mb24 thumb_pequeno" src="<?php echo $image; ?>"><?php echo $sobre ?><br><?php echo $telefone; ?><br>
								<a href="<?php echo $email; ?>"><?php echo $email; ?></a>
							</p>
						</div>
					<?php endwhile ?>
					
				</div>
				<div class="col-md-3 hidden-sm widget-methodus">
					<h6 class="title"><?php the_field('pesquisar_empresas',401); ?></h6>
					<div class="busca-widget">
						<form action="<?php bloginfo('siteurl'); ?>" id="searchform" method="get">
							<div>
								 <input placeholder="Pesquisar por" type="text" id="empresa" name="s" value="<?php the_search_query(); ?>" />
								<div class="text-center">
									<input type="submit" class="btn btn-input btn-sm" value="Buscar" id="searchsubmit" />
								</div>
							</div>
						</form>
					</div>

					<h6 class="title"><?php the_field('categorias_empresas',401); ?></h6>

					<?php
					$args = array(
						'taxonomy' => 'categoria_empresas',
						'orderby' => 'date',
						'order'   => 'DESC',
						'hide_empty' => false
					);

					$cats = get_terms($args);
					$cont = 1;

					?>

					<ul class="link-list">
						<?php
						foreach($cats as $cat) { 
							?>

							<li><a href="<?php echo get_term_link($cat->term_id); ?>"><?php echo $cat->name; ?></a></li>

							<?php 
						}
						?>
					</ul>

					<h6 class="title"><?php the_field('texto_mais_acessados',326); ?></h6>
					<ul class="link-list">



						<?php 
						$args = array(
							'post_type' => 'artigos',
							'post_status' => 'publish',
							'posts_per_page' => '20',
							'meta_key' => 'post_views_count',
							'order' => 'DESC'
						);
						$my_posts = new WP_Query( $args );

						if ( $my_posts->have_posts() ) : 
							?>

							<?php while ( $my_posts->have_posts() ) : $my_posts->the_post(); 

								?>


								<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>

							<?php endwhile; ?>
						<?php endif; ?>

					</ul>


					<?php //dynamic_sidebar ( 'sidebar-artigos' ) ; ?>

				</div>
			</div>
		</div>
	</section>
<?php endif ?>

<?php get_footer(); ?>