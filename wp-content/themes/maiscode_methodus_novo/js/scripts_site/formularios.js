// JavaScript Document

///// ############################################ ///////
///// VALIDAÇÃO DE FORMULÁRIOS 
///// ############################################ ///////

////////////// FUNÇÃO QUE CHECA VALORES NULOS ////////////////////
function checa_nulo(campo)
{
	if (campo.value.length <= 0 || campo.value == campo.lang)
	{
		return("- O campo "+campo.lang+" é de preenchimento obrigatório.<BR />");
	}else{
		return("");
	}
}

///////////// FUNÇÃO QUE CHECA VALIDADE DE E-MAIL ///////////////
function checa_email(campo)
{
	//regexp_email = /^[a-z0-9\-](\.?\w)*(\-?\w)*@[a-z0-9\-]+(\.[a-z0-9]+)*(\.[a-z0-9]{2,4})$/i;
	regexp_email = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
	
	if (!regexp_email.test(campo.value))
	{
        return("- O campo "+campo.lang+" não é uma conta válida.<BR />");
	}else{
		return("");
	}
}

///////////// FUNÇÃO QUE CHECA TAMANHO DOS CAMPOS ///////////////
function checa_tamanho(campo,tamanho)
{
	if (campo.value.length < tamanho)
	{
		return("- O campo "+campo.lang+" deve ter no mínimo "+tamanho+" caracteres.<BR />");
	}else{
		return("");;
	}	
}

///////////// FUNÇÃO QUE CHECA CARACTERES ///////////////
function checa_caracter(campo,caracteres)
{
	var ver_numero = caracteres;
	var sk15 = campo.value;
	var invalido = true;
	for (i = 0;  i < sk15.length;  i++){
		ch = sk15.charAt(i);
		for (j = 0;  j < ver_numero.length;  j++)
		if (ch == ver_numero.charAt(j))
			break;
		if (j == ver_numero.length){
			invalido = false;
			break;
		}
	}
	if (!invalido){
		return("- O campo "+campo.lang+" possui caracteres inválidos.<BR />");
	}else{
		return("");
	}
}

/////// FUNÇÃO QUE CHECA PREENCHIMENTO DE CAMPOS RADIO E CHECKOX //////////
/*function checa_opcoes(campo,n_campo){
	var objeto 		= campo;
	var controle 	= 0;
	var qtd_itens 	= objeto.length;
	
	if (qtd_itens == undefined)
		qtd_itens = 1;
	
	if (qtd_itens == 1){
		if (objeto.checked == true){
			controle++;
		}
	}else{
		for (i=0; i < qtd_itens; i++){
			if (objeto[i].checked == true){
				controle++;
				break;
			}
		}
	}
	
	if (controle == 0){
		return("- Escolha alguma opção de "+n_campo+".<BR />");
	}else{
		return("");
	}
}*/

function checa_opcoes(campo,n_campo){
	var objeto 		= campo;
	var controle 	= 0;
	var qtd_itens 	= objeto.length;
	
	if (typeof qtd_itens == 'undefined')
		qtd_itens = 1;
	
	if (qtd_itens == 1){
		if (objeto.checked == true){
			controle++;
		}
	}else{
		for (i=0; i < qtd_itens; i++){
			if (objeto[i].checked == true){
				controle++;
				break;
			}
		}
	}
	
	if (controle == 0){
		// return("- Escolha alguma opção de "+n_campo+".<BR />");
		return("- Selecione alguma opção.<BR />");
	}else{
		return("");
	}
}

/////// FUNÇÃO QUE RETORNA O VALOR DE UM CAMPO RADIO //////////
function retorna_opcao(campo)
{
	var objeto 		= campo;
	var controle 	= 0;
	var qtd_itens 	= objeto.length;
	
	if (qtd_itens == undefined)
		qtd_itens = 1;
	
	for (i=0; i < qtd_itens; i++)
	{
		if (objeto[i].checked == true)
		{
			return(objeto[i].value);
			break;
		}
	}
	
	return('0');
}
/////////////////////////////////////////////////////////

// Scripts para limitar número de opções  //
function limita_opcoes(campo, objeto_atual, qtd)
{
	var conta	= 0;
	var objeto 	= document.getElementsByName(campo);
	
	for (i=0; i < objeto.length; i++)
	{
		
		if (objeto[i].checked == true)
			conta++; 
		
		if (conta > qtd)
		{
			$(objeto_atual).screwDefaultButtons("uncheck");
			notifica( 'Escolha apenas '+qtd+' opções', 'atencao', 2 );
			break;
		}
	}
}

function bug_label(objeto){
	var objeto 	= document.getElementById(objeto);
	$(objeto).screwDefaultButtons("toggle");	
}
/////////////////////////////////////////////////////

// FUNÇÃO QUE CHECA VALIDADE DO CPF //
function checa_cpf( strcpf ) {
	
	numcpf=strcpf.value;
	
	numcpf= numcpf.replace(".", "");
	numcpf= numcpf.replace(".", "");
	numcpf= numcpf.replace("-", "");
	
	x = 0;
	soma = 0;
	dig1 = 0;
	dig2 = 0;
	texto = "";
	numcpf1="";
	len = numcpf.length; x = len -1;
	// var numcpf = "12345678909";
	for (var i=0; i <= len - 3; i++) {
		y = numcpf.substring(i,i+1);
		soma = soma + ( y * x);
		x = x - 1;
		texto = texto + y;
	}
	dig1 = 11 - (soma % 11);
	if (dig1 == 10) dig1=0 ;
	if (dig1 == 11) dig1=0 ;
	numcpf1 = numcpf.substring(0,len - 2) + dig1 ;
	x = 11; soma=0;
	for (var i=0; i <= len - 2; i++) {
		soma = soma + (numcpf1.substring(i,i+1) * x);
		x = x - 1;
	}
	dig2= 11 - (soma % 11);
	if (dig2 == 10) dig2=0;
	if (dig2 == 11) dig2=0;

	//alert ("Digito Verificador : " + dig1 + "" + dig2);
	if ((dig1 + "" + dig2) == numcpf.substring(len,len-2)) {
		return ("");
	}
	else
	{
		return("- "+strcpf.lang+" inválido.<BR />");
	}
			
}

////////////////// VALIDA CNPJ //////////////////////////////
function checa_cnpj( CNPJ_obj ) 
{
	CNPJ= CNPJ_obj.value; 
	CNPJ= CNPJ.replace("-","");
	CNPJ= CNPJ.replace(".","");
	CNPJ= CNPJ.replace(".","");
	CNPJ= CNPJ.replace("/","");
	
	g=CNPJ.length-2;
		
	if(RealTestaCNPJ(CNPJ,g) == 1) 
	 { 
		g=CNPJ.length-1; 
		if(RealTestaCNPJ(CNPJ,g) == 1) 
		 { 
			 CNPJ_valido=1; 
		 } 
		 else 
		 { 
			 CNPJ_valido=0; 
		 } 
	 } 
	 else 
	 { 
		 CNPJ_valido=0; 
	 } 
	
	if (CNPJ_valido==0)
	{
		return("- "+CNPJ_obj.lang+" inválido.<BR />");
	}else{
		return("");	
	}
}

function RealTestaCNPJ(CNPJ,g) 
{ 
	var VerCNPJ=0; 
	var ind=2; 
	var tam; 
	
	for(f=g;f>0;f--) 
	{ 
		VerCNPJ+=parseInt(CNPJ.charAt(f-1))*ind; 
		if(ind>8) 
		{ 
			ind=2; 
		} 
		else 
		{ 
			ind++; 
		} 
	} 
	
	VerCNPJ%=11; 
	if(VerCNPJ==0 || VerCNPJ==1) 
	{ 
		VerCNPJ=0; 
	} 
	else 
	{ 
		VerCNPJ=11-VerCNPJ; 
	} 
	if(VerCNPJ!=parseInt(CNPJ.charAt(g))) 
	{ 
		return(0); 
	} 
	else 
	{ 
		return(1); 
	} 
}

// Verifica se a data apresentada ou no uma data válida
function checa_data( data ) {
	
	if (data.value == ''){
		return("- "+data.lang+" inválida.<BR />");
	}else{
		
		// se é data parcial
		if (data.value.length <= 7){
			data_controle = '01/'+data.value;
		}else{
			data_controle = data.value;	
		}
		
		var date = new Date();
		var blnRet = false;
		var blnDay;
		var blnMonth;
		var blnYear;
		
		day = data_controle.substring(0,2);
		month = data_controle.substring(3,5);
		year = data_controle.substring(6);
		
		date.setFullYear(year, month -1, day);
		
		blnDay   = (date.getDate()      == day);
		blnMonth = (date.getMonth()     == month -1);
		blnYear  = (date.getFullYear()  == year);
		
		//alert(blnDay+'-'+blnMonth+'-'+blnYear);
		
		if (blnDay && blnMonth && blnYear && year >= 1753)
			return("");
		else
			return("- "+data.lang+" inválida.<BR />");
		
	}

}
/////////////////////////////////////////////////////////////

// Retorna uma data em formato que é possível de comparar
function data_timestamp( data ) {
	
	if (data){
		
		// se é data parcial
		if (data.length <= 7){
			data = '01/'+data;	
		}
		
		day = data.substring(0,2);
		month = data.substring(3,5);
		year = data.substring(6);
		
		return(parseInt(year+''+month+''+day));
	
	}else{
		return(0);	
	}

}
/////////////////////////////////////////////////////////////


//############################################################
//# Título            : Mácasra_para_campos_só_números       #
//# Criado por        : Dinho Ferrari                        #
//# Data da Criação   : 09/04/2007                           #                                                          
//# E-mail            : gleydy1@hotmail.com                  #  
//############################################################

//############################################################
//#                                                          #
//#      Por favor mantenha os créditos!!!!                  #
//# Caso tenha gostado desse script me mande um e-mail!      #
//# Bugs e comentários - gleydy1@hotmail.com                 #
//# * Se alterar este script para melhor me avise!           #
//#                                                          #
//############################################################
function mascara(o,f){
    v_obj=o
    v_fun=f
    setTimeout("execmascara()",1)
}

function execmascara(){
    v_obj.value=v_fun(v_obj.value)
}

function leech(v){
    v=v.replace(/o/gi,"0")
    v=v.replace(/i/gi,"1")
    v=v.replace(/z/gi,"2")
    v=v.replace(/e/gi,"3")
    v=v.replace(/a/gi,"4")
    v=v.replace(/s/gi,"5")
    v=v.replace(/t/gi,"7")
    return v
}

function mascaraSoNumeros(v){
    return v.replace(/\D/g,"")
}

function mascaraTelefone(v){
    //return v.replace(/[^\d-]/g,"")
	v=v.replace(/\D/g,"")                 //Remove tudo o que não é dígito
    v=v.replace(/^(\d\d)(\d)/g,"($1) $2") //Coloca parênteses em volta dos dois primeiros dígitos
	if (v.length >=14)
    	v=v.replace(/(\d{5})(\d)/,"$1-$2")    //Coloca hífen entre o quarto e o quinto dígitos
	else
		v=v.replace(/(\d{4})(\d)/,"$1-$2")
    return v
}

function mascaraDinheiro(v){
    //return "R$ " + v.replace(/[^\d,.]/g,"")
	v=v.replace(/\D/g,"");//Remove tudo o que não é dígito
    v=v.replace(/(\d)(\d{8})$/,"$1.$2");//coloca o ponto dos milhões
    v=v.replace(/(\d)(\d{5})$/,"$1.$2");//coloca o ponto dos milhares
 
    v=v.replace(/(\d)(\d{2})$/,"$1,$2");//coloca a virgula antes dos 2 últimos dígitos
	if (v){
    	return "R$ "+v;
	}else{
		return "";	
	}
}

function mascaraCpf(v){
    v=v.replace(/\D/g,"")                    //Remove tudo o que não é dígito
    v=v.replace(/(\d{3})(\d)/,"$1.$2")       //Coloca um ponto entre o terceiro e o quarto dígitos
    v=v.replace(/(\d{3})(\d)/,"$1.$2")       //Coloca um ponto entre o terceiro e o quarto dígitos
                                             //de novo (para o segundo bloco de números)
    v=v.replace(/(\d{3})(\d{1,2})$/,"$1-$2") //Coloca um hífen entre o terceiro e o quarto dígitos
    return v
}

function mascaraCep(v){
    //v=v.replace(/D/g,"")                //Remove tudo o que não é dígito
	v=v.replace(/\D/g,"")  
	v=v.replace(/^(\d{5})(\d)/,"$1-$2") //Esse é tão fácil que não merece explicações
    return v
}

function mascaraData(v){
    v=v.replace(/\D/g,"")                    //Remove tudo o que não é dígito
    v=v.replace(/(\d{2})(\d)/,"$1/$2")       //Coloca um ponto entre o terceiro e o quarto dígitos
    v=v.replace(/(\d{2})(\d)/,"$1/$2")       //Coloca um ponto entre o terceiro e o quarto dígitos
                                             //de novo (para o segundo bloco de números)
    return v
}

function mascaraDataParcial(v){
    v=v.replace(/\D/g,"")                    //Remove tudo o que não é dígito
    v=v.replace(/(\d{2})(\d)/,"$1/$2")       //Coloca um ponto entre o terceiro e o quarto dígitos
    return v
}

function mascaraCnpj(v){
    v=v.replace(/\D/g,"")                           //Remove tudo o que não é dígito
    
	if (v.length >=15){
    	v=v.replace(/^(\d{3})(\d)/,"$1.$2")             //Coloca ponto entre o segundo e o terceiro dígitos
    	v=v.replace(/^(\d{3})\.(\d{3})(\d)/,"$1.$2.$3") //Coloca ponto entre o quinto e o sexto dígitos
    	v=v.replace(/\.(\d{3})(\d)/,".$1/$2")           //Coloca uma barra entre o oitavo e o nono dígitos
    	v=v.replace(/(\d{4})(\d)/,"$1-$2")              //Coloca um hífen depois do bloco de quatro dígitos
	}else{
		v=v.replace(/^(\d{2})(\d)/,"$1.$2")             //Coloca ponto entre o segundo e o terceiro dígitos
		v=v.replace(/^(\d{2})\.(\d{3})(\d)/,"$1.$2.$3") //Coloca ponto entre o quinto e o sexto dígitos
		v=v.replace(/\.(\d{3})(\d)/,".$1/$2")           //Coloca uma barra entre o oitavo e o nono dígitos
		v=v.replace(/(\d{4})(\d)/,"$1-$2")              //Coloca um hífen depois do bloco de quatro dígitos
	}
	
    return v
}

function mascaraSite(v){                                   //Faça seu comentário
    v=v.replace(/^http:\/\/?/,"")
    dominio=v
    caminho=""
    if(v.indexOf("/")>-1)
        dominio=v.split("/")[0]
        caminho=v.replace(/[^\/]*/,"")
    dominio=dominio.replace(/[^\w\.\+-:@]/g,"")
    caminho=caminho.replace(/[^\w\d\+-@:\?&=%\(\)\.]/g,"") 
    caminho=caminho.replace(/([\?&])=/,"$1")
    if(caminho!="")dominio=dominio.replace(/\.+$/,"")
    v="http://"+dominio+caminho
    return v
}

///// ############################################ ///////
///// ############################################ ///////