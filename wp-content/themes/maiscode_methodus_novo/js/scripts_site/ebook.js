// JavaScript Document

/* Helper function */
function download_file(fileURL) {
    // for non-IE
	var filename = fileURL.substring(fileURL.lastIndexOf('/')+1);
	
    if (!window.ActiveXObject) {
        var save = document.createElement('a');
        save.href = fileURL;
        save.target = '_blank';
        save.download = filename;
	       if ( navigator.userAgent.toLowerCase().match(/(ipad|iphone|safari)/) && navigator.userAgent.search("Chrome") < 0) {
				document.location = save.href; 
// window event not working here
			}else{
		        var evt = new MouseEvent('click', {
		            'view': window,
		            'bubbles': true,
		            'cancelable': false
		        });
		        save.dispatchEvent(evt);
		        (window.URL || window.webkitURL).revokeObjectURL(save.href);
			}	
    }

    // for IE < 11
    else if ( !! window.ActiveXObject && document.execCommand)     {
        var _window = window.open(fileURL, '_blank');
        _window.document.close();
        _window.document.execCommand('SaveAs', true, filename || fileURL)
        _window.close();
    }
}

function checaEbook(formData, jqForm, options){
	
	var form = jqForm[0];
	var erro = "";
	
	erro+= checa_nulo( form.nome );
	erro+= checa_nulo( form.email );
	erro+= checa_email( form.email );
	erro+= checa_opcoes( form.conheceu );

	console.log('teste');
	
	if ( erro != "" ){
		notifica( erro, 'atencao', 10 );
		return false;
	}else{
		exibe_carregamento( true );
		return true;
	}
	
	return(false);	
}

function sucessoEbook(data){
	if (data.erro == 0){
		exibe_carregamento( false );

		url_destino = document.getElementById('form_ebook').destino.value;
		//notifica( url_destino, 'sucesso', 15 );

		var win = window.open(url_destino, '_blank');
		if (win) {
			//Browser has allowed it to be opened
			win.focus();
		} else {
			//Browser has blocked it
			notifica( 'Seu navegador bloqueou a janela com o arquivo. Permita a abertura de popups para baixar', 'erro', 15 );
		}
		
		document.getElementById('form_ebook').nome.value = '';
		document.getElementById('form_ebook').email.value = '';

	}else{
		exibe_carregamento( false );
		notifica( data.mensagem, 'erro', 5 );
	}
}