
$(document).ready(function() { 
	
	formAjax();
	
});

// EXIBIR TELA DE CARREGAMENTO ////////////////
function exibe_carregamento( condicao ){
	
	if (condicao){
		
		objeto = $('#div_carregamento');
		
		// criar tooltip se ela ainda não existe no body
		if (objeto.length <= 0){
			$('body').append('<div class="div_carregamento" id="div_carregamento"><div class="icone_carregando carregando" id="icone_carregando"></div></div>');
		}
		
		$('#div_carregamento').show();
		
		altura_navegador 	= $(window).height();
		largura_navegador 	= $(document).width();
		
		$('#div_carregamento').css("width",largura_navegador+"px");
		$('#div_carregamento').css("height",altura_navegador+"px");
		
		$('#icone_carregando').css("left",((largura_navegador/2)-($('#icone_carregando').width() / 2))+"px");
		$('#icone_carregando').css("top",((altura_navegador/2)-($('#icone_carregando').height() / 2))+"px");
		
	
	}else{
		$('#div_carregamento').hide();	
	}

}
//////////////////////////////////////////////

window.addEventListener("beforeunload", function(event) {
    exibe_carregamento( true );
});

// Função para encntrar todos os formulários que tenham AJAX e prepara-los
function formAjax( objeto ){
	
	if (objeto){
		
		if ( $('#'+objeto).attr('data-resultado') ){
			
			str_opcoes = "var options = { ";
			if ( $('#'+objeto).attr('data-validacao') )
				str_opcoes = str_opcoes + "beforeSubmit: "+$('#'+objeto).attr('data-validacao')+",";
			
			str_opcoes = str_opcoes + "success: "+$('#'+objeto).attr('data-resultado')+",dataType: 'json'};";
			eval(str_opcoes);
			
			$('#'+objeto).submit(function() { 
				$('#'+objeto).ajaxSubmit(options); 
				return false; 
			});
			
			$('#'+objeto).removeClass('formAjax');
		
		}
		
	}else{
	
		$('.formAjax').each(function(index, value) { 
			
			if ( $(this).attr('data-resultado') ){
				
				str_opcoes = "var options = { ";
				if ( $(this).attr('data-validacao') )
					str_opcoes = str_opcoes + "beforeSubmit: "+$(this).attr('data-validacao')+",";
				
				str_opcoes = str_opcoes + "success: "+$(this).attr('data-resultado')+",dataType: 'json'};";
				eval(str_opcoes);
				
				$(this).submit(function() { 
					$(this).ajaxSubmit(options);
					return false;
				});
				
				$(this).removeClass('formAjax');
			
			}
			
		});
	
	}
		
}
//////////////////////////////////////////////////////

// AJAX PADRÃO /////////////////////////////////////
function carregaAjax( metodo, url, dados, callback, erro ){
	
	if (metodo == "GET"){
		str_url = url;
		if (dados)
			str_url += "?"+dados;
		str_data = "";
	}else{
		str_url = url;
		str_data = dados;	
	}
	
	if (typeof callback != 'function' && callback.indexOf("var ") < 0 && callback.indexOf("[retorno]") < 0){
		exibe_carregamento( true );
	}
	
	$.ajax({
		type: metodo,
		url: str_url,
		data: str_data,
		success: function (retorno) {
			if (typeof callback != 'function'){
					
				if (callback.indexOf("var ") >= 0)
					eval( callback.replace("var ", "") + ' = '+retorno+'' );
				else if (callback.indexOf("[retorno]") >= 0){ 
					
					// extrai qualquer script em Java para o DOM
					//extraiScript(retorno, callback);
					
					retorno = retorno.replace(/'/g, "\\'");
					retorno = retorno.replace(/"/g, '\\"');
					retorno = replaceTudo(retorno, String.fromCharCode(10), '');
					retorno = replaceTudo(retorno, String.fromCharCode(13), '');
					//console.log( callback.replace("[retorno]", "'"+retorno+"'") );
					
					eval( callback.replace("[retorno]", "'"+retorno+"'") );
					
				}else{
					
					$('#'+callback).html(retorno);
					
					// extrai qualquer script em Java para o DOM
					//extraiScript(retorno, callback);
					
				}
				
			}else{
				callback( retorno );
			}
			
			exibe_carregamento( false );
		},
		error: function () {
			exibe_carregamento( false );
			if (typeof erro == 'function'){
				erro();	
			}
		}
	});	
	
}
////////////////////////////////////////////////////

// Função para executar Javascript carregado pelo Ajax;
function extraiScript(texto, id){
	
	var ini, pos_src, fim, codigo;
	var objScript = null;
	
	ini = texto.indexOf('<script', 0);
	
	while (ini != -1){
		var objScript = document.createElement("script");
		
		//Busca se tem algum "src" a partir do inicio do script.
		pos_src = texto.indexOf(' src', ini)
		ini = texto.indexOf('>', ini) + 1;
		
		//Verifica se este é um bloco de script ou include para um arquivo de scripts.
		if (pos_src < ini && pos_src >= 0){ // Se encontrou um "src" dentro da tag script, esta é um include de um arquivo script.
			// Marca como sendo o inicio do nome do arquivo para depois do src.
			ini = pos_src + 4;
			// Procura pelo ponto do nome da extenção do arquivo e marca para depois dele.
			fim = texto.indexOf('.', ini) + 4;
			// Pega o nome do arquivo.
			codigo = texto.substring(ini, fim);
			// Elimina do nome do arquivo os caracteres que possam ter sido pegos por engano.
			codigo = codigo.replace("=","").replace(" ","").replace("\"","").replace("\"","").replace("\'","").replace("\'","").replace(">","");
			// Adiciona o arquivo de script ao objeto que sera adicionado ao documento
			objScript.src = codigo;
		} else { // Se não encontrou um "src" dentro da tag script, esta é um bloco de codigo script.
			// Procura o final do script.
			fim = texto.indexOf('</script>', ini);
			// Extrai apenas o script
			codigo = texto.substring(ini, fim);
			// Adiciona o bloco de script ao objeto que será adicionado ao documento.
			objScript.text = codigo;
		}
	
		//Adiciona o script ao documento
		//document.body.appendChild(objScript);
		if (document.getElementById(id+"_java_extra"))
		{
			document.getElementById(id+"_java_extra").innerHTML = "";
		}else{
			var novo_div = document.createElement("div");
			novo_div.id = id+"_java_extra";
			document.body.appendChild(novo_div);	
		}
		
		
		document.getElementById(id+"_java_extra").appendChild(objScript);
		// Procura a proxima tag de <script
		ini = texto.indexOf('<script', fim);
		
		//Limpa o objeto de script
		objScript = null;
	}
	
}
////////////////////////////////////////////////////

// TRATAR ERROS DE CHAMADAS EM AJAX //////////
$( document ).ajaxError(function( event, request, settings ) {
	exibe_carregamento( false );
	notifica( 'Ops!! Ocorreu um erro inesperado. Verifique se ainda esta conectado com a Internet e recarregue a página. Se o erro persistir contate o suporte.', 'erro', 25 );
	console.log( 'ERRO AJAX: URL:</b> '+settings.url+'<BR /><b>STATUS:</b> Erro '+request.status+'<BR />'+request.responseText+'' );
});
//////////////////////////////////////////////


// EXIBIR NOTIFICAÇÕES NA TELA /////////////////////
function notifica( texto, tipo, tempo, callback ) {
	
	if (tipo == 'info'){
		tipo = 'information';	
	}else if (tipo == 'erro'){
		tipo = 'error';	
	}else if (tipo == 'atencao'){
		tipo = 'warning';	
	}else if (tipo == 'notifica'){
		tipo = 'notification';	
	}else if (tipo == 'sucesso'){
		tipo = 'success';	
	}else{
		tipo = 'alert';	
	}
	
	if (!tempo){
		tempo = false;	
	}else{
		tempo = tempo * 1000;	
	}
	
	
	var n = noty({
		text        : texto,
		type        : tipo, // 'alert','information','error','warning','notification','success'
		dismissQueue: true,
		killer      : true,
		layout      : 'center',
		theme       : 'cfTheme',
		closeWith	: ['click','button'],
		timeout		: tempo, 
		animation: {
			open: {height: 'toggle'},
			close: {height: 'toggle'},
			easing: 'swing',
			speed: 200
		},
		modal       : true,
		callback: {
			afterClose: function() { eval(callback) }
		}
	});
	//console.log('html: ' + n.options.id);
}

// como chamar: confirma('teste','info',[{estilo:'erro',botao:'OK',url:'http://www.google.com.br'},{estilo:'info',botao:'cancela'}])
function confirma( texto, tipo, botoes ) {
	
	if (tipo == 'info'){
		tipo = 'information';	
	}else if (tipo == 'erro'){
		tipo = 'error';	
	}else if (tipo == 'atencao'){
		tipo = 'warning';	
	}else if (tipo == 'notifica'){
		tipo = 'notification';	
	}else if (tipo == 'sucesso'){
		tipo = 'success';	
	}else{
		tipo = 'alert';	
	}
	
	str_botoes = "";
	
	if (!botoes){
		str_botoes = "{addClass: 'btn btn-primary', text: 'OK', onClick: function($noty) {$noty.close();}}"
	}else{
		
		for (i=0; i < botoes.length; i++){
			
			if (!botoes[i].estilo){
				tmp_estilo = 'primary';
			}else{
				if (botoes[i].estilo == 'erro'){
					tmp_estilo = 'danger'
				}else if (botoes[i].estilo == 'atencao'){
					tmp_estilo = 'warning';	
				}else if (botoes[i].estilo == 'notifica'){
					tmp_estilo = 'notification';	
				}else if (botoes[i].estilo == 'sucesso'){
					tmp_estilo = 'success';
				}else if (botoes[i].estilo == 'info'){
					tmp_estilo = 'primary';	
				}else{
					tmp_estilo = botoes[i].estilo;
				}
			}
			
			if (!botoes[i].botao){
				tmp_botao = 'OK';
			}else{
				tmp_botao = botoes[i].botao;	
			}
			
			str_botoes += "{addClass: 'btn btn-"+tmp_estilo+"', text: '"+tmp_botao+"', onClick: function($noty) {";
			
			if (!botoes[i].url){
				str_botoes += "$noty.close();";
			}else{
				if (botoes[i].url.indexOf('(') > 0){
					str_botoes += "$noty.close();";
					str_botoes += botoes[i].url+";";
					
				}else{
					str_botoes += "notifica('<img src=\""+url_site+"/_imagens/carregando.gif\" />','notifica');";
					str_botoes += "setTimeout('document.location=\""+botoes[i].url+"\";\',500);";	
				}
			}
			
			str_botoes += "}}";
			
			if (i < (botoes.length-1)){
				str_botoes += ","
			}
		}
	
	}
	
	var str_botoes = eval("["+str_botoes+"]");
	
	var n = noty({
		text        : texto,
		type        : tipo,
		dismissQueue: true,
		killer      : true,
		layout      : 'center',
		theme       : 'cfTheme',
		animation: {
			open: {height: 'toggle'},
			close: {height: 'toggle'},
			easing: 'swing',
			speed: 200
		},
		modal       : true,
		buttons		: str_botoes
	});
	
}
//$(document).ready(function () {
	//$.noty.closeAll()
//});
////////////////////////////////////////////////////

// Controla os perfis d select de cadastro
function mudaPerfis( identificador_form_generico, valor ){
	ID_curso = valor.value;
	
	$("#generico_"+identificador_form_generico+" #generico_perfil option").each(function() {
		$(this).remove();
	});
	$("#generico_"+identificador_form_generico+" #generico_perfil").append($('<option>', {
		value: '',
		text: 'Selecione seu perfil'
	}));
	
	if (ID_curso != ''){
		
		$.ajax({url: "/api/?a=perfis&metodo=listar", success: function(result){
			
			var xmlDoc = $.parseXML( (new XMLSerializer()).serializeToString(result) ); 
			var $xml = $(xmlDoc);
			
			var $perfis = $xml.find('perfil[ID_curso="'+ID_curso+'"]');
			
			$perfis.each(function(){
				
				var titulo_perfil 	= $(this).text();
				var ID_perfil 		= $(this).attr('codigo');
				
				$("#generico_"+identificador_form_generico+" #generico_perfil").append($('<option>', {
					value: ID_perfil,
					text: titulo_perfil
				}));
				
			});
			
			
		}});
	}
}

function checaGenerico(formData, jqForm, options){
	
	var form = jqForm[0];
	var erro = "";
	
	erro+= checa_nulo( form.nome );
	erro+= checa_nulo( form.email );
	erro+= checa_email( form.email );
	erro+= checa_opcoes( form.curso, 'Curso' );
	//erro+= checa_nulo( form.curso );
	//erro+= checa_nulo( form.perfil );
	
	if ( erro != "" ){
		notifica( erro, 'atencao', 10 );
		return false;
	}else{
		ID_formulario = $( form ).attr('id');
		$( '#secao_'+ID_formulario ).hide('fast');
		exibe_carregamento( true );
		return true;
	}
	
	return(false);	
}

function sucessoGenerico(data){
	exibe_carregamento( false );
	if (data.erro == 0){
		notifica( data.mensagem, 'sucesso', 5 );
	}else{
		notifica( data.mensagem, 'erro', 5 );
	}
}


////////////////////// ABRE JANELA QUALQUER //////////////////////
var win= null;
function janela(pagina,w,h,scroll,dimensao)
{
  var winl = ((screen.width-w)/2);
  var wint = ((screen.height-h)/2);
  var settings  ='height='+h+',';
      settings +='width='+w+',';
      settings +='top='+wint+',';
      settings +='left='+winl+',';
      settings +='scrollbars='+scroll+',';
      settings +='resizable='+dimensao+', status=no';
  win=window.open(pagina,"_blank",settings);
  if(parseInt(navigator.appVersion) >= 4){win.window.focus();}
}
//////////////////////////////////////////////////////////////////

// FUNÇÃO PARA ENVIAR POR JAVASCRIPT UM POST OU GET //
function encaminhar_pagina(path, params, method) {
	method = method || "post"; // Set method to post by default if not specified.

	// The rest of this code assumes you are not using a library.
	// It can be made less wordy if you use one.
	var form = document.createElement("form");
	form.setAttribute("method", method);
	form.setAttribute("action", path);

	for(var key in params) {
		if(params.hasOwnProperty(key)) {
			var hiddenField = document.createElement("input");
			hiddenField.setAttribute("type", "hidden");
			hiddenField.setAttribute("name", key);
			hiddenField.setAttribute("value", params[key]);

			form.appendChild(hiddenField);
		 }
	}

	document.body.appendChild(form);
	form.submit();
}
/////////////////////////////////////////////

// Para ver os dias de uam determinada turma
function verDiasTurma(id){
	
	$('.dias_turma').fadeOut('fast',function(){
		setTimeout(function(){
			$('#dias_turma_'+id).fadeIn('fast');
		},500);
	});
		
}

// Para girar depoimentos dos cursos
depoimento_atual = 1;
mouse_depoimento = false;

function iniciaDepoimentosCursos(){
	qtd_depoimentos = $('.curso_depoimento').length;

	setInterval(function(){

		if ( mouse_depoimento == false ){

			depoimento_atual++;

			if (depoimento_atual > qtd_depoimentos){
				depoimento_atual = 1;
			}

			$('.curso_depoimento').each(function(index, value) { 
				
				if ($(this).is(":visible") == true) { 
					$(this).fadeOut('fast',function(){
						$('#depoimento_'+depoimento_atual).fadeIn('fast');
					});
				}

			});

		}

	},5000);

	$('.curso_depoimentos').mouseenter(function() {
		mouse_depoimento = true;
	}).mouseleave(function() {      
		mouse_depoimento = false;
	});

}

function depoimentoAlterar( proximo ){

	qtd_depoimentos = $('.curso_depoimento').length;
	conta_depoimento = 0;

	$('.curso_depoimento').each(function(index, value) { 
		conta_depoimento++;

		proximo_depoimento = (conta_depoimento+proximo);

		if (proximo_depoimento > qtd_depoimentos){
			proximo_depoimento = 1;
		}else if (proximo_depoimento < 1){
			proximo_depoimento = qtd_depoimentos;
		}
				
		if ($(this).is(":visible") == true) { 
			$(this).fadeOut('fast',function(){
				$('#depoimento_'+proximo_depoimento).fadeIn('fast');
			});
			return false;
		}

	});

}


