// JavaScript Document

function checaCurso(formData, jqForm, options){
	
	var form = jqForm[0];
	var erro = "";
	
	erro+= checa_nulo( form.nome );
	erro+= checa_nulo( form.email );
	erro+= checa_email( form.email );
	//erro+= checa_nulo( form.perfil );
	
	if ( erro != "" ){
		notifica( erro, 'atencao', 10 );
		return false;
	}else{
		exibe_carregamento( true );
		return true;
	}
	
	return(false);	
}

function sucessoCurso(data){
	if (data.erro == 0){
		exibe_carregamento( true );
		
		if ( $('#testes_form').attr('data-conteudo') ){
			document.location = url_site+'/conteudo/?ID='+$('#testes_form').attr('data-conteudo');
		}else{
			document.location = url_site+'/testes/testes.php';
		}
	}else{
		exibe_carregamento( false );
		notifica( data.mensagem, 'erro', 5 );
	}
}