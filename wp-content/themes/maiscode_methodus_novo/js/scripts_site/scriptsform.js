// JavaScript Document

function checaConvenio(formData, jqForm, options){
	
	var form = jqForm[0];
	var erro = "";
	
	erro+= checa_nulo( form.razao );
	erro+= checa_nulo( form.nome );
	erro+= checa_nulo( form.cargo );
	erro+= checa_nulo( form.endereco );
	erro+= checa_tamanho( form.cep, 9 );
	erro+= checa_caracter( form.cep, '0123456789-' );
	erro+= checa_nulo( form.cidade );
	erro+= checa_nulo( form.estado );
	erro+= checa_nulo( form.nome );
	erro+= checa_nulo( form.email );
	erro+= checa_email( form.email );
	erro+= checa_tamanho( form.telefone, 14 );
	erro+= checa_caracter( form.telefone, '0123456789() -' );
	
	if ( form.funcionarios ){
		erro+= checa_nulo( form.funcionarios );
		erro+= checa_caracter( form.funcionarios, '0123456789' );
	}
	
	erro+= checa_opcoes( form.elements['cursos[]'], 'Cursos de Interesse' );
		
	if ( erro != "" ){
		notifica( erro, 'atencao', 10 );
		return false;
	}else{
		exibe_carregamento( true );
		return true;
	}
	
	return(false);	
}

function sucessoConvenio(data){
	if (data.erro == 0){
		exibe_carregamento( false );
		notifica( data.mensagem, 'sucesso', 5, 'location.reload(true);' );
	}else{
		exibe_carregamento( false );
		notifica( data.mensagem, 'erro', 5 );
	}
}