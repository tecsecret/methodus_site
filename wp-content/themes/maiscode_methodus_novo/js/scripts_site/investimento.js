// JavaScript Document

function checaCurso(formData, jqForm, options){
	
	var form = jqForm[0];
	var erro = "";
	
	erro+= checa_nulo( form.nome );
	erro+= checa_nulo( form.email );
	erro+= checa_email( form.email );
	//erro+= checa_opcoes( form.conheceu, 'Perfil' );
	erro+= checa_opcoes( form.conheceu );
	
	if ( erro != "" ){
		notifica( erro, 'atencao', 10 );
		return false;
	}else{
		exibe_carregamento( true );
		return true;
	}
	
	return(false);	
}


function sucessoCurso(data){
	if (data.erro == 0){
		exibe_carregamento( true );
		// Vai para tela de investimento

		var id_curso = $('#investimento').attr('data-curso');
		var form = $('<form action="' + url_site + '/matricula" method="post">' +
		  '<input type="text" name="id_curso" id="id_curso" value="' + id_curso + '" />' +
		  '</form>');
		$('body').append(form);

		exibe_carregamento( false );
		notifica( data.mensagem, 'sucesso', 5 );

		form.submit();

		//document.location = url_site+'/matricula/?ID='+$('#investimento').attr('data-curso');
		
		//exibe_carregamento( false );
		//notifica( data.mensagem, 'sucesso', 5 );
	}else{
		exibe_carregamento( false );
		notifica( data.mensagem, 'erro', 5 );
	}
}