// JavaScript Document

function checaContato(formData, jqForm, options){
	
	var form = jqForm[0];
	var erro = "";
	
	erro+= checa_nulo( form.nome );
	erro+= checa_nulo( form.email );
	erro+= checa_email( form.email );
	erro+= checa_nulo( form.mensagem );
	erro+= checa_tamanho(form.mensagem,10)
	
	if ( erro != "" ){
		notifica( erro, 'atencao', 10 );
		return false;
	}else{
		exibe_carregamento( true );
		return true;
	}
	
	return(false);	
}

function contatoSucesso(data){
	if (data.erro == 0){
		notifica( data.mensagem, 'sucesso', 5 , 'exibe_carregamento( true ); location.reload();' );
	}else{
		exibe_carregamento( false );
		notifica( data.mensagem, 'erro', 5 );
	}
}