<?php get_header(); ?>

<div class="main-container">
	<section>
		<div class="container">
			<div class="row">

			<?php 
			
				if (have_posts()) : while (have_posts()) : the_post();

					$tipo_post = get_post_type();			

				endwhile; endif;

		 		wp_reset_postdata();

				set_query_var('my_tipo_post', $_GET['tipo']);


				if($_GET['tipo'] === 'artigos') :
					 get_template_part('inc/template', 'buscaArtigos'); 					
				endif;	

				if($_GET['tipo'] === 'empresas') :
					 get_template_part('inc/template', 'buscaEmpresas'); 					
				endif;	
				
				if($_GET['tipo'] === 'links') :
					 get_template_part('inc/template', 'buscaLinks'); 					
				endif;				

				if($_GET['tipo'] === 'livros') :
					 get_template_part('inc/template', 'buscaLivros'); 					
				endif;

				if($_GET['tipo'] === 'noticias') :
					 get_template_part('inc/template', 'buscaNoticias'); 					
				endif;	

				if($_GET['tipo'] === 'videos') :
					 get_template_part('inc/template', 'buscaVideos'); 					
				endif;	
			?>

			</div>
		</div>
	</section>
</div>

<?php get_footer(); ?>
