<footer class="footer-1 bg-secondary_methodus">
    <div class="container">
        <div class="row">
            <div class="social-media-box">
                <?php redes_sociais(); ?>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="widget">
                    <?php 
                        $footer_oratoria = array(
                            'theme_location'  => 'menu-footer-oratoria',
                            'container'       => false,
                            'menu_class'      => 'link-list recent-posts',
                            'menu_id'         => 'menu-footer-oratoria',
                            'echo'            => true
                        );
                        wp_nav_menu($footer_oratoria);
                    ?>
                </div>
            </div>

            <div class="col-md-4 col-sm-6">
                <div class="widget">

                    <?php
                        $footer_oratoria = array(
                            'theme_location'  => 'menu-footer-cursos-online',
                            'container'       => false,
                            'menu_class'      => 'link-list recent-posts',
                            'menu_id'         => 'menu-footer-oratoria',
                            'echo'            => true
                        );
                        wp_nav_menu($footer_oratoria);
                    ?>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="widget">
                    <?php 
                        $footer_oratoria = array(
                            'theme_location'  => 'menu-footer-mentoria',
                            'container'       => false,
                            'menu_class'      => 'link-list recent-posts',
                            'menu_id'         => 'menu-footer-oratoria',
                            'echo'            => true
                        );
                        wp_nav_menu($footer_oratoria);
                    ?>
                </div>
            </div>
            <?php /*<div class="col-md-3 col-sm-6">
                <div class="widget">
                    <?php 
                        $footer_oratoria = array(
                            'theme_location'  => 'menu-footer-cursos',
                            'container'       => false,
                            'menu_class'      => 'link-list recent-posts',
                            'menu_id'         => 'menu-footer-oratoria',
                            'echo'            => true
                        );
                        wp_nav_menu($footer_oratoria);
                    ?>
                </div>
            </div>*/ ?>

    <?php 
        /*


        <div class="col-md-2 col-sm-6">
            <div class="widget">
                <?php 
                    $footer_oratoria = array(
                        'theme_location'  => 'menu-footer-conteudos',
                        'container'       => false,
                        'menu_class'      => 'link-list recent-posts',
                        'menu_id'         => 'menu-footer-oratoria',
                        'echo'            => true
                    );
                    wp_nav_menu($footer_oratoria);
                ?>
            </div>
        </div>


        */
     ?>


<?php 

/*


        <div class="col-md-2 col-sm-6">
            <div class="widget">
                <h6 class="title">Facebook</h6>
                <hr>
                <div class="fb-page fb_iframe_widget" data-href="https://www.facebook.com/cursomethodus/" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" fb-xfbml-state="rendered" fb-iframe-plugin-query="adapt_container_width=true&amp;app_id=1302272369795939&amp;container_width=165&amp;hide_cover=false&amp;href=https%3A%2F%2Fwww.facebook.com%2Fcursomethodus%2F&amp;locale=pt_BR&amp;sdk=joey&amp;show_facepile=true&amp;small_header=true"><span style="vertical-align: bottom; width: 180px; height: 137px;"><iframe name="f152d24c672c358" width="1000px" height="1000px" title="fb:page Facebook Social Plugin" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" allow="encrypted-media" src="https://www.facebook.com/v2.11/plugins/page.php?adapt_container_width=true&amp;app_id=1302272369795939&amp;channel=https%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter.php%3Fversion%3D44%23cb%3Dfee568890de824%26domain%3Dwww.methodus.com.br%26origin%3Dhttps%253A%252F%252Fwww.methodus.com.br%252Ff1b4a2980124b%26relation%3Dparent.parent&amp;container_width=165&amp;hide_cover=false&amp;href=https%3A%2F%2Fwww.facebook.com%2Fcursomethodus%2F&amp;locale=pt_BR&amp;sdk=joey&amp;show_facepile=true&amp;small_header=true" style="border: none; visibility: visible; width: 180px; height: 137px;" class=""></iframe></span></div>
            </div>
        </div>


*/


     ?>
            
        </div>





    </div>



    <!-- <a class="btn btn-sm fade-half back-to-top inner-link" href="#top">Voltar para o topo</a> -->
</footer> 

<section class="copy">
    
       <div class="container">
         <div class="row">
            <div class="col-12">
                <span class="sub"><?= get_option('cabecalho_informacao'); ?></span>
            </div>
        </div>
    </div>


</section>

<?php

$number = get_option('whatsapp');
$number = filter_var($number, FILTER_SANITIZE_NUMBER_INT);
$number = str_replace('-', '', $number);

?>
    <script src="<?php bloginfo('template_url'); ?>/js/scripts_site/bootstrap.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/scripts_site/flexslider.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/scripts_site/twitterfetcher.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/scripts_site/lightbox.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/scripts_site/spectragram.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/scripts_site/smooth-scroll.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/scripts_site/parallax.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/scripts_site/scripts.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/scripts_site/jquery.noty.packaged.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/scripts_site/jquery.form.min.js"></script>

    
 <!-- <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-58ecd64ba0ab078f"></script>  -->
	
		<!-- Código de monitoramento da RD Station 

	<script type="text/javascript" async src="https://d335luupugsy2.cloudfront.net/js/loader-scripts/67cc1fec-89be-4715-b86e-5eda535a7d57-loader.js" ></script>

    <script type="text/javascript" src="https://d335luupugsy2.cloudfront.net/js/lead-tracking/stable/lead-tracking.min.js"></script>
    <script type="text/javascript" src="https://d335luupugsy2.cloudfront.net/js/rdstation-popups/bricks/rdstation-popup.min.js?v=1"></script>

    <script type="text/javascript" src="https://d335luupugsy2.cloudfront.net/js/integration/stable/rd-js-integration.min.js?v=1"></script>

    -->


    

	<?php wp_footer(); ?>	
</body>
</html>