<?php
/**
* Template Name: Página Video Single
* Template Post Type: post, page, videos
*
* @package WordPress
* @author Mais Code Tecnologia
* @since First Version
*/
get_header(); 
$url_api  =  get_option('link_api');
$image = wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() ));
?>
<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-7">
                <div class="post-title">
                    <h1 class="inline-block"><?php the_title(); ?></h1>
                </div>
                <hr>
                <?php the_field('video'); ?>
            </div>
            <?php 

            $relacao = get_field('relacione_curso');            

            if( $relacao ): ?>
              <ul>
                <?php foreach( $relacao as $post): // variable must be called $post (IMPORTANT) ?>
                  <?php setup_postdata($post); 
                  //var_dump($post);
                  ?>
                  <style type="text/css">
                    .btn_top{
                      color: #fff !important;
                  }
                  .btn_top:hover{
                      background-color: #fff !important;
                      color: <?php the_field('cor_curso'); ?> !important;
                  }
                  .btn_pg_curso{
                      border: 2px solid <?php the_field('cor_curso'); ?> !important;;
                  }
                  .btn_pg_curso:hover{
                      background-color: #fff !important;
                      color: <?php the_field('cor_curso'); ?> !important;
                  }
                  .btn_chamada:hover{
                      color: <?php the_field('cor_curso'); ?> !important;
                  }
              </style>
              <div class="col-sm-6 col-md-4 col-md-offset-1">
                <div class="box-contato" id="investir">
                  <h2 class="titulo-box" style="background: <?php the_field('cor_curso',get_the_ID()); ?>;"><?php the_field('titulo_curto',get_the_ID()); ?></h2>
                  <div class="interno-box">
                  </div>

                  <h4 class="dados-box" style="background: <?php the_field('cor_curso',get_the_ID()); ?>;"><strong><?php the_field('texto_preencha_seus_dados',get_the_ID()); ?></strong></h4>
                  <div class="interno-box">
                    <script src="<?php bloginfo('template_url'); ?>/js/scripts_site/investimento.js"></script>
                    <form action="<?= $url_api; ?>" method="post" class="form-email" id="investimento" data-validacao="checaCurso" data-resultado="sucessoCurso" data-curso="<?php the_field('id_do_curso',get_the_ID()); ?>">
                      <input type="hidden" name="a" value="aluno">
                      <input type="hidden" name="metodo" value="cadastro">
                      <input type="hidden" name="origem" value="Investimento">
                      <input type="hidden" name="curso" value="<?php the_field('id_do_curso',get_the_ID()); ?>">
                      <input type="text" class="validate-required field-error" name="nome" placeholder="Nome" lang="Nome">
                      <input type="text" class="validate-required validate-email field-error" name="email" placeholder="Email" lang="E-mail">
                      <br>
                      <button type="submit" class="btn_pg_curso" style="background: <?php the_field('cor_curso',get_the_ID()); ?>;">
                        <?php the_field('texto_veja_investimento',get_the_ID()); ?>
                    </button>
                </form>
                <div class="fb-page fb_iframe_widget" data-href="https://www.facebook.com/cursomethodus/" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-height="720" fb-xfbml-state="rendered" fb-iframe-plugin-query="adapt_container_width=true&amp;app_id=1302272369795939&amp;container_width=340&amp;height=720&amp;hide_cover=false&amp;href=https%3A%2F%2Fwww.facebook.com%2Fcursomethodus%2F&amp;locale=pt_BR&amp;sdk=joey&amp;show_facepile=true&amp;small_header=false&amp;tabs=timeline">
                  <span style="vertical-align: bottom; width: 340px; height: 720px;">
                    <iframe name="f18bd715b60fd8" width="1000px" height="720px" title="fb:page Facebook Social Plugin" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" allow="encrypted-media" src="https://www.facebook.com/v2.11/plugins/page.php?adapt_container_width=true&amp;app_id=1302272369795939&amp;channel=https%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter.php%3Fversion%3D44%23cb%3Df1813178e3d996%26domain%3Dwww.methodus.com.br%26origin%3Dhttps%253A%252F%252Fwww.methodus.com.br%252Ff33a0b0868dbb98%26relation%3Dparent.parent&amp;container_width=340&amp;height=720&amp;hide_cover=false&amp;href=https%3A%2F%2Fwww.facebook.com%2Fcursomethodus%2F&amp;locale=pt_BR&amp;sdk=joey&amp;show_facepile=true&amp;small_header=false&amp;tabs=timeline" style="border: none; visibility: visible; width: 340px; height: 720px;" class=""></iframe>
                </span>
            </div>
        </div>
    </div>
</div>


<?php endforeach; ?>
</ul>
<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
<?php endif; ?>
    </div>
</section>

<?php get_footer(); ?>