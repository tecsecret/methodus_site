<?php
/**
* Template Name: Página Artigos
*
* @package WordPress
* @author Mais Code Tecnologia
* @since First Version
*/ 

get_header(); 
// global $wpdb;

// $categoria = get_queried_object();


?>    
<div class="main-container">
    <section id="artigos-pagina">       
        <div class="container">
            <div class="row">            
                <div class="col-md-9 mb-xs-24" id="conteudo_paginado">

                <h1><?php $postType = get_queried_object(); echo esc_html($postType->labels->name); ?></h1>
                
                <?php 
                        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

                        $limit = 6;
                        $offset = $limit * ($paged - 1);                        
                        $args = array(
                            'post_type'      => 'artigos',
                            //'posts_per_page' => 10,
                            'offset'         => $offset,
                            'orderby'        => 'date',
                            'order'          => 'ASC',
                        );

                        $postsList = new WP_Query($args);
                    ?>


                        <?php if ( $postsList->have_posts() ) :  ?>
                        
                         <?php while ( $postsList->have_posts() ) : $postsList->the_post(); ?>

                            <?php 
                            
                            $image = wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() )); 

                           
                              
                            ?>

                            <div class="container-artigo">
                                <?php if ($image != ""): ?>
                                    <div class="imagem-noticia">                                    
                                        <a href="<?php the_permalink(); ?>"><img alt="Post Image" src="<?php echo $image; ?>"></a>
                                    </div>
                                <?php endif ?> 
                                <div class="conteudo-artigo">
                                    <a href="<?php the_permalink(); ?>">
                                        <h4><?php the_title(); ?></h4>
                                        <?php the_excerpt(); ?>
                                        <div class="footer-artigo">
                                            <span>Publicado em: <?php the_time('d/m/Y'); ?></span>
                                            <a href="<?php the_permalink(); ?>">Ler mais<i class="fas fa-arrow-right"></i></a>
                                        </div>
                                    </a>
                                </div>
                            </div>                              

                            <?php endwhile ?>
                            
                    <?php endif ?>    
                    
                    

                    <div class="text-center">
                        
                        <?php paginacao($limit); ?>
                    
                    </div>
                </div>
                <div class="col-md-3 hidden-sm widget-methodus">                    
                    <div class="busca-widget">
                        <form action="<?php bloginfo('siteurl'); ?>" id="searchform" method="get">
                            <div class="input-artigo">
                                <input type="hidden" name="tipo" value="artigos" id="artigo">
                                <input type="text" name="s" value="<?php the_search_query(); ?>" />                                
                                <input type="submit" class="btn btn-input btn-sm" value="Buscar" id="searchsubmit" />
                                
                            </div>
                        </form>
                    </div>


                    <h6 class="title">Categoria dos artigos</h6>                

                     <?php
                          $args = array(
                           'taxonomy' => 'categoria_artigo',
                           'orderby' => 'date',
                           'order'   => 'DESC',
                           'hide_empty' => false
                         );

                          $cats = get_terms($args);
                          $cont = 1;

                        ?>

                        <ul class="link-list">
                        <?php
                          foreach($cats as $cat) { 
                          ?>

                            <li><a href="<?php echo get_term_link($cat->term_id); ?>"><?php echo $cat->name; ?></a></li>

                          <?php 
                            }
                           ?>
                        </ul>

                        <h6 class="title">Artigos mais acessados </h6>
                        <ul class="link-list">



                        <?php 
                        $args = array(
                            'post_type' => 'artigos',
                            'post_status' => 'publish',
                            'posts_per_page' => '20',
                            'meta_key' => 'post_views_count',
                            'order' => 'DESC'
                        );
                        $my_posts = new WP_Query( $args );
       
                        if ( $my_posts->have_posts() ) : 
                        ?>
                            
                          <?php while ( $my_posts->have_posts() ) : $my_posts->the_post(); 

                          ?>


                            <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
     
                          <?php endwhile; ?>
                         <?php endif; ?>
         
                        </ul>


            		 <?php //dynamic_sidebar ( 'sidebar-artigos' ) ; ?>

                </div>

            </div>
        </div>

    </section>
</div>

<?php get_footer(); ?>