<?php
/**
* Template Name: Página Professor
*
* @package WordPress
* @author Mais Code Tecnologia
* @since First Version
*/
get_header(); ?>

<section>
	<div class="container">		
		<div class="row">
			<div class="col-sm-6 col-md-7">
				<div class="post-title">
					<h1 class="inline-block"><?php the_title(); ?></h1>
				</div>
				<hr>
				<p>
					<img alt="<?php the_title(); ?>" class="imagem-texto" src="<?php the_field('foto_professor'); ?>" style="width: 113px; height: 116px; float: left;">
					<?php the_field('sobre_professor'); ?>
				</p>
				<p>
					&nbsp;
				</p>
				<?php the_field('texto_professor'); ?>
			</div>
			<div class="col-sm-6 col-md-4 col-md-offset-1">
				<?php get_template_part( 'inc/contato' ); ?>
			</div>
		</div>		
	</div>
</section>

<?php get_footer(); ?>