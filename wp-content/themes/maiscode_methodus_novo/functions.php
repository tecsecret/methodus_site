<?php
	
  // Clean up wp_head
    // Remove Really simple discovery link
    remove_action('wp_head', 'rsd_link');
    // Remove Windows Live Writer link
    remove_action('wp_head', 'wlwmanifest_link');
    // Remove the version number
    remove_action('wp_head', 'wp_generator');

    // Remove curly quotes
    remove_filter('the_content', 'wptexturize');
    remove_filter('comment_text', 'wptexturize');

    // Allow HTML in user profiles
    remove_filter('pre_user_description', 'wp_filter_kses');

    //Optimize Database
    function optimize_database(){
        global $wpdb;
        $all_tables = $wpdb->get_results('SHOW TABLES',ARRAY_A);
        foreach ($all_tables as $tables){
            $table = array_values($tables);
            $wpdb->query("OPTIMIZE TABLE ".$table[0]);
        }
    }

    function simple_optimization_cron_on(){
        wp_schedule_event(time(), 'daily', 'optimize_database');
    }

    function simple_optimization_cron_off(){
        wp_clear_scheduled_hook('optimize_database');
    }
    register_activation_hook(__FILE__,'simple_optimization_cron_on');
    register_deactivation_hook(__FILE__,'simple_optimization_cron_off');

    // Logout url link

    add_filter( 'login_headerurl', 'custom_logout' );
    function custom_logout($url) {
      return home_url('/');
    }

    // Google Maps
    //----------------------------------------//
    function my_acf_google_map_api( $api ){
      
      $api['key'] = get_option('api_maps');
      
      return $api;
      
    }

    add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');

remove_filter('wpcf7_validate_radio', 'wpcf7_checkbox_validation_filter', 10);

add_filter('wpcf7_validate_radio', function($result, $tag) {
  if (in_array('class:required', $tag->options)) {
    $result = wpcf7_checkbox_validation_filter($result, $tag);
  }
  return $result;
}, 10, 2);

    // Sidebar
        if (function_exists('register_sidebar')) {
        	register_sidebar(array(
        		'name' => 'Sidebar Widgets',
        		'id'   => 'sidebar-widgets',
        		'description'   => 'These are widgets for the sidebar.',
        		'before_widget' => '<div id="%1$s" class="widget %2$s">',
        		'after_widget'  => '</div>',
        		'before_title'  => '<h2>',
        		'after_title'   => '</h2>'
        	));
        }
    
    // Menu Register 
        function register_my_menus() {
            register_nav_menus(
              array(
                'header-menu' => __( 'Header Menu' ),
                'extra-menu' => __( 'Extra Menu' ),
                'menu-footer-oratoria' => __( 'Menu Footer Oratoria' ),
                'menu-footer-leitura' => __('Menu Footer Leitura Dinâmica e Memória'),
                'menu-footer-administracao-tempo' => __('Menu Footer Administração do Tempo'),
                'menu-footer-cursos' => __('Menu Footer Cursos'),
                'menu-footer-conteudos' => __('Menu Footer Conteúdos'),
                'menu-footer-mentoria' => __('Menu Footer Mentoria'),
                'menu-footer-cursos-online' => __('Menu Footer Cursos Online')
              )
            );
        }
        add_action( 'init', 'register_my_menus' );

    // Themes Supports
       remove_action( 'wp_head', 'print_emoji_detection_script', 7 ); remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );

	//Let WordPress manage the document title. / By adding theme support, we declare that this theme does not use a hard-coded <title> tag in the document head, and expect WordPress to provide it for us.
    	add_theme_support('title-tag');

	// Thumbnail Support and Sizes
		add_theme_support('post-thumbnails');

	// Image sets
		if(isset($content_width)){
			$content_width = 1170; //pixels - Max Bootstrap width
		}



    // Script Register
        function theme_name_scripts() {
            wp_deregister_script('jquery');
            //Bootstrap 4.0.0
            wp_enqueue_style( 'mainStyle', get_template_directory_uri() . '/css/stylesheet.css', array(), rand(111,9999), 'all' );
            
            //Jquery 3.3.1.min - popper.js/1.12.9 - bootstrap/4.0.0
            wp_enqueue_script( 'jquery', get_template_directory_uri() . '/js/main.js', array(), '1.0.0', true );
            // wp_enqueue_script('google-map-api','https://maps.googleapis.com/maps/api/js?key='.get_option('api_maps').'',array(), '1.0.0', false);
        }


        add_action( 'wp_enqueue_scripts', 'theme_name_scripts' );


    //Include to Custom Posts Types
      include (TEMPLATEPATH . '/theme-settings/cpt.php' );
      include (TEMPLATEPATH . '/theme-settings/social-settings.php' );


      // Wp-Login.php customization stylesheet
          function my_login_stylesheet() {
              wp_enqueue_style( 'custom-login', get_template_directory_uri() . '/css/style-login.css' );
          }
          add_action( 'login_enqueue_scripts', 'my_login_stylesheet' );

      // Change the footer text
          function wptutsplus_admin_footer_text () {
              echo 'Tema desenvolvido por <br><a target="_blank" href="http://www.maiscode.com.br"><img src="' . get_template_directory_uri() . '/img/maiscode.png"></a>';
          }
          add_filter( 'admin_footer_text', 'wptutsplus_admin_footer_text' );
          
      // Admin styles
          function wptutsplus_admin_styles() {
              wp_register_style( 'wptuts_admin_stylesheet', get_template_directory_uri() . '/css/style-login.css' );
              wp_enqueue_style( 'wptuts_admin_stylesheet' );
          }
          add_action( 'admin_enqueue_scripts', 'wptutsplus_admin_styles' );


  // show_admin_bar(false);


      // Paginacao
      //----------------------------------------//
        function paginacao() {
              if( is_singular() )
                return;
              global $wp_query;
              /** Stop execution if there's only 1 page */
              if( $wp_query->max_num_pages <= 1 )
                return;
              $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
              $max   = intval( $wp_query->max_num_pages );
              //var_dump($max);
              /** Add current page to the array */
              if ( $paged >= 1 )
                $links[] = $paged;
              /** Add the pages around the current page to the array */
              if ( $paged >= 3 ) {
                $links[] = $paged - 1;
                $links[] = $paged - 2;
              }
              if ( ( $paged + 2 ) <= $max ) {
                $links[] = $paged + 2;
                $links[] = $paged + 1;
              }
              echo '<ul  class="pagination">' . "\n";
              /** Previous Post Link */
              if ( get_previous_posts_link() )
                printf( '<li class="text page-item">%s</li>' . "\n", get_previous_posts_link('<span class="fa fa-chevron-left"></span>') );
              /** Link to first page, plus ellipses if necessary */
              if ( ! in_array( 1, $links ) ) {
                $class = 1 == $paged ? ' class="active"' : '';
                printf( '<a href="%s"><li%s class="page-item">%s</li></a>' . "\n", esc_url(get_pagenum_link(1)), $class,'1');
                if ( ! in_array( 2, $links ) )
                  echo '<li class="page-item">…</li>';
              }

              /** Link to current page, plus 2 pages in either direction if necessary */
              sort( $links );
              foreach ( (array) $links as $link ) {
                $class = $paged == $link ? ' class="active"' : '';
                printf( '<a href="%s"><li%s class="page-item">%s</li></a>' . "\n", esc_url( get_pagenum_link( $link ) ), $class, $link );
              }
              /** Link to last page, plus ellipses if necessary */
              if ( ! in_array( $max, $links ) ) {
                if ( ! in_array( $max - 1, $links ) )
                  echo '<li class="page-item">…</li>' . "\n";
                $class = $paged == $max ? ' class="active"' : '';
                printf( '<a href="%s"><li%s class="page-item">%s</li></a>' . "\n", esc_url( get_pagenum_link( $max ) ), $class, $max );
              }

              /** Next Post Link */
              if ( get_next_posts_link() )
                printf( '<li class="txt page-item">%s</li>' . "\n", get_next_posts_link('<span class="fa fa-chevron-right"></span>') );
              echo '</ul">' . "\n";
          }



      // Custom Dashboard Widget
          add_action('wp_dashboard_setup', 'my_custom_dashboard_widgets');
          function my_custom_dashboard_widgets() {
              global $wp_meta_boxes;
              wp_add_dashboard_widget('custom_help_widget', 'Área de Suporte', 'custom_dashboard_help');
          }
        // Dashboard Box
           function custom_dashboard_help() { ?>
              <h2 style="font-weight: 700!important; text-transform: uppercase;">Suporte  <strong>MAISCODE</strong></h2>
              <p>Precisa de Ajuda? Entre em contato com nosso suporte <a href="mailto:suporte@maiscode.com.br">contato@maiscode.com.br</a> ou <a target="_blank" href="http://www.maiscode.com.br">click aqui</a> para acessar nosso site ou nos de uma ligada <a href="#">(67) 3211-8509</a> . Estamos a sua disposição para te ajudar.</p>
              <p><strong>Equipe MaisCode</strong></p>
          <?php }


    /**
     * Rename files upload
     */
    function my_custom_file_name( $filename ) {
        
        $info = pathinfo( $filename );
        $ext = empty( $info['extension'] ) ? '' : '.' . $info['extension'];
        $name = basename( $filename, $ext );        
        if ( isset( $_REQUEST['post_id'] ) && is_numeric( $_REQUEST['post_id'] ) ) {
            $postObj = get_post( $_REQUEST['post_id'] );
            $postSlug = sanitize_title( $postObj->post_title );
        }
        if(isset($postSlug) && !empty($postSlug) && $postSlug != 'rascunho-automatico')
            $finalFileName = $postSlug; // File name will be the same as the post slug.
        else
            $finalFileName = sanitize_title ($name ); // File name will be the same as the image file name, but sanitized.
        return $finalFileName . $ext;
        
    }
    add_filter( 'sanitize_file_name', 'my_custom_file_name', 100 );


add_action( 'widgets_init', 'maiscode_methodus_sidebar' );


function maiscode_methodus_sidebar() {
    
    register_sidebar(array(
            'name' => 'Lateral Artigos Cursos',
            'id'            => 'sidebar-artigos-curso',
            'description'   => __( 'Lateral Cursos', 'maiscode_methodus' ),
            'before_widget' => '<div class="sidebar-artigos-curso">',
            'after_widget' => '</div>',
    ));

     register_sidebar(array(
            'name' => 'Lateral Artigos',
            'id'            => 'sidebar-artigos',
            'description'   => __( 'Lateral Artigos', 'maiscode_methodus' ),
            'before_widget' => '<div class="sidebar-artigos">',
            'after_widget' => '</div>',
    ));


     register_sidebar(array(
            'name' => 'Lateral Artigos single',
            'id'            => 'sidebar-artigos-single',
            'description'   => __( 'Lateral Artigos Single', 'maiscode_methodus' ),
            'before_widget' => '<div class="sidebar-artigos-single">',
            'after_widget' => '</div>',
    ));
    

    register_sidebar(array(
            'name' => 'Lateral Livros',
            'id'            => 'sidebar-livros',
            'description'   => __( 'Lateral Livros', 'maiscode_methodus' ),
            'before_widget' => '<div class="sidebar-livros">',
            'after_widget' => '</div>',
    ));

    register_sidebar(array(
            'name' => 'Lateral Sobre',
            'id'            => 'sidebar-sobre',
            'description'   => __( 'Lateral Sobre Methodus', 'maiscode_methodus' ),
            'before_widget' => '<div class="sidebar-sobre">',
            'after_widget' => '</div>',
    ));

    
}


function setPostViews($postID) {
    $countKey = 'post_views_count';
    $count = get_post_meta($postID, $countKey, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $countKey);
        add_post_meta($postID, $countKey, '0');
    }else{
        $count++;
        update_post_meta($postID, $countKey, $count);
    }
}

/*
function tgm_io_cpt_search( $query ) {
  
    if ( $query->is_search ) {
      $query->set( 'post_type', array( 'artigos', 'livros','empresas','noticias','links','videos', 'post' ) );
        }
        
        return $query;
        
    }*/

$pagina_curso = 1;

function searchfilter($query) {
    if ($query->is_search) {   
        if(isset($_GET['tipo'])) {
            $type = $_GET['tipo'];   

            if($type == 'artigos') {
                $query->set('post_type',array('artigos'));
            }

            if($type == 'empresas') {
                $query->set('post_type',array('empresas'));
            }

            if($type == 'links') {
                $query->set('post_type',array('links'));
            }

            if($type == 'livros') {
                $query->set('post_type',array('livros'));
            }

            if($type == 'noticias') {
                $query->set('post_type',array('noticias'));
            }

            if($type == 'videos') {
                $query->set('post_type',array('videos'));
            } 
        }       
    }
    return $query;
}

add_filter('pre_get_posts','searchfilter');



// Executar uma página externa e entregar o resultado
function executaPagina( $pagina, $dados='', $metodo='get', $charset='utf-8' ){
    
      // URL utilizada para caminhos de arquivos (URLs amigáveis)
$url_site = get_option('link_site'); 
$url_api  =  get_option('link_api'); 

// URLs amigáveis
$url_amigavel_on  = 1;
$url_amigavel   =  get_option('link_site_amigavel');

    // global $url_site;
    
    if (strpos($pagina,'http://') === false && strpos($pagina,'https://') === false){
        if ($url_site)
            $pagina = $url_site.$pagina;
        else
            $pagina = "http://".$_SERVER['SERVER_NAME']."/".$pagina;
    }

	$headers = [
	  'Content-type: application/x-www-form-urlencoded;', 
	  'charset='.$charset
	];
	
	$apikey = '';
	
	$curl = curl_init(); // Get cURL resource
	
	if ($method == "POST") {
		curl_setopt($curl, CURLOPT_POST, true);
		$apikey = '&api_chave=m3th0du5';
	} else {			
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
	}
	
	if (!empty($dados)) {
		$qs = http_build_query($dados); // query string encode the parameters
		$request = $pagina."?{$qs}".$apikey ; // create the request URL
	} else {
		$request = $pagina.$apikey ;
	}
	
	// Set cURL options
	curl_setopt_array($curl, array(
	  CURLOPT_URL => $request,            // set the request URL
	  CURLOPT_HTTPHEADER => $headers,     // set the headers 
	  CURLOPT_POSTFIELDS => http_build_query($dados),		  // sets the request body
	  CURLOPT_RETURNTRANSFER => 1,         // ask for raw response instead of bool
	  CURLOPT_SSL_VERIFYPEER => 0
	));

	$response = curl_exec($curl); // Send the request, save the response
	$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	// $result = json_decode($response,true); // store json decoded response
	// $result = $response; // store json decoded response
	curl_close($curl); // Close request
    
    return $response;
}



// Função para verificar se arquivo existe
function checaArquivo( $arquivo ){
    if (file_exists($arquivo)) {
        return 1;
    } else {
        return 0;
    }   
}

// Ler XML
function lerXML( $arquivo ){
    
    if ( strpos($arquivo,'<?xml') === false ){
    
        if ( checaArquivo($arquivo) ) {
            $xml = simplexml_load_file($arquivo);
            return($xml);
        } else {
            return(0);
        }
    
    }else{
        $xml = simplexml_load_string($arquivo);
        return($xml);   
    }
    
}



// Função para exibir data
function formataData( $data=null, $tipo='tela' ){
  
  if ( $data ){
  
    if ( $data == 'agora' )
      $data=null;
      
    switch ($tipo){
      case 'tela':
        $tipo = 'd/m/Y';
        break;
      case 'eua':
        $tipo = 'm/d/Y';
        break;
      case 'base':
        $tipo = 'Y-m-d';
        break;
    }
    
    if ($data){
      if ( is_string($data) ){
        $data = strtotime( $data );
        return date( $tipo, $data );
      }else{
        if (is_a($data, 'DateTime')==false)  //if ( !($data instanceof DateTime) )
          return date_format( new DateTime($data), $tipo );
        else
          return date_format( $data, $tipo );
      }
    }else
      return date( $tipo );
  
  }else{
    return '';  
  }
    
}

// Função para exibir hora
function formataHora( $hora=null, $tipo='tela' ){
  
  if ( $hora ){
  
    if ( $hora == 'agora' )
      $hora=null;
      
    switch ($tipo){
      case 'tela': // 23h45
        $tipo = 'H\hi';
        break;
      case 'completo': // 23h45m12s
        $tipo = 'H\hi\ms\s';
        break;
      case 'base': // 23:45:12
        $tipo = 'H:i:s';
        break;
    }
    
    if ($hora){
      if ( is_string($hora) ){
        $hora = strtotime( $hora );
        return date( $tipo, $hora );
      }else{
        if (is_a($hora, 'DateTime')==false)
          return date_format( new DateTime($hora), $tipo );
        else
          return date_format( $hora, $tipo );
      }
    }else
      return date( $tipo );
  
  }else{
    return '';  
  }
    
}

// Função para formatar data e hora para base de dados
function dataBD( $data=null ){
  
  if ( $data ){
    //exit('teste: '.strpos( substr($data,0,5), '-' ) );
    if ( is_string($data) && strpos( substr($data,0,5), '-' ) === FALSE && $data != 'agora' ) //if ( is_string($data) && validarData($data) == false )
      $data = converteData($data);
      
    return formataData( $data, 'base' ).' '.formataHora( $data, 'base' );
  
  }else{
    return '';  
  }
    
}


//Validar se é data
function validarData($date, $format = 'Y-m-d H:i:s')
{
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) == $date;
}

// Converter data brasileira para universal
function converteData( $data=null ){
  
  if ($data){
    //$temp = substr($data,3,2).'/'.substr($data,0,2).'/'.substr($data,6,4);
    $temp = substr($data,6,4).'-'.substr($data,3,2).'-'.substr($data,0,2);
    
    // se tem hora
    if ( strlen($data) > 10 ){
      $temp.= ' '.substr($data,11,2).':'.substr($data,14,2).':'.substr($data,17,2);
    }
    
    return $temp;
  }else{
    return null;
  }
    
}



// Encodar string para texto de URL
function encodarURL( $texto ){
  
  $texto = str_replace( ' ', '-', $texto );
  
  $texto = str_replace( 'á', 'a', $texto );
  $texto = str_replace( 'ã', 'a', $texto );
  $texto = str_replace( 'à', 'a', $texto );
  $texto = str_replace( 'â', 'a', $texto );
  $texto = str_replace( 'ä', 'a', $texto );
  
  $texto = str_replace( 'é', 'e', $texto );
  $texto = str_replace( 'ê', 'e', $texto );
  $texto = str_replace( 'è', 'e', $texto );
  $texto = str_replace( 'ë', 'e', $texto );
  
  $texto = str_replace( 'í', 'i', $texto );
  $texto = str_replace( 'î', 'i', $texto );
  $texto = str_replace( 'ì', 'i', $texto );
  $texto = str_replace( 'ï', 'i', $texto );
  
  $texto = str_replace( 'ó', 'o', $texto );
  $texto = str_replace( 'ô', 'o', $texto );
  $texto = str_replace( 'õ', 'o', $texto );
  $texto = str_replace( 'ò', 'o', $texto );
  $texto = str_replace( 'ö', 'o', $texto );
  
  $texto = str_replace( 'ú', 'u', $texto );
  $texto = str_replace( 'û', 'u', $texto );
  $texto = str_replace( 'ù', 'u', $texto );
  $texto = str_replace( 'ü', 'u', $texto );
  
  $texto = str_replace( 'ç', 'c', $texto );
  
  $texto = str_replace( ',', '', $texto );
  $texto = str_replace( '.', '', $texto );
  $texto = str_replace( '?', '', $texto );
  $texto = str_replace( '!', '', $texto );
  $texto = str_replace( '\'', '', $texto );
  $texto = str_replace( '"', '', $texto );
  
  $texto = strtolower( $texto );
  $texto = urlencode( $texto ); 
  
  return $texto;
}


// Função para limpar uma variável e prevenir SQL Injection
function formataVar( $variavel, $metodo='*', $tipo='normal' ) { 
  
  // apenas para testes
  //$metodo='*';
  
  if ($metodo == 'post'){
    $texto = @$_POST[$variavel];
  }elseif ($metodo == 'get'){
    $texto = @$_GET[$variavel];
  }elseif ($metodo == 'var'){
    $texto = $variavel;
  }else{
    if (isset($_GET[$variavel])) {
      $texto = @$_GET[$variavel];
    }else{
      $texto = @$_POST[$variavel];
    } 
  }
  
  switch ($tipo){
    
    case 'textarea':
      
      $texto = str_replace( "'", "''", $texto );
      $texto = str_replace( '"', '""', $texto );
      $texto = str_replace( "<", "&lt;", $texto );
      $texto = str_replace( ">", "&gt;", $texto );
      $texto = str_replace( chr(10), "<BR />", $texto );
      
      break;
    
    case 'html':
      
      $texto = str_replace( "'", "''", $texto );
      
      break;
    
    case 'tela':
      
      $texto = str_replace( "<BR>", chr(10), $texto );
      $texto = str_replace( "<BR />", chr(10), $texto );
      $texto = str_replace( "<br>", chr(10), $texto );
      $texto = str_replace( "<br />", chr(10), $texto );
      
      break;
    
    // Se é uma variável normal
    default:
      $texto = str_replace( "'", "''", $texto );
      $texto = str_replace( '"', '""', $texto );
      $texto = str_replace( "<", "&lt;", $texto );
      $texto = str_replace( ">", "&gt;", $texto );
  
  }
  
  pararInjection($texto);

    return $texto; 
} 



// Função para limpar uma variável e prevenir SQL Injection
function pararInjection($texto=null) { 
  
  $palavras_malditas = array( 'select ', 'update ', 'drop table', 'insert into', 'show ', ' table ', 'cast(', 'delete from ', 'xp_', 'exec ', 'exec(', 'declare ', 'truncate', 'case when', '1=1--' );
  $str_erro = '';
  
  if ( $texto ){
    
    // se é um array, implodir em uma string separada por virgulas
    if ( is_array($texto) ){
      $texto = implode(',',$texto); 
    }
    
    $texto = strtolower($texto);
    foreach($palavras_malditas as $palavra){
      if ( strpos( $texto, $palavra ) ){
        $str_erro .= '- Carregou uma variável com texto: '.$texto.' | Palavra proibida: '.$palavra.PHP_EOL; 
      }
    }
  
  }else{
    
    //POST
    foreach ($_POST as $key => $value) {
      
      // se é um array, implodir em uma string separada por virgulas
      if ( is_array($value) ){
        $texto = implode(',',$value); 
      }else{
        $texto = $value ;
      }
      
      $texto = strtolower($texto);
      foreach($palavras_malditas as $palavra){
        if ( strpos( $texto, $palavra ) ){
          $str_erro .= 'POST: '.$key.': '.$texto.' | Palavra proibida: '.$palavra.PHP_EOL;    
        }
      }
    }
    
    //GET
    foreach ($_GET as $key => $value) {
      
      // se é um array, implodir em uma string separada por virgulas
      if ( is_array($value) ){
        $texto = implode(',',$value); 
      }else{
        $texto = $value ;
      }
      
      $texto = strtolower($texto);
      foreach($palavras_malditas as $palavra){
        if ( strpos( $texto, $palavra ) ){
          $str_erro .= 'GET: '.$key.': '.$texto.' | Palavra proibida: '.$palavra.PHP_EOL; 
        }
      }
    }
    
    //COOKIES
    foreach ($_COOKIE as $key => $value) {
      
      // se é um array, implodir em uma string separada por virgulas
      if ( is_array($value) ){
        $texto = implode(',',$value); 
      }else{
        $texto = $value ;
      }
      
      $texto = strtolower($texto);
      foreach($palavras_malditas as $palavra){
        if ( strpos( $texto, $palavra ) ){
          $str_erro .= 'COOKIE: '.$key.': '.$texto.' | Palavra proibida: '.$palavra.PHP_EOL;
        }
      }
    }
      
  }
  
  // se tem erro, não executar página e gravar LOG
  if ( empty($str_erro)==false ){
    //criarLog( $str_erro );
    exit('ERRO: falha nos inputs de dados.'); 
  }
}



// Para formatar moeda
function formataMoeda( $valor=0, $tipo='tela' ){
  
  $valor = floatval($valor);
  
  if ( $tipo == 'base' ){
    return number_format($valor, 2, '.', '');
  }else{
    return number_format($valor, 2, ',', '.');  
  }
}