  <?php
	// Paginação

	// total de páginas
	$total_paginas = ceil($quantidade/$paginacao_tamanho);


	// Página atual
	$pagina_atual = ceil( ($paginacao_inicio/$paginacao_tamanho) );

	// var_dump($pagina_atual);
	if ( $total_paginas > 0 ){

		// Verificar se existe uma página anterior
		if ( $paginacao_inicio && $paginacao_inicio > 1 ){
			$pagina_anterior = $paginacao_inicio - $paginacao_tamanho ;
			
			echo 	'<li>
						<a href="#'.$pagina_anterior.'" aria-label="Anterior">
							<span aria-hidden="true">&laquo;</span>
						</a>
					</li>';
		}
		
		// Listar as páginas intermediárias
		if ( $pagina_atual < 3 ){
			$pag_inicio = 1;
			$pag_fim	= 5;
		}elseif ( $pagina_atual+2 > $total_paginas ){
			$pag_inicio = $total_paginas-5;
			$pag_fim	= $total_paginas;
		}else{
			$pag_inicio = $pagina_atual-2;
			$pag_fim	= $pagina_atual+2;	
		}
		
		if ($pag_inicio <= 0){
			$pag_inicio = 1;	
		}
		if ($pag_fim > $total_paginas){
			$pag_fim = $total_paginas;	
		}
		
		for ($i=$pag_inicio; $i<=$pag_fim; $i++){
			
			if ($i == $pagina_atual){
				echo '<li class="active">';
			}else{
				echo '<li>';	
			}
			echo	'	<a href="#'.((($i-1)*$paginacao_tamanho)+1).'">'.$i.'</a>
					</li>';
		}
		
		
		// Verificar se existe uma página seguinte
		if ( ($paginacao_inicio + $paginacao_tamanho) <= $quantidade ){
			$pagina_seguinte = $paginacao_inicio + $paginacao_tamanho ;
			
			echo 	'<li>
						<a href="#'.$pagina_seguinte.'" aria-label="Seguinte">
							<span aria-hidden="true">&raquo;</span>
						</a>
					</li>';
		}	

	}
	?>	