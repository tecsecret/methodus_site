<?php
/**
*Author: Mais Code Tecnologia - Equipe Gestão Ativa
*Tema: Modelo Padrao - WordPress Theme
*Produção: Mais Code - Revisão Rodolfo
*Arquivo: Arquivo de Redes Sociais
*@link http://www.maiscode.com.br
*@package WordPress Communicate English Theme
*@since: 23/04/2018
*/

add_action('admin_menu', 'create_menu');
//add_action('widgets_init', create_function('', 'return '));

add_action( 'widgets_init', 'wpdocs_register_widgets' );
 
/**
 * Register the new widget.
 *
 * @see 'widgets_init'
 */
function wpdocs_register_widgets() {
    register_widget( 'social_widget' );
}
function create_menu() {		

	add_menu_page(
		'Contato', 
		'Contato', 
		'manage_options', 
		'espaco-contato', 
		'settingsSobre_page', 
		'dashicons-share');
	add_submenu_page( 
		'espaco-contato', 
		'Redes Sociais', 
		'Redes Sociais', 
		'manage_options',
		'redes-socias', 
		'settingsSocial_page');

	add_submenu_page(
		'options-general.php',
		'Tag Manager',
		'Tag Manager',
		'manage_options',
		'tag-manager',
		'wp_tag_manager_submenu_page' );

		//call register settings function
	add_action( 'admin_init', 'register_mysettings' );
	add_action( 'admin_init', 'register_mysettingsSobre' );
	add_action( 'admin_init', 'register_mysettingsTag' );
}


function register_mysettings() {
	register_setting( 'settings-group', 'facebook' );
	register_setting( 'settings-group', 'twitter' );
	register_setting( 'settings-group', 'google' );
	register_setting( 'settings-group', 'youtube' );
	register_setting( 'settings-group', 'instagram' );
	register_setting( 'settings-group', 'linkedin' );
	register_setting( 'settings-group', 'texto_redes_sociais' );
}

function register_mysettingsSobre() {
	register_setting( 'settings-group-sobre', 'email' );
	register_setting( 'settings-group-sobre', 'telefone' );
	register_setting( 'settings-group-sobre', 'endereco' );
	register_setting( 'settings-group-sobre', 'whatsapp_texto' );
	register_setting( 'settings-group-sobre', 'whatsapp' );
	register_setting( 'settings-group-sobre', 'header_logo' );
	register_setting( 'settings-group-sobre', 'aluno' );
	register_setting( 'settings-group-sobre', 'link_aluno' );
	register_setting( 'settings-group-sobre', 'link_site' );

	register_setting( 'settings-group-sobre', 'link_api' );
	register_setting( 'settings-group-sobre', 'cabecalho_informacao' );
}
function register_mysettingsTag() {
	register_setting( 'settings-group-tag', 'tag_head' );
	register_setting( 'settings-group-tag', 'tag_body' );
	register_setting( 'settings-group-tag', 'api_maps' );
}


function mypage() { ?>

<?php }
// Setter
function settingsSobre_page() {
	?>
	<div class="wrap">
		<!--area exibida no painel no wordpress-->
		<form method="post" action="options.php" style="float:left; width:70%">
			<?php settings_fields( 'settings-group-sobre' ); ?>
			<fieldset style=" width:100%; padding:20px; margin:30px;">
				<h2 style="font-weight:bold; font-size:24px;">Header</h2>
                <div class="clear" style="margin-bottom:20px">
                                <label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Logo do header</label>
                                <?php 
                                if(function_exists( 'wp_enqueue_media' )){
                                    wp_enqueue_media();
                                }else{
                                    wp_enqueue_style('thickbox');
                                    wp_enqueue_script('media-upload');
                                    wp_enqueue_script('thickbox');
                                } ?>
                                <img class="header_logo" src="<?php echo get_option('header_logo'); ?>" height="100" width="250" style="margin-bottom: 10px;"/><br>
                                <input class="header_logo_url" type="hidden" name="header_logo" size="60" value="<?php echo get_option('header_logo'); ?>">
                                <a href="#" class="page-title-action header_logo_upload">Upload</a>
                    <script>
                        jQuery(document).ready(function($) {
                            $('.header_logo_upload').click(function(e) {
                                e.preventDefault();
                                var custom_uploader = wp.media({
                                    title: 'Custom Image',
                                    button: {
                                        text: 'Upload Image'
                                    },
                                    multiple: false  // Set this to true to allow multiple files to be selected
                                })
                                .on('select', function() {
                                    var attachment = custom_uploader.state().get('selection').first().toJSON();
                                    $('.header_logo').attr('src', attachment.url);
                                    $('.header_logo_url').val(attachment.url);
                                })
                                .open();
                            });
                        });
                    </script>
                    <div class="clear" style="margin-bottom:20px">
                    	
                    	<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Botão para Área do aluno</label>
                    	<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="aluno" type="text" value="<?php echo get_option('aluno'); ?>" />
                    	<p style="color: #999999;font-size: 11px;margin: 5px 0 0 5px">Ex: Área do aluno</p>
                    </div>
                    <div class="clear" style="margin-bottom:20px">
                    	<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Link para Área do Aluno</label>
                    	<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="link_aluno" type="text" value="<?php echo get_option('link_aluno'); ?>" />
                    	<p style="color: #999999;font-size: 11px;margin: 5px 0 0 5px">Ex: https://api.methodus.com.br/asa/login/</p>
                    </div>

                   <div class="clear" style="margin-bottom:20px">
                    	<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Link Site</label>
                    	<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="link_site" type="text" value="<?php echo get_option('link_site'); ?>" />
                    	<p style="color: #999999;font-size: 11px;margin: 5px 0 0 5px">Ex: https://api.methodus.com.br/</p>
                    </div>


                   <div class="clear" style="margin-bottom:20px">
                    	<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Link API</label>
                    	<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="link_api" type="text" value="<?php echo get_option('link_api'); ?>" />
                    	<p style="color: #999999;font-size: 11px;margin: 5px 0 0 5px">Ex: www.methodus/api/</p>
                    </div>
                </div>
				<h2 style="font-weight:bold; font-size:24px;">Contato</h2>

				<?php if (isset($_GET['settings-updated'])) : ?>
					<div class="notice notice-success is-dismissible"><p>Atualizado com sucesso.</p></div>
				<?php endif; ?>

				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">E-Mail</label>
					<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="email" type="text" value="<?php echo get_option('email'); ?>" />
					<p style="color: #999999;font-size: 11px;margin: 5px 0 0 5px">Ex: info@methodus.com.br</p>
				</div>

				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Telefone</label>
					<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="telefone" type="text" value="<?php echo get_option('telefone'); ?>" />
					<p style="color: #999999;font-size: 11px;margin: 5px 0 0 5px">Ex: (99) 9 9999-9999</p>
				</div>

				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Whatsapp Texto</label>
					<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="whatsapp_texto" type="text" value="<?php echo get_option('whatsapp_texto'); ?>" />
					<p style="color: #999999;font-size: 11px;margin: 5px 0 0 5px">Ex: Chamar no Whatsapp</p>
				</div>

				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Whatsapp</label>
					<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="whatsapp" type="text" value="<?php echo get_option('whatsapp'); ?>" />
					<p style="color: #999999;font-size: 11px;margin: 5px 0 0 5px">Ex: (99) 9 9999-9999</p>
				</div>

				<h2 style="font-weight:bold; font-size:24px;">Endereço</h2>

				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Endereço</label>
					<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="endereco" type="text" value="<?php echo get_option('endereco'); ?>" />
					<p style="color: #999999;font-size: 11px;margin: 5px 0 0 5px">Ex: Endereço</p>
				</div>
		

				<div class="clear" style="margin-bottom:20px">
					<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Copyright</label>
					<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="cabecalho_informacao" type="text" value="<?php echo get_option('cabecalho_informacao'); ?>" />
					<p style="color: #999999;font-size: 11px;margin: 5px 0 0 5px">Ex: © Copyright 2010 - Todos os direitos reservados à Methodus</p>
				</div>


				<div style="clear:both"></div>
				<p class="submit">
					<input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
				</p>

			</fieldset>		

		</form>
	</div>

	<?php };




	function settingsSocial_page() {
		?>
		<div class="wrap">
			<!--area exibida no painel no wordpress-->
			<form method="post" action="options.php" style="float:left; width:70%">
				<?php settings_fields( 'settings-group' ); ?>
				<fieldset style=" width:100%; padding:20px; margin:30px;">
					<h2 style="font-weight:bold; font-size:24px;">Configurações de Redes Sociais</h2>

					<?php if (isset($_GET['settings-updated'])) : ?>
						<div class="notice notice-success is-dismissible"><p>Atualizado com sucesso.</p></div>
					<?php endif; ?>

				
					<div class="clear" style="margin-bottom:20px">
						<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Twitter</label>
						<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="twitter" type="text" value="<?php echo get_option('twitter'); ?>" />
						<p style="color: #999999;font-size: 11px;margin: 5px 0 0 5px">Ex: http://www.twitter.com/</p>
					</div>
					<div class="clear" style="margin-bottom:20px">
						<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Facebook</label>
						<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="facebook" type="text" value="<?php echo get_option('facebook'); ?>" />
						<p style="color: #999999;font-size: 11px;margin: 5px 0 0 5px">Ex: http://www.facebook.com/</p>
					</div>

					<div class="clear" style="margin-bottom:20px">
						<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Youtube</label>
						<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="youtube" type="text" value="<?php echo get_option('youtube'); ?>" />
						<p style="color: #999999;font-size: 11px;margin: 5px 0 0 5px">Ex: http://www.youtube.com/</p>
					</div>
					<div class="clear" style="margin-bottom:20px">
						<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Instagram</label>
						<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="instagram" type="text" value="<?php echo get_option('instagram'); ?>" />
						<p style="color: #999999;font-size: 11px;margin: 5px 0 0 5px">Ex: http://www.instagram.com/</p>
					</div>
					<div class="clear" style="margin-bottom:20px">
						<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">LinkedIn</label>
						<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="linkedin" type="text" value="<?php echo get_option('linkedin'); ?>" />
						<p style="color: #999999;font-size: 11px;margin: 5px 0 0 5px">Ex: http://www.linkedin.com/</p>
					</div>

					<div class="clear" style="margin-bottom:20px">
						<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Texto redes sociais</label>
						<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="texto_redes_sociais" type="text" value="<?php echo get_option('texto_redes_sociais'); ?>" />
						<p style="color: #999999;font-size: 11px;margin: 5px 0 0 5px">Ex: Encontre-me também nas redes sociais e fique por dentro de todas as novidades e conteúdos exclusivos de cada plataforma.</p>
					</div>


					<div style="clear:both"></div>
					<p class="submit">
						<input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
					</p>
				</fieldset>		

			</form>
		</div>
		<?php };

	// Getter
		function redes_sociais(){
			$facebook = get_option('facebook'); 
			$twitter = get_option('twitter'); 
			$google = get_option('google');
			$youtube = get_option('youtube');
			$instagram = get_option('instagram');
			$linkedin = get_option('linkedin');
			echo "<ul class='list-inline list-unstyled' id='redes_sociais'>";
			if (!empty($facebook)) {
				echo "<a target='_blank' href='$facebook'><li><span class='fa fa-facebook-square'></span></li></a>";
			};
			if (!empty($twitter)) {
				echo "<a target='_blank' href='$twitter'><li><span class='fa fa-twitter'></span></li></a>";
			}
			if (!empty($google)) {
				echo "<li><a target='_blank' href='$google'><span class='fa fa-google'></span></a></li>";
			}
			if (!empty($instagram)) {
				echo "<a target='_blank' href='$instagram'><li><span class='fa fa-instagram'></span></li></a>";
			}
			if (!empty($linkedin)) {
				echo "<a target='_blank' href='$linkedin'><li><span class='fa fa-linkedin'></span></li></a>";
			}
			if (!empty($youtube)) {
				echo "<a target='_blank' href='$youtube'><li><span class='fa fa-youtube'></span></li></a>";
			}
			echo "</ul>";
		}

		class social_widget extends WP_Widget {

	    // constructor

	    function __construct() {
	        // Instantiate the parent object.
	        parent::__construct( false, __( 'Links Redes Sociais', 'wp_widget_plugin' ) );
	    }
	 
	    // display widget
			function widget($args, $instance) {
				extract( $args );

				echo $before_widget; ?>

				<div class="widget-text social_widget_box">
					<?php redes_sociais(); ?>
				</div>

				<?php echo $after_widget;
			}
		}

/* Google Tag Manager
/---------------------------------------------------------------------------- */

function wp_tag_manager_submenu_page() { ?>
<div class="wrap">
	<div id="icon-tools" class="icon32"></div>
	<form method="post" action="options.php" style="float:left; width:70%">
		<?php settings_fields( 'settings-group-tag' ); ?>
		<fieldset style=" width:100%; padding:20px; margin:30px;">
			<h2 style="font-weight:bold; font-size:24px;">Google Tag Manager</h2>

			<div class="clear" style="margin-bottom:20px">
				<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Script Tag Manager HEAD</label>
				<textarea style="width:100%; background:#fff; border-radius:5px; height:100px" name="tag_head" type="text"><?php echo get_option('tag_head'); ?></textarea>
			</div>

			<div class="clear" style="margin-bottom:20px">
				<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">Script Tag Manager BODY</label>
				<textarea  style="width:100%; background:#fff; border-radius:5px; height:100px" name="tag_body"><?php echo get_option('tag_body'); ?></textarea>
			</div>

			<div class="clear" style="margin-bottom:20px">
				<label style="font-weight:bold; color:#173C69; margin: 10px 0 3px 0; display:block; font-size:16px;">API Google Maps</label>
				<input style="width:100%; background:#fff; border-radius:5px; height:40px" name="api_maps" type="text" value="<?php echo get_option('api_maps'); ?>" />

			</div>

			<div style="clear:both"></div>
			<p class="submit">
				<input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
			</p>

		</fieldset>		

	</form>
</div>
<?php
}

?>