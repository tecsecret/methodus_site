<%Response.Expires = 0%>
<% 
Response.Buffer=true 
Response.AddHeader "cache-control", "private" 
Response.AddHeader "pragma", "no-cache" 
Response.AddHeader "Cache-Control", "must-revalidate" 
Response.AddHeader "Cache-Control", "no-cache" 
Response.Addheader "Last-Modified:", Now() 
%>
<%
'Cria o Objeto pra manipular a nossa imagem
Set Jpeg = Server.CreateObject("Persits.Jpeg")

'Abre nossa imagem de fundo
Jpeg.Open Server.MapPath("seguranca_fundo.gif")

'Configuração de cor, nome e tamanho da fonte
Jpeg.Canvas.Font.Color = &H000000
Jpeg.Canvas.Font.Family = "Courier New"
Jpeg.Canvas.Font.Size = 29

'Define a variavel de sessão "seguranca"
'e guarda nela o valor de nosso código
Session("seguranca") = GerarCodigo

'Escreve na imagem o valor do código
Jpeg.Canvas.Print 10, 0, Session("seguranca")

'Envia ao browser, os dados binarios da imagem
Jpeg.SendBinary
%>

<%
Private Function GerarCodigo()
Dim valores, i

'Inicia a função com valor em branco
GerarCodigo = ""

'Define um vetor com os elementos de nosso código
valores = Array("0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z")

	Randomize
	For i = 1 to 5
		GerarCodigo = GerarCodigo & valores(Int(uBound(valores) * Rnd))
	Next
End Function
%>