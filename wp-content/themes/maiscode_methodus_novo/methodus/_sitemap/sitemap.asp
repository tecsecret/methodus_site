<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.google.com/schemas/sitemap/0.84" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.google.com/schemas/sitemap/0.84 http://www.google.com/schemas/sitemap/0.84/sitemap.xsd">
<!-- #include file="../../facix2/_base/config.asp" -->
<!-- #include file="../_base/config.asp" -->
<url>
    <loc>http://www.methodus.com.br/_ambiente_aula/methodus/home/</loc>
    <changefreq>daily</changefreq>
    <priority>0.8</priority>
    <lastmod><%=FORMATA_DATA(now(),3)%>T<%=FORMATA_HORA(now(),4)%>Z</lastmod>
</url>
<url>
    <loc>http://www.methodus.com.br/_ambiente_aula/methodus/contato/</loc>
    <changefreq>daily</changefreq>
    <priority>0.8</priority>
    <lastmod><%=FORMATA_DATA(now(),3)%>T<%=FORMATA_HORA(now(),4)%>Z</lastmod>
</url>
<%
sql = 	"select CURSOS.* from CURSOS"&_
		" where status_curso="&verdade&" order by titulo_curso"
set rs = conexao.execute(sql)

do while not rs.EOF
%>
<url>
    <loc>http://www.methodus.com.br/_ambiente_aula/methodus/cursos/curso_default.asp?ID=<%=rs("ID_curso")%></loc>
    <changefreq>daily</changefreq>
    <priority>0.8</priority>
    <lastmod><%=FORMATA_DATA(now(),3)%>T<%=FORMATA_HORA(now(),4)%>Z</lastmod>
</url>
<%
rs.movenext:loop
%>
<%
sql = 	"select ID_noticia, titulo_noticia, data_noticia from NOTICIAS where status_noticia="&verdade&" and data_noticia <='"&DATA_BD(now())&"' order by data_noticia desc"
set rs = conexao.execute(sql)

do while not rs.EOF
%>
<url>
    <loc>http://www.methodus.com.br/_ambiente_aula/methodus/noticias/detalhes.asp?ID=<%=rs("ID_noticia")%></loc>
    <changefreq>daily</changefreq>
    <priority>0.8</priority>
    <lastmod><%=FORMATA_DATA(rs("data_noticia"),3)%>T<%=FORMATA_HORA(rs("data_noticia"),4)%>Z</lastmod>
</url>
<%
rs.movenext:loop
%>
<%
sql = 	"select ID_artigo, titulo_artigo, data_artigo from ARTIGOS where status_artigo="&verdade&" and data_artigo <='"&DATA_BD(now())&"' order by data_artigo desc"
set rs = conexao.execute(sql)

do while not rs.EOF
%>
<url>
    <loc>http://www.methodus.com.br/_ambiente_aula/methodus/artigos/detalhes.asp?ID=<%=rs("ID_artigo")%></loc>
    <changefreq>daily</changefreq>
    <priority>0.8</priority>
    <lastmod><%=FORMATA_DATA(rs("data_artigo"),3)%>T<%=FORMATA_HORA(rs("data_artigo"),4)%>Z</lastmod>
</url>
<%
rs.movenext:loop
%>
<%
sql = 	"select ID_artigo, titulo_artigo, data_artigo from ARTIGOS_CARREIRAS where status_artigo="&verdade&" and data_artigo <='"&DATA_BD(now())&"' order by data_artigo desc"
set rs = conexao.execute(sql)

do while not rs.EOF
%>
<url>
    <loc>http://www.methodus.com.br/_ambiente_aula/methodus/artigos_carreiras/detalhes.asp?ID=<%=rs("ID_artigo")%></loc>
    <changefreq>daily</changefreq>
    <priority>0.8</priority>
    <lastmod><%=FORMATA_DATA(rs("data_artigo"),3)%>T<%=FORMATA_HORA(rs("data_artigo"),4)%>Z</lastmod>
</url>
<%
rs.movenext:loop
%>
<%
sql = 	"select ID_video, titulo_video, data_video from VIDEOS where status_video="&verdade&" and data_video <='"&DATA_BD(now())&"' order by data_video desc"
set rs = conexao.execute(sql)

do while not rs.EOF
%>
<url>
    <loc>http://www.methodus.com.br/_ambiente_aula/methodus/videos/detalhes.asp?ID=<%=rs("ID_video")%></loc>
    <changefreq>daily</changefreq>
    <priority>0.8</priority>
    <lastmod><%=FORMATA_DATA(rs("data_video"),3)%>T<%=FORMATA_HORA(rs("data_video"),4)%>Z</lastmod>
</url>
<%
rs.movenext:loop
%>
<%
sql = 	"select ID_conteudo, titulo_conteudo from CONTEUDOS where status_conteudo="&verdade&" order by titulo_conteudo desc"
set rs = conexao.execute(sql)

do while not rs.EOF
%>
<url>
    <loc>http://www.methodus.com.br/_ambiente_aula/methodus/conteudo/default.asp?ID=<%=rs("ID_conteudo")%></loc>
    <changefreq>daily</changefreq>
    <priority>0.8</priority>
    <lastmod><%=FORMATA_DATA(now(),3)%>T<%=FORMATA_HORA(now(),4)%>Z</lastmod>
</url>
<%
rs.movenext:loop
%>
</urlset>