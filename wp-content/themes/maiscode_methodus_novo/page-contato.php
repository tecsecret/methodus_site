<?php
/**
* Template Name: Página de Contato
*
* @package WordPress
* @author Mais Code Tecnologia
* @since First Version
*/
get_header(); 

$url_api      =  get_option('link_api');


?>


<section class="p0">
    <div class="map-holder pt160 pb160">
        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d6150.828257181422!2d-46.66228144720525!3d-23.5575098142689!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce59cd53abddf7%3A0xf5c2726314a848a1!2sAv.+Paulista%2C+2202+-+Consola%C3%A7%C3%A3o%2C+S%C3%A3o+Paulo+-+SP%2C+01310-300%2C+Brasil!5e0!3m2!1spt-BR!2sus!4v1491314726852" frameborder="0" style="border:0" allowfullscreen=""></iframe>
    </div>
</section>

<section id="contato-conteudo">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-6">
                <h4><?php the_field('titulo_contato'); ?></h4>
                <p>
                    <?php the_field('breve_texto_para_contato'); ?>
                </p>
                <hr>
                <p class="endereco-contato">
                    <?php echo get_option('endereco'); ?>
                </p>
                <hr>
                <p><i class="fas fa-phone-alt"></i> <?php echo get_option('telefone'); ?></p>
                <p><i class="fab fa-whatsapp"></i> <?php echo get_option('whatsapp'); ?></p>
                <p><i class="far fa-envelope"></i> <?php echo get_option('email'); ?></p> 
            </div>
            <div class="col-sm-6 col-md-5 col-md-offset-1">
                    <script src="<?php bloginfo('template_url'); ?>/js/scripts_site/contato.js"></script>
                    <form class="form-email formAjax" action="<?php echo $url_api; ?>/api/" method="post" data-validacao="checaContato" data-resultado="contatoSucesso">
                        <input type="hidden" name="a" value="contato" />
                        <input type="text" class="validate-required" name="nome" placeholder="Seu nome" lang="Nome" />
                        <input type="text" class="validate-required validate-email" name="email" placeholder="Seu email" lang="E-mail" />
                        <textarea class="validate-required" name="mensagem" rows="4" placeholder="Digite aqui sua mensagem" lang="Mensagem"></textarea>
                        
                        <script src="https://www.google.com/recaptcha/api.js" async defer></script>
                        <div class="g-recaptcha" data-sitekey="6LfSbAsTAAAAANWurZ8wHzk5-kvEIUGJOA5Dhgbp"></div>
                        <BR />
                        
                        <button type="submit">Enviar mensagem</button>
                    </form>

            </div>
        </div>
        <!--end of row-->
    </div>
    <!--end of container-->
</section>

<?php get_footer(); ?>