<?php
/**
* Template Name: Curso Orataria
* @package WordPress
* @author Mais Code Tecnologia
* @since First Version
*/

$url_site = get_option('link_site'); 
$url_api  =  get_option('link_api'); 
$url_amigavel_wp = home_url(); 
// URLs amigáveis
$url_amigavel_on  = 1;
$url_amigavel   =  get_option('link_site_amigavel');

$pagina_curso = 1;

$urlWP = get_template_directory_uri();
// $urlWP = site_url();
// $urlWP = 'https://www.methodus.com.br/';
  
// Verificar
$ID = get_field('id_do_curso');
$ID_Depoimento_Curso = get_field('id_depoimento_curso');
$ebookWp = get_field('ebook');
$xml_curso  = executaPagina( 'api/', array( 'a'=>'cursos', 'metodo'=>'ver', 'ID'=>$ID ) );
$curso      = lerXML( $xml_curso );
    

// var_dump($curso);
    
if ( $curso->erro == 0 ){
    
    $curso              = $curso->curso;
    $status             = $curso->status;
    $titulo_curso       = $curso->titulo;
    $url_curso          = $curso->url;
    $distancia_curso    = $curso->distancia;
    $banner_curso       = $curso->banner;
    $resumo_curso       = $curso->resumo;
    $descricao_curso    = $curso->descricao;
    $variaveis_curso    = $curso->variaveis; 
    $planos_curso       = $curso->planos;
    $turmas_curso       = $curso->turmas->turma;
    $perfis_curso       = $curso->perfis->perfil;
    $ID_tema            = $curso->tema;
    
    $seo_metatags       = $curso->metatags;
    $seo_title          = $titulo_curso.' - Methodus';
    $seo_imagem         = 'https://'.$_SERVER['HTTP_HOST'].''.$banner_curso;
    
    if ( isset( $variaveis_curso->classe ) ){
        $classe_curso = $variaveis_curso->classe;
    }else{
        $classe_curso = 'vermelho'; 
    }
    
    if ( isset( $curso->fotos ) ){
        $fotos_curso = $curso->fotos->foto;
    }

    if ( $status != 1 ){
        exit("Erro ao acessar os dados do curso 1");
    }
    
}else{
    exit("Erro ao acessar os dados do curso");      
}

?>


<?php get_header(); ?>

<style type="text/css">
    .caixa_ebook img{
    width: 170px;
  }
        .btn_top{
            color: #fff !important;
        }
        .btn_top:hover{
            background-color: #fff !important;
            color: #a00000 !important;
        }
        .btn_pg_curso{
            border: 2px solid #a00000 !important;;
        }
        .btn_pg_curso:hover{
            background-color: #fff !important;
            color: #a00000 !important;
        }
        .btn_chamada:hover{
            color: #a00000 !important;
        }
    </style><!-- TOPO -->

<style type="text/css">
  .courseColor {
    color: #a00000;
  }
  .courseBackgroundColor {
    background-color: #a00000;
  }
  #secStoryModB .storyContent svg path {
    fill: #a00000;
  }
  .sliderCard svg path {
    fill: #a00000;
  }
  #secTopModB:before {
    content: '';
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    background: linear-gradient(270deg, rgba(160, 0, 0, 0.7) 23.16%, rgba(52, 50, 50, 0) 118.16%);
  }
  #secCallBannerModB .bgTransformation:before {
    content: '';
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    background: linear-gradient(179.64deg, rgba(96, 94, 94, 0) -36.93%, rgba(160, 0, 0, 0.6) 85.54%);
  }
  .loadd{
    display: none;
    opacity: .5;
    background: #000;
    width: 100%;
    position: absolute;
    top: 0px;
    height: 100vh;
    z-index: 10000000;
  }
 
  .sliderCardDepo .depo-prev
  {
    height: 36px;
    width: 220px;
    z-index: 100000;
    object-fit: contain;
  }
  .sliderCardDepo .depo-next{
    width: 235px;
    height: 36px;
    object-fit: contain;
  }
</style>



<section id="secTopModB" style="background-image: url('https://methodus.com.br/wp-content/uploads/2020/08/curso-de-oratoria-lp.png')">
  <div class="container">
    <div class="row">
      <div class="col-md-7 col-md-offset-5 col-sm-7 col-sm-offset-5 topContent">
        <h2>Finalmente, uma maneira testada e aprovada de...</h2>
        <p>ao invés de falar vamos deixar você assistir</p>
        <img src="https://methodus.com.br/wp-content/uploads/2020/08/curso-de-oratoria-lp-1.png">
      </div>
    </div>
  </div>
</section>

        <?php
    if ( isset( $variaveis_curso->ebook_download ) ){
        $temp_ebook = explode( '|', $variaveis_curso->ebook_download );

        if ( count( $temp_ebook ) >= 4 ){

            $titulo_ebook           = $temp_ebook[0];
            $descricao_ebook        = $temp_ebook[1];
            $destino_ebook          = $temp_ebook[2];
            $identificador_ebook    = $temp_ebook[3];
            $urlWP = get_template_directory_uri();
            ?>


            <section class="caixa_ebook ">
                <div class="container-fluid bg_ebook">
                    <div class="container">                       
                 
                        <script src="<?php echo $urlWP; ?>/js/scripts_site/ebook.js?ver=1"></script>
                        <form action="<?php echo $url_api; ?>/api/" method="post" class="form-email formAjax" id="form_ebook" data-validacao="checaEbook" data-resultado="sucessoEbook">
                            <input type="hidden" name="destino" value="<?php echo $ebookWp ?>" />
                            <input type="hidden" name="a" value="aluno" />
                            <input type="hidden" name="metodo" value="cadastro" />
                            <input type="hidden" name="origem" value="Ebook_<?php echo $identificador_ebook; ?>" />
                            <input type="hidden" name="curso" value="<?php echo $ID; ?>" />
                    
                            <div class="row">
                                <div class="col-xs-12 col-md-8">
                                    <img src="<?php echo $urlWP ?>/img/ebook_<?php echo $classe_curso; ?>.png" width="80" />
                                    <div class="chamada-ebook"><?php echo $descricao_ebook ?></div>
                                </div>
                                <div class="col-xs-12 col-md-4">
                                    <h5 class="mb0 inline-block p0-xs">
                                        <?php echo $titulo_ebook ?>
                                    </h5>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <input type="text" class="validate-required cada-input" name="nome" placeholder="Nome" lang="Nome">
                                        </div>
                                        <div class="col-sm-12 form-ebook">
                                            <input type="text" class="validate-required cada-input" name="email" placeholder="E-mail" lang="E-mail">
                                            <?php
                                            $i = 0;
                                            if( have_rows('formulario') ):
                                                while ( have_rows('formulario') ) : the_row(); $i++?> 
                                                    <div class="radio_cursos">  
                                                        <label for="ebook-<?php echo $i; ?>" class="radio">
                                                            <input type="radio" class="validate-required" id="ebook-<?php echo $i; ?>" name="conheceu" value="<?php the_sub_field('opcoes'); ?>" lang="opções">
                                                            <span></span>
                                                        </label>          
                                                        <label for="ebook-<?php echo $i; ?>"><?php the_sub_field('opcoes'); ?></label><br>
                                                    </div>
                                                <?php endwhile;
                                            endif;
                                            ?>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <button class="btn <?php echo $classe_curso; ?>" type="submit">Download</button>
                                        </div>
                                    </div>
                                </div>
                            </div>                            
                        </form>
                    </div>
                </div>
            </section>
        <?php
        }
    }
    ?>

<!-- VIDEO -->
<section id="secVideoModB">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h3 class="courseColor">Andreia</h3>
        <h5 class="courseColor">Aluna da Methodus no Curso de Oratória</h5>
      </div>
                                          <div class="col-md-6 col-sm-6 videoContent text-center">
            <h5 class="courseColor">Primeira aula</h5>

            <div class="local-video-container">
              <div class="background-image-holder" style="background: url(&quot;https://methodus.com.br/wp-content/uploads/2020/08/curso-de-oratoria-lp-2.png&quot;); "> 
                <img alt="Background Image" class="background-image" src="https://methodus.com.br/wp-content/uploads/2020/08/curso-de-oratoria-lp-2.png" style="display: none;"> 
              </div>
              <video controls="">
                <source src="https://methodus.com.br/wp-content/uploads/2020/08/editado-final-aula-1.mp4" type="video/mp4">                                
              </video> 
              <div class="play-button" style="opacity: 0;"></div>
            </div>
            <ul class="videoDescription">
                                                <li style="color: #a00000"><span>Mãos para baixo</span></li>
                                  <li style="color: #a00000"><span>Introversão</span></li>
                                  <li style="color: #a00000"><span>Parada no palco</span></li>
                                  <li style="color: #a00000"><span>Discurso rápido</span></li>
                                  <li style="color: #a00000"><span>Voz “para dentro”</span></li>
                                  <li style="color: #a00000"><span>Sorriso “sem graça”</span></li>
                                  <li style="color: #a00000"><span>Tímida</span></li>
                                  <li style="color: #a00000"><span>Insegurança</span></li>
                            </ul>
          </div>
                            <div class="col-md-6 col-sm-6 videoContent text-center">
            <h5 class="courseColor">Décima aula</h5>

            <div class="local-video-container">
              <div class="background-image-holder" style="background: url(&quot;https://methodus.com.br/wp-content/uploads/2020/08/curso-de-oratoria-lp-2.png&quot;); "> 
                <img alt="Background Image" class="background-image" src="https://methodus.com.br/wp-content/uploads/2020/08/curso-de-oratoria-lp-2.png" style="display: none;"> 
              </div>
              <video controls="">
                <source src="https://methodus.com.br/wp-content/uploads/2020/08/aula-10-editado-final.mp4" type="video/mp4">                                
              </video> 
              <div class="play-button" style="opacity: 0;"></div>
            </div>
            <ul class="videoDescription">
                                                <li style="color: #0df205"><span>Mãos expressivas</span></li>
                                  <li style="color: #0df205"><span>Olhar confiante</span></li>
                                  <li style="color: #0df205"><span>Voz assertiva e profissional</span></li>
                                  <li style="color: #0df205"><span>Segurança ao caminhar</span></li>
                                  <li style="color: #0df205"><span>Liga o discurso à apresentação</span></li>
                                  <li style="color: #0df205"><span>Tem a atenção do público</span></li>
                                  <li style="color: #0df205"><span>Domínio do palco</span></li>
                            </ul>
          </div>
            </div>
  </div>
</section>
 
<!-- BANNER CHAMADA -->
<section id="secCallBannerModB" class="courseBackgroundColor">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2>A transformação de Andreia é de dar orgulho!</h2>
                  <ul>
                          <li>
                VERGONHA              </li>
                          <li>
                MEDO              </li>
                          <li>
                DAR BRANCO              </li>
                          <li>
                FRUSTRAÇÃO              </li>
                      </ul>
                <p>Agora repetimos para você:
        <span>&#x25cf;</span>
        <span>&#x25cf;</span>
        <span>&#x25cf;</span></p>
      </div>
    </div>
  </div>
    <div class="bgTransformation" style="background-image: url('https://methodus.com.br/wp-content/uploads/2020/08/curso-de-oratoria-lp-3.png')">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h2>Finalmente, uma maneira testada e aprovada de vencer o medo de falar em público!</h2>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- HISTORIA -->
<section id="secStoryModB">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="storyContent">
              </div>
      </div>
  </div>
    <div class="row">
      <div class="col-md-12">
        <h2 class="courseColor">&#x25cf; A historia de Andreia &#x25cf;</h2>
        <div class="storyContent">
          <svg class="quoteStart" width="71" height="56" viewBox="0 0 71 56" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M16.2565 0L26.072 7.67124C21.4225 12.4444 18.3229 16.962 16.7731 21.2237C15.2232 25.3151 14.4483 29.4916 14.4483 33.7534L8.24908 29.4064H27.1052V56H0.5V37.3333C0.5 30.344 1.61931 23.8661 3.85793 17.8996C6.26876 11.7626 10.4016 5.79605 16.2565 0ZM59.6513 0L69.4668 7.67124C64.8173 12.4444 61.7177 16.962 60.1679 21.2237C58.6181 25.3151 57.8432 29.4916 57.8432 33.7534L51.6439 29.4064H70.5V56H43.8948V37.3333C43.8948 30.344 45.0141 23.8661 47.2528 17.8996C49.6636 11.7626 53.7964 5.79605 59.6513 0Z"/>
          </svg>
                                    <p>Eu cheguei até o curso de oratória da Methodus porque eu não conseguia nem dizer meu nome sem tirar a mão do bolso&#8230; e de jaleco é fácil ficar com as mãos nos bolsos&#8230; (risos). Eu sou da equipe de enfermagem de um dos mais importantes hospitais de coração do Brasil, em São Paulo. <mark>Eu precisava ser mais ativa, notada e expressiva</mark> no meu relacionamento com a equipe.</p>

                                        <p>Eu tinha insegurança de expor o meu trabalho, como em reuniões ou em palestras, por exemplo. E eu queria isso para me aprimorar, melhorar meu autoconhecimento para conseguir superar o que tanto me incomodava. Era um autodesenvolvimento, uma satisfação e felicidade pessoal que eu queria alcançar.</p>

                              <div class="boxContrast">
                                      <span class="courseColor">E aí, eu disse:</span>
                                    <h2>Chega! vou mudar isso! já!</h2>
                </div>
                                        
                                        <p><strong>Faz muita diferença no dia a dia</strong> do trabalho e na comunicação com os colegas, sem dúvida. Só que mais que isso, tem <mark>a sensação de conquista, de vitória sobre o medo de falar em público.</mark> E foi incrível! Com a Methodus, eu consegui encontrar maneiras diferentes de alcançar isso. O Prof. Alcides me mostrou como funciona o processo de aprendizado e mudança no nosso cérebro! E ensinou também técnicas importantíssimas de como não esquecer o que vamos falar em uma apresentação, por exemplo. Isso faz toda diferença para entendermos e conseguirmos mudar de verdade.</p>

                                        <p><strong>Com a Methodus é diferente&#8230;</strong> é transformador! Hoje faz parte de mim falar melhor. Nossa! A alegria que eu fico de conseguir falar sem medo das pessoas, de esquecer, de dar branco, me faz me sentir muito poderosa. Eu fiquei boba como eu mesma consegui!</p>

                              <div class="boxContrast">
                                    <h2>É uma sensação Incrível!</h2>
                </div>
                                              <svg class="quoteEnd" width="73" height="58" viewBox="0 0 73 58" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M56.2934 0L46.1974 7.94521C50.9797 12.8889 54.1679 17.5677 55.762 21.9817C57.3561 26.2192 58.1531 30.5449 58.1531 34.9589L64.5295 30.4566H45.1347V58H72.5V38.6667C72.5 31.4277 71.3487 24.7184 69.0461 18.5388C66.5664 12.1827 62.3155 6.00305 56.2934 0ZM11.6587 0L1.56272 7.94521C6.34501 12.8889 9.5332 17.5677 11.1273 21.9817C12.7214 26.2192 13.5184 30.5449 13.5184 34.9589L19.8948 30.4566H0.5V58H27.8653V38.6667C27.8653 31.4277 26.714 24.7184 24.4114 18.5388C21.9317 12.1827 17.6808 6.00305 11.6587 0Z"/>
          </svg>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="secReminder">
  <div class="reminderBanner courseBackgroundColor">
    <img class="small" src='https://methodus.com.br/wp-content/themes/maiscode_methodus_novo/img/small.png'>
    <div class="">
      <div class="row">
        <div class="col-md-12">
          <h2>Lembrete Methodus</h2>
          <h3>Máximo de 15 alunos por turma</h3>
          <a data-toggle="modal" data-target="#cursoVejaInvestimento" href="#">Fique atento à próxima turma e à oferta de retorno às aulas presenciais!</a>
        </div>
      </div>
    </div>
    <img class="big" src='https://methodus.com.br/wp-content/themes/maiscode_methodus_novo/img/big.png'>
  </div>
</section>

<!-- POR QUE A METHODUS -->
<section id="secWhyMethodus">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2 class="courseColor">● Por que a methodus funciona melhor para você ●</h2>
        <div class="methodusContent">
                                                    <p>Para você entender quais os diferencias reais, vamos falar primeiro sobre o criador da Methodus, o professor Alcides Schotten. O professor Alcides é professor e filósofo, ou seja, ele vai além do básico e aprofunda-se muito na mente humana. Estudioso por natureza, aprimora-se todos os anos, buscando na neurociência, o entendimento de como o cérebro funciona e reage. E assim, usa toda sua base e conhecimento de professor e filósofo, para criar os estímulos perfeitos para que o cérebro aprenda de verdade. Não é simplesmente aprender por uns dias. <mark>A metodologia é feita para transformar você para sempre.</mark> E de maneira fácil.</p>
                                                        <h3 class="courseColor">&#x25cf; NOSSA MOTIVAÇÃO &#x25cf;</h3>
                              <p>Alcides Schotten, fundador da Methodus, sempre foi professor, sempre foi um mestre, e por isso, ensinar é a motivação de vida que o move a querer sempre ver seus alunos conquistando seus sonhos! E isso é verdade. Basta ver a felicidade estampada no rosto de quem completa os cursos.</p>
                              <div class="boxMethodusImg">
                  <img src="https://methodus.com.br/wp-content/uploads/2020/04/home.jpeg">
                  <span class="courseColor">Prof Alcides Schotten, educador e filósofo.</span>
                </div>
                                                        <h3 class="courseColor">&#x25cf; Como fazemos para você aprender? &#x25cf;</h3>
                              <p>Alguma coisa difícil de aprender na escola você lembra até hoje? Não, né? Por isso, a gente sempre pensa em jeitos fáceis de você aprender. Porque senão, você esquece. O método utilizado nos cursos e treinamentos oferecidos pela Methodus foi desenvolvido pelo professor e filósofo Alcides Schotten, que procurou em todo o conhecimento humano, desde Platão e Aristóteles, na Grécia Antiga, até os pensadores mais modernos, um ponto de equilíbrio e o segredo do aperfeiçoamento de talentos naturais.</p>
                                                        <p>Através do método usado nos cursos da Methodus, <mark>o aluno desenvolverá de forma prática, gradual e relativa às suas necessidades, habilidades de otimização pessoal.</mark> Ou seja, não é uma fórmula que pode funcionar para um e não para o outro. Existe uma fórmula e dosagem certa para você. Por isso é fácil e duradouro.</p>
                                                        <h3 class="courseColor">&#x25cf; E COMO ISSO É APLICADO NO CURSO DE ORATÓRIA? &#x25cf;</h3>
                              <p>O Curso de Oratória é destinado a quem, além de querer se comunicar de forma eficiente, deseja: adquirir inteligência emocional para administrar seus sentimentos, vencer o medo, a introversão e ter segurança.</p>
                                            </div>
        <div class="differenceTitle">
          <h2 class="courseColor">VIU A DIFERENÇA?</h2>
          <h3 class="courseColor">“Adquirir inteligência emocional para administrar seus sentimentos, vencer o medo, a introversão e ter segurança.”</h3>
        </div>
        <div class="differenceContent">
                                    <p>Este é o segredo para perder o medo de falar em público tanto presencial <mark>(reuniões, apresentações, ou com o seu chefe)</mark> quanto online <mark>(Zoom, Skype, etc)!</mark> Afinal, saber falar nas vídeo-conferências é fundamental hoje em dia. Através de métodos práticos e vários exercícios, o Professor está lá, do seu lado, te ensinando a ter autoconhecimento e ao mesmo tempo, ensinando as técnicas dos bons discursos:</p>
                                                <ul>
                                          <li><span>Acabar com a introversão</span></li>
                                          <li><span>Agilidade de raciocínio</span></li>
                                          <li><span>Argumentação assertiva</span></li>
                                          <li><span>Associação rápida de informações</span></li>
                                          <li><span>Técnicas para não esquecer o texto</span></li>
                                          <li><span>Utilização das suas qualidades individuais para a criação de oratórias únicas (esse só a Methodus possui)</span></li>
                                      </ul>
                                                        <p>Pois só com o conhecimento e percepção de quem há mais de 3 décadas olha centenas e centenas de tipos diferentes de pessoas e tem uma base teórica fortíssima, consegue fazer você aprimorar as suas técnicas do seu jeito.</p>
                                            </div>
      </div>
    </div>
  </div>
</section>

<!-- VANTAGENS -->
<section id="secAdvantages">
  <div class="bannerSatisfaction courseBackgroundColor">
    <h2>Olha só a satisfação de quem fez o curso:</h2>
    <h3>Assista até o final. A decisão sobre o seu futuro agora é sua.</h3>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
                  <div class="advantagesVideos">
                      <div class="col-md-4 col-sm-6 text-center">
              <div class="local-video-container">
                <div class="background-image-holder" style="background: url(&quot;https://methodus.com.br/wp-content/uploads/2020/08/curso-de-oratoria-lp-7.png&quot;); "> <img alt="Background Image" class="background-image" src="https://methodus.com.br/wp-content/uploads/2020/08/curso-de-oratoria-lp-7.png" style="display: none;"> 
                </div>
                <video controls="">
                  <source src="https://api.methodus.com.br/site/cursos/videos/1-eleuzia.mp4" type="video/mp4">                                
                </video> 
                <div class="play-button" style="opacity: 0;"></div>
              </div>
            </div>
                      <div class="col-md-4 col-sm-6 text-center">
              <div class="local-video-container">
                <div class="background-image-holder" style="background: url(&quot;https://methodus.com.br/wp-content/uploads/2020/08/curso-de-oratoria-lp-5.png&quot;); "> <img alt="Background Image" class="background-image" src="https://methodus.com.br/wp-content/uploads/2020/08/curso-de-oratoria-lp-5.png" style="display: none;"> 
                </div>
                <video controls="">
                  <source src="https://api.methodus.com.br/site/cursos/videos/3-ricardo.mp4" type="video/mp4">                                
                </video> 
                <div class="play-button" style="opacity: 0;"></div>
              </div>
            </div>
                      <div class="col-md-4 col-sm-6 text-center">
              <div class="local-video-container">
                <div class="background-image-holder" style="background: url(&quot;https://methodus.com.br/wp-content/uploads/2020/08/curso-de-oratoria-lp-6.png&quot;); "> <img alt="Background Image" class="background-image" src="https://methodus.com.br/wp-content/uploads/2020/08/curso-de-oratoria-lp-6.png" style="display: none;"> 
                </div>
                <video controls="">
                  <source src="https://api.methodus.com.br/site/cursos/videos/4.mp4" type="video/mp4">                                
                </video> 
                <div class="play-button" style="opacity: 0;"></div>
              </div>
            </div>
                    </div>
                <span class="courseColor">Veja como a vida mudou em apenas 10 aulas! Agora é a sua vez!</span>        <h2 class="courseColor">&#x25cf; SÓ COM A METHODUS VOCÊ CONSEGUIRÁ &#x25cf;</h2>        <div class="advantagesBox">
                                    <h3 class="courseColor">&#x25cf; Acabar com a introversão:</h3>
                              <p>Sabe aquela sensação de saber o que falar numa reunião e não conseguir (a voz não sai)? Você sabe o que dizer, mas outra pessoa fala e leva os aplausos que deveriam ser seus? Isso nunca mais vai acontecer.</p>
                                                      <h3 class="courseColor">&#x25cf; Perder o medo de falar em público...</h3>
                              <p>...e não ficar nervoso, com a didática exclusiva Methodus, que acelera o processo.</p>
                                                      <h3 class="courseColor">&#x25cf; Preparar apresentações</h3>
                              <p>Isso faz parte de como ter segurança para falar!</p>
                                                      <h3 class="courseColor">&#x25cf; Técnicas</h3>
                              <p>de como não esquecer o que vai dizer!</p>
                                                      <h3 class="courseColor">&#x25cf; Saber entrar no assunto a qualquer momento...</h3>
                              <p>e falar nas reuniões presenciais ou online (conferência).</p>
                                            <span>E mais:</span>
                                        <h3 class="courseColor">&#x25cf; Reciclagem permanente!</h3>
                              <p>Você pode fazer o curso novamente de graça quantas vezes quiser!</p>
                                                      <h3 class="courseColor">&#x25cf; Um professor ao seu lado que te ajuda!</h3>
                                                          </div>
      </div>
    </div>

<section id="secTestimony">
    <div class="row">
      <div class="col-md-12">
              </div>
    </div>
  </section>
  </div>
</section>

<section id="secBannerAdvantages" class="courseBackgroundColor">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2>Você vai querer ficar de fora dessa evolução?</h2>
        <div class="advantagesBox">
                                    <h3>Remuneração em média 37% maior numa equipe. Quem lidera ganha mais!</h3>
                          <h3>Possibilidade de ser promovido e aí subir de cargo e ganhar até mais que 100%</h3>
                          <h3>Ser referência na empresa e no mercado de trabalho.</h3>
                          <h3>Ir muito melhor em entrevistas de emprego!</h3>
                          <h3>Certificado de conclusão que você poderá colocar no seu currículo.</h3>
                          <a data-toggle="modal" data-target="#cursoVejaInvestimento" href="#">Clique Aqui e Seja um Aluno Methodus </a>
        </div>
      </div>
    </div>
  </div>
</section>
<section id="secLogistic">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2 class="courseColor">LOGÍSTICA DAS AULAS</h2>
        <h3 class="courseColor">&#x25cf; O MÉTODO METHODUS, QUE É O DIFERENCIAL &#x25cf;</h3>
        <div class="boxLogistic">
                      <ul>
                              <li><span>Apenas 10 a 15 participantes por grupo.</span></li>
                              <li><span>Em todas as aulas os participantes falarão pelo menos duas vezes.</span></li>
                              <li><span>Apresentações filmadas e analisadas com o professor.</span></li>
                              <li><span>Professor preparado para atender participantes em suas dúvidas.</span></li>
                              <li><span>Manual contendo a teoria abordada.</span></li>
                              <li><span>Gravações das suas apresentações para você baixar.</span></li>
                          </ul>
                    <h3 class="courseColor">OK, MAS E QUAL O VALOR DO INVESTIMENTO?</h3>
                      <div class="boxInvestiment">
              <ul>
                                  <li><span>Quanto vale colocar isso no seu currículo?</span></li>
                                  <li><span>Quanto vale ser promovido (quanto você ganhará a mais de salário)?</span></li>
                                  <li><span>Quanto vale conseguir ir bem numa entrevista de emprego?</span></li>
                                  <li><span>Quanto custam todas as mensalidades de uma pós-graduação (que demora e pode não ser decisiva para ser promovido)?</span></li>
                                  <li><span>Qual o preço de fazer outro curso e não ficar satisfeito (jogar dinheiro fora)?</span></li>
                              </ul>
            </div>
                  </div>
      </div>
    </div>
  </div>
</section>



<!-- Modal Cupom 
<div class="modal fade" id="cupomModal" tabindex="-1" role="dialog" aria-labelledby="cupomModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div role="form" class="wpcf7" id="wpcf7-f6273-o1" lang="pt-BR" dir="ltr">
<div class="screen-reader-response"><p role="status" aria-live="polite" aria-atomic="true"></p> <ul></ul></div>
<form action="/cursos/curso-de-oratoria/#wpcf7-f6273-o1" method="post" class="wpcf7-form init" novalidate="novalidate" data-status="init">
<div style="display: none;">
<input type="hidden" name="_wpcf7" value="6273" />
<input type="hidden" name="_wpcf7_version" value="5.3" />
<input type="hidden" name="_wpcf7_locale" value="pt_BR" />
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f6273-o1" />
<input type="hidden" name="_wpcf7_container_post" value="0" />
<input type="hidden" name="_wpcf7_posted_data_hash" value="" />
</div>
<div class="box-contato" id="investir">
<h2 class="titulo-box vermelho">Preencha abaixo e receba um cupom de desconto de 10% no seu e-mail!</h2>
<div class="interno-box">
<div class="inputCupomForm">
<span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Primeiro nome" /></span><br />
<span class="wpcf7-form-control-wrap email"><input type="email" name="email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="E-mail" /></span>
</div>
<p><label>Curso de Preferência</label><br />
<span class="wpcf7-form-control-wrap menu-140"><select name="menu-140" class="wpcf7-form-control wpcf7-select" id="coursePref" aria-invalid="false"><option value="">---</option><option value="Oratória">Oratória</option><option value="Leitura Dinâmica / Memória">Leitura Dinâmica / Memória</option><option value="Administração do Tempo">Administração do Tempo</option></select></span></p>
<div class="oratory radioCupom" style="display: none">
<p>Eu sou: Oratória</p>
<p><span class="wpcf7-form-control-wrap radio-972"><span class="wpcf7-form-control wpcf7-radio"><span class="wpcf7-list-item first"><input type="radio" name="radio-972" value="Sou profissional / apresentações / treinamentos" /><span class="wpcf7-list-item-label">Sou profissional / apresentações / treinamentos</span></span><span class="wpcf7-list-item"><input type="radio" name="radio-972" value="Sou profissional vendas / líder religioso / político / vídeos" /><span class="wpcf7-list-item-label">Sou profissional vendas / líder religioso / político / vídeos</span></span><span class="wpcf7-list-item"><input type="radio" name="radio-972" value="Sou concursando e me preparo para prova oral" /><span class="wpcf7-list-item-label">Sou concursando e me preparo para prova oral</span></span><span class="wpcf7-list-item last"><input type="radio" name="radio-972" value="Sou universitário / apresento seminários / processo seletivo" /><span class="wpcf7-list-item-label">Sou universitário / apresento seminários / processo seletivo</span></span></span></span>
</p></div>
<div class="memory radioCupom" style="display: none">
<p>Eu sou: Leitura Dinâmica / Memória</p>
<p><span class="wpcf7-form-control-wrap radio-429"><span class="wpcf7-form-control wpcf7-radio"><span class="wpcf7-list-item first"><input type="radio" name="radio-429" value="Vestibular / concursos / ensino médio" /><span class="wpcf7-list-item-label">Vestibular / concursos / ensino médio</span></span><span class="wpcf7-list-item"><input type="radio" name="radio-429" value="Cursando ensino superior / estagiário / trainee" /><span class="wpcf7-list-item-label">Cursando ensino superior / estagiário / trainee</span></span><span class="wpcf7-list-item last"><input type="radio" name="radio-429" value="Sou profissional / faço pós e especializações" /><span class="wpcf7-list-item-label">Sou profissional / faço pós e especializações</span></span></span></span>
</p></div>
<div class="time radioCupom" style="display: none">
<p>Eu sou: Administração do Tempo</p>
<p><span class="wpcf7-form-control-wrap radio-429"><span class="wpcf7-form-control wpcf7-radio"><span class="wpcf7-list-item first"><input type="radio" name="radio-429" value="Sou empreendedor / tenho sonhos a realizar" /><span class="wpcf7-list-item-label">Sou empreendedor / tenho sonhos a realizar</span></span><span class="wpcf7-list-item"><input type="radio" name="radio-429" value="Sou profissional formado / preciso administrar o tempo / procrastino" /><span class="wpcf7-list-item-label">Sou profissional formado / preciso administrar o tempo / procrastino</span></span><span class="wpcf7-list-item last"><input type="radio" name="radio-429" value="Estou empregado / não tenho curso superior / preciso me organizar" /><span class="wpcf7-list-item-label">Estou empregado / não tenho curso superior / preciso me organizar</span></span></span></span>
</p></div>
<p><input type="submit" value="Enviar" class="wpcf7-form-control wpcf7-submit" />
</p></div>
</div>
</div>
<div class="wpcf7-response-output" aria-hidden="true"></div></form></div>      </div>
    </div>
  </div>
</div>
-->

<!-- PREÇOS -->
<section id="secPrices" class="courseBackgroundColor" style="background-color: ">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="pricesTop featurePrice">
          <h2>Somou tudo isso de verdade?</h2>
<h2>Cursos + Pós + 1 ano estudando? Custaria mais de 8.750 Reais</h2>
<h2>Na Methodus o Curso Presencial é: <del>R$ 3.350,00</del>.</h2>
<h2>MAS NESTA OFERTA ESPECIAL VOCÊ PAGA BEM MENOS, VEJA:</h2>
        </div>
  
        <a data-toggle="modal" data-target="#cursoVejaInvestimento" href="#">Quero ver investimentos e formas de pagamento!</a>
      </div>
    </div>
  </div>
</section>
<!-- proximas turmas -->
<section id="secPrices" class="courseBackgroundColor">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
    <span>Com um pequeno aumento no seu salário ou um novo emprego, isso sai de graça.</span>
        <div class="boxWorkload">
          <p><p>Carga horária: 10 aulas de 3 horas<br />
Participantes: limite de 15 por turma<br />
Vídeos: 2 por aula &#8211; com acesso imediato</p>
</p>
        </div>
        <h2>PRÓXIMAS TURMAS</h2>
                              <div class="col-md-6 col-sm-12">
              <div class="classesDate">
                <p>26/06/2021 a 01/08/2021</p>
                <small>(5 sáb. e 5 dom. - 9h às 12h)</small>
              </div>
            </div>
                      <div class="col-md-6 col-sm-12">
              <div class="classesDate">
                <p>26/05/2021 à 30/06/2021</p>
                <small>(10 noites - 2ª e 4ª - 19h às 22h)</small>
              </div>
            </div>
            <!--DATAS
                      <div class="col-md-6 col-sm-12">
              <div class="classesDate">
                <p>11/01/2021 à 22/01/2021</p>
                <small>(10 manhãs - 2ª à 6ª - 9h às 12h)</small>
              </div>
            </div>
                      <div class="col-md-6 col-sm-12">
              <div class="classesDate">
                <p>26/01/2021 à 05/02/2021</p>
                <small>(9  tardes - 2ª à 6ª - 14h às 17h20)</small>
              </div>
            </div>
              -->
      </div>
    </div>
  <div class="row">
    <div class="col-md-12">
              <div>
              <a data-toggle="modal" data-target="#cursoVejaInvestimento" href="#">Clique aqui e mude seu futuro profissional agora!</a>
        </div>
    </div>
  </div>
  </div>
</section>

<section id="secTestimony">
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1 col-sm-12">
        <h2 class="courseColor">VEJA MAIS ALGUNS DEPOIMENTOS DE QUEM CHEGOU LÁ!</h2>
                  
            <div class="row sliderCard">
                              <div class="col-md-12">
                  <div class="boxTestimony">
                    <img class="avatarImg" src="https://methodus.com.br/wp-content/uploads/2020/08/curso-de-oratoria-lp-2.jpg">
                    <h3 class="courseColor">Luciano B. Mussolin</h3>
                    <span>Bacharel em Ciências Contábeis</span>
                    <div class="testimonyText">
                      <svg class="quoteSlickStart" width="39" height="33" viewBox="0 0 39 33" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M9.0535 0.201172L14.3819 4.58474C11.8579 7.31229 10.1753 9.89371 9.33395 12.329C8.49262 14.6669 8.07196 17.0535 8.07196 19.4888L4.70664 17.0048H14.9428V32.2012H0.5V21.5345C0.5 17.5406 1.10763 13.8389 2.32288 10.4295C3.63161 6.92264 5.87515 3.5132 9.0535 0.201172ZM32.6107 0.201172L37.9391 4.58474C35.4151 7.31229 33.7325 9.89371 32.8911 12.329C32.0498 14.6669 31.6292 17.0535 31.6292 19.4888L28.2638 17.0048H38.5V32.2012H24.0572V21.5345C24.0572 17.5406 24.6648 13.8389 25.8801 10.4295C27.1888 6.92264 29.4324 3.5132 32.6107 0.201172Z"/>
                      </svg>
                      <p><strong>O instrutor teve uma precisão cirúrgica em apontar todas as causas que me travavam ao falar em público. </strong><br />
Me agradou muito encontrar confiança em mim mesmo e deixar de lado a preocupação com o pensamento dos outros sobre mim. Além disso, através do curso percebi que a confiança se adquire quando você está em paz e confiante em sim mesmo. Falar em público só depende do treinamento e da persistência. O instrutor teve uma precisão cirúrgica em apontar todas as causas que me faziam travar diante da possibilidade de ter que me expressar em público. Claro que as exposições técnicas ao longo do treinamento formam também muito relevantes.</p>
                      <svg class="quoteSlickEnd" width="39" height="31" viewBox="0 0 39 31" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M29.9465 0.201172L24.6181 4.31076C27.1421 6.86784 28.8247 9.28793 29.6661 11.571C30.5074 13.7628 30.928 16.0003 30.928 18.2834L34.2934 15.9546H24.0572V30.2012H38.5V20.2012C38.5 16.4569 37.8924 12.9866 36.6771 9.79022C35.3684 6.50255 33.1248 3.3062 29.9465 0.201172ZM6.3893 0.201172L1.06088 4.31076C3.58487 6.86784 5.26752 9.28793 6.10885 11.571C6.95018 13.7628 7.37085 16.0003 7.37085 18.2834L10.7362 15.9546H0.5V30.2012H14.9428V20.2012C14.9428 16.4569 14.3352 12.9866 13.1199 9.79022C11.8112 6.50255 9.56765 3.3062 6.3893 0.201172Z"/>
                      </svg>
                    </div>
                  </div>
                </div>
                              <div class="col-md-12">
                  <div class="boxTestimony">
                    <img class="avatarImg" src="https://methodus.com.br/wp-content/uploads/2020/08/curso-de-oratoria-lp.jpg">
                    <h3 class="courseColor">Mônica dos Santos Silva Pimenta</h3>
                    <span>Bacharel em Química e Pós em Engenharia Química de Polímeros, Empreendedora</span>
                    <div class="testimonyText">
                      <svg class="quoteSlickStart" width="39" height="33" viewBox="0 0 39 33" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M9.0535 0.201172L14.3819 4.58474C11.8579 7.31229 10.1753 9.89371 9.33395 12.329C8.49262 14.6669 8.07196 17.0535 8.07196 19.4888L4.70664 17.0048H14.9428V32.2012H0.5V21.5345C0.5 17.5406 1.10763 13.8389 2.32288 10.4295C3.63161 6.92264 5.87515 3.5132 9.0535 0.201172ZM32.6107 0.201172L37.9391 4.58474C35.4151 7.31229 33.7325 9.89371 32.8911 12.329C32.0498 14.6669 31.6292 17.0535 31.6292 19.4888L28.2638 17.0048H38.5V32.2012H24.0572V21.5345C24.0572 17.5406 24.6648 13.8389 25.8801 10.4295C27.1888 6.92264 29.4324 3.5132 32.6107 0.201172Z"/>
                      </svg>
                      <p><strong>Com a Oratória realmente aprendemos a nos valorizar.</strong><br />
O curso superou minhas expectativas, saio daqui hoje uma nova mulher, plenamente convicta de onde quero e vou chegar; nasci para ser águia e quando compreendi isso perdi o medo de voar, estou muito mais segura e acreditando no meu potencial. Sim, todos deveriam ter a oportunidade de se autoconhecer; a oratória vai além de uma boa comunicação, realmente aprendemos a nos valorizar.</p>
                      <svg class="quoteSlickEnd" width="39" height="31" viewBox="0 0 39 31" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M29.9465 0.201172L24.6181 4.31076C27.1421 6.86784 28.8247 9.28793 29.6661 11.571C30.5074 13.7628 30.928 16.0003 30.928 18.2834L34.2934 15.9546H24.0572V30.2012H38.5V20.2012C38.5 16.4569 37.8924 12.9866 36.6771 9.79022C35.3684 6.50255 33.1248 3.3062 29.9465 0.201172ZM6.3893 0.201172L1.06088 4.31076C3.58487 6.86784 5.26752 9.28793 6.10885 11.571C6.95018 13.7628 7.37085 16.0003 7.37085 18.2834L10.7362 15.9546H0.5V30.2012H14.9428V20.2012C14.9428 16.4569 14.3352 12.9866 13.1199 9.79022C11.8112 6.50255 9.56765 3.3062 6.3893 0.201172Z"/>
                      </svg>
                    </div>
                  </div>
                </div>
                              <div class="col-md-12">
                  <div class="boxTestimony">
                    <img class="avatarImg" src="https://methodus.com.br/wp-content/uploads/2020/08/curso-de-oratoria-lp-1.jpg">
                    <h3 class="courseColor">Fernanda Ros de Obernaes</h3>
                    <span>Bacharel em Direito, Autônoma</span>
                    <div class="testimonyText">
                      <svg class="quoteSlickStart" width="39" height="33" viewBox="0 0 39 33" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M9.0535 0.201172L14.3819 4.58474C11.8579 7.31229 10.1753 9.89371 9.33395 12.329C8.49262 14.6669 8.07196 17.0535 8.07196 19.4888L4.70664 17.0048H14.9428V32.2012H0.5V21.5345C0.5 17.5406 1.10763 13.8389 2.32288 10.4295C3.63161 6.92264 5.87515 3.5132 9.0535 0.201172ZM32.6107 0.201172L37.9391 4.58474C35.4151 7.31229 33.7325 9.89371 32.8911 12.329C32.0498 14.6669 31.6292 17.0535 31.6292 19.4888L28.2638 17.0048H38.5V32.2012H24.0572V21.5345C24.0572 17.5406 24.6648 13.8389 25.8801 10.4295C27.1888 6.92264 29.4324 3.5132 32.6107 0.201172Z"/>
                      </svg>
                      <p><strong>Falar em todas as aulas foi importante para atingir autoconfiança.</strong><br />
Perceber que nem todos somos iguais e isso é normal, me fez sentir mais segura sobre falar em público. Acredito que, pessoalmente, ainda possa melhorar, principalmente na busca por descobrir quem sou eu e transmitir isso na minha fala, nos meus gestos e nas minhas apresentações. Termino o curso com a certeza que adquirir conhecimentos para me expressar bem em público. Recomendo o curso pois achei as técnicas de ensino boas para o aprendizado. A prática de levantar e falar em todas as aulas foi um exercício fundamental para sentir autoconfiança.</p>
                      <svg class="quoteSlickEnd" width="39" height="31" viewBox="0 0 39 31" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M29.9465 0.201172L24.6181 4.31076C27.1421 6.86784 28.8247 9.28793 29.6661 11.571C30.5074 13.7628 30.928 16.0003 30.928 18.2834L34.2934 15.9546H24.0572V30.2012H38.5V20.2012C38.5 16.4569 37.8924 12.9866 36.6771 9.79022C35.3684 6.50255 33.1248 3.3062 29.9465 0.201172ZM6.3893 0.201172L1.06088 4.31076C3.58487 6.86784 5.26752 9.28793 6.10885 11.571C6.95018 13.7628 7.37085 16.0003 7.37085 18.2834L10.7362 15.9546H0.5V30.2012H14.9428V20.2012C14.9428 16.4569 14.3352 12.9866 13.1199 9.79022C11.8112 6.50255 9.56765 3.3062 6.3893 0.201172Z"/>
                      </svg>
                    </div>
                  </div>
                </div>
                          </div>
          
              </div>
    </div>
  </div>
</section>

<section id="secFeaturedBanner" class="courseBackgroundColor">
  <div class="row">
    <div class="col-md-12">
      <div class="featuredList">
        <h2>Olhe o texto abaixo.<br> Agora, pare e imagine acontecendo com você.</h2>
                  <ul>
                          <li><span>Falando sem medo, sem inibição, sem sentir vergonha e com naturalidade.</span></li>
                          <li><span>Expressando ideias com desenvoltura, convicção e dinamismo.</span></li>
                          <li><span>Fazendo apresentações objetivas sem errar.</span></li>
                          <li><span>Sabendo segredos para convencer públicos difíceis.</span></li>
                          <li><span>Não falando mais “ééé” ou “né?, né?” nem outros cacoetes.</span></li>
                          <li><span>Usando projetor, flip chart ou quadro branco e explicando algo.</span></li>
                          <li><span>Conversando tanto na vida pessoal quanto profissional com desconhecidos.</span></li>
                          <li><span>Falando elegantemente com gestos.</span></li>
                          <li><span>Falando com olhar impactante e seguro.</span></li>
                          <li><span>Desenvolvendo habilidade de liderar (ser ouvido com razão pelos colegas). </span></li>
                          <li><span>Falando naturalmente em pé ou sentado.</span></li>
                          <li><span>Sabendo iniciar, desenvolver e concluir palestras.</span></li>
                          <li><span>Ampliando o vocabulário para diversas situações.</span></li>
                      </ul>
              </div>
      <div class="featuredCta">
        <h2>E aí? Vai ver todo mundo conseguindo e você não?</h2>
        <h3>Faça isso AGORA! Aproveite as últimas vagas da próxima turma com desconto especial!</h3>
        <div id="formRegistration" class="row">
          <div class="col-md-8 col-md-offset-2">
            <form>
              <input type="hidden" name="curso" value="" />
              <div class="btt2linhas">
              <a data-toggle="modal" data-target="#cursoVejaInvestimento" href="#">
                Quero me inscrever com o desconto agora!                </a>
                </div>
            </form>
          </div>
        </div>
              </div>
    </div>
  </div>
</section>


<!-- Modal  veja investimento -->
<div id="cursoVejaInvestimento" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <?php echo do_shortcode( '[contact-form-7 id="6289" title="VEJA INVESTIMENTOS E PRÓXIMOS GRUPOS!"]'); ?>

      </div>
    </div>

  </div>
</div>


  <script type='text/javascript' async src='https://d335luupugsy2.cloudfront.net/js/loader-scripts/67cc1fec-89be-4715-b86e-5eda535a7d57-loader.js'></script><script type='text/javascript' src='https://methodus.com.br/wp-content/themes/maiscode_methodus_novo/js/main.js?ver=1.0.0' id='jquery-js'></script>
<script type='text/javascript' id='contact-form-7-js-extra'>
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/methodus.com.br\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script type='text/javascript' src='https://methodus.com.br/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.3' id='contact-form-7-js'></script>
<script type='text/javascript' src='https://methodus.com.br/wp-includes/js/wp-embed.min.js?ver=5.5.3' id='wp-embed-js'></script>

<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script> 
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>

<script type="text/javascript">
$(document).ready(function(){

  $('.sliderCard').slick({
    dots: false,
    slidesToShow: 1,
    infinite: false,
    autoplay: false,
    fade: true,
    arrows: true,
    prevArrow: "<img class='a-left control-c prev slick-prev' src='https://methodus.com.br/wp-content/themes/maiscode_methodus_novo/img/arrowPrev.png'>",
    nextArrow: "<img class='a-right control-c next slick-next' src='https://methodus.com.br/wp-content/themes/maiscode_methodus_novo/img/arrowNext.png'>",
    responsive: [
      {
        breakpoint: 780,
        settings: {
          slidesToShow: 1,
          arrows: false,
        }
      },        
    ]
  });

  $('.sliderCardDepo').slick({
    dots: false,
    slidesToShow: 1,
    infinite: true,
    autoplay: true,
    fade: true,
    arrows: true,
    prevArrow: "<img class='a-left control-c prev slick-prev depo-prev' src='https://methodus.com.br/wp-content/themes/maiscode_methodus_novo/img/arrowPrev.png'>",
    nextArrow: "<img class='a-right control-c next slick-next depo-next' src='https://methodus.com.br/wp-content/themes/maiscode_methodus_novo/img/arrowNext.png'>",
    responsive: [
      {
        breakpoint: 780,
        settings: {
          slidesToShow: 1,
          arrows: false,
        }
      },        
    ]
  });


 });


</script>

<?php get_footer(); ?>