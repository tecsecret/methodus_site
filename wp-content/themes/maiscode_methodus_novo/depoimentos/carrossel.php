<?php 
	$url_site       =  get_option('link_api');
 ?>

<section class="bg-secondary carrocel-dep">
  <div class="container" style="text-align: center;">
    <div class="row">
      <div class="col-sm-8 col-sm-offset-2 text-center">
        <div>
			<h3 class="mb64" style="margin-bottom: 0;">Depoimentos</h3>
			<div class="barra"></div>
		</div>
        <div class="testimonials text-slider slider-arrow-controls">
          <ul class="slides">
          	<?php
          	
			// Listar depoimentos
			if ( !isset($array_depoimentos) ){
				$xml_depoimentos 	= executaPagina( 'api/', array( 'a'=>'depoimentos', 'metodo'=>'listar' ) );
				$array_depoimentos 	= lerXML( $xml_depoimentos );
			}
				
			if ( $array_depoimentos->erro == 0 ){
				$depoimentos = $array_depoimentos->depoimentos->depoimento;	
				//print_r($depoimentos);
				foreach( $depoimentos as $depoimento ){
					//print_r($depoimento);
					
					if ($url_amigavel_on){ 
						$link_depoimento	= $url_amigavel.'/'.encodarURL( $depoimento->nome ).'/'.$depoimento['codigo'].'-depoimento.html'; 
					}else{ 
						$link_depoimento	= $url_site.'/depoimentos/detalhes.php?ID='.$depoimento['codigo'].''; 
					}
					
					if ( strlen( $depoimento->descricao ) > 2560 ){
						$str_link_continuar_lendo = ' <a href="'.$link_depoimento.'">...continuar lendo</a>';
					}else{
						$str_link_continuar_lendo = '';
					}

					echo	'<li>
							  
							  <div class="quote-author"> <img alt="'.$depoimento->nome.'" src="'.$url_site.''.$depoimento->foto.'">

						<h6 class="depo-nome">'.$depoimento->nome.', </h6><span class="depo-nome">';
					
					if ( $depoimento->formacao != '' )
						echo	$depoimento->formacao;
					
					if ( $depoimento->formacao != '' && $depoimento->empresa != '' )
						echo	' em ';
					
					if ( $depoimento->empresa != '' )
						echo	$depoimento->empresa;
										 
						
					echo	'</span>';

					echo '<p class="lead cada-depoimento"><b>'.$depoimento->resumo.'</b><BR /> <i id="aspa_1">“</i>'.substr( $depoimento->descricao, 0, 2560 ).''.$str_link_continuar_lendo.'<i id="aspa_2"> “</i></p>'; ' </div></li>';
				}
			}
			?>
            
          </ul>
        </div>
      </div>
    </div>
    <!-- <a class="btn btn-lg mt-xs-24" style="margin-top: 20px;" href="<?php if ($url_amigavel_on){ echo $url_amigavel.'/depoimentos.html'; }else{ echo $url_site.'/depoimentos/'; } ?>">Veja mais</a> -->
  </div>
</section>