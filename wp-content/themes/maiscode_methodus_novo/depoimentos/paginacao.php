<?php
include_once '../../api/biblioteca/funcoes.php';
include_once '../biblioteca/variaveis.php';

$paginacao_inicio	= formataVar( 'paginacao_inicio', 'GET' );
$ID_tema			= formataVar( 'ID_tema', 'GET' );
$paginacao_tamanho	= 10;

if (!$paginacao_inicio){
	$paginacao_inicio = 1;	
}

// Paginas
$xml_pagina = executaPagina( 'api/', array( 'a'=>'depoimentos', 'metodo'=>'listar_todos', 'inicio'=>$paginacao_inicio, 'paginacao'=>$paginacao_tamanho, 'ID_tema'=>$ID_tema  ) );
$pagina 	= lerXML( $xml_pagina );
	
if ( $pagina->erro == 0 ){
	
	$quantidade		= $pagina->quantidade;
	$registros		= $pagina->depoimentos->depoimento;
	
}else{
	exit("Erro ao acessar os dados da página");		
}

foreach( $registros as $registro ){
	
	if ($url_amigavel_on){ 
		$link_registro = $url_amigavel.'/depoimentos/'.encodarURL( $registro->nome ).'/'.$registro['codigo'].'-depoimento.html'; 
	}else{ 
		$link_registro = $url_site.'/depoimentos/detalhes.php?ID='.$registro['codigo']; 
	}
	
	echo 	'<div class="post-snippet mb64">
				<div class="post-title">
					<span class="label">'.formataData( $registro->data, 'tela' ).'</span>
					<!--<a href="'.$link_registro.'">-->
						<h4 class="inline-block">'.$registro->nome.'</h4>
					<!--</a>-->
				</div>
				<hr>
				<p>';
	
	if ( $registro->foto != '' ){			
		echo 	'	<!--<a href="'.$link_registro.'" class="thumb_pequeno">-->
					<span class="thumb_pequeno">
						<img class="mb24" alt="Post Image" src="'.$registro->foto.'" width="100%" />
					</span>
					<!--</a>-->';
	}
					
	echo 		'	<b>'.$registro->resumo.'</b><BR />"'.$registro->descricao.'"<BR /><b>';
	
	if ( $registro->formacao != '' )
		echo	$registro->formacao;
	
	if ( $registro->formacao != '' && $registro->empresa != '' )
		echo	', ';
	
	if ( $registro->empresa != '' )
		echo	$registro->empresa;
	
	echo 		'</b></p>
				<!--<a class="btn btn-sm" href="'.$link_registro.'">Leia mais</a>-->
				&nbsp;
			</div>';
			
}
?>


<div class="text-center">
    <ul class="pagination">
    	<?php
		// Paginação
		include_once '../biblioteca/paginacao_paginas.php';
		?>
    </ul>
</div>