<?php
include_once '../../api/biblioteca/funcoes.php';
include_once '../biblioteca/variaveis.php';

$ID		= formataVar( 'ID', 'GET' );

// Dados do conteúdo
$xml_registro 	= executaPagina( 'api/', array( 'a'=>'depoimentos', 'metodo'=>'ver', 'ID'=>$ID ) );
$registro 		= lerXML( $xml_registro );
	
if ( $registro->erro == 0 ){
	
	$registro				= $registro->depoimentos->depoimento;
	
}else{
	exit("Erro ao acessar os dados do depoimento");		
}

/* CABEÇALHO */
include_once '../biblioteca/cabecalho.php';
?>

<div class="main-container">
	<section>
        
        <div class="container">
            <div class="row">
                <div class="col-md-9 mb-xs-24" id="conteudo_paginado">
                	<?php
					echo 	'<div class="post-snippet mb64">
								<div class="post-title">
									<span class="label">'.formataData( $registro->data, 'tela' ).'</span>
									<h4 class="inline-block">'.$registro->nome.'</h4>
								</div>
								<hr>
								<p>';
					
					if ( $registro->foto != '' ){			
						echo 	'	<span class="thumb_pequeno">
										<img class="mb24" alt="Post Image" src="'.$registro->foto.'" width="100%" />
									</span>';
					}
					
					echo 		'	<b>'.$registro->resumo.'</b><BR />"'.$registro->descricao.'"<BR /><b>';
	
					if ( $registro->formacao != '' )
						echo	$registro->formacao;
					
					if ( $registro->formacao != '' && $registro->empresa != '' )
						echo	', ';
					
					if ( $registro->empresa != '' )
						echo	$registro->empresa;
					
					echo 		'</b></p>
								&nbsp;
							</div>';
					?>
                </div>
                <div class="col-md-3 hidden-sm">
                    
                    <div class="widget">
                        <h6 class="title">Depoimentos em:</h6>
                        <hr>
                        
                        <ul class="link-list">
                        	<?php
							if ($url_amigavel_on){ 
								$link_registro = $url_amigavel.'/depoimentos.html'; 
							}else{ 
								$link_registro = $url_site.'/depoimentos/'; 
							}
							
							// Temas
							$xml_temas 	= executaPagina( 'api/', array( 'a'=>'temas' ) );
							$temas 		= lerXML( $xml_temas );
								
							if ( $temas->erro == 0 ){
								foreach( $temas->temas->tema as $tema ){
									echo '<li><a href="'.$link_registro.'?ID_tema='.$tema['codigo'].'">'.$tema.'</a></li>';
								}
							}
							?>
                        </ul>
                    </div>
                    
                </div>
                
                <center><a class="btn btn-lg mt-xs-24" href="<?php if ($url_amigavel_on){ echo $url_amigavel.'/depoimentos.html'; }else{ echo $url_site.'/depoimentos/'; } ?>">Veja mais depoimentos</a></center>
                
            </div>
        </div>
        
       
	
    </section>
</div>

<?php
/* RODAPÉ */	
include_once '../biblioteca/rodape.php';
?>
