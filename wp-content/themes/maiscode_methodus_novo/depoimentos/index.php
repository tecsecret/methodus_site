<?php
include_once '../../api/biblioteca/funcoes.php';
include_once '../biblioteca/variaveis.php';

/* CABEÇALHO */
include_once '../biblioteca/cabecalho.php';
?>

<script>
	ID_tema 		= '<?php echo formataVar( 'ID_tema', 'GET' ) ?>';
	pesquisa	 	= '';
	paginacao_tipo	= 'depoimentos';
</script>
<script src="<?php echo $url_site; ?>/biblioteca/js/paginacao.js"></script>
<div class="main-container">
	<section>
        
        <div class="container">
            <div class="row">
                <div class="col-md-9 mb-xs-24" id="conteudo_paginado">
                	
                </div>
                <div class="col-md-3 hidden-sm">
                    
                    <div class="widget">
                        <h6 class="title">Depoimentos em:</h6>
                        <hr>
                        
                        <ul class="link-list">
                        	<?php
							// Temas
							$xml_temas 	= executaPagina( 'api/', array( 'a'=>'temas' ) );
							$temas 		= lerXML( $xml_temas );
								
							if ( $temas->erro == 0 ){
								foreach( $temas->temas->tema as $tema ){
									echo '<li><a href="?ID_tema='.$tema['codigo'].'">'.$tema.'</a></li>';
								}
							}
							?>
                        </ul>
                    </div>
                    
                </div>
                
            </div>
        </div>
	
    </section>
</div>

<?php
/* RODAPÉ */	
include_once '../biblioteca/rodape.php';
?>
