<?php
/**
* Template Name: Página Home
*
* @package WordPress
* @author Mais Code Tecnologia
* @since First Version
*/
get_header(); ?>

<div class="main-container">
  
<section class="image-slider slider-all-controls controls-inside parallax pt0 pb0 height-70">

  <ul class="slides">
    <?php if ( have_rows('slider_home') ): ?>
      <?php while ( have_rows('slider_home') ) : the_row(); ?>
        <li class="overlay image-bg">
          <div class="background-image-holder"> 
            <img alt="image" class="background-image" alt="Curso de Oratória" src="<?php the_sub_field('imagem_de_fundo'); ?>"> 
          </div>
          <div class="container v-align-transform">
            <div class="row text-center">
              <div class="col-md-10 col-md-offset-1">

                <h1 class="mb-xs-16 bold"><?php the_sub_field('titulo_slider_home'); ?></h1>
                <p class="lead mb40"><?php the_sub_field('texto_slider_home'); ?></p>

                <a class="btn btn-lg" href="<?php the_sub_field('link_btn_vermais'); ?>"><?php the_sub_field('btn_vermais'); ?></a> 
                <a class="btn btn-lg btn-filled inner-link" href="#nossos_cursos"><?php the_sub_field('btn_outroscursos'); ?></a> 

              </div>
            </div>
          </div>
        </li>
      <?php endwhile;  ?>
    <?php endif ?>
  </ul>
</section>

<section id="home-section-methodus" class="bg-secondary_methodus" >
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-6 methodus-cursos"> 
        <div class="content-cursos">
          <img alt="Logo Methodus" src="<?php the_field('logo_home');?>" style="height: 100px; margin-bottom: 20px">        
          <div class="lead">
            <?php the_field('texto_sobre_home'); ?>
          </div> 
        </div>       
      </div>
      <div class="col-md-6 methodus-curso-presencial" style="background: linear-gradient(rgba(48,77,165,.98),rgba(48,77,165,.98)),url(<?php echo esc_url( get_template_directory_uri() ); ?>/img/section-home-bg.png) repeat-x;">        
          <div class="content-cursos">
            <h3 class=""><?php the_field('texto_curso_saibamais'); ?><br>
              &nbsp;</h3>
            <div class="botao-section-methodus">
              <a class="btn btn-lg btn-white mb8 mt-xs-24" href="<?php the_field('link_btn_vermais_curso'); ?>"><?php the_field('btn_vermais_curso'); ?></a>  
            </div> 
          </div>      
      </div>
    </div>


    </div>
  </div>
</section>
  
<section class="bg-secondary_methodus" id="home-nossos-cursos-section">  
  <div class="container" id="nossos_cursos">  
    <div class="row mb24 mb-xs-24 nossos-cursos-titulo">
      <div class="col-md-10 col-md-offset-1 col-sm-12 position-relative d-flex justify-content-center">
        <div>
          <h3 class="mb-0 "><?php the_field('titulo_nossos_cursos'); ?></h3>
          <div class="barra"></div>
        </div>
      </div>
    </div>    
    <div class="row">
      <?php if (have_rows('cursos')): ?>
        <?php while (have_rows('cursos')) : the_row(); ?>
        
          <div class="col-sm-4 mb18 mb-xs-24"> 
            <a class="no-hover home-cursos-box" href="<?php the_sub_field('link_do_curso'); ?>">
              <img src="<?php the_sub_field('imagem_curso'); ?>" />
              <h4 class="mb-0"><?php the_sub_field('titulo_curso'); ?></h4>
            </a>
            <p><?php the_sub_field('sobre_curso'); ?></p>
          </div>

        <?php endwhile ?>
      <?php endif ?>

    </div> 
    
  </div>
  
</section>
  
<section id="home-teste" style="background: linear-gradient(rgba(48,77,165,.98),rgba(48,77,165,.98)),url(<?php echo esc_url( get_template_directory_uri() ); ?>/img/section-home-bg.png) repeat-x;">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 text-center">
        <h3 class="mb0 inline-block p32 p0-xs"><?php the_field('texto_teste'); ?></h3>        
      </div>
      <div class="col-sm-12 text-center">        
        <a class="btn btn-lg btn-white mb8 mt-xs-24" href="<?php the_field('link_btn_teste'); ?>"><?php the_field('btn_teste'); ?></a> 
      </div>
    </div>
  </div>
</section>

<section id="home-imagem-section">
  <div class="container">
    <div class="row">
      <div class="col-sm-6 text-center">
        <img alt="Prof. Alcides Schotten" class="mb-xs-24" src="<?php the_field('imagem_metodo'); ?>"> 
      </div>
      <div class="col-sm-6 ">
        <div class="position-relative d-flex justify-content-center">
          <h6 class="">Método <b>exclusivo</b> criado pelo professor e filósofo <b>Alcides Schotten</b> <?php //the_field('titulo_metodo'); ?></h6>
          <div class="barra-imagem"></div>
        </div>
        <p class="lead mb40"><?php the_field('sobre_metodo'); ?></p>
        <div class="botao-section-imagem">
        <a class="btn-lg btn" href="<?php the_field('link_saiba_mais'); ?>"><?php the_field('btn_saibamais_metodo'); ?></a> 
        </div>          
      </div>
    </div>
  </div>
</section>
 
<?php get_template_part( 'depoimentos/carrossel', 'Depoimentos' ); ?>

<?php /*get_template_part( 'form/form-newslleter', 'Newsletter' );*/ ?>

<section id="home-nossos-cursos" style="background: linear-gradient(rgba(196,196,196,.98),rgba(196,196,196,.98)),url(<?php echo esc_url( get_template_directory_uri() ); ?>/img/section-home-bg.png) repeat-x;">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 text-center">
        <h3 class="mb0 inline-block p32 p0-xs"><?php the_field('breve_texto'); ?>
        </h3>
      </div>
      <div class="col-sm-12 text-center">
        <a class="btn btn-lg btn-white mb8 mt-xs-24 inner-link" href="<?php the_field('link_btn_lider'); ?>"><?php the_field('btn_lider'); ?></a> 
      </div>
    </div>
  </div>
</section>



<?php get_footer(); ?>