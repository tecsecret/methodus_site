<?php
/**
* Template Name: Página Pagamento
* 
*
* @package WordPress
* @author Mais Code Tecnologia
* @since First Version
*/

get_header();


$urlWP = get_template_directory_uri();
$cupom = $_GET["cupom"];
$discount = $_GET["bf_discount"];

// URL utilizada para caminhos de arquivos (URLs amigáveis)
$url_site = get_option('link_site'); 
$url_site_wp     =  site_url();
$url_api      =  get_option('link_api');

// URLs amigáveis
$url_amigavel_on  = 1;
$url_amigavel    =  get_option('link_site_amigavel');

//$ID_inscricao   = formataVar( 'ID_inscricao', '*' ); //, 'POST' ); // para teste, vou permitir enviar ID da inscrição por GET

// Variável estática para testar checkout 
$ID_inscricao = $_POST["ID_inscricao"];

// Dados da inscrição
$xml_inscricao  = executaPagina( 'api/', array( 'a'=>'matricula', 'metodo'=>'ver', 'ID'=>$ID_inscricao ) );
$inscricao      = lerXML( $xml_inscricao );

if ( $inscricao->erro == 0 ){
    
    $inscricao          = $inscricao->inscricao;
    $ID_curso           = $inscricao->ID_curso;
    $ID_plano           = $inscricao->ID_plano;
    $ID_data            = $inscricao->ID_data;
    $forma              = $inscricao->pagamento->forma;
    $meio               = $inscricao->pagamento->meio;
    $valor              = $inscricao->pagamento->valor;

    
} else {
    exit("Erro ao acessar os dados da inscrição");  
}
if (isset($_GET["cupom"])) {
  $desc = 0.1;
  $valor = ($valor - ($valor * $desc));
}
// Dados do curso
$xml_curso  = executaPagina( 'api/', array( 'a'=>'cursos', 'metodo'=>'ver', 'ID'=>(int)$ID_curso ) );
$curso      = lerXML( $xml_curso );
    
if ( $curso->erro == 0 ){
    
    $curso              = $curso->curso;
    $titulo_curso       = $curso->titulo;
    $banner_curso       = $curso->banner;
    $variaveis_curso    = $curso->variaveis;
    $planos_curso       = $curso->planos;
    $turmas_curso       = $curso->turmas->turma;
    
    if ( isset( $variaveis_curso->classe ) ){
        $classe_curso = $variaveis_curso->classe;
    }else{
        $classe_curso = 'vermelho'; 
    }
    
    if ( isset( $curso->planos->plano ) == false ){
        exit("Erro ao acessar planos do curso");        
    }else{
            
        $plano_escolhido = $curso->planos->xpath("plano[@codigo='".$ID_plano."']");
        if ( count( $plano_escolhido ) >= 1 ){
            $plano_escolhido = $plano_escolhido[0]; 
        }else{
            exit("Erro com o plano escolhido");     
        }
            
    }
    
    if ( isset( $curso->turmas->turma ) == false ){
        exit("Erro ao acessar turmas do curso");        
    }else{
            
        $data_escolhida = $curso->turmas->xpath("turma[@codigo='".$ID_data."']");
        if ( count( $data_escolhida ) >= 1 ){
            $data_escolhida = $data_escolhida[0];   
        }else{
            exit("Erro com a turma escolhida");     
        }
            
    }
    
    // verificar se é plano empresarial
    if ( $plano_escolhido['empresarial'] == '1' ){
        $plano_empresarial = true;  
    }else{
        $plano_empresarial = false;     
    }
    
}else{
    exit("Erro ao acessar os dados do curso");      
}

//BLACK FRIDAY
if (isset($_GET["bf_discount"])) {
    $oldValor = $valor;
    $valor = $_GET["bf_discount"];
}
 
?>
<div class="main-container">

    <section class="page-title page-title-4 image-bg overlay parallax">
      <div class="background-image-holder"> <img alt="Background Image" class="background-image" src="<?php echo $urlWP; echo $banner_curso; ?>"> </div>
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <h1 class="titulo-curso"><?php echo $titulo_curso; ?></h1>
          </div>
        </div>
      </div>
    </section>
    
    <section>
        <div class="container">
           <div class="row mb0">
           
              <div class="col-sm-12">
                 <h2 class="number">Pagamento</h2>
                 <hr>
                 <strong>Curso:</strong> <?php echo $titulo_curso; ?><BR />
                 <strong>Grupo:</strong> <?php echo ( $data_escolhida->horario . ' / de ' . formataData( $data_escolhida->inicio, 'tela' ) . ' à ' . formataData( $data_escolhida->fim, 'tela' ) ) ;?>
                 <p class="mb0">Número de inscrição: <span class="texto-<?php echo $classe_curso; ?>"><?php echo $ID_inscricao; ?></span></p>
                 <?php /*<p class="mb0">Investimento: <strong>Em <?php echo $forma; ?>x de R$ <?php echo formataMoeda( ($valor / $forma), 'tela' ) ?></strong></p>*/ ?>
                 <p class="mb0">Investimento: <strong>Em <?php echo $forma; ?>x de R$ <?php echo formataMoeda( ($valor), 'tela' ) ?></strong></p>
              </div>

           </div>
        </div>
    </section>
    
    <?php
    // Exibir informações de pagamento para cada meio
    if ( $meio == '4' ){ // Depósito
    ?>
        
        <section>
            <div class="container">
               <div class="row mb0">
               
                  <div class="col-sm-12">
                     <h3 class="number">Instruções de pagamento</h3>
                     <hr>
                     <p>
                        <?php 
                        if ($forma == 1){ 
                            echo 'Faça o depósito no valor de R$ '.formataMoeda( $valor, 'tela' );
                        }else{
                            echo 'Faça o depósito da primeira parcela no valor de R$ '.formataMoeda( ($valor / $forma), 'tela' );   
                        }
                        ?> 
                        no Banco Bradesco - Agência 3114-3 - Conta 169361-1 - Favorecido: Methodus Consultoria<br />
                        <?php 
                        if ($forma > 1){ 
                            echo 'As demais parcelas serão acertadas na primeira aula através de cartão de crédito ou cheques pré-datados.<BR />';
                        }
                        ?>
                        Envie o comprovante para o e-mail <a href="mailto:info@methodus.com.br">info@methodus.com.br</a>
                     </p>
                  </div>
    
               </div>
            </div>
        </section>
    
    <?php
    }else if ( $meio == '0' ){ // Cheques
    ?>
        
        <section>
            <div class="container">
               <div class="row mb0">
               
                  <div class="col-sm-12">
                     <h3 class="number">Instruções de pagamento</h3>
                     <hr>
                     <p>
                        <?php 
                        if ($forma == 1){ 
                            echo 'Faça um cheque no valor de R$ '.formataMoeda( $valor, 'tela' );
                        }else{
                            echo 'Faça um cheque da primeira parcela no valor de R$ '.formataMoeda( ($valor / $forma), 'tela' );    
                        }
                        ?> 
                        e entregue na unidade da Methodus (Av. Paulista 2202, Cj 134)<br />
                        <?php 
                        if ($forma > 1){ 
                            echo 'As demais parcelas serão acertadas na primeira aula através de cheques pré-datados ou cartão de crédito.';
                        }
                        ?>
                     </p>
                  </div>
    
               </div>
            </div>
        </section>
        
    <?php
    }else if ( $meio == '6' ){ // Boleto bancário
    ?>
        
        <section>
            <div class="container">
               <div class="row mb0">
               
                  <div class="col-sm-12">
                     <h3 class="number">Instruções de pagamento</h3>
                     <hr>
                     <p>
                        Clique no botão abaixo para imprimir seu boleto bancário. 
                        <?php 
                        if ($forma > 1){ 
                            echo 'As demais parcelas serão enviadas em seu e-mail ou podem ser acertadas na primeira aula através de cartão de crédito.';
                        }
                        ?>
                        
                        <br><br>
                        <input id="conversao_imprimirboleto" type="button" value="Imprimir boleto" onclick="janela('<?php echo $url_site; ?>site/boleto/?ID=<?php echo $ID_inscricao; ?>&referencia=0','710','600','yes','no');" />
                     
                     </p>
                  </div>
    
               </div>
            </div>
        </section>
        
    <?php
    }else{ // Cartão de crédito
    ?>
        <script src="<?php echo $urlWP; ?>/matricula/pagamento.js"></script>
        <form method="post" action="<?php echo $url_api ?>/api/" id="formPagamento" class="formAjax" data-validacao="checaPagamento" data-resultado="pagamentoSucesso">
        <input type="hidden" name="a" value="matricula" />
        <input type="hidden" name="metodo" value="pagamento" />
        <input type="hidden" name="ID_inscricao" value="<?php echo $ID_inscricao ?>" />
        
        <?php 
           if (isset($_GET["cupom"])) {
         ?>
            <input type="hidden" name="cupom" value="1" />
 
         <?php 
            }
         ?>
        
            <div class="container">
                <h3 class="number">Dados do cartão de crédito</h3>
                <hr>   

                    <div class="row">
                     
                        <div class="caixa_meio_pagamento col-sm-3">
                            <label class="radio"><input type="radio" name="meio" value="visa" /><span></span>
                                <img src="<?php echo $url_site; ?>site/img/meios_visa.png" width="48" height="34" align="absmiddle" /> &nbsp; VISA
                            </label> 
                        </div>
                        <div class="caixa_meio_pagamento col-sm-3">
                            <label class="radio"><input type="radio" name="meio" value="mastercard" /><span></span>
                                <img src="<?php echo $url_site; ?>site/img/meios_mastercard.png" width="48" height="34" align="absmiddle" /> &nbsp; MASTERCARD
                            </label> 
                        </div>
                        <div class="caixa_meio_pagamento col-sm-3">
                            <label class="radio"><input type="radio" name="meio" value="amex" /><span></span>
                                <img src="<?php echo $url_site; ?>site/img/meios_amex.png" width="48" height="34" align="absmiddle" /> &nbsp; AMEX
                            </label> 
                        </div>
                        <div class="caixa_meio_pagamento col-sm-3">
                            <label class="radio"><input type="radio" name="meio" value="diners" /><span></span>
                                <img src="<?php echo $url_site; ?>site/img/meios_diners.png" width="48" height="34" align="absmiddle" /> &nbsp; DINERS
                            </label> 
                        </div>
                        
                    </div>
                  
                    <div class="row">
                        <div class="col-sm-6">
                            <input type="text" name="cartao_nome" placeholder="NOME DO PORTADOR DO CARTÃO" lang="Nome do portador do cartão" maxlength="150" /> 
                        </div>
                        <div class="col-sm-6">
                            <input type="text" name="cartao_numero" placeholder="NÚMERO DO CARTÃO" lang="Número do cartão" onKeyPress="mascara(this,mascaraSoNumeros)" maxlength="16" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <input type="text" name="cartao_seguranca" placeholder="CÓDIGO DE SEGURANÇA" lang="Código de segurança" onKeyPress="mascara(this,mascaraSoNumeros)" maxlength="5" /> 
                        </div>
                        <div class="col-sm-3">
                            <div class="codigo_seguranca" onclick="$('#codigo_seguranca').toggle('fast');">
                                O que é o código de segurança?
                                <div id="codigo_seguranca">
                                    O código de segurança fica atrás do cartão de crédito logo após os números finais do cartão, normalmente possui 3 ou 4 dígitos.<center/><BR /><img src="<?php echo $url_site ?>/img/codigo_seguranca.png" width="100%" /></center>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" name="cartao_validade" placeholder="VALIDADE" lang="Validade" onKeyPress="mascara(this,mascaraDataParcial)" maxlength="7" /> 
                        </div>
                        <div class="col-sm-3">
                            <input type="submit" value="Efetuar pagamento" />
                        </div>
                    </div>

            </div>
        
        
        </form>
        
    <?php
    }
    ?>
    
    
</div>


<?php get_footer();