<?php
/**
* Template Name: Página Boleto
* 
*
* @package WordPress
* @author Mais Code Tecnologia
* @since First Version
*
*/

// include_once '../../api/biblioteca/funcoes.php';
// include_once '../biblioteca/variaveis.php'; 
get_header();
get_template_part('api/biblioteca/funcoes');


$urlWP = get_template_directory_uri();

 // URL utilizada para caminhos de arquivos (URLs amigáveis)
$url_site       = 'https://www.methodus.com.br/site';
$url_site_wp     =  site_url();
$url_api      = 'https://www.methodus.com.br/api/';

// URLs amigáveis
$url_amigavel_on  = 1;
$url_amigavel   = 'https://www.methodus.com.br';
 
 


// $conexao = conexaoBD();

$ID_inscricao   = formataVar( 'ID', 'GET' );
$referencia     = formataVar( 'referencia', 'GET' );
$parcela        = formataVar( 'parcela', 'GET' );



// Dados da inscrição
$xml_inscricao  = executaPagina( 'api/', array( 'a'=>'matricula', 'metodo'=>'ver', 'ID'=>$ID_inscricao ) );
$inscricao      = lerXML( $xml_inscricao );
    
if ( $inscricao->erro == 0 ){
    
    $inscricao          = $inscricao->inscricao;
    $ID_curso           = $inscricao->ID_curso;
    $ID_plano           = $inscricao->ID_plano;
    $ID_data            = $inscricao->ID_data;
    $data_inscricao     = $inscricao->data;
    $forma              = $inscricao->pagamento->forma;
    $meio               = $inscricao->pagamento->meio;
    $valor              = $inscricao->pagamento->valor;

    // verificar se parcela escolhida é menor ou igual a parcelamento do pedido
    if (!$parcela){
        $parcela = 1;
    }else{
        if ( $parcela > $forma ){
            exit("Parcela não existe"); 
        }
    }
    
    $participante       = $inscricao->participantes->participante;
    $cobrado_nome       = $participante->nome;
    $cobrado_doc        = formataCPF( $participante->cpf );
    $cobrado_email      = $participante->email;
    $cobrado_ID_usuario = $participante->codigo;

    // recuperar dados do usuário
    $sql = "select endereco_usuario, bairro_usuario, cidade_usuario, estado_usuario, cep_usuario from USUARIOS where ID_usuario=$cobrado_ID_usuario";
    $rs = abrirRs( $sql );
            
    if ( sqlsrv_num_rows( $rs ) > 0 ){  
        $aluno                  = sqlsrv_fetch_array( $rs );

        $cobrado_endereco = $aluno['endereco_usuario'];
        $cobrado_bairro = $aluno['bairro_usuario'];
        $cobrado_cidade = $aluno['cidade_usuario'];
        $cobrado_estado = $aluno['estado_usuario'];
        $cobrado_pais = 'Brasil';
        $cobrado_cep = $aluno['cep_usuario'];
    }else{
        exit('Aluno não encontrado');
    }

    // Se é inscrição e nome de empresa, substituir dados do sacado
    if ( isset( $inscricao->empresa->razao ) && !empty( $inscricao->empresa->razao ) ){
        $cobrado_nome       = $inscricao->empresa->razao;
        $cobrado_doc        = formataCNPJ( $inscricao->empresa->cnpj );
    }
    
    // variáveis para iPag
    $meio_ipag  = 'boletoshopfacil'; //'boleto_bradesco';
    $forma_ipag = 1;
    $valor_ipag = $valor / $forma;

    // calcular vencimento
    $vencimento = dateAdd( $data_inscricao, ($parcela - 1), 'm' );
    $vencimento = dateAdd( $vencimento, 3, 'd' ); // adicionar 3 dias
    $vencimento = formataData( $vencimento, 'tela' );

    // Nosso número (máximo de 11 dígitos) (Prefixo 9 para boletos emitidos depois de 31/01/2018)
    $nosso_numero = '9'. substr( ('0000000000'.$ID_inscricao.substr( ('00'.$parcela), -2 )), -10 );

    // temporário, para corrigir boleto emitido errado
    if ( $ID_inscricao == 86519 ){
        $nosso_numero = '0'. substr( ('0000000000'.$ID_inscricao.substr( ('00'.$parcela), -2 )), -10 );
    }
    
}else{
    exit("Erro ao acessar os dados da inscrição");  
}

// Dados do curso
$xml_curso  = executaPagina( 'api/', array( 'a'=>'cursos', 'metodo'=>'ver', 'ID'=>$ID_curso ) );
$curso      = lerXML( $xml_curso );
    
if ( $curso->erro == 0 ){
    
    $curso              = $curso->curso;
    $titulo_curso       = $curso->titulo;
    
}else{
    exit("Erro ao acessar os dados do curso");      
}

// Para enviar e-mail
if ( strval($referencia.'') != '1' ){
    
    $mensagem   =   'Olá <b>'.$cobrado_nome.'</b>, <BR />
                    Você gerou um boleto bancário em nosso site referente ao curso <b>'.$titulo_curso.'</b> <BR />';
    
    if ( $forma > 1 ){
        $mensagem   .=  'Nos links abaixo você pode emitir seus boletos:<BR /><BR />';

        for ($conta_parcela=1; $conta_parcela <= $forma; $conta_parcela++){
            $vencimento_temp = dateAdd( $data_inscricao, ($conta_parcela - 1), 'm' );
            $vencimento_temp = dateAdd( $vencimento_temp, 3, 'd' ); // adicionar 3 dias
            $vencimento_temp = formataData( $vencimento_temp, 'tela' );
            $mensagem   .=  '<a href="http://www.methodus.com.br/site/boleto/?ID='.$ID_inscricao.'&referencia=1&parcela='.$conta_parcela.'">Boleto da '.$conta_parcela.'ª parcela</a> com vencimento em '.$vencimento_temp.'.<BR />';
        }

    }else{
        $mensagem   .=  'No link abaixo você pode emitir seu boleto:<BR /><BR />
                        <a href="http://www.methodus.com.br/site/boleto/?ID='.$ID_inscricao.'&referencia=1">Boleto à vista</a> com vencimento em '.$vencimento.'.<BR />';
    }

    

    $mensagem   .=  '<BR />Pague em qualquer agência bancária ou pelo internet bank. <BR /><BR />
                    Atenciosamente,<BR />
                    Methodus <BR />
                    <a href="http://www.methodus.com.br">www.methodus.com.br</a><BR />
                    <a href="mailto:info@methodus.com.br">info@methodus.com.br</a>';
    
    $assunto = 'Methodus - Inscrição e Emissão de Boleto';
    
    enviaEmail( $assunto, $mensagem, $cobrado_email, $cobrado_nome, $email_principal, 'Methodus', 1 );
    enviaEmail( $assunto, $mensagem, $email_principal, 'Methodus', $email_principal, 'Methodus', 1 );
    //echo 'DEBUG: '.$mensagem;
        
}



//dados do pedido
$fields = array(
    'identificacao' => $ipag_login,
    'pedido'        => $nosso_numero,
    'operacao'      => 'Pagamento',
    'url_retorno'   => 'https://www.methodus.com.br/retornoipag/profile_id/'.$nosso_numero.'/',
    'retorno_tipo'  => 'xml',
    'valor'         => strval($valor_ipag),
    'nome'          => $cobrado_nome,
    'doc'           => $cobrado_doc,
    'email'         => $cobrado_email,
    'endereco'      => $cobrado_endereco,
    'bairro'        => $cobrado_bairro,
    'cidade'        => $cobrado_cidade,
    'estado'        => $cobrado_estado,
    'pais'          => $cobrado_pais,
    'cep'           => $cobrado_cep,
    'metodo'        => $meio_ipag,
    'parcelas'      => $forma_ipag,
    'vencto'        => $vencimento
);

//print_r($fields);
//exit();

$fields_string ='';
foreach($fields as $key=>$value) { 
    $fields_string .= $key.'='.urlencode($value).'&'; 
}
$fields_string = rtrim($fields_string, '&');

$ch = curl_init();
curl_setopt( $ch, CURLOPT_URL, $ipag_api.'pagamento' );
curl_setopt( $ch, CURLOPT_POST, true );
curl_setopt( $ch, CURLOPT_POSTFIELDS, $fields_string );
curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
curl_setopt($ch, CURLOPT_HEADER, false);
curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
curl_setopt( $ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)' );
curl_setopt( $ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1 );

$retorno_ipag = curl_exec( $ch );
curl_close( $ch );

// Transformar o boleto de ISO-8859-1 para UTF-8
//$retorno_ipag = str_replace( 'ISO-8859-1', 'UTF-8', $retorno_ipag );
//$retorno_ipag =   charSet($retorno_ipag,'utf8');

// Exibir boleto
//header('Content-Type: text/html; charset=iso-8859-1',true); 
//echo($retorno_ipag);


// Ler retorno XML do boleto
/*<retorno>
    <id_transacao>f1f3e96846807621dd3f4e0833a9887e</id_transacao>
    <valor>298.33</valor>
    <num_pedido>90008671801</num_pedido>
    <status_pagamento>2</status_pagamento>
    <mensagem_transacao>REGISTRO EFETUADO COM SUCESSO - CIP NAO CONFIRMADA</mensagem_transacao>
    <metodo>boletoshopfacil</metodo>
    <operadora>bradesco</operadora>
    <operadora_mensagem>REGISTRO EFETUADO COM SUCESSO - CIP NAO CONFIRMADA</operadora_mensagem>
    <id_librepag>1992001</id_librepag>
    <autorizacao_id></autorizacao_id>
    <url_autenticacao>https://meiosdepagamentobradesco.com.br/apiboleto/Bradesco?token=ZzQ3cUp2M0dLQnZHVXEvQlpkeGFJSkxZV05pbTRwVU15YVlhaHhQaUdhY0VsZmJUbjY1RFhnPT0.</url_autenticacao>
    <linha_digitavel>23793.11422  69000.867189  01016.936104  7  76980000029833</linha_digitavel>
</retorno>*/

$boleto_ipag = lerXML( $retorno_ipag );

if ( isset( $boleto_ipag-> status_pagamento ) && $boleto_ipag-> status_pagamento == 2 ){
    $ID_ipag            = $boleto_ipag-> id_librepag;
    $numero_pedido      = $boleto_ipag-> num_pedido;
    $url_boleto         = $boleto_ipag-> url_autenticacao;
    $linha_boleto       = $boleto_ipag-> linha_digitavel;

    //echo 'DEBUG: '.$url_boleto;
    header('Location: '.$url_boleto);

}else if ( isset( $boleto_ipag-> status_pagamento ) ){

    $ID_ipag            = $boleto_ipag-> id_librepag;
    $numero_pedido      = $boleto_ipag-> num_pedido;
    $id_transacao       = $boleto_ipag-> id_transacao;
    $status_pagamento       = $boleto_ipag-> status_pagamento;
    $url_boleto         = $boleto_ipag-> url_autenticacao;
    $linha_boleto       = $boleto_ipag-> linha_digitavel;
    $mensagem_transacao = $boleto_ipag-> mensagem_transacao;
    $operadora_mensagem = $boleto_ipag-> operadora_mensagem;
    $valor              = $boleto_ipag-> valor;

    echo    "<h2>Erro ao processar o boleto:</h2>
            ID_ipag: $ID_ipag
            <BR />valor: $valor
            <BR />status_pagamento: $status_pagamento
            <BR />numero_pedido: $numero_pedido
            <BR />id_transacao: $id_transacao
            <BR />mensagem_transacao: $mensagem_transacao
            <BR />operadora_mensagem: $operadora_mensagem";
    exit();

}else{
    echo '<h2>Erro ao processar o boleto:</h2><BR />';
    var_dump($retorno_ipag);
    exit();
}

exit();?>

<?php get_footer(); 