<?php
/**
* Template Name: Página Paginacao Depoimentos
*
* @package WordPress
* @author Mais Code Tecnologia
* @since First Version
*/
//get_header('paginacao'); 

$url_amigavel   =  get_option('link_api');
$paginacao_inicio   = formataVar( 'paginacao_inicio', 'GET' );
$ID_tema            = formataVar( 'ID_tema', 'GET' );
$paginacao_tamanho  = 10;

if (!$paginacao_inicio){
    $paginacao_inicio = 1;  
}

// Paginas
$xml_pagina = executaPagina( 'api/', array( 'a'=>'depoimentos', 'metodo'=>'listar_todos', 'inicio'=>$paginacao_inicio, 'paginacao'=>$paginacao_tamanho, 'ID_tema'=>$ID_tema  ) );
$pagina     = lerXML( $xml_pagina );
    
if ( $pagina->erro == 0 ){
    
    $quantidade     = $pagina->quantidade;
    $registros      = $pagina->depoimentos->depoimento;
    
}else{
    exit("Erro ao acessar os dados da página");     
}

foreach( $registros as $registro ){
    
    if ($url_amigavel_on){ 
        $link_registro = $url_amigavel.'/depoimentos/'.encodarURL( $registro->nome ).'/'.$registro['codigo'].'-depoimento.html'; 
    }else{ 
        $link_registro = $url_site.'/depoimentos/detalhes.php?ID='.$registro['codigo']; 
    }
    
    echo    '<div class="post-snippet mb64">
                <div class="post-title">
                    <span class="label">'.formataData( $registro->data, 'tela' ).'</span>
                    <!--<a href="'.$link_registro.'">-->
                        <h4 class="inline-block">'.$registro->nome.'</h4>
                    <!--</a>-->
                </div>
                <hr>
                <p>';
    
    if ( $registro->foto != '' ){           
        echo    '   <!--<a href="'.$link_registro.'" class="thumb_pequeno">-->
                    <span class="thumb_pequeno">
                        <img class="mb24" alt="Post Image" src="'.$url_amigavel.''.$registro->foto.'" width="100%" />
                    </span>
                    <!--</a>-->';
    }
                    
    echo        '   <b>'.$registro->resumo.'</b><BR />"'.$registro->descricao.'"<BR /><b>';
    
    if ( $registro->formacao != '' )
        echo    $registro->formacao;
    
    if ( $registro->formacao != '' && $registro->empresa != '' )
        echo    ', ';
    
    if ( $registro->empresa != '' )
        echo    $registro->empresa;
    
    echo        '</b></p>
                <!--<a class="btn btn-sm" href="'.$link_registro.'">Leia mais</a>-->
                &nbsp;
            </div>';
            
}
?>


<div class="text-center">
    <ul class="pagination">
              <?php
            // Paginação

            // total de páginas
            $total_paginas = ceil($quantidade/$paginacao_tamanho);

            // Página atual
            $pagina_atual = ceil( ($paginacao_inicio/$paginacao_tamanho) );

            if ( $total_paginas > 0 ){

                // Verificar se existe uma página anterior
                if ( $paginacao_inicio && $paginacao_inicio > 1 ){
                    $pagina_anterior = $paginacao_inicio - $paginacao_tamanho ;
                    
                    echo    '<li>
                                <a href="#'.$pagina_anterior.'" aria-label="Anterior">
                                    <span aria-hidden="true">&laquo;</span>
                                </a>
                            </li>';
                }
                
                // Listar as páginas intermediárias
                if ( $pagina_atual < 3 ){
                    $pag_inicio = 1;
                    $pag_fim    = 5;
                }elseif ( $pagina_atual+2 > $total_paginas ){
                    $pag_inicio = $total_paginas-5;
                    $pag_fim    = $total_paginas;
                }else{
                    $pag_inicio = $pagina_atual-2;
                    $pag_fim    = $pagina_atual+2;  
                }
                
                if ($pag_inicio <= 0){
                    $pag_inicio = 1;    
                }
                if ($pag_fim > $total_paginas){
                    $pag_fim = $total_paginas;  
                }
                
                for ($i=$pag_inicio; $i<=$pag_fim; $i++){
                    
                    if ($i == $pagina_atual){
                        echo '<li class="active">';
                    }else{
                        echo '<li>';    
                    }
                    echo    '   <a href="#'.((($i-1)*$paginacao_tamanho)+1).'">'.$i.'</a>
                            </li>';
                }
                
                
                // Verificar se existe uma página seguinte
                if ( ($paginacao_inicio + $paginacao_tamanho) <= $quantidade ){
                    $pagina_seguinte = $paginacao_inicio + $paginacao_tamanho ;
                    
                    echo    '<li>
                                <a href="#'.$pagina_seguinte.'" aria-label="Seguinte">
                                    <span aria-hidden="true">&raquo;</span>
                                </a>
                            </li>';
                }

            }
            ?>
    </ul>
</div>
