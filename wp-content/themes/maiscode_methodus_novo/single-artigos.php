<?php 
get_header(); 

setPostViews(get_the_ID());

$ID = get_field('id_do_curso');

$url_api  =  get_option('link_api'); 


?>


<section class="single-view">
    <div class="container">
        <div class="row">
            <?php if ( have_posts() ) : the_post();
                $image = wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() ));

             ?>
            <div class="col-sm-6 col-md-7">
                <div class="post-title">
                    <h1 class="inline-block"><?php the_title(); ?></h1>
                </div>
                <hr><img src="<?php echo $image; ?>" width="100%">
                <?php the_field('conteudo_artigo'); ?>
                <?php the_content(); ?>
            </div>
            <?php 

            $relacao = get_field('relacione_curso');            

            if( $relacao ): ?>
              <ul>
                <?php foreach( $relacao as $post): // variable must be called $post (IMPORTANT) ?>
                  <?php setup_postdata($post); 
                  //var_dump($post);
                  ?>
                  <style type="text/css">
                    .btn_top{
                      color: #fff !important;
                    }
                    .btn_top:hover{
                      background-color: #fff !important;
                      color: <?php the_field('cor_curso'); ?> !important;
                    }
                    .btn_pg_curso{
                      border: 2px solid <?php the_field('cor_curso'); ?> !important;;
                    }
                    .btn_pg_curso:hover{
                      background-color: #fff !important;
                      color: <?php the_field('cor_curso'); ?> !important;
                    }
                    .btn_chamada:hover{
                      color: <?php the_field('cor_curso'); ?> !important;
                    }
                  </style>

                  <div class="col-sm-6 col-md-4 col-md-offset-1">
                  

                    <?php
                    
                     // $ID =  $id_atual ;

                    // Formulário para ver investimento
                    // include_once 'formulario.php';

                    get_template_part('formulario');

                    ?>

                  </div>

                   
                <?php endforeach; ?>
              </ul>
              <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
            <?php endif; ?>

            

              
           
        <?php endif; ?>
    </div>
</section>

<?php get_footer(); ?>