<?php
/**
* Template Name: Simulados de alto desempenho
*
* @package WordPress
* @author Mais Code Tecnologia
* @since First Version
*/
?>
<?php get_header(); ?>
<div class="container">
  <div class="row">
  	  <div class="col-md-12">
        <h4>SIMULADOS DE ALT�SSIMO DESEMPENHO</h4> 
        <h1>COMO ATINGIR 90% DE ACERTOS EM CONCURSOS, VESTIBULARES OU ENEM.</h1>
      </div>
  </div>
</div>

<br/>
  
<div class="container">
  <div class="row">
    <div class="col-sm-12">
      <p>Comprovado e real: apresentamos a voc� uma metodologia muito poderosa e eficiente para estar entre os primeiros colocados.<p>
    </div>
    <div class="col-sm-12">
      <p>
        Concursos, Vestibulares e ENEM: <br/>
        <strong>O SEGREDO REAL PARA PASSAR EM PROVAS E CONCURSOS.</strong>
      </p>
    </div>
    <div class="col-sm-12">
      <h4>O QUE �:</h4>        
      <p>
      �Simulados de Alt�ssimo Desempenho� � um M�TODO de como atingir 
      uma performance realmente alta na prova para a qual voc� estiver
      se preparando.
      </p>
    </div>
    <div class="col-sm-12">
      <h4>COMO FUNCIONA?</h4>        
      <p>
     S�o 3 M�dulos de prepara��o. De acordo com o passo a passo de cada um dos m�dulos, voc� ir� encaixar a mat�ria que estiver estudando e os simulados tradicionais que j� faz, por�m ir� realiza-los de acordo com as estrat�gias e m�todos CIENTIFICAMENTE COMPROVADOS para aumento da agilidade mental, percep��o de dificuldades e concentra��o.
      </p>
    </div>
    <div class="col-sm-12">
      <h4>MAS FUNCIONA COMIGO?</h4>        
      <p>
      Sim! Ao se propor a realizar o passo a passo, voc� estar� bem � frente de seus concorrentes na disputa. E com certeza, em um patamar t�o superior, que ser� muito prov�vel inclusive atingir os primeiros lugares.
      </p>
    </div>
    <div class="col-sm-12">
      <h4>QUERO MAIS DETALHES:</h4>        
      <p>
     Irei descrever abaixo a linha de racioc�nio de cada um dos m�dulos para entendimento da metodologia.
      </p>
    </div>
  </div>
</div>

<div class="container">
  <div class="row">
    <div class="col-sm-6">
    	<img src="#"/>
    </div>
    <div class="col-sm-6">
    	<h3>O QUE N�O FAZER E O QUE FAZER PARA PASSAR</h3>
        <p>Voc� conhecer� os erros b�sicos cometidos por 70% a 90% dos candidatos na prepara��o para Concursos, Vestibulares e ENEM.</p>
        <p>Saber� as 5 atitudes primordiais dos 10 primeiros colocados nas listas de aprovados.</p>
    </div>
  </div>
  
  <div class="row">
    <div class="col-sm-6">
    	<h3>SABER COMO TREINAR COM OS SIMULADOS: O SEGREDO PARA PASSAR</h3>
        <p>Voc� aprender� OS 4 PRINC�PIOS CIENT�FICOS que dever�o ser aprendidos e seguidos para que voc� tenha sucesso.</p>

        <p>Neste m�dulo voc� aprender� o passo a passo detalhado explicativo de cada um dos 4 PRINC�PIOS: Per�odo, Quantidade, Dificuldade, Tempo.</p>

        <p>Seguindo este passo a passo, voc� entrar� na prova muito seguro e ter� disposi��o mental e f�sica para pensar com clareza ao longo de toda a prova.</p>

        <p>E ATEN��O � MUITO IMPORTANTE: voc� ter� energia e discernimento para resolver os 10% de quest�es dif�ceis nos �ltimos 50 minutos da prova. N�o � o m�ximo?</p>

        <p>Seguindo o treinamento de cada um deles, voc� ir� passar com seguran�a.</p>

    </div>
    <div class="col-sm-6">
    	<img src="#"/>
    </div>
  </div>
  
  <div class="row">
    <div class="col-sm-6">
    	<img src="#"/>
    </div>
    <div class="col-sm-6">
    	<h3>V�SPERA E DIA DAS PROVAS</h3>
        <p>Aqui, n�o s� dicas para ir para a prova mais tranquilo, mas tamb�m ��de o que fazer na Hora H.</p>
        <p><strong>Voc� chega na prova pronto para realizar o nocaute. � s� uma quest�o de tempo at� entregar a prova e derrubar o advers�rio.
Voc� chega ultra preparado e confiante. A vit�ria � certeira!</strong></p>
    </div>
  </div>
</div>

<br/>

<div class="container" style="background-color:red;">
  <div class="row">
    <div class="col-md-12 text-center">
        <strong>A CHAVE DO SUCESSO:
            NO SEGUNDO M�DULO, VOC� VAI APRENDER OS
            4 Princ�pios Cient�ficos de
            Aprendizagem de Alt�ssima Performance.
        </strong>
    </div>
  </div>
</div>

<br/>

<div class="container">
  <div class="row">
    <div class="col-md-12 text-center">
    	<h4>COMPARA��ES DE INVESTIMENTO</h4>
        <p>1 ANO DE CURSINHO PREPARAT�RIO: A partir de R$ 650 (online) at� R$ 3.500 (presencial)
        </p>
        <p><strong>E VOC� AINDA PERDE MAIS UM ANO ESTUDANDO!</strong></p>
        <p>Simulados de Alt�ssimo Desempenho
Tiro certeiro para voc� passar!</p>
		
        <h4>De: 549,00</h4>
        <h2>Somente: 12 x R$ 34,00</h2>
        
        <button>Quero comprar agora!</button>
        
    </div>
  </div>
</div>

<br/>
<div class="container">
  <div class="row">
    <div class="col-md-12">
        <h3>SOBRE A METHODUS</h3>
        <p>
        Antes de voc� adquirir os 3 e-books, queria te contar quem sou:  Alcides Schotten, Professor e Fil�sofo. Lecionei em col�gios e universidades e sempre percebi um grande
drama dos estudantes � n�o saber estudar. Por isso, n�o aprendem e apenas decoram os conte�dos das disciplinas para fazer as provas.
Educar deriva do latim �educare�, que significa �trazer para fora�. S�crates, o maior dos fil�sofos da hist�ria, sabiamente praticava a genu�na educa��o atrav�s da t�cnica da mai�utica. Conclu�, ent�o, que s� tem um jeito de aprender � sendo autodidata.</p>

 <p>E � por isso que ensino voc� todas as t�cnicas para que ao estudar voc� tenha conhecimento cient�fico de como seu c�rebro funciona e porque voc� deve estudar desta ou daquela maneira.</p>

 <p>E garanto que d�cadas de ensinamentos e estudos est�o aqui ao seu alcance de maneira f�cil e num passo a passo que vai fazer voc� entender o porque de cada t�cnica que est� fazendo e conseguir extrair o m�ximo dos seus estudos, se tornando uma verdadeira m�quina de acertar quest�es.</p>

 <p>A Methodus tem sede na Av. Paulista desde 1998 � s�o mais de 20 anos treinando milhares de profissionais e estudantes, desenvolvendo-os em diversas �reas, e levando a upgrades de carreira, desenvolvimento pessoal e profissional.</p>

<h4>Conhe�a nossos outros cursos:</h4>

  Presenciais:<br/>
  <ul>
    <li>Leitura din�mica | Mem�ria | Mind map | Concentra��o</li>
    <li>Administra��o do Tempo - Hiperprodutividade - Saber Empreender</li>
    <li>Orat�ria - Perder o Medo de Falar - A Arte de Comunicar Bem</li>
    <li>Mentorias para Estudantes/Vestibulandos/Concursandos</li>
  </ul>

  <br/>
  Online:<br/>
  <ul>
    <li>Simulados de Alt�ssimo Desempenho</li>
    <li>Mentorias para Estudantes/Vestibulandos/Concursandos</li>
    <li>Mentoria - Orat�ria para Profissionais e Acad�micos</li>
  </ul>

    </div>
  </div>
</div>

<?php get_footer(); ?>
