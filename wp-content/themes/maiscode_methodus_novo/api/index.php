<?php
include 'biblioteca/funcoes.php';
include 'biblioteca/classes.php';
$conexao = conexaoBD();

permissaoApi();

// Variáveis comuns a todos os métodos
$acao 		= formataVar( 'a', '*' ); // Método chamado
$formato 	= formataVar( 'formato', '*' );
$metodo		= formataVar( 'metodo', '*' );
$ID			= formataVar( 'ID', '*' );
$forcar 	= formataVar( 'forcar', '*' );

if ($acao){
	
	// verificar se a ação existe
	if ( checaArquivo( 'includes/'.$acao.'.php' ) ){
		include 'includes/'.$acao.'.php';
	}else{
		$retorno["erro"] = 1;
		$retorno["mensagem"] = "Ação inválida";	
	}

// NÃO ENVIOU O PARÂMETRO DE AÇÃO
}else{
	$retorno["erro"] = 1;
	$retorno["mensagem"] = "Faltam parâmetros";	
}


// Retorno em XML
if ( $formato == 'xml' ){
	
	header("Content-type: text/xml");
	echo '<?xml version="1.0" encoding="utf-8"?>';
	echo '<retorno>';
	
	foreach ($retorno as $campo=>$valor) {
		if ( detectaUTF8( $valor ) ){
			echo '<'.$campo.'>'.$valor.'</'.$campo.'>';
		}else{
			echo '<'.$campo.'>'.charSet( $valor, 'utf8' ).'</'.$campo.'>';
		}
	}
	
	echo '</retorno>';


// Retorno em JSON
}else{
	
	echo '{"status":"200"';
	
	foreach ($retorno as $campo=>$valor) {
		if ( detectaUTF8( $valor ) ){
			echo ',"'.$campo.'":"'.$valor.'"';
		}else{
			echo ',"'.$campo.'":"'.charSet( $valor, 'utf8' ).'"';
		}
	}
	
	echo '}';
	
}
?>