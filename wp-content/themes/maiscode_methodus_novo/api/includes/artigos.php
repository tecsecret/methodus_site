<?php
/* Ver dados do artigo */
if ($metodo == 'ver'){
	
	// Forçar exibição em XML
	if ( $formato == '' )
		$formato = 'xml';
	
	if ($ID){
		
		// Verificar cache
		$str_xml = verificarCache( 'artigos', 'artigo_'.$ID, 24 );
		
		if ($str_xml){
			
			$retorno["erro"] 		= 0;
			$retorno["mensagem"] 	= 'Artigo encontrado. Arquivo em cache.';
			$retorno["artigo"] 		= $str_xml;
			
		}else{
			
			if ( is_numeric($ID) == false ){
				$retorno["erro"] 		= 1;
				$retorno["mensagem"] 	= 'Artigo não encontrado.';
				goto fim;
			}
			
			$sql = 	"select * from ARTIGOS where ID_artigo=$ID";
			$rs = abrirRs( $sql );
			
			if ( sqlsrv_num_rows( $rs ) > 0 ){
			
				$artigo = sqlsrv_fetch_array( $rs );
				
				if ($artigo['foto']){
					$artigo['foto'] = '/methodus/_arquivos_home/'.$artigo['foto'];
				}
				
				$str_xml = 	'<codigo>'.$ID.'</codigo>
							<ID_tema>'.$artigo['ID_tema'].'</ID_tema>
							<status>'.$artigo['status_artigo'].'</status>
							<data>'.dataBD( $artigo["data_artigo"] ).'</data>
							<titulo><![CDATA['.$artigo['titulo_artigo'].']]></titulo>
							<chamada><![CDATA['.$artigo['chamada'].']]></chamada>
							<foto><![CDATA['.$artigo['foto'].']]></foto>
							<corpo><![CDATA['.$artigo['corpo_artigo'].']]></corpo>
							<palavras><![CDATA['.$artigo['palavras'].']]></palavras>';				
				
				
				$retorno["erro"] 		= 0;
				$retorno["mensagem"] 	= 'Artigo encontrado';
				$retorno["artigo"] 		= $str_xml;
				
				// gravar arquivo de cache
				gravarCache( 'artigos', 'artigo_'.$ID, $str_xml );
				
				
			}else{
				$retorno["erro"] 		= 1;
				$retorno["mensagem"] 	= "Artigo não encontrado";		
			}			
	
		}
	
	}else{
		$retorno["erro"] 		= 1;
		$retorno["mensagem"] 	= "Campos obrigatórios não preenchidos";		
	}
	
	// Se retornou sem erros acrescentar um ao contador de visualizações
	if ( $retorno["erro"] == 0 ){
		$sql = 	"update ARTIGOS set acessos=IsNull(acessos, 0)+1 where ID_artigo=$ID";
		executarQuery( $sql );	
	}
	
	fim:


/* Listar artigos */
}else if ($metodo == 'listar'){
	
	// Forçar exibição em XML
	if ( $formato == '' )
		$formato = 'xml';
	
	iniciaPaginacao();
	
	$ID_tema	= formataVar( 'ID_tema', 'get' );
	$pesquisa	= formataVar( 'pesquisa', 'get' );
	$ordem		= formataVar( 'ordem', 'get' );
	
	// definir ordenação
	if ($ordem == 'top'){
		$ordem = 'acessos desc';
	}else{
		$ordem = 'data_artigo desc';	
	}
	
	$str_xml = '';
	
	// Monta a query com as condições
	$sql_where = 	"from ARTIGOS with (NOLOCK) where status_artigo=1 and data_artigo <=getDate()";
	if ($ID_tema && is_numeric($ID_tema)){
		$sql_where .= ' and ID_tema='.$ID_tema;
	}
	if ($pesquisa){
		$sql_where .= " and ( titulo_artigo like '%".$pesquisa."%' or chamada like '%".$pesquisa."%' or palavras like '%".$pesquisa."%' or corpo_artigo like '%".$pesquisa."%' )";
	}
	
	// query para contar número de registros
	$sql_conta = "select count(1) as qtd ".$sql_where;
	$rsConta = abrirRs( $sql_conta );
	
	if ( sqlsrv_num_rows( $rsConta ) > 0 ){
		
		$quantidade = sqlsrv_fetch_array( $rsConta );
		$quantidade_registros = $quantidade['qtd'];
		
		$sql = 	"SELECT * FROM ( SELECT ID_artigo, titulo_artigo, data_artigo, ID_tema, chamada, foto, corpo_artigo, indice = ROW_NUMBER() OVER (ORDER BY ".$ordem.")
				".$sql_where." ) AS temp WHERE indice BETWEEN ".$paginacao_inicio." AND ".$paginacao_fim;
		$rs = abrirRs( $sql );
		
		while( $registro = sqlsrv_fetch_array( $rs ) ) {
			
			if ($registro['foto']){
				$registro['foto'] = '/methodus/_arquivos_home/'.$registro['foto'];
			}
			
			if ( is_null($registro['chamada']) || empty($registro['chamada']) || $registro['chamada'] == $registro['titulo_artigo'] ){
				$registro['chamada'] = removerHtml( $registro['corpo_artigo'], 350 );
			}
			
			$str_xml	.= 	'<artigo codigo="'.$registro['ID_artigo'].'" ID_tema="'.$registro['ID_tema'].'">
								<titulo><![CDATA['.$registro['titulo_artigo'].']]></titulo>
								<chamada><![CDATA['.$registro['chamada'].']]></chamada>
								<foto><![CDATA['.$registro['foto'].']]></foto>
								<data>'.dataBD( $registro["data_artigo"] ).'</data>
							</artigo>';
			
		}

		
		$retorno["erro"] 		= 0;
		$retorno["mensagem"] 	= 'Artigos listados';
		$retorno["quantidade"] 	= $quantidade_registros;
		$retorno["artigos"] 	= $str_xml;
		
	}else{
		$retorno["erro"] 		= 0;
		$retorno["mensagem"] 	= 'Nenhum artigo encontrado';	
		$retorno["quantidade"] 	= 0;
	}


	
}else{
	$retorno["erro"] 		= 1;
	$retorno["mensagem"] 	= "Faltam parâmetros";
}

?>