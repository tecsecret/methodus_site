<?php
/* Listar links */
if ($metodo == 'listar'){
	
	// Forçar exibição em XML
	if ( $formato == '' )
		$formato = 'xml';

		
	// Verificar cache
	$str_xml = verificarCache( 'posts', 'posts'.$ID, 24 );
	
	if ($str_xml){
		
		$retorno["erro"] 		= 0;
		$retorno["mensagem"] 	= 'Posts listados. Arquivo em cache.';
		$retorno["posts"] 		= $str_xml;
		
	}else{
		
		$str_xml = '';

		if ( $ID == 5 ){
			$parametros_api_blog = array( 'per_page'=>'3', 'search' => 'curso-de-oratoria' );
		}else if ( $ID == 6 ){
			$parametros_api_blog = array( 'per_page'=>'3', 'search' => 'administracao-do-tempo' );
		}else if ( $ID == 4 || $ID == 7 ){
			$parametros_api_blog = array( 'per_page'=>'3', 'search' => 'leitura-dinamica' );
		}else{
			$parametros_api_blog = array( 'per_page'=>'3' );
		}
		
		$json_posts = executaPagina( 'http://blog.methodus.com.br/wp-json/wp/v2/posts', $parametros_api_blog );
		if ($json_posts){

			$posts = json_decode($json_posts);

			foreach ($posts as $post){

				$imagem_post = $post->featured_media;
				$arquivo_imagem_post = '';
	
				if ( is_numeric($imagem_post) ){
	
					// descobrir imagem
					$json_posts = executaPagina( 'http://blog.methodus.com.br/wp-json/wp/v2/media/'.$imagem_post );
					if ($json_posts){

						$arquivo_imagem_post = json_decode($json_posts);
						$variavel_temp = 'epico-tiny';
						if ( isset($arquivo_imagem_post->media_details->sizes->$variavel_temp->source_url) ){
							$arquivo_imagem_post = $arquivo_imagem_post->media_details->sizes->$variavel_temp->source_url;
						}else if ( isset($arquivo_imagem_post->media_details->sizes->medium->source_url) ){
							$arquivo_imagem_post = $arquivo_imagem_post->media_details->sizes->medium->source_url;
						}

					}
	
				}
				
				$str_xml .= 	'<post>
									<titulo><![CDATA['.$post->title->rendered.']]></titulo>
									<link><![CDATA['.$post->link.']]></link>
									<imagem><![CDATA['.$arquivo_imagem_post.']]></imagem>
								</post>';
						
			}
			
			
			$retorno["erro"] 		= 0;
			$retorno["mensagem"] 	= 'Posts listados';
			$retorno["posts"] 		= $str_xml;
			
			// gravar arquivo de cache
			gravarCache( 'posts', 'posts'.$ID, $str_xml );
			
			
		}else{
			$retorno["erro"] 		= 1;
			$retorno["mensagem"] 	= "Erro ao conectar-se à API do Wordpress";	
		}			

	}

	
}else{
	$retorno["erro"] 		= 1;
	$retorno["mensagem"] 	= "Faltam parâmetros";
}

?>