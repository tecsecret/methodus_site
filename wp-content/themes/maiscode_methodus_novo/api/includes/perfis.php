<?php
/* Listar perfis */
if ($metodo == 'listar'){
	
	// Forçar exibição em XML
	if ( $formato == '' )
		$formato = 'xml';
		
	// Verificar cache
	$str_xml = verificarCache( 'variados', 'perfis', 12 );
	
	if ($str_xml){
		
		$retorno["erro"] 		= 0;
		$retorno["mensagem"] 	= 'perfis listados. Arquivo em cache.';
		$retorno["perfis"] 		= $str_xml;
		
	}else{
		
		
		$str_xml = '';
				
		$sql = 	"select ID_perfil, ID_curso, titulo_perfil from PERFIL where status_perfil=1 order by ID_curso, titulo_perfil";
		$rsPerfis = abrirRs( $sql );
		
		if ( sqlsrv_num_rows( $rsPerfis ) > 0 ){
		
			while( $perfil = sqlsrv_fetch_array( $rsPerfis ) ) {
				$str_xml	.= 	'<perfil codigo="'.$perfil['ID_perfil'].'" ID_curso="'.$perfil['ID_curso'].'"><![CDATA['.$perfil['titulo_perfil'].']]></perfil>';
			}
			
			$retorno["erro"] 		= 0;
			$retorno["mensagem"] 	= 'Cursos listados';
			$retorno["perfis"] 		= $str_xml;
			
			// gravar arquivo de cache
			gravarCache( 'variados', 'perfis', $str_xml );
			
		}else{
			$retorno["erro"] 		= 1;
			$retorno["mensagem"] 	= 'Nenhum perfil encontrado';
		}
	
	}


	
}else{
	$retorno["erro"] 		= 1;
	$retorno["mensagem"] 	= "Faltam parâmetros";
}

?>