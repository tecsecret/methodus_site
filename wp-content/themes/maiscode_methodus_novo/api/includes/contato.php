<?php
$nome		= formataVar( 'nome', 'post' );
$sobrenome	= formataVar( 'sobrenome', 'post' );
$email		= formataVar( 'email', 'post' );
$sexo		= formataVar( 'sexo', 'post' );
$telefone	= formataVar( 'telefone', 'post' );
$celular	= formataVar( 'celular', 'post' );
$area		= formataVar( 'area', 'post' );
$conheceu 	= formataVar( 'conheceu', 'post' );
$mensagem	= formataVar( 'mensagem', 'post', 'textarea' );
$contato	= formataVar( 'contato', 'post' );
$newsletter	= formataVar( 'newsletter', 'post' );
$captcha 	= formataVar( 'g-recaptcha-response', 'post' );

// Verificar campos obrigatórios
if ( empty($nome) || empty($email) || empty($mensagem) ){
	
	$retorno["erro"] = 1;
	$retorno["mensagem"] = 'Campos obrigatórios não preenchidos';
	
}else{
	
	$recaptcha = executaPagina( 'https://www.google.com/recaptcha/api/siteverify', array( 'secret'=>'6LfSbAsTAAAAAHovqQsnCcLRW9JwKPbT4euLgB29', 'response'=>$captcha, 'remoteip'=>ipUsuario() ), 'post' );
	$recaptcha = json_decode($recaptcha);
	
	if ( $recaptcha->success == 0 ){
		
		$retorno["erro"] = 1;
		$retorno["mensagem"] = 'Ops! Acho que você não é humano... Certifique-se que você não é um robô.';
		
	}else{
	
		$mensagem	=	'Um visitante solicitou contato através do web site.<BR /><BR /><table border="0" cellpadding="3" cellspacing="0">
						<TR><TD><b>Nome: </b></TD><TD>'.$nome.'</TD></TR>
						<TR><TD><b>E-mail: </b></TD><TD><a href="mailto:'.$email.'">'.$email.'</a></TD></TR>
						<TR><TD valign="top"><b>Mensagem: </b></TD><TD>'.$mensagem.'</TD></TR>
						</table>';
		
		// Enviar e-mail
		enviaEmail( 'Contato do site', $mensagem, $email_principal, 'Methodus', $email, $nome, 1 );
		
		
		// Cadastrar aluno no Facix	
		$sql = "select top 1 ID_usuario, entrou_usuario from USUARIOS where email_usuario='".$email."' order by ID_usuario desc";
		$rs = abrirRs( $sql );
				
		if ( sqlsrv_num_rows( $rs ) > 0 ){	
			$aluno 				= sqlsrv_fetch_array( $rs );
			$campos["entrou"] 	= $aluno['entrou_usuario'].' Contato no site;';
			$ID					= $aluno['ID_usuario'];
		}else{
			$campos["nome"] 	= $nome;							
			$campos["email"] 	= $email;	
			$campos["entrou"] 	= 'Contato no site;';
			$ID					= null;
		}
								
		$usuario = new Usuario( $campos );
		$cadastro = $usuario->Cadastro( $ID );
		
		
		$retorno["erro"] = 0;
		$retorno["mensagem"] = 'Contato enviado com sucesso';
	
	}
	
}

?>