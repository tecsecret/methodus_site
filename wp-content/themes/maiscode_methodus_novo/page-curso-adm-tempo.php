<?php
/**
* Template Name: Curso Administração do Tempo
* @package WordPress
* @author Mais Code Tecnologia
* @since First Version
*/

$url_site = get_option('link_site'); 
$url_api  =  get_option('link_api'); 
$url_amigavel_wp = home_url(); 
// URLs amigáveis
$url_amigavel_on  = 1;
$url_amigavel   =  get_option('link_site_amigavel');

$pagina_curso = 1;

$urlWP = get_template_directory_uri();
// $urlWP = site_url();
// $urlWP = 'https://www.methodus.com.br/';
  
// Verificar
$ID = get_field('id_do_curso');
$ID_Depoimento_Curso = get_field('id_depoimento_curso');
$ebookWp = get_field('ebook');
$xml_curso  = executaPagina( 'api/', array( 'a'=>'cursos', 'metodo'=>'ver', 'ID'=>$ID ) );
$curso      = lerXML( $xml_curso );
    

// var_dump($curso);
    
if ( $curso->erro == 0 ){
    
    $curso              = $curso->curso;
    $status             = $curso->status;
    $titulo_curso       = $curso->titulo;
    $url_curso          = $curso->url;
    $distancia_curso    = $curso->distancia;
    $banner_curso       = $curso->banner;
    $resumo_curso       = $curso->resumo;
    $descricao_curso    = $curso->descricao;
    $variaveis_curso    = $curso->variaveis; 
    $planos_curso       = $curso->planos;
    $turmas_curso       = $curso->turmas->turma;
    $perfis_curso       = $curso->perfis->perfil;
    $ID_tema            = $curso->tema;
    
    $seo_metatags       = $curso->metatags;
    $seo_title          = $titulo_curso.' - Methodus';
    $seo_imagem         = 'https://'.$_SERVER['HTTP_HOST'].''.$banner_curso;
    
    if ( isset( $variaveis_curso->classe ) ){
        $classe_curso = $variaveis_curso->classe;
    }else{
        $classe_curso = 'laranja'; 
    }
    
    if ( isset( $curso->fotos ) ){
        $fotos_curso = $curso->fotos->foto;
    }

    if ( $status != 1 ){
        exit("Erro ao acessar os dados do curso 1");
    }
    
}else{
    exit("Erro ao acessar os dados do curso");      
}

?>


<?php get_header(); ?>

<style type="text/css">
    .caixa_ebook img{
    width: 170px;
  }
        .btn_top{
            color: #fff !important;
        }
        .btn_top:hover{
            background-color: #fff !important;
            color: #e38902 !important;
        }
        .btn_pg_curso{
            border: 2px solid #e38902 !important;;
        }
        .btn_pg_curso:hover{
            background-color: #fff !important;
            color: #e38902 !important;
        }
        .btn_chamada:hover{
            color: #e38902 !important;
        }
    </style><!-- TOPO -->

<style type="text/css">
  .courseColor {
    color: #e38902;
  }
  .courseBackgroundColor {
    background-color: #e38902;
  }
  #secStoryModB .storyContent svg path {
    fill: #e38902;
  }
  .sliderCard svg path {
    fill: #e38902;
  }
  #secTopModB:before {
    content: '';
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    background: linear-gradient(270deg, rgba(227,137,2,0.7) 23.16%, rgba(52, 50, 50, 0) 118.16%);
  }
  #secCallBannerModB .bgTransformation:before {
    content: '';
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    background: linear-gradient(179.64deg, rgba(96, 94, 94, 0) -36.93%, rgba(160, 0, 0, 0.6) 85.54%);
  }
  .loadd{
    display: none;
    opacity: .5;
    background: #000;
    width: 100%;
    position: absolute;
    top: 0px;
    height: 100vh;
    z-index: 10000000;
  }
 
  .sliderCardDepo .depo-prev
  {
    height: 36px;
    width: 220px;
    z-index: 100000;
    object-fit: contain;
  }
  .sliderCardDepo .depo-next{
    width: 235px;
    height: 36px;
    object-fit: contain;
  }
</style>



<section id="secTopModB" style="background-image: url('https://methodus.com.br/wp-content/uploads/2019/11/curso-de-administracao-do-tempo-scaled.jpg">
  <div class="container">
    <div class="row">
      <div class="col-md-7 col-md-offset-1 col-sm-7 col-sm-offset-1 topContent">
        <h2>PROCRASTINAR ATRAPALHA TODOS OS SEUS PLANOS IMPORTANTES?</h2>
        <h4></h4>
        <!--<img src="https://methodus.com.br/wp-content/uploads/2020/08/curso-de-oratoria-lp-1.png">-->
      </div>
    </div>
  </div>
</section>

        <?php
    if ( isset( $variaveis_curso->ebook_download ) ){
        $temp_ebook = explode( '|', $variaveis_curso->ebook_download );

        if ( count( $temp_ebook ) >= 4 ){

            $titulo_ebook           = $temp_ebook[0];
            $descricao_ebook        = $temp_ebook[1];
            $destino_ebook          = $temp_ebook[2];
            $identificador_ebook    = $temp_ebook[3];
            $urlWP = get_template_directory_uri();
            ?>


            <section class="caixa_ebook ">
                <div class="container-fluid bg_ebook">
                    <div class="container">                       
                 
                        <script src="<?php echo $urlWP; ?>/js/scripts_site/ebook.js?ver=1"></script>
                        <form action="<?php echo $url_api; ?>/api/" method="post" class="form-email formAjax" id="form_ebook" data-validacao="checaEbook" data-resultado="sucessoEbook">
                            <input type="hidden" name="destino" value="<?php echo $ebookWp ?>" />
                            <input type="hidden" name="a" value="aluno" />
                            <input type="hidden" name="metodo" value="cadastro" />
                            <input type="hidden" name="origem" value="Ebook_<?php echo $identificador_ebook; ?>" />
                            <input type="hidden" name="curso" value="<?php echo $ID; ?>" />
                    
                            <div class="row">
                                <div class="col-xs-12 col-md-8">
                                    <img src="<?php echo $urlWP ?>/img/ebook_<?php echo $classe_curso; ?>.png" width="80" />
                                    <div class="chamada-ebook"><?php echo $descricao_ebook ?></div>
                                </div>
                                <div class="col-xs-12 col-md-4">
                                    <h4 class="mb0 inline-block p0-xs">
                                        <?php echo $titulo_ebook ?>
                                    </h4>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <input type="text" class="validate-required cada-input" name="nome" placeholder="Nome" lang="Nome">
                                        </div>
                                        <div class="col-sm-12 form-ebook">
                                            <input type="text" class="validate-required cada-input" name="email" placeholder="E-mail" lang="E-mail">
                                            <?php
                                            $i = 0;
                                            if( have_rows('formulario') ):
                                                while ( have_rows('formulario') ) : the_row(); $i++?> 
                                                    <div class="radio_cursos">  
                                                        <label for="ebook-<?php echo $i; ?>" class="radio">
                                                            <input type="radio" class="validate-required" id="ebook-<?php echo $i; ?>" name="conheceu" value="<?php the_sub_field('opcoes'); ?>" lang="opções">
                                                            <span></span>
                                                        </label>          
                                                        <label for="ebook-<?php echo $i; ?>"><?php the_sub_field('opcoes'); ?></label><br>
                                                    </div>
                                                <?php endwhile;
                                            endif;
                                            ?>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <button class="btn <?php echo $classe_curso; ?>" type="submit">Download</button>
                                        </div>
                                    </div>
                                </div>
                            </div>                            
                        </form>
                    </div>
                </div>
            </section>
        <?php
        }
    }
    ?>
<!-- BANNER CHAMADA -->
<section id="secCallBannerModB" class="courseBackgroundColor" style="">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2>JÁ IMAGINOU CONSEGUIR COM SEGURANÇA: TIRAR DA CABEÇA PRO PAPEL E DO PAPEL PARA A REALIZAÇÃO?</h2>
      </div>
    </div>
  </div>
    
</section>

<!-- HISTORIA -->
<section id="secStoryModB">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="storyContent">
		<h2 style="padding-bottom:40px"> É isso que você vai conseguir com o Curso de Administração do Tempo e Automotivação da Methodus. </h2>
		<h4> Porque muito mais do que nos preocuparmos com o dia a dia ou a sua agenda, nós nos preocupamos com os seus sonhos.</h4>
		<h4> O Objetivo maior é esse: quando você olha, seus planos começam a ganhar corpo. Cada dia um pouco. E a realização enche o peito de alegria! </h4>
		<h4> É uma sensação incrível. E tudo começa a tomar forma e a acontecer de verdade.</h4>
              </div>
      </div>
  </div>
  <section id="secTestimony">
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1 col-sm-12">
        <h2 class="courseColor">VEJA MAIS ALGUNS DEPOIMENTOS DE QUEM CHEGOU LÁ!</h2>
                  
            <div class="row sliderCard">
                <div class="col-md-12">
                    <div class="boxTestimony">
                    <img class="avatarImg" src="https://methodus.com.br/wp-content/uploads/2019/11/curso-de-administracao-do-tempo-16.jpg">
                    <h3 class="courseColor">Paulo Conceição Lopes</h3>
                    <span>Etwas | Bacharel em Gestão de TI</span>
                    <div class="testimonyText">
                      <svg class="quoteSlickStart" width="39" height="33" viewBox="0 0 39 33" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M9.0535 0.201172L14.3819 4.58474C11.8579 7.31229 10.1753 9.89371 9.33395 12.329C8.49262 14.6669 8.07196 17.0535 8.07196 19.4888L4.70664 17.0048H14.9428V32.2012H0.5V21.5345C0.5 17.5406 1.10763 13.8389 2.32288 10.4295C3.63161 6.92264 5.87515 3.5132 9.0535 0.201172ZM32.6107 0.201172L37.9391 4.58474C35.4151 7.31229 33.7325 9.89371 32.8911 12.329C32.0498 14.6669 31.6292 17.0535 31.6292 19.4888L28.2638 17.0048H38.5V32.2012H24.0572V21.5345C24.0572 17.5406 24.6648 13.8389 25.8801 10.4295C27.1888 6.92264 29.4324 3.5132 32.6107 0.201172Z"/>
                      </svg>
                      <h4><strong>O Curso é simplesmente surpreendente.</strong><br />
						Minhas expectativas eram de somente saber lidar com minha agenda de compromissos e terminei com uma bagagem gigantesca; foi surpreendente. Recomendo o Curso de Administração da Methodus porque quero que todos se desenvolvam e achem a sua própria realização assim como eu encontrei a minha</h4>
                      <svg class="quoteSlickEnd" width="39" height="31" viewBox="0 0 39 31" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M29.9465 0.201172L24.6181 4.31076C27.1421 6.86784 28.8247 9.28793 29.6661 11.571C30.5074 13.7628 30.928 16.0003 30.928 18.2834L34.2934 15.9546H24.0572V30.2012H38.5V20.2012C38.5 16.4569 37.8924 12.9866 36.6771 9.79022C35.3684 6.50255 33.1248 3.3062 29.9465 0.201172ZM6.3893 0.201172L1.06088 4.31076C3.58487 6.86784 5.26752 9.28793 6.10885 11.571C6.95018 13.7628 7.37085 16.0003 7.37085 18.2834L10.7362 15.9546H0.5V30.2012H14.9428V20.2012C14.9428 16.4569 14.3352 12.9866 13.1199 9.79022C11.8112 6.50255 9.56765 3.3062 6.3893 0.201172Z"/>
                      </svg>
                    </div>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="boxTestimony">
                    <img class="avatarImg" src="https://methodus.com.br/wp-content/uploads/2019/11/curso-de-administracao-do-tempo-14.jpg">
                    <h3 class="courseColor">Cinthia Paula Aidar</h3>
                    <span>Lubizol Adit do Brasil | Bacharel em Química</span>
                    <div class="testimonyText">
                      <svg class="quoteSlickStart" width="39" height="33" viewBox="0 0 39 33" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M9.0535 0.201172L14.3819 4.58474C11.8579 7.31229 10.1753 9.89371 9.33395 12.329C8.49262 14.6669 8.07196 17.0535 8.07196 19.4888L4.70664 17.0048H14.9428V32.2012H0.5V21.5345C0.5 17.5406 1.10763 13.8389 2.32288 10.4295C3.63161 6.92264 5.87515 3.5132 9.0535 0.201172ZM32.6107 0.201172L37.9391 4.58474C35.4151 7.31229 33.7325 9.89371 32.8911 12.329C32.0498 14.6669 31.6292 17.0535 31.6292 19.4888L28.2638 17.0048H38.5V32.2012H24.0572V21.5345C24.0572 17.5406 24.6648 13.8389 25.8801 10.4295C27.1888 6.92264 29.4324 3.5132 32.6107 0.201172Z"/>
                      </svg>
                      <h4><strong>É sobre como definir nossa vida!</strong><br />
						Um curso muito bacana para refletir sobre o que queremos para nossa vida no futuro, curso que auxilia na percepção do autoconhecimento pessoal e profissional. Ajuda a entender as possibilidades de que queremos para nossa vida pessoal e profissional. Muito mais profundo que um simples curso de como pensar no que vamos fazer em nosso dia, semana ou ano, ajuda a pensar e refletir sobre como se planeja a vida toda.</h4>
                      <svg class="quoteSlickEnd" width="39" height="31" viewBox="0 0 39 31" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M29.9465 0.201172L24.6181 4.31076C27.1421 6.86784 28.8247 9.28793 29.6661 11.571C30.5074 13.7628 30.928 16.0003 30.928 18.2834L34.2934 15.9546H24.0572V30.2012H38.5V20.2012C38.5 16.4569 37.8924 12.9866 36.6771 9.79022C35.3684 6.50255 33.1248 3.3062 29.9465 0.201172ZM6.3893 0.201172L1.06088 4.31076C3.58487 6.86784 5.26752 9.28793 6.10885 11.571C6.95018 13.7628 7.37085 16.0003 7.37085 18.2834L10.7362 15.9546H0.5V30.2012H14.9428V20.2012C14.9428 16.4569 14.3352 12.9866 13.1199 9.79022C11.8112 6.50255 9.56765 3.3062 6.3893 0.201172Z"/>
                      </svg>
                    </div>
                  </div>
                </div>
                          </div>
          
              </div>
    </div>
  </div>
</section>
    <section id="secStoryModB">
  <div class="container">
    <div class="row">
      <div class="col-12">
  
	  <h4></h4>
        <h2 class="courseColor" style="padding-bottom:40px">&#x25cf; Olá, sou o Professor Alcides Schotten, fundador da Methodus e vou te contar um fato que você pode até não acreditar. &#x25cf;</h2>
        <div class="storyContent">
          <svg class="quoteStart" width="71" height="56" viewBox="0 0 71 56" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M16.2565 0L26.072 7.67124C21.4225 12.4444 18.3229 16.962 16.7731 21.2237C15.2232 25.3151 14.4483 29.4916 14.4483 33.7534L8.24908 29.4064H27.1052V56H0.5V37.3333C0.5 30.344 1.61931 23.8661 3.85793 17.8996C6.26876 11.7626 10.4016 5.79605 16.2565 0ZM59.6513 0L69.4668 7.67124C64.8173 12.4444 61.7177 16.962 60.1679 21.2237C58.6181 25.3151 57.8432 29.4916 57.8432 33.7534L51.6439 29.4064H70.5V56H43.8948V37.3333C43.8948 30.344 45.0141 23.8661 47.2528 17.8996C49.6636 11.7626 53.7964 5.79605 59.6513 0Z"/>
          </svg>
                                    <h4>Eu comecei a minha carreira lecionando em escolas e colégios particulares (alguns entre os melhores do país). E tudo ia bem. Eu era jovem e já estava bem conceituado como professor. Porém eu via que muito do que eu ensinava não era aprendido pelos alunos, simplesmente porque o aluno que estava ali na escola, não necessariamente queria estar ali. Não era uma decisão dele aprender.</h4>

									<h4>Sim, a escola é fundamental para qualquer criança e adolescente. Porém, como Projeto de Satisfação Pessoal eu queria ensinar quem quisesse realmente aprender.</h4>
									<h4>E foi aí que coloquei na minha mente que eu iria um dia ter meu curso e poder ensinar a quem fosse lá pois seriam pessoas que estava sim, por vontade própria querendo aprender algo.</h4>
									<h4>E aí foi que comecei a perceber que eu precisava de um plano.</h4>
									<h4>A ideia ficava na minha cabeça, mas todo dia eu levantava e fazia as mesmas coisas que eu fazia no dia anterior.</h4>

                              <div class="boxContrast">
                                      <span class="courseColor"><strong> O ser humano, por natureza, acaba se repetindo todos os dias. E isso é que atrapalha a construção de novos caminhos e novos sonhos.</strong></span>
                                   
								</div>
                                        
                                        <h4><em></em>E neste momento, antes mesmo de fundar o meu Instituto Methodus, de ter os meus cursos, foi que eu precisei me libertar da procrastinação, da repetição de dias, e comecei a ADMINISTRAR O MEU TEMPO com sabedoria. E isso tudo com métodos que fui estudar (e que hoje trago para vocês neste curso de Administração do Tempo) e com toda a minha experiência.</h4>
										<h4>	É uma satisfação tão grande, que é difícil colocar em palavras.</h4>

                              <div class="boxContrast">
                                    <span class="courseColor">Quando você vence as ondas que te impedem de ir nadar mar afora, parece que tudo flui. Porque cada braçada é no mar aberto. Sem maré nem ondas puxando contra, te levando para a areia de volta.</span>
                </div>
				<h4><em><strong> </strong></em>Simplesmente você encaixa no seu novo modo de viver o dia a dia, o seu sonho.</h4>
				<h4>E esse sonho deixa de virar uma procrastinação, deixar de ser um peso para executar na hora que você já está cansado. Ele passa a ser algo natural e normal do dia a dia, sem te cansar nem fisicamente nem mentalmente.</h4>
				<h4>E na hora que você vê, já deslanchou.</h4>

<div class="boxContrast">
                                    <span class="courseColor">E não importa qual o seu sonho: seja passar no vestibular mais complicado, seja montar sua empresa, seja estudar para um concurso, ou fazer uma pós graduação para conseguir uma promoção no trabalho, ou mesmo seja ele ter mais tempo livre para aproveitar sua família, amigos e vida pessoal.</span>
                </div>
				<h4 style="padding-bottom:40px"><em><strong> Depois que você entende o método como eu entendi, a vida é muito mais leve e realizada.</strong></em></h4>

                                              <svg class="quoteEnd" width="73" height="58" viewBox="0 0 73 58" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M56.2934 0L46.1974 7.94521C50.9797 12.8889 54.1679 17.5677 55.762 21.9817C57.3561 26.2192 58.1531 30.5449 58.1531 34.9589L64.5295 30.4566H45.1347V58H72.5V38.6667C72.5 31.4277 71.3487 24.7184 69.0461 18.5388C66.5664 12.1827 62.3155 6.00305 56.2934 0ZM11.6587 0L1.56272 7.94521C6.34501 12.8889 9.5332 17.5677 11.1273 21.9817C12.7214 26.2192 13.5184 30.5449 13.5184 34.9589L19.8948 30.4566H0.5V58H27.8653V38.6667C27.8653 31.4277 26.714 24.7184 24.4114 18.5388C21.9317 12.1827 17.6808 6.00305 11.6587 0Z"/>
          </svg>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- POR QUE A METHODUS -->
<section id="secWhyMethodus">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2 class="courseColor"> No Curso Administração do Tempo e Automotivação você vai aprender: </h2>
        <div class="methodusContent">
                                 <h3 class="courseColor">&#x25cf; Administração do Tempo &#x25cf;</h3>
                              &nbsp;

<img class="aligncenter wp-image-6362" src="https://methodus.com.br/wp-content/uploads/2019/11/curso-de-administracao-do-tempo-5-1024x683.jpg" alt="" width="800" height="534" />

<h4> &nbsp;&nbsp;
<ul>
<li></li><li></li>
 	<li>● Acabar com a Procrastinação! Métodos comprovados por diversos alunos que já fizeram o curso!</li>
</ul>
&nbsp;
<ul>
 	<li>● Identificar o que rouba seu tempo.</li>
</ul>
&nbsp;
<ul>
 	<li>● Classificar em um quadrante o que você faz no dia a dia e com uma clareza incrível você começa no dia seguinte a mudar isso.</li>
</ul>
&nbsp;
<ul>
 	<li>● Ajustar seu tempo entre o trabalho atual e o plano de empreender.</li>
</ul>
&nbsp;
<ul>
 	<li>● Conhecer qual seu tipo de personalidade em relação à administração do tempo.</li>
</ul>
&nbsp;
<ul>
 	<li>● Saber como se organizar de uma maneira compatível com VOCÊ.</li>
</ul>
&nbsp;
<ul>
 	<li>● Você vai tirar seus sonhos do papel e transformar em realidade!</li>
</ul>
&nbsp;
<ul>
 	<li>● Vai conseguir passar mais tempo com família, amigos e cuidar da vida pessoal.</li>
</ul>
&nbsp;</h4>
            <h3 class="courseColor">&#x25cf; Automotivação &#x25cf;</h3>
                              <h4>&nbsp;

<img class="aligncenter wp-image-6366" src="https://methodus.com.br/wp-content/uploads/2019/11/curso-de-administracao-do-tempo-7-1024x683.jpg" alt="" width="800" height="534" />
<ul>&nbsp;&nbsp;
<li></li><li></li>
 	<li>● Identificar hábitos desmotivadores.</li>
</ul>
&nbsp;
<ul>
 	<li>● Entender o sistema de crenças do ser humano e como dominá-lo a seu favor.</li>
</ul>
&nbsp;
<ul>
 	<li>● Conseguir vencer todas as fases das Conquistas de Objetivos.</li>
</ul>
&nbsp;
<ul>
 	<li>● O Poder de Alavancagem: como estar sempre na sua melhor versão.</li>
</ul>
&nbsp;
<ul>
 	<li>● Técnicas de Relaxamento para visualização de objetivos e aumento da energia.</li>
</ul></h4>
                                            </div>
        <div class="differenceTitle" style="padding-bottom:40px">
          <h2 class="courseColor">AS MUDANÇAS NA SUA VIDA COMEÇAM NO 1º DIA!</h2>
        </div>
        <div class="differenceContent">
                                    <h3><strong>●●● Exercícios Práticos com a ajuda do Prof. ●●●</strong></h3>
&nbsp;

<h4>- Você irá fazer a identificação de como gasta seu tempo.</h4>

<h4>- Vai colocar seus objetivos listados em uma tabela.</h4>

<h4>- Vai aprender o que precisa fazer na prática para ganhar tempo no final do dia.</h4>

<h4>- Você vai saber como estar conectado com você mesmo para realizar as tarefas.</h4>

<h4>- Vai aprender sobre energia do corpo: como realizar relaxamentos que “zeram o corpo e a mente” e fazem você em 15 minutos estar pronto e cheio de energia para realizar as suas tarefas (sejam pessoais ou de trabalho).</h4>

<h4>- Vai começar a executar isso na sua vida imediatamente e ver na aula seguinte como foi o resultado.</h4>
<h4></h4>
<h4></h4>
<h4 style="text-align: center;"><strong>Tudo isso explicado passo a passo!</strong></h4>
<h4></h4>
<h4></h4>
</section>

   <!-- DEPOIMENTOS-->
  <section id="secTestimony">
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1 col-sm-12">
        <h2 class="courseColor">VEJA MAIS ALGUNS DEPOIMENTOS DE QUEM CHEGOU LÁ!</h2>
                  
            <div class="row sliderCard">
                <div class="col-md-12">
                    <div class="boxTestimony">
                    <img class="avatarImg" src="https://methodus.com.br/wp-content/uploads/2019/11/curso-de-administracao-do-tempo-15.jpg">
                    <h3 class="courseColor">Jessica Marins Porto</h3>
                    <span>Pós-graduado em Controladoria de Gestão | Covestro Indústria e Comércio de Polímeros Ltda</span>
                    <div class="testimonyText">
                      <svg class="quoteSlickStart" width="39" height="33" viewBox="0 0 39 33" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M9.0535 0.201172L14.3819 4.58474C11.8579 7.31229 10.1753 9.89371 9.33395 12.329C8.49262 14.6669 8.07196 17.0535 8.07196 19.4888L4.70664 17.0048H14.9428V32.2012H0.5V21.5345C0.5 17.5406 1.10763 13.8389 2.32288 10.4295C3.63161 6.92264 5.87515 3.5132 9.0535 0.201172ZM32.6107 0.201172L37.9391 4.58474C35.4151 7.31229 33.7325 9.89371 32.8911 12.329C32.0498 14.6669 31.6292 17.0535 31.6292 19.4888L28.2638 17.0048H38.5V32.2012H24.0572V21.5345C24.0572 17.5406 24.6648 13.8389 25.8801 10.4295C27.1888 6.92264 29.4324 3.5132 32.6107 0.201172Z"/>
                      </svg>
                      <h4><strong>Alavancagem Profissional e Pessoal.</strong><br />
						Minhas expectativas eram de somente saber lidar com minha agenda de compromissos e terminei com uma bagagem gigantesca; foi surpreendente. Recomendo o Curso de Administração da Methodus porque quero que todos se desenvolvam e achem a sua própria realização assim como eu encontrei a minhaEstou feliz e muito satisfeita com o curso e as reflexões feitas durante as sessões e sugestões de temas a refletir após o curso, onde pude aprender muito. Tenho certeza de que a alavancagem profissional e pessoal que desejo serão plenamente realizadas com a ajuda das ferramentas de autogerenciamento ensinada, com o apoio da literatura sugerida e do professor. Há muito o que melhorar, muito o que fazer e com a ajuda da Methodus estou confiante que vou conseguir</h4>
                      <svg class="quoteSlickEnd" width="39" height="31" viewBox="0 0 39 31" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M29.9465 0.201172L24.6181 4.31076C27.1421 6.86784 28.8247 9.28793 29.6661 11.571C30.5074 13.7628 30.928 16.0003 30.928 18.2834L34.2934 15.9546H24.0572V30.2012H38.5V20.2012C38.5 16.4569 37.8924 12.9866 36.6771 9.79022C35.3684 6.50255 33.1248 3.3062 29.9465 0.201172ZM6.3893 0.201172L1.06088 4.31076C3.58487 6.86784 5.26752 9.28793 6.10885 11.571C6.95018 13.7628 7.37085 16.0003 7.37085 18.2834L10.7362 15.9546H0.5V30.2012H14.9428V20.2012C14.9428 16.4569 14.3352 12.9866 13.1199 9.79022C11.8112 6.50255 9.56765 3.3062 6.3893 0.201172Z"/>
                      </svg>
                    </div>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="boxTestimony">
                    <img class="avatarImg" src="https://methodus.com.br/wp-content/uploads/2019/11/curso-de-administracao-do-tempo-17.jpg">
                    <h3 class="courseColor">Robinson Washington Flauzino</h3>
                    <span>Ultrapar | Bacharel em Engenharia de Produção</span>
                    <div class="testimonyText">
                      <svg class="quoteSlickStart" width="39" height="33" viewBox="0 0 39 33" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M9.0535 0.201172L14.3819 4.58474C11.8579 7.31229 10.1753 9.89371 9.33395 12.329C8.49262 14.6669 8.07196 17.0535 8.07196 19.4888L4.70664 17.0048H14.9428V32.2012H0.5V21.5345C0.5 17.5406 1.10763 13.8389 2.32288 10.4295C3.63161 6.92264 5.87515 3.5132 9.0535 0.201172ZM32.6107 0.201172L37.9391 4.58474C35.4151 7.31229 33.7325 9.89371 32.8911 12.329C32.0498 14.6669 31.6292 17.0535 31.6292 19.4888L28.2638 17.0048H38.5V32.2012H24.0572V21.5345C24.0572 17.5406 24.6648 13.8389 25.8801 10.4295C27.1888 6.92264 29.4324 3.5132 32.6107 0.201172Z"/>
                      </svg>
                      <h4><strong>Identifiquei o quão importante é definir o rumo!</strong><br />
						O curso trata de técnicas bem elaboradas e objetivas sobre o assunto, porém o grande diferencial é o quanto ele nos faz refletir sobre nós mesmos e o quanto uma "virada" depende de nós mesmos. Saio muito diferente do que entrei, porém levando comigo a compreensão do quão importante é definir o rumo (olhar a bússola) e dedicar tempo ao planejamento para chegar aos destinos estabelecidos.</h4>
                      <svg class="quoteSlickEnd" width="39" height="31" viewBox="0 0 39 31" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M29.9465 0.201172L24.6181 4.31076C27.1421 6.86784 28.8247 9.28793 29.6661 11.571C30.5074 13.7628 30.928 16.0003 30.928 18.2834L34.2934 15.9546H24.0572V30.2012H38.5V20.2012C38.5 16.4569 37.8924 12.9866 36.6771 9.79022C35.3684 6.50255 33.1248 3.3062 29.9465 0.201172ZM6.3893 0.201172L1.06088 4.31076C3.58487 6.86784 5.26752 9.28793 6.10885 11.571C6.95018 13.7628 7.37085 16.0003 7.37085 18.2834L10.7362 15.9546H0.5V30.2012H14.9428V20.2012C14.9428 16.4569 14.3352 12.9866 13.1199 9.79022C11.8112 6.50255 9.56765 3.3062 6.3893 0.201172Z"/>
                      </svg>
                    </div>
                  </div>
                </div>
                          </div>
          
              </div>
    </div>
  </div>
</section>
<section id="secWhyMethodus">
  <div class="container">
  <div class="row">
      <div class="col-md-10 col-md-offset-1 col-sm-12" style="padding-bottom:40px">
            <h2 class="courseColor" style="text-align: center;"><strong>Por que a Methodus funciona melhor para você:</strong></h2>

		<h4>Para você entender quais os diferencias reais, vamos falar primeiro sobre o criador da Methodus, o professor Alcides Schotten – também conhecido como O MESTRE DOS ESTUDOS.</h4>

		<h4>Alcides Schotten é PROFESSOR E FILÓSOFO, ou seja, ele vai além do básico e aprofunda-se realmente na mente humana.</h4>

		<h4>Estudioso por natureza, aprimora-se todos os anos, buscando na neurociência, o entendimento de como o cérebro funciona e reage. E assim, usa toda sua base e conhecimento de professor e filósofo, para criar os estímulos perfeitos para que o cérebro aprenda de verdade. Não é simplesmente aprender por uns dias. A metodologia é feita para ficar. E de maneira fácil.</h4>

<img class="wp-image-6367 align-center" style="align: center;" src="https://methodus.com.br/wp-content/uploads/2019/11/curso-de-administracao-do-tempo-8-1024x682.jpg" alt="" width="800" height="533" /><br>
<p style="text-align: center;">Prof. Alcides Schotten – O MESTRE DOS ESTUDOS</h4>

                                            </div>
      </div>
    </div>
  </div>
</section>
<!-- Lembrete 
<section id="secReminder">
  <div class="reminderBanner courseBackgroundColor">
    <img class="small" src='https://methodus.com.br/wp-content/themes/maiscode_methodus_novo/img/small.png'>
    <div class="">
      <div class="row">
        <div class="col-md-12">
          <h2>Lembrete Methodus</h2>
          <h3>Acabe com a procrastinação, gerencie seu tempo e veja sua vida deslanchar!</h3>
          <a data-toggle="modal" data-target="#cursoVejaInvestimento" href="#">Nossa maior felicidade ver é a gratidão de quem faz o curso!</a>
        </div>
      </div>
    </div>
    <img class="big" src='https://methodus.com.br/wp-content/themes/maiscode_methodus_novo/img/big.png'>
  </div>
</section> -->

<!-- VANTAGENS
<section id="secAdvantages">
  <div class="bannerSatisfaction courseBackgroundColor">
    <h2>Olha só a satisfação de quem fez o curso:</h2>
    <h3>Assista até o final. A decisão sobre o seu futuro agora é sua.</h3>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
                  <div class="advantagesVideos">
                      <div class="col-md-4 col-sm-6 text-center">
              <div class="local-video-container">
                <div class="background-image-holder" style="background: url(&quot;https://methodus.com.br/wp-content/uploads/2020/08/curso-de-oratoria-lp-7.png&quot;); "> <img alt="Background Image" class="background-image" src="https://methodus.com.br/wp-content/uploads/2020/08/curso-de-oratoria-lp-7.png" style="display: none;"> 
                </div>
                <video controls="">
                  <source src="https://api.methodus.com.br/site/cursos/videos/1-eleuzia.mp4" type="video/mp4">                                
                </video> 
                <div class="play-button" style="opacity: 0;"></div>
              </div>
            </div>
                      <div class="col-md-4 col-sm-6 text-center">
              <div class="local-video-container">
                <div class="background-image-holder" style="background: url(&quot;https://methodus.com.br/wp-content/uploads/2020/08/curso-de-oratoria-lp-5.png&quot;); "> <img alt="Background Image" class="background-image" src="https://methodus.com.br/wp-content/uploads/2020/08/curso-de-oratoria-lp-5.png" style="display: none;"> 
                </div>
                <video controls="">
                  <source src="https://api.methodus.com.br/site/cursos/videos/3-ricardo.mp4" type="video/mp4">                                
                </video> 
                <div class="play-button" style="opacity: 0;"></div>
              </div>
            </div>
                      <div class="col-md-4 col-sm-6 text-center">
              <div class="local-video-container">
                <div class="background-image-holder" style="background: url(&quot;https://methodus.com.br/wp-content/uploads/2020/08/curso-de-oratoria-lp-6.png&quot;); "> <img alt="Background Image" class="background-image" src="https://methodus.com.br/wp-content/uploads/2020/08/curso-de-oratoria-lp-6.png" style="display: none;"> 
                </div>
                <video controls="">
                  <source src="https://api.methodus.com.br/site/cursos/videos/4.mp4" type="video/mp4">                                
                </video> 
                <div class="play-button" style="opacity: 0;"></div>
              </div>
            </div>
                    </div>
                <span class="courseColor">Veja como a vida mudou em apenas 10 aulas! Agora é a sua vez!</span>        <h2 class="courseColor">&#x25cf; SÓ COM A METHODUS VOCÊ CONSEGUIRÁ &#x25cf;</h2>        <div class="advantagesBox">
                                    <h3 class="courseColor">&#x25cf; Acabar com a introversão:</h3>
                              <h4>Sabe aquela sensação de saber o que falar numa reunião e não conseguir (a voz não sai)? Você sabe o que dizer, mas outra pessoa fala e leva os aplausos que deveriam ser seus? Isso nunca mais vai acontecer.</h4>
                                                      <h3 class="courseColor">&#x25cf; Perder o medo de falar em público...</h3>
                              <h4>...e não ficar nervoso, com a didática exclusiva Methodus, que acelera o processo.</h4>
                                                      <h3 class="courseColor">&#x25cf; Preparar apresentações</h3>
                              <h4>Isso faz parte de como ter segurança para falar!</h4>
                                                      <h3 class="courseColor">&#x25cf; Técnicas</h3>
                              <h4>de como não esquecer o que vai dizer!</h4>
                                                      <h3 class="courseColor">&#x25cf; Saber entrar no assunto a qualquer momento...</h3>
                              <h4>e falar nas reuniões presenciais ou online (conferência).</h4>
                                            <span>E mais:</span>
                                        <h3 class="courseColor">&#x25cf; Reciclagem permanente!</h3>
                              <h4>Você pode fazer o curso novamente de graça quantas vezes quiser!</h4>
                                                      <h3 class="courseColor">&#x25cf; Um professor ao seu lado que te ajuda!</h3>
                                                          </div>
      </div>
    </div>

<section id="secTestimony">
    <div class="row">
      <div class="col-md-12">
              </div>
    </div>
  </section>
  </div>
</section>-->

<section id="secBannerAdvantages" class="courseBackgroundColor">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
	  
        <h2>IMAGINE...</h2>
        <div class="advantagesBox">
                                    <h3>Queria convidar você a se imaginar daqui a 3 meses fazendo tudo que não conseguiu estes últimos anos.</h3>
                          <h3>Tudo que você começou e parou porque não encontrou tempo para fazer.</h3>
                          <h3>E também aquele sonho específico que você sempre procrastina... deixa para depois, ou espera uma condição ideal do tipo:</br>
						- Quando eu estiver ganhando mais eu faço...</br>
						- Quando meus filhos crescerem eu penso nisso...</br>
						- Quando eu receber o décimo-terceiro...</br>
</h3>
                          <h3>Pense agora naquele objetivo que você sempre deixa para depois.</h3>
                          <h3>Não seria MARAVILHOSO conseguir dar passos nele sem sofrer, sem estar cansado e com consistência?</h3>
                          <h3> Pois, então. A Methodus pega você pela mão e te leva pelo caminho certo. </h3>
						  <h3>Você vai conseguir isso tudo naturalmente. </h3>
						  <h3> Imaginou seu objetivo? Imaginou seu sonho tornando-se realidade? </h3>

        </div>
      </div>
    </div>
  </div>
</section>
<!--<section id="secLogistic">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2 class="courseColor">LOGÍSTICA DAS AULAS</h2>
        <h3 class="courseColor">&#x25cf; O MÉTODO METHODUS, QUE É O DIFERENCIAL &#x25cf;</h3>
        <div class="boxLogistic">
                      <ul>
                              <li><span>Apenas 10 a 15 participantes por grupo.</span></li>
                              <li><span>Em todas as aulas os participantes falarão pelo menos duas vezes.</span></li>
                              <li><span>Apresentações filmadas e analisadas com o professor.</span></li>
                              <li><span>Professor preparado para atender participantes em suas dúvidas.</span></li>
                              <li><span>Manual contendo a teoria abordada.</span></li>
                              <li><span>Gravações das suas apresentações para você baixar.</span></li>
                          </ul>
                    <h3 class="courseColor">OK, MAS E QUAL O VALOR DO INVESTIMENTO?</h3>
                      <div class="boxInvestiment">
              <ul>
                                  <li><span>Quanto vale colocar isso no seu currículo?</span></li>
                                  <li><span>Quanto vale ser promovido (quanto você ganhará a mais de salário)?</span></li>
                                  <li><span>Quanto vale conseguir ir bem numa entrevista de emprego?</span></li>
                                  <li><span>Quanto custam todas as mensalidades de uma pós-graduação (que demora e pode não ser decisiva para ser promovido)?</span></li>
                                  <li><span>Qual o preço de fazer outro curso e não ficar satisfeito (jogar dinheiro fora)?</span></li>
                              </ul>
            </div>
                  </div>
      </div>
    </div>
  </div>
</section>-->



<!-- Modal Cupom 
<div class="modal fade" id="cupomModal" tabindex="-1" role="dialog" aria-labelledby="cupomModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div role="form" class="wpcf7" id="wpcf7-f6273-o1" lang="pt-BR" dir="ltr">
<div class="screen-reader-response"><p role="status" aria-live="polite" aria-atomic="true"></h4> <ul></ul></div>
<form action="/cursos/curso-de-oratoria/#wpcf7-f6273-o1" method="post" class="wpcf7-form init" novalidate="novalidate" data-status="init">
<div style="display: none;">
<input type="hidden" name="_wpcf7" value="6273" />
<input type="hidden" name="_wpcf7_version" value="5.3" />
<input type="hidden" name="_wpcf7_locale" value="pt_BR" />
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f6273-o1" />
<input type="hidden" name="_wpcf7_container_post" value="0" />
<input type="hidden" name="_wpcf7_posted_data_hash" value="" />
</div>
<div class="box-contato" id="investir">
<h2 class="titulo-box vermelho">Preencha abaixo e receba um cupom de desconto de 10% no seu e-mail!</h2>
<div class="interno-box">
<div class="inputCupomForm">
<span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Primeiro nome" /></span><br />
<span class="wpcf7-form-control-wrap email"><input type="email" name="email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="E-mail" /></span>
</div>
<h4><label>Curso de Preferência</label><br />
<span class="wpcf7-form-control-wrap menu-140"><select name="menu-140" class="wpcf7-form-control wpcf7-select" id="coursePref" aria-invalid="false"><option value="">---</option><option value="Administração do Tempo">Administração do Tempo</option><option value="Leitura Dinâmica / Memória">Leitura Dinâmica / Memória</option><option value="Administração do Tempo">Administração do Tempo</option></select></span></h4>
<div class="oratory radioCupom" style="display: none">
<h4>Eu sou: Administração do Tempo</h4>
<h4><span class="wpcf7-form-control-wrap radio-972"><span class="wpcf7-form-control wpcf7-radio"><span class="wpcf7-list-item first"><input type="radio" name="radio-972" value="Sou empreendedor / tenho sonhos a realizar" /><span class="wpcf7-list-item-label">Sou profissional formado / preciso administrar o tempo / procastino</span></span><span class="wpcf7-list-item"><input type="radio" name="radio-972" value="Estou empregado / não tenho curso superior / preciso me organizar" /><span class="wpcf7-list-item-label">Sou estudante / procastino / preciso aprender a me organizar</span></span><span class="wpcf7-list-item"><input type="radio" name="radio-972" value="Sou concursando e me preparo para prova oral" /><span class="wpcf7-list-item-label">Sou concursando e me preparo para prova oral</span></span><span class="wpcf7-list-item last"><input type="radio" name="radio-972" value="Sou universitário / apresento seminários / processo seletivo" /><span class="wpcf7-list-item-label">Sou universitário / apresento seminários / processo seletivo</span></span></span></span>
</h4></div>
<div class="memory radioCupom" style="display: none">
<h4>Eu sou: Leitura Dinâmica / Memória</h4>
<h4><span class="wpcf7-form-control-wrap radio-429"><span class="wpcf7-form-control wpcf7-radio"><span class="wpcf7-list-item first"><input type="radio" name="radio-429" value="Vestibular / concursos / ensino médio" /><span class="wpcf7-list-item-label">Vestibular / concursos / ensino médio</span></span><span class="wpcf7-list-item"><input type="radio" name="radio-429" value="Cursando ensino superior / estagiário / trainee" /><span class="wpcf7-list-item-label">Cursando ensino superior / estagiário / trainee</span></span><span class="wpcf7-list-item last"><input type="radio" name="radio-429" value="Sou profissional / faço pós e especializações" /><span class="wpcf7-list-item-label">Sou profissional / faço pós e especializações</span></span></span></span>
</h4></div>
<div class="time radioCupom" style="display: none">
<h4>Eu sou: Administração do Tempo</h4>
<h4><span class="wpcf7-form-control-wrap radio-429"><span class="wpcf7-form-control wpcf7-radio"><span class="wpcf7-list-item first"><input type="radio" name="radio-429" value="Sou empreendedor / tenho sonhos a realizar" /><span class="wpcf7-list-item-label">Sou empreendedor / tenho sonhos a realizar</span></span><span class="wpcf7-list-item"><input type="radio" name="radio-429" value="Sou profissional formado / preciso administrar o tempo / procrastino" /><span class="wpcf7-list-item-label">Sou profissional formado / preciso administrar o tempo / procrastino</span></span><span class="wpcf7-list-item last"><input type="radio" name="radio-429" value="Estou empregado / não tenho curso superior / preciso me organizar" /><span class="wpcf7-list-item-label">Estou empregado / não tenho curso superior / preciso me organizar</span></span></span></span>
</h4></div>
<h4><input type="submit" value="Enviar" class="wpcf7-form-control wpcf7-submit" />
</h4></div>
</div>
</div>
<div class="wpcf7-response-output" aria-hidden="true"></div></form></div>      </div>
    </div>
  </div>
</div>
-->

<!-- PREÇOS -->
<section id="secPrices" class="" style="">
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1 col-sm-12" style="align:center">
        <div class="pricesTop featurePrice">
		<img class="wp-image-6367 align-center" src="https://methodus.com.br/wp-content/uploads/2019/11/curso-de-administracao-do-tempo-13-scaled.jpg" alt="" width="800" height="533" style="padding-bottom:40px"/><br>
          <h2 style= "color:#e38902">Quanto custaria para você fazer isso AGORA e começar a virar sua vida?</h2>
<h3>- Muito menos do que o valor de uma pós-graduação ou MBA para conseguir aumento e ainda deixar de ir atrás do seu sonho.</h3>
<h3>- Muito menos que um ano de cursinho a mais porque não conseguiu tempo ou energia para se dedicar aos estudos e passar no vestibular ou concurso dos seus sonhos.</h3>
<h3>- Quanto custa perder uma oportunidade de um novo trabalho por não ir atrás (ficar procrastinando)?</h3>
<h3>- - Quanto você vai gastar em médicos, nutricionistas ou terapia anti-estresse por não conseguir se organizar ou procrastinar para fazer atividades físicas?</h3>
<h2 style="color: #e38902;">- Em tudo isso acima, você gastaria, em bons cursos e bons profissionais mais de <strong> 10 Mil Reais</strong> em um ano!</h2>
<h3>Clique agora e descubra que com um investimento <strong> muito menor</strong>, você resolve não só o seu sonho de agora, mas os objetivos durante a sua vida TODA, EM TODO O SEU FUTURO!</h3>
<h3>Você vai aprender a gerenciar seu tempo, se automotivar com as coisas que realmente importam e terá uma vida mais leve, com menos estresse e muito feliz e realizada!</h3>
</section>
<section id="secPrices" class="courseBackgroundColor">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
		<h3 style="color: #fff;">Nunca mais vai sentir a dor de procrastinar!</h3>
		<h3 style="color: #fff;">VEJA VALORES E DATAS:</h3>
          <h3 style="color: #fff;"><strong>CLIQUE AQUI E DESCUBRA QUE, SIM, CABE NO SEU BOLSO E MAIS: <br> VAI MUDAR A SUA VIDA! </strong></h3>
		  <br />
          <a data-toggle="modal" data-target="#cursoVejaInvestimento" href=#>EU QUERO PARAR DE PROCRASTINAR AGORA!</a>
        </div>
      </div>
    </div>
 
   
</section>


<!-- proximas turmas -->
<section id="secPrices" class="">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
	  <h2 style= "color:#e38902">CURSO COMPLETO:</h2>
        <div class="boxWorkload">
          <h4>Carga horária: 4 Aulas de 3 Horas<br />
			Participantes: limite de 20 por turma </h4>
        </div>
		</div>
		</div>
		</div>
		</section>
<section id="secPrices" class="courseBackgroundColor">
<div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2>PRÓXIMA TURMA</h2>
                              <div class="col-md-12 col-sm-12" style="text-align:center">
              <div class="classesDate">
                <h4>02/08/2021 a 05/08/2021</h4>
                <small>(Segunda à Quinta - 19h às 22h)</small>
              </div>
            </div>
            <!--DATAS
                      <div class="col-md-6 col-sm-12">
              <div class="classesDate">
                <h4>11/01/2021 à 22/01/2021</h4>
                <small>(10 manhãs - 2ª à 6ª - 9h às 12h)</small>
              </div>
            </div>
                      <div class="col-md-6 col-sm-12">
              <div class="classesDate">
                <h4>26/01/2021 à 05/02/2021</h4>
                <small>(9  tardes - 2ª à 6ª - 14h às 17h20)</small>
              </div>
            </div>
              -->
      </div>
	  
    </div>
  <div class="row">
    <div class="col-md-12">
              <div>
              <a data-toggle="modal" data-target="#cursoVejaInvestimento" href=#>Clique aqui e mude seu futuro profissional agora!</a>
        </div>
    </div>
  </div>
  </div>
  </div>
</section>

<section id="secPrices" class="courseBackgroundColor">
  <div class="row">
    <div class="col-md-12">
      <div class="featuredList">
        <h2>E + 2 Bônus Exclusivos e 1 SUPER Bônus!</h2>
		
		<div class="col-md-4 col-sm-12">
		<div class="boxWorkload">
			<h3 style="color: #e38902;">Bônus 1:</h3>
			<h4>Teste / Guia Rápido da Procrastinação</h4>
			<h4>Um Manual enxuto para consultar sempre que começar a procrastinar.</h4>
			<h4></h4>
        </div>
		</div>
		<div class="col-md-4 col-sm-12">
        <div class="boxWorkload">
			<h3 style="color: #e38902;">Bônus 2:</h3>
			<h4>Infográfico Completo: 5 Passos Infalíveis para Realizar GRANDES SONHOS.</h4>
			<h4>Um material único e revelador. Seguindo estes 5 passos, nos 3 níveis de implementação necessários, você REALIZA SEU GRANDE SONHO!</h4>
        </div>
		</div>
		<div class="col-md-4 col-sm-12">
		<div class="boxWorkload">
			<h3 style="color: #e38902;">SUPER BÔNUS:</h3>
			<h4>1 AULA EXCLUSIVA E INDIVIDUAL COM O PROF. ALCIDES SCHOTTEN, O MESTRE DOS ESTUDOS.</h4>
			<h4>sso, só a Methodus tem. A oportunidade de tirar dúvidas, absorver conhecimentos e se transformar de dentro para fora!</h4>
        </div>
		</div>
		<h3 style="color:#fff">Venha viver esta experiência realizadora de mudar de vida! </h4>

		<h3 style="color:#fff">Clique para ver os valores e datas!</h4>
		<div>
              <a data-toggle="modal" data-target="#cursoVejaInvestimento" href=#>EU QUERO MUDAR DE VIDA! </a>
        </div>
  
              </div>

    </div>
  </div>
</section>


<!-- Modal  veja investimento -->
<div id="cursoVejaInvestimento" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <?php echo do_shortcode( '[contact-form-7 id="6430" title="VEJA INVESTIMENTOS E PRÓXIMOS GRUPOS!"]'); ?>

      </div>
    </div>

  </div>
</div>


  <script type='text/javascript' async src='https://d335luupugsy2.cloudfront.net/js/loader-scripts/67cc1fec-89be-4715-b86e-5eda535a7d57-loader.js'></script><script type='text/javascript' src='https://methodus.com.br/wp-content/themes/maiscode_methodus_novo/js/main.js?ver=1.0.0' id='jquery-js'></script>
<script type='text/javascript' id='contact-form-7-js-extra'>
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/methodus.com.br\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script type='text/javascript' src='https://methodus.com.br/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.3' id='contact-form-7-js'></script>
<script type='text/javascript' src='https://methodus.com.br/wp-includes/js/wp-embed.min.js?ver=5.5.3' id='wp-embed-js'></script>

<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script> 
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>

<script type="text/javascript">
$(document).ready(function(){

  $('.sliderCard').slick({
    dots: false,
    slidesToShow: 1,
    infinite: false,
    autoplay: false,
    fade: true,
    arrows: true,
    prevArrow: "<img class='a-left control-c prev slick-prev' src='https://methodus.com.br/wp-content/themes/maiscode_methodus_novo/img/arrowPrev.png'>",
    nextArrow: "<img class='a-right control-c next slick-next' src='https://methodus.com.br/wp-content/themes/maiscode_methodus_novo/img/arrowNext.png'>",
    responsive: [
      {
        breakpoint: 780,
        settings: {
          slidesToShow: 1,
          arrows: false,
        }
      },        
    ]
  });

  $('.sliderCardDepo').slick({
    dots: false,
    slidesToShow: 1,
    infinite: true,
    autoplay: true,
    fade: true,
    arrows: true,
    prevArrow: "<img class='a-left control-c prev slick-prev depo-prev' src='https://methodus.com.br/wp-content/themes/maiscode_methodus_novo/img/arrowPrev.png'>",
    nextArrow: "<img class='a-right control-c next slick-next depo-next' src='https://methodus.com.br/wp-content/themes/maiscode_methodus_novo/img/arrowNext.png'>",
    responsive: [
      {
        breakpoint: 780,
        settings: {
          slidesToShow: 1,
          arrows: false,
        }
      },        
    ]
  });


 });


</script>

<?php get_footer(); ?>