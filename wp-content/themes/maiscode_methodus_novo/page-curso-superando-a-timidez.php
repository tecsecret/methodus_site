<?php
/**
* Template Name: Curso Online de como vencer a timidez
* @package WordPress
* @author Mais Code Tecnologia
* @since First Version
*/

$url_site = get_option('link_site'); 
$url_api  =  get_option('link_api'); 
$url_amigavel_wp = home_url(); 
// URLs amigáveis
$url_amigavel_on  = 1;
$url_amigavel   =  get_option('link_site_amigavel');

$pagina_curso = 1;

$urlWP = get_template_directory_uri();
// $urlWP = site_url();
// $urlWP = 'https://www.methodus.com.br/';
  
// Verificar
$ID = get_field('id_do_curso');
$ID_Depoimento_Curso = get_field('id_depoimento_curso');
$ebookWp = get_field('ebook');
$xml_curso  = executaPagina( 'api/', array( 'a'=>'cursos', 'metodo'=>'ver', 'ID'=>$ID ) );
$curso      = lerXML( $xml_curso );
    

// var_dump($curso);
    
if ( $curso->erro == 0 ){
    
    $curso              = $curso->curso;
    $status             = $curso->status;
    $titulo_curso       = $curso->titulo;
    $url_curso          = $curso->url;
    $distancia_curso    = $curso->distancia;
    $banner_curso       = $curso->banner;
    $resumo_curso       = $curso->resumo;
    $descricao_curso    = $curso->descricao;
    $variaveis_curso    = $curso->variaveis; 
    $planos_curso       = $curso->planos;
    $turmas_curso       = $curso->turmas->turma;
    $perfis_curso       = $curso->perfis->perfil;
    $ID_tema            = $curso->tema;
    
    $seo_metatags       = $curso->metatags;
    $seo_title          = $titulo_curso.' - Methodus';
    $seo_imagem         = 'https://'.$_SERVER['HTTP_HOST'].''.$banner_curso;
    
    if ( isset( $variaveis_curso->classe ) ){
        $classe_curso = $variaveis_curso->classe;
    }else{
        $classe_curso = 'laranja'; 
    }
    
    if ( isset( $curso->fotos ) ){
        $fotos_curso = $curso->fotos->foto;
    }

    if ( $status != 1 ){
        exit("Erro ao acessar os dados do curso 1");
    }
    
}else{
    exit("Erro ao acessar os dados do curso");      
}

?>


<?php get_header(); ?>

<style type="text/css">
    .caixa_ebook img{
    width: 170px;
  }
        .btn_top{
            color: #fff !important;
        }
        .btn_top:hover{
            background-color: #fff !important;
            color: #a00000 !important;
        }
        .btn_pg_curso{
            border: 2px solid #a00000 !important;;
        }
        .btn_pg_curso:hover{
            background-color: #fff !important;
            color: #a00000 !important;
        }
        .btn_chamada:hover{
            color: #a00000 !important;
        }
    </style><!-- TOPO -->

<style type="text/css">
  .courseColor {
    color: #a00000;
  }
  .courseBackgroundColor {
    background-color: #a00000;
  }
  #secStoryModB .storyContent svg path {
    fill: #a00000;
  }
  .sliderCard svg path {
    fill: #a00000;
  }
  #secTopModB:before {
    content: '';
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    background: linear-gradient(270deg, rgba(160, 0, 0, 0.5) 23.16%, rgba(52, 50, 50, 0) 118.16%);
  }
  #secCallBannerModB .bgTransformation:before {
    content: '';
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    background: linear-gradient(179.64deg, rgba(96, 94, 94, 0) -36.93%, rgba(160, 0, 0, 0.6) 85.54%);
  }
  .loadd{
    display: none;
    opacity: .5;
    background: #000;
    width: 100%;
    position: absolute;
    top: 0px;
    height: 100vh;
    z-index: 10000000;
  }
 
  .sliderCardDepo .depo-prev
  {
    height: 36px;
    width: 220px;
    z-index: 100000;
    object-fit: contain;
  }
  .sliderCardDepo .depo-next{
    width: 235px;
    height: 36px;
    object-fit: contain;
  }
</style>



<section id="secTopModB" style="background-image: url('https://methodus.com.br/wp-content/uploads/2020/08/imagem1.jpg')">
  <div class="container">
    <div class="row">
      <div class="col-md-7 col-md-offset-3 col-sm-7 col-sm-offset-3 topContent">
        <h2>Curso Online de como Vencer a Timidez e a Introversão</h2>
        <h4></h4>
		<h5><br><br><br><br></h5>

      </div>
    </div>
  </div>
</section>

        <?php
    if ( isset( $variaveis_curso->ebook_download ) ){
        $temp_ebook = explode( '|', $variaveis_curso->ebook_download );

        if ( count( $temp_ebook ) >= 4 ){

            $titulo_ebook           = $temp_ebook[0];
            $descricao_ebook        = $temp_ebook[1];
            $destino_ebook          = $temp_ebook[2];
            $identificador_ebook    = $temp_ebook[3];
            $urlWP = get_template_directory_uri();
            ?>


            <section class="caixa_ebook ">
                <div class="container-fluid bg_ebook">
                    <div class="container">                       
                 
                        <script src="<?php echo $urlWP; ?>/js/scripts_site/ebook.js?ver=1"></script>
                        <form action="<?php echo $url_api; ?>/api/" method="post" class="form-email formAjax" id="form_ebook" data-validacao="checaEbook" data-resultado="sucessoEbook">
                            <input type="hidden" name="destino" value="<?php echo $ebookWp ?>" />
                            <input type="hidden" name="a" value="aluno" />
                            <input type="hidden" name="metodo" value="cadastro" />
                            <input type="hidden" name="origem" value="Ebook_<?php echo $identificador_ebook; ?>" />
                            <input type="hidden" name="curso" value="<?php echo $ID; ?>" />
                    
                            <div class="row">
                                <div class="col-xs-12 col-md-8">
                                    <img src="<?php echo $urlWP ?>/img/ebook_timidez.png" width="80" />
                                    <div class="chamada-ebook"><strong>NOVO INFOGRÁFICO</strong> <br> 4 ERROS BÁSICOS QUE TODO PALESTRANTE INICIANTE COMETE </div>
                                </div>
                                <div class="col-xs-12 col-md-4">
                                    <h5 class="mb0 inline-block p0-xs">
                                        <?php echo $titulo_ebook ?>
                                    </h5>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <input type="text" class="validate-required cada-input" name="nome" placeholder="Nome" lang="Nome">
                                        </div>
                                        <div class="col-sm-12 form-ebook">
                                            <input type="text" class="validate-required cada-input" name="email" placeholder="E-mail" lang="E-mail">
                                            <?php
                                            $i = 0;
                                            if( have_rows('formulario') ):
                                                while ( have_rows('formulario') ) : the_row(); $i++?> 
                                                    <div class="radio_cursos">  
                                                        <label for="ebook-<?php echo $i; ?>" class="radio">
                                                            <input type="radio" class="validate-required" id="ebook-<?php echo $i; ?>" name="conheceu" value="<?php the_sub_field('opcoes'); ?>" lang="opções">
                                                            <span></span>
                                                        </label>          
                                                        <label for="ebook-<?php echo $i; ?>"><?php the_sub_field('opcoes'); ?></label><br>
                                                    </div>
                                                <?php endwhile;
                                            endif;
                                            ?>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <button class="btn <?php echo $classe_curso; ?>" type="submit">Download</button>
                                        </div>
                                    </div>
                                </div>
                            </div>                            
                        </form>
                    </div>
                </div>
            </section>
        <?php
        }
    }
    ?>
<!-- BANNER CHAMADA -->
<section id="secCallBannerModB" class="courseBackgroundColor">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2>FINALMENTE O CURSO ONLINE DA METHODUS PARA VENCER A TIMIDEZ E INTROVERSÃO! </h2>
		<h3 style="color:#fff;"> Isso que você vai sentir em público e em diversas situações do dia a dia: <strong class="h2">Alívio!</strong></h3>	
      </div>
    </div>
  </div>
    
</section>

<!-- HISTORIA -->
<section id="secStoryModB">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="storyContent">
		<h3> A Methodus está há mais de duas décadas no mercado treinando profissionais e melhorando a comunicação em diversos setores da vida e agora traz para você um curso online de oratória, para fazer no seu horário, no seu tempo e com a segurança de qualidade da Methodus. </h3>
		<h2> Direto ao ponto! Este curso é para você que quer:</h2>
		<ul class="h3 p3">
			<li> ● eliminar a ansiedade </li>
			<li> ● superar a introversão</li>
			<li> ● acabar com a vergonha</li>
			<li> ● vencer a timidez</li>
			<li> ● nunca mais dar branco</li>
			<li> ● não ter medo de ser julgado</li>
			<li> ● perder o medo da plateia</li>
			<li> ● perder o medo de falar em público</li>
			<li> ● acabar com a insônia na noite anterior</li>
			<li> ● se sair bem nas videoconferências</li>
			<li> ● suas lives com segurança e desenvoltura</li>
			<li><br></li>
		</ul>
		<h2> “OK! – Todo mundo promete isso! Mas eu vou te explicar a diferença!”</h2>
		<h4 style="text-align:center">Prof. Alcides Schotten – O Mestre dos Estudos</h4>
		<div style="text-align:center">
		<div class="col-3" style="display:inline; padding:15px;"><div class="local-video-container">
				<div class="background-image-holder" style="background: url(&quot;https://methodus.com.br/wp-content/uploads/2020/08/curso-de-oratoria-lp-2.png&quot;); "> 
				
            </div>
            <video controls="">
               <source src="https://methodus.com.br/wp-content/uploads/2021/05/timidez.mp4" type="video/mp4">                                
              </video> 
              <div class="play-button" style="opacity: 0;"></div>
            </div></div>
              </div>
              </div>
      </div>
  </div>
  </section>
  <section id="secTestimony">
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1 col-sm-12">
        <h2 class="courseColor">E funciona mesmo! </h2>
                  
            <div class="row sliderCard">
                <div class="col-md-12">
                    <div class="boxTestimony">
                    <img class="avatarImg" src="https://methodus.com.br/wp-content/uploads/2020/08/curso-de-oratoria-2.png">
                    <h3 class="courseColor">Rafael da Silva Passoni </h3>
                    <span>Autoneum | Bacharel em Administração de Empresas</span>
                    <div class="testimonyText">
                      <svg class="quoteSlickStart" width="39" height="33" viewBox="0 0 39 33" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M9.0535 0.201172L14.3819 4.58474C11.8579 7.31229 10.1753 9.89371 9.33395 12.329C8.49262 14.6669 8.07196 17.0535 8.07196 19.4888L4.70664 17.0048H14.9428V32.2012H0.5V21.5345C0.5 17.5406 1.10763 13.8389 2.32288 10.4295C3.63161 6.92264 5.87515 3.5132 9.0535 0.201172ZM32.6107 0.201172L37.9391 4.58474C35.4151 7.31229 33.7325 9.89371 32.8911 12.329C32.0498 14.6669 31.6292 17.0535 31.6292 19.4888L28.2638 17.0048H38.5V32.2012H24.0572V21.5345C24.0572 17.5406 24.6648 13.8389 25.8801 10.4295C27.1888 6.92264 29.4324 3.5132 32.6107 0.201172Z"/>
                      </svg>
                      <h4><strong>Surpreendeu pela autoconfiança adquirida.</strong><br />
						O principal é a autoconfiança no momento da apresentação. Saio deste curso com um fortalecimento deste quesito. Como outros fatores, levo comigo a lição de praticar o laboratório em meu crescimento contínuo, aprimorando o dia a dia. Agradeço por todos os ensinamentos</h4>
                      <svg class="quoteSlickEnd" width="39" height="31" viewBox="0 0 39 31" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M29.9465 0.201172L24.6181 4.31076C27.1421 6.86784 28.8247 9.28793 29.6661 11.571C30.5074 13.7628 30.928 16.0003 30.928 18.2834L34.2934 15.9546H24.0572V30.2012H38.5V20.2012C38.5 16.4569 37.8924 12.9866 36.6771 9.79022C35.3684 6.50255 33.1248 3.3062 29.9465 0.201172ZM6.3893 0.201172L1.06088 4.31076C3.58487 6.86784 5.26752 9.28793 6.10885 11.571C6.95018 13.7628 7.37085 16.0003 7.37085 18.2834L10.7362 15.9546H0.5V30.2012H14.9428V20.2012C14.9428 16.4569 14.3352 12.9866 13.1199 9.79022C11.8112 6.50255 9.56765 3.3062 6.3893 0.201172Z"/>
                      </svg>
                    </div>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="boxTestimony">
                    <img class="avatarImg" src="https://methodus.com.br/wp-content/uploads/2020/08/curso-de-oratoria-3.png">
                    <h3 class="courseColor">Vivian Seidl</h3>
                    <span>Mary Kay | Bacharel em Administração de Empresas / Pós em Marketing</span>
                    <div class="testimonyText">
                      <svg class="quoteSlickStart" width="39" height="33" viewBox="0 0 39 33" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M9.0535 0.201172L14.3819 4.58474C11.8579 7.31229 10.1753 9.89371 9.33395 12.329C8.49262 14.6669 8.07196 17.0535 8.07196 19.4888L4.70664 17.0048H14.9428V32.2012H0.5V21.5345C0.5 17.5406 1.10763 13.8389 2.32288 10.4295C3.63161 6.92264 5.87515 3.5132 9.0535 0.201172ZM32.6107 0.201172L37.9391 4.58474C35.4151 7.31229 33.7325 9.89371 32.8911 12.329C32.0498 14.6669 31.6292 17.0535 31.6292 19.4888L28.2638 17.0048H38.5V32.2012H24.0572V21.5345C24.0572 17.5406 24.6648 13.8389 25.8801 10.4295C27.1888 6.92264 29.4324 3.5132 32.6107 0.201172Z"/>
                      </svg>
                      <h4><strong>Muito além das técnicas de oratória.</strong><br />
						Iniciei o curso com intenção de melhorar minha maneira de falar e de me preparar para ministrar treinamentos. Depois de duas semanas intensas de aulas saio não apenas me expressando melhor, mas sim transformada de dentro para fora, mais segura e som maior consciência de quem sou. O curso foi muito além de técnicas sobre oratória; é um curso para a vida.</h4>
                      <svg class="quoteSlickEnd" width="39" height="31" viewBox="0 0 39 31" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M29.9465 0.201172L24.6181 4.31076C27.1421 6.86784 28.8247 9.28793 29.6661 11.571C30.5074 13.7628 30.928 16.0003 30.928 18.2834L34.2934 15.9546H24.0572V30.2012H38.5V20.2012C38.5 16.4569 37.8924 12.9866 36.6771 9.79022C35.3684 6.50255 33.1248 3.3062 29.9465 0.201172ZM6.3893 0.201172L1.06088 4.31076C3.58487 6.86784 5.26752 9.28793 6.10885 11.571C6.95018 13.7628 7.37085 16.0003 7.37085 18.2834L10.7362 15.9546H0.5V30.2012H14.9428V20.2012C14.9428 16.4569 14.3352 12.9866 13.1199 9.79022C11.8112 6.50255 9.56765 3.3062 6.3893 0.201172Z"/>
                      </svg>
                    </div>
                  </div>
                </div>
                          </div>
          
              </div>
    </div>
  </div>
</section>

<!-- Lembrete  -->
<section id="secReminder">
  <div class="reminderBanner courseBackgroundColor">
    
    <div class="">
      <div class="row">
        <div class="col-md-12" style="padding-inline:50px">
          <h2>LEMBRETE METHODUS !</h2>
          <h3>Muitas pessoas confundem este curso com cursos de oratória online, mas o fato é que este curso vai direto ao ponto em questões fundamentais que outros cursos de oratória online não abordam.</h3>
          <a data-toggle="" data-target="#cursoVejaInvestimento" href="#">PERFEITO PARA SITUAÇÕES DE DIA A DIA NO TRABALHO E NA VIDA PESSOAL!</a>
        </div>
      </div>
    </div>
    
  </div>
</section>

<!-- POR QUE A METHODUS -->
<section id="secWhyMethodus">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2 class="courseColor">Por que a Methodus?</h2>
        <div class="methodusContent">
                                 

<img class="aligncenter wp-image-6362" src="hhttps://methodus.com.br/wp-content/uploads/2020/08/curso-de-oratoria-2.jpg" alt="" width="auto" height="100%" />

            <h4 class="">Porque só aqui você vai aprender com uma metodologia pensada por um professor e filósofo e que sabe falar em público, diferentemente de cursos dados por pessoas que sabem falar em público, mas não têm a didática e o pensamento de como fazer VOCÊ aprender!</h4>
                              <h3 style="text-align:center">O PROF. ALCIDES SCHOTTEN, O MESTRE DOS ESTUDOS, EXPLICA ABAIXO O MÉTODO:</h3>

<img class="aligncenter wp-image-6366" src="https://methodus.com.br/wp-content/uploads/2019/11/curso-de-administracao-do-tempo-8.jpg" alt="" width="auto" height="100%" />
<p> Prof. Alcides Schotten – Mestre, Pedagogo e Filósofo – Estudioso da Mente Humana</p>

                                            </div>

        <div class="differenceContent">
                    <h3 class="courseColor"><strong>●●● Como fazemos para você aprender? ●●● </strong></h3>
										
						<h4>Alguma coisa difícil de aprender na escola você lembra até hoje? Não, né?</h4>

						<h3>Por isso, a gente sempre pensa em jeitos fáceis de você aprender.</h3>

						<h4>Porque senão, você esquece. O método utilizado nos cursos e treinamentos oferecidos pela Methodus foi desenvolvido pelo professor e filósofo Alcides Schotten, que procurou em todo o conhecimento humano, desde Platão e Aristóteles, na Grécia Antiga, até os pensadores mais modernos, um ponto de equilíbrio e o segredo do aperfeiçoamento de talentos naturais. Tudo embasado em teorias científicas comprovadas.</h4>

						<h4>Através do método usado nos cursos da Methodus, o aluno desenvolverá de forma prática, gradual e relativa às suas necessidades, tudo para alcançar os seus objetivos. Ou seja, não é uma fórmula que pode funcionar para um e não para o outro. Existe uma fórmula e dosagem certa para você. Por isso é fácil e duradouro.</h4>

					<h3 class="courseColor"><strong>●●● E COMO ISSO É APLICADO NO CURSO? ●●● </strong></h3>
					
						<h4>O Curso “Como Vencer a Timidez e a Introversão” é destinado a quem, além de querer se comunicar de forma eficiente, deseja: <strong>adquirir inteligência emocional para administrar seus sentimentos, vencer o medo, vencer a introversão e ter segurança ao falar no trabalho, nos estudos (escola ou faculdade) e no dia a dia.</strong></h4>
						
					<h2 class="courseColor" style= "padding-top:60px;">E o que você vai aprender?</h2>
					
					<h4>O macro você já sabe: a ideia é vencer o medo de falar em público e acabar com a timidez e introversão. É mais que um curso de oratória online. Além das dicas de como falar em público tanto em reuniões online para quem está home office ou fazer vídeos e lives no Instagram. É também um Curso de Palestrante, abordando tudo que você precisa para ter segurança e sucesso!</h4>
					<img class="aligncenter wp-image-6366" src="https://methodus.com.br/wp-content/uploads/2020/08/curso-de-oratoria-3.jpg" alt="" width="auto" height="100%" />
					<div style="text-align:left">
					<h3 class="courseColor" style="text-align:left;"><strong>Aula 1</strong></h3>
					<h4>Consciência da Individualidade: você é único – como acreditar mais em você e não se comparar com os outros.</h4>
					<h3 class="courseColor" style="text-align:left;"><strong>Aula 2 </strong></h3>
					<h4>Os 3 Sintomas: Antes, Durante e Depois de Apresentações.</h4>
					<h3 class="courseColor" style="text-align:left;"><strong>Aula 3</strong></h3>
					<h4>As 3 Origens do Medo: Timidez, Introversão e Experiências Ruins.</h4>
					
					<h3> Aulas 4, 5 e 6: As 3 Soluções </h3>
					<h3 class="courseColor" style="text-align:left;"><strong>Aula 4</strong></h3>
					<h4>Solução 1: Autoconcepção - O que é?  Como fazer sua Autoreprogramação. Exercícios práticos.</h4>
					<h3 class="courseColor" style="text-align:left;"><strong>Aula 5 </strong></h3>
					<h4>Solução 2: Conhecimento + Organização + Memorização – Técnicas de como fazer.</h4>
					<h3 class="courseColor" style="text-align:left;"><strong>Aula 6 </strong></h3>
					<h4>Solução 3: Como perder a timidez através da experiência de falar. Como dominar os Palcos: Físicos e Virtuais (lives, reuniões online).</h4>
					<h3 class="courseColor" style="text-align:left;"><strong>Aula 7</strong></h3>
					<h4>O poder do Impacto ao falar em Público. Um Guia para: Linguagem Corporal (55%), Modulação da Voz (38%), Lógica + Adequação Verbal (7% - como falar a língua do seu público).</h4>
					<h3 class="courseColor" style="text-align:left;"><strong>Aula 8 </strong></h3>
					<h4>Inteligência Interpessoal: Dicas de empatia e como conquistar pessoas e a sua atenção.</h4>
					<h3 class="courseColor" style="text-align:left;"><strong>Aula 9 </strong></h3>
					<h4>A Dinâmica do Poder: O Xeque-Mate! Como se empoderar perante seus ouvintes na vida pessoal, chefes e parceiros de trabalho, e ter sua importância reconhecida e valorizada.</h4>
					<h4><br><br><br></h4></div>
</section>

<!-- SOBRE O CURSO -->
<section id="secWhyMethodus">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
	  <h2 class="courseColor">Leia esta história e entenda o que o curso pode realmente fazer por você:</h2>
        <div class="methodusContent">
		<img class="aligncenter wp-image-6362" src="https://methodus.com.br/wp-content/uploads/2020/08/curso-de-oratoria-4-scaled.jpg" alt="" width="auto" height="100%" />
			<h4><br></h4>
            <h4><strong>E ALÉM DE TUDO ISSO, COM AS TÉCNICAS DE INTELIGÊNCIA INTERPESSOAL, VOCÊ APREDENDE “O QUE AS PESSOAS GOSTAM DE OUVIR” E ASSIM ENCANTA E CONQUISTA TANTO NA VIDA PESSOAL QUANTO PROFISSIONAL.</strong></h4>
		<h4><strong>QUANDO PERCEBE, AS PESSOAS ADORAM FALAR COM VOCÊ, POIS SENTEM SUA ATENÇÃO E SUAS PALAVRAS QUE AGRADAM. E TUDO ISSO VIRA NATURAL PARA VOCÊ.</strong></h4>
            <h4 class="">O Marco Aurélio tinha 26 anos, área financeira de uma empresa de médio porte, e a dificuldade de comunicação não era só no trabalho com os superiores – ou quando precisava apresentar algo internamente. Mas ele tinha dificuldade também na vida pessoal.</h4>
			<h4 class="">No Happy Hour da empresa ou quando saía com os amigos, sempre ficava introvertido, tímido. E via muita coisa acontecer e acabava não participando.</h4>
			<h4 class="">O curso ajudou de uma maneira que nem ele mesmo acreditava. </h4>
            <h3>“Eu sempre achei que ser tímido e introvertido demais era meu jeito e que seria impossível mudar. Na verdade, eu tinha até medo de mudar”.</h3>
			<h4 class="">Eu queria muito acabar com a timidez, saber como ser mais extrovertido. E não sabia como! E o curso foi muito além do que eu imaginava.</h4>
			<h4 class="">Entender como o medo funciona na mente das pessoas introvertidas, entender como as outras pessoas reagem quando você fala com elas e, a partir daí, aprender as técnicas de como falar e de que jeito falar, e automaticamente perceber que os outros funcionam com mecanismos que você pode dominar para conseguir a atenção deles, e que isso funciona mesmo, foi libertador!</h4>
			<h4><strong>A primeira vez é até assustador! Você usa as técnicas para acabar com a timidez e falar em público, mas... dá o mesmo frio na barriga, mas só por dentro. Por fora, a postura é totalmente diferente. E aí, como num passe de mágica, é incrível. Você vê as reações das pessoas e elas agem normalmente. E acontece tudo de uma forma natural! E aí, vem um alívio imenso, gigante! E você respira fundo e pensa: vou fazer isso de novo!</strong></h4>
			<h3 class="courseColor">A recompensa e a felicidade são indescritíveis! E todo mundo pode!</h3>
			<div style="text-align:center">
<div class="col-3" style="display:inline; padding:15px;"><div class="local-video-container">
				<div class="background-image-holder" style="background: url(&quot;https://methodus.com.br/wp-content/uploads/2020/08/curso-de-oratoria-lp-2.png&quot;); "> 
				
            </div>
            <video controls="">
               <source src="https://methodus.com.br/wp-content/uploads/2021/05/matheus.mp4" type="video/mp4">                                
              </video> 
              <div class="play-button" style="opacity: 0;"></div>
            </div></div>
</div>
</div>
</div>
</div>
</div>
</section>

<!-- O QUE O CURSO PODE FAZER POR VOCÊ -->
<section id="secWhyMethodus">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
	  <br>
	  <h2 class="courseColor">O Curso de Oratória Online “Como Vencer a Timidez e a Introversão” é o Guia Definitivo para Combater: </h2>
        <div class="methodusContent">
			
			<ul class="h4 p3">
			<li> ● Ansiedade </li>
			<li> ● Medo de comprometer a imagem</li>
			<li> ● Mãos suadas</li>
			<li> ● Insônia na véspera de apresentações</li>
			<li> ● Frio na barriga</li>
			<li> ● Suar frio</li>
			<li> ● Adrenalina a mil</li>
			<li> ● Desconforto</li>
			<li> ● Movimentos travados</li>
			<li> ● Voz embargada</li>
			<li> ● Dar branco na hora de falar</li>
			<li> ● Não saber se posicionar nas reuniões online</li>
		</ul>
		<h4><br></h4>
		<h3> E você vai aprender também as 4 dicas infalíveis:</h4>
		<h4>Para onde olhar quando fala?</h4>
		<h4>Como soltar o corpo e a voz?</h4>
		<h4>Como evitar dar branco?</h4>
		<h4>Como canalizar a adrenalina e diminuir a tensão?</h4>
		<h4>Essas respostas você terá com muita clareza e conseguirá entender QUE VOCÊ PODE!</h4>
		<h4>E muito mais fácil do que imagina!</h4>
		<h4>Sem sofrer meses e anos com terapia.</h4>
		<h4>Sem se sentir desconfortável demais e querer desistir.</h4>
		<h4>É feito para você!</h4>
		<h3>Vantagens de Fazer nosso Curso Online:</h3>
		<ul class="h3">
			<li> ● Não pega trânsito </li>
			<br>
			<li> ● Faz no seu tempo</li>
			<br>
			<li> ● Consegue encaixar na rotina do dia a dia</li>
			<br>
			<li> ● Custo mais baixo de investimento</li>
			<br>
			
		</ul>
		<img class="aligncenter wp-image-6362" src="https://methodus.com.br/wp-content/uploads/2020/08/curso-de-oratoria-1-scaled.jpg" alt="" width="auto" height="100%" />
</div>
</div>
</div>
</div>
</section>


<section id="secPrices" class="" style="">
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1 col-sm-12" style="align:center">
        <div class="pricesTop featurePrice">
          <h2 style= "color:#a00000">AGORA RESPONDA:<br> QUANTO VALE ISSO PARA VOCÊ?</h2>
		<ul class="h3">
			<li style="text-align: left"> ● Quanto vale colocar isso no seu currículo? </li>
			<br>
			<li style="text-align: left"> ● Quanto vale ser promovido (quanto você ganhará a mais de salário)?</li>
			<br>
			<li style="text-align: left"> ● Quanto vale conseguir ir bem numa entrevista de emprego?</li>
			<br>
			<li style="text-align: left"> ● Qual o preço de fazer outro curso e não ficar satisfeito (jogar dinheiro fora)?</li>
			<br>
			<li style="text-align: left"> ● Qual o preço de meses de sessões de Coaching ou de Terapia?</li>
			<br>
		</ul>
<h3>Se somasse estes itens, em questão de 1 ano, o que você deixaria de ganhar com uma promoção no emprego ou até o que gastaria em coaching e terapia, passaria tranquilamente de <br><strong style="color: #a00000;">R$ 12 mil reais em um ano.</strong></h3>
<h3>Mas no curso de “Como Vencer a Timidez e a Introversão” da Methodus, você não vai gastar nem mesmo um décimo disso. </h3>
<h3>Sim, faça as contas!</h3>
<h3 style="color: #a00000;"><strong>Não vai gastar nem 10% deste valor!</strong></h3>
<h3>PREÇO SUPER ACESSÍVEL E PARCELADO!</h3>
<a data-toggle="" data-target="#cursoVejaInvestimento" href="https://go.hotmart.com/S52078658M">Clique Aqui agora para vere nosso valor!</a>
</section>
<section id="secPrices" class="courseBackgroundColor">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
		<h3 style="color: #fff;">Sim, pois o curso foi desenvolvido para auxiliar e trazer alívio para o maior número de pessoas possível! </h3>
		<h3 style="color: #fff;">E feito de forma online, economizamos custos e possibilitamos a todos o acesso a conseguir comprar.</h3>
          <h3 style="color: #fff;">Você consegue usando as técnicas na prática, ir muito mais rápido do que se passasse meses ou até anos em sessões de terapia ou coaching.</h3>
          <h3 style="color: #fff;">E são técnicas comprovadas por uma empresa sólida que já está há mais de 20 anos no mercado de ensino e desenvolvimento pessoal.</h3>
          <h3 style="color: #fff;">E agora preparou para você o curso direto e online. Com todas as teorias e técnicas para entender como fazer e saber como fazer.</h3>
		  <br />
          <a data-toggle="" data-target="#cursoVejaInvestimento" href="https://go.hotmart.com/S52078658M">Clique Aqui agora para mudar a sua vida!</a>
        </div>
      </div>
    </div>
 
   
</section>
 <section id="secTestimony">
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1 col-sm-12">
                  
            <div class="row sliderCard">
                <div class="col-md-12">
                    <div class="boxTestimony">
                    <img class="avatarImg" src="https://methodus.com.br/wp-content/uploads/2020/08/curso-de-oratoria-1.png">
                    <h3 class="courseColor" >Leonardo Daniel Pinto </h3>
                    <span>EUROFARMA | Bacharel em Farmácia, Pós em Gestão Estratégica de Pessoas</span>
                    <div class="testimonyText">
                      <svg class="quoteSlickStart" width="39" height="33" viewBox="0 0 39 33" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M9.0535 0.201172L14.3819 4.58474C11.8579 7.31229 10.1753 9.89371 9.33395 12.329C8.49262 14.6669 8.07196 17.0535 8.07196 19.4888L4.70664 17.0048H14.9428V32.2012H0.5V21.5345C0.5 17.5406 1.10763 13.8389 2.32288 10.4295C3.63161 6.92264 5.87515 3.5132 9.0535 0.201172ZM32.6107 0.201172L37.9391 4.58474C35.4151 7.31229 33.7325 9.89371 32.8911 12.329C32.0498 14.6669 31.6292 17.0535 31.6292 19.4888L28.2638 17.0048H38.5V32.2012H24.0572V21.5345C24.0572 17.5406 24.6648 13.8389 25.8801 10.4295C27.1888 6.92264 29.4324 3.5132 32.6107 0.201172Z"/>
                      </svg>
                      <h4></strong><br />
						Aprendi muitas técnicas que posso utilizar no dia a dia e, desde a primeira aula consegui praticar no meu âmbito pessoal e profissional. Ponto muito positivo: desde a primeira aula a interação e participação direto no palco foi um detalhe importante para a minha vinda em todas as aulas. Já comentei com vários colegas e amigos sobre esse treinamento</h4>
                      <svg class="quoteSlickEnd" width="39" height="31" viewBox="0 0 39 31" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M29.9465 0.201172L24.6181 4.31076C27.1421 6.86784 28.8247 9.28793 29.6661 11.571C30.5074 13.7628 30.928 16.0003 30.928 18.2834L34.2934 15.9546H24.0572V30.2012H38.5V20.2012C38.5 16.4569 37.8924 12.9866 36.6771 9.79022C35.3684 6.50255 33.1248 3.3062 29.9465 0.201172ZM6.3893 0.201172L1.06088 4.31076C3.58487 6.86784 5.26752 9.28793 6.10885 11.571C6.95018 13.7628 7.37085 16.0003 7.37085 18.2834L10.7362 15.9546H0.5V30.2012H14.9428V20.2012C14.9428 16.4569 14.3352 12.9866 13.1199 9.79022C11.8112 6.50255 9.56765 3.3062 6.3893 0.201172Z"/>
                      </svg>
                    </div>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="boxTestimony">
                    <img class="avatarImg" src="https://methodus.com.br/wp-content/uploads/2020/08/curso-de-oratoria.png">
                    <h3 class="courseColor">Ana Luiza Campos Silva de Siqueira</h3>
                    <span>Autônoma | Bacharel em Direito</span>
                    <div class="testimonyText">
                      <svg class="quoteSlickStart" width="39" height="33" viewBox="0 0 39 33" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M9.0535 0.201172L14.3819 4.58474C11.8579 7.31229 10.1753 9.89371 9.33395 12.329C8.49262 14.6669 8.07196 17.0535 8.07196 19.4888L4.70664 17.0048H14.9428V32.2012H0.5V21.5345C0.5 17.5406 1.10763 13.8389 2.32288 10.4295C3.63161 6.92264 5.87515 3.5132 9.0535 0.201172ZM32.6107 0.201172L37.9391 4.58474C35.4151 7.31229 33.7325 9.89371 32.8911 12.329C32.0498 14.6669 31.6292 17.0535 31.6292 19.4888L28.2638 17.0048H38.5V32.2012H24.0572V21.5345C24.0572 17.5406 24.6648 13.8389 25.8801 10.4295C27.1888 6.92264 29.4324 3.5132 32.6107 0.201172Z"/>
                      </svg>
                      <h4><strong>Experiência incrível de autoconhecimento!</strong><br />
						Experiência incrível e extremamente inovadora de autocrítica e autoconhecimento. O "olhar para si" jamais será esquecido e será sempre praticado. A rápida passagem por este curso viciante me fez perceber que quase não me conhecia. Estou muito grata por ter aprendido a olhar para dentro e ter captado a minha essência e como aplicá-la da melhor maneira possível no meu cotidiano e vida profissional. O curso de oratória me proporcionou grandes momentos que nunca imaginei possíveis. Gratidão é a palavra.</h4>
                      <svg class="quoteSlickEnd" width="39" height="31" viewBox="0 0 39 31" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M29.9465 0.201172L24.6181 4.31076C27.1421 6.86784 28.8247 9.28793 29.6661 11.571C30.5074 13.7628 30.928 16.0003 30.928 18.2834L34.2934 15.9546H24.0572V30.2012H38.5V20.2012C38.5 16.4569 37.8924 12.9866 36.6771 9.79022C35.3684 6.50255 33.1248 3.3062 29.9465 0.201172ZM6.3893 0.201172L1.06088 4.31076C3.58487 6.86784 5.26752 9.28793 6.10885 11.571C6.95018 13.7628 7.37085 16.0003 7.37085 18.2834L10.7362 15.9546H0.5V30.2012H14.9428V20.2012C14.9428 16.4569 14.3352 12.9866 13.1199 9.79022C11.8112 6.50255 9.56765 3.3062 6.3893 0.201172Z"/>
                      </svg>
                    </div>
                  </div>
                </div>
                          </div>
          
              </div>
    </div>
  </div>
</section>
<section id="secPrices" class="courseBackgroundColor">
  <div class="row">
    <div class="col-md-12">
      <div class="featuredList">
        <h2>E além de tudo isso, 2 Bônus Super Especiais, pensados pela Methodus para você!</h2>
		
		<div class="col-md-6 col-sm-12">
		<div class="boxWorkload">
			<h3 style="color: #a00000;">Bônus 1 • E-book</h3>
			<h5>Apresentações Sem Branco</h5>
			<h5>Aprenda os segredos de como organizar as ideias para que elas nunca mais saiam da sua mente na hora de apresentar. Organização Lógica e Mind Maps, em um guia expresso para você ter o conteúdo todo na mente e nunca mais “dar branco”. </h5>
			<h5></h5>
        </div>
		</div>
		<div class="col-md-6 col-sm-12">
        <div class="boxWorkload">
			<h3 style="color: #a00000;">Bônus 2 • E-book</h3>
			<h5>Manual dos Cacoetes Verbais e Não Verbais.</h5>
			<h5>Sem perceber, todos nós realizamos diversas vezes durante uma reunião ou apresentação ou até mesmo palestra, alguns cacoetes (manias) tanto físicas quanto verbais. Desde mexer nos óculos o tempo inteiro até repetir expressões como “né”, “né”, “né”... Para nós, nem percebemos. Para quem escuta fica um incômodo.</h5>
			<h5>Com este e-book você vai identificar e ver técnicas de como eliminar! </h5>
        </div>
		</div>
		<div class="col-md-12 col-sm-12 d-block">
		<div class="boxWorkload">
			<h3 style="color: #a00000;">GARANTIA 7 DIAS</h3>
			<h5>VOCÊ PODE COMPRAR O CURSO E SE NÃO GOSTAR, DEVOLVEMOS INTEGRALMENTE O VALOR EM 7 DIAS! SEM PERGUNTAS OU MOTIVOS!</h5>
        </div>
		</div>
		<div class="col-12 d-block">
			<h2 style="color:#fff">Não espere Mais! </h2>
              <a data-toggle="" data-target="#cursoVejaInvestimento" href="https://go.hotmart.com/S52078658M">Clique Aqui Agora para Falar sem medo em público! </a>
        </div>
  
              </div>

    </div>
  </div>
</section>




<!-- Modal  veja investimento -->
<div id="cursoVejaInvestimento" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <?php echo do_shortcode( '[contact-form-7 id="6289" title="VEJA INVESTIMENTOS E PRÓXIMOS GRUPOS!"]'); ?>

      </div>
    </div>

  </div>
</div>


  <script type='text/javascript' async src='https://d335luupugsy2.cloudfront.net/js/loader-scripts/67cc1fec-89be-4715-b86e-5eda535a7d57-loader.js'></script><script type='text/javascript' src='https://methodus.com.br/wp-content/themes/maiscode_methodus_novo/js/main.js?ver=1.0.0' id='jquery-js'></script>
<script type='text/javascript' id='contact-form-7-js-extra'>
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/methodus.com.br\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script type='text/javascript' src='https://methodus.com.br/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.3' id='contact-form-7-js'></script>
<script type='text/javascript' src='https://methodus.com.br/wp-includes/js/wp-embed.min.js?ver=5.5.3' id='wp-embed-js'></script>

<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script> 
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>

<script type="text/javascript">
$(document).ready(function(){

  $('.sliderCard').slick({
    dots: false,
    slidesToShow: 1,
    infinite: false,
    autoplay: false,
    fade: true,
    arrows: true,
    prevArrow: "<img class='a-left control-c prev slick-prev' src='https://methodus.com.br/wp-content/themes/maiscode_methodus_novo/img/arrowPrev.png'>",
    nextArrow: "<img class='a-right control-c next slick-next' src='https://methodus.com.br/wp-content/themes/maiscode_methodus_novo/img/arrowNext.png'>",
    responsive: [
      {
        breakpoint: 780,
        settings: {
          slidesToShow: 1,
          arrows: false,
        }
      },        
    ]
  });

  $('.sliderCardDepo').slick({
    dots: false,
    slidesToShow: 1,
    infinite: true,
    autoplay: true,
    fade: true,
    arrows: true,
    prevArrow: "<img class='a-left control-c prev slick-prev depo-prev' src='https://methodus.com.br/wp-content/themes/maiscode_methodus_novo/img/arrowPrev.png'>",
    nextArrow: "<img class='a-right control-c next slick-next depo-next' src='https://methodus.com.br/wp-content/themes/maiscode_methodus_novo/img/arrowNext.png'>",
    responsive: [
      {
        breakpoint: 780,
        settings: {
          slidesToShow: 1,
          arrows: false,
        }
      },        
    ]
  });


 });


</script>

<?php get_footer(); ?>