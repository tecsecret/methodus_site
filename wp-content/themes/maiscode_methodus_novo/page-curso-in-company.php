<?php
/**
* Template Name: Página In-company
*
* @package WordPress
* @author Mais Code Tecnologia
* @since First Version
*/
get_header(); ?>

<section>
    <div class="container">
        <div class="post-title">
            <h1 class="inline-block"><?php the_title(); ?></h1>
        </div>
        <hr>
        <?php the_field('texto_in-company'); ?>
    </div>
</section>

<?php get_footer(); ?>