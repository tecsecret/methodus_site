<?php
/**
* Template Name: Página Matricula
* 
*
* @package WordPress
* @author Mais Code Tecnologia
* @since First Version
*/


get_header();
$ID_cupom = $_POST['ID_cupom'];

$urlWP = get_template_directory_uri();

// URL utilizada para caminhos de arquivos (URLs amigáveis)
$url_site = get_option('link_site'); 
$url_site_wp     =  site_url();
$url_api      =  get_option('link_api');

// URLs amigáveis
$url_amigavel_on  = 1;
$url_amigavel    =  get_option('link_site_amigavel');


// $ID = formataVar( 'ID', 'GET' );
// $ID = 10;
$ID = $_POST["id_curso"];

if ($ID == "") {
  $ID = formataVar( 'ID', 'GET' );
}else{
  $_POST["id_curso"];
}



// Dados do curso
$xml_curso  = executaPagina( 'api/', array( 'a'=>'cursos', 'metodo'=>'ver', 'ID'=>$ID ) );
$curso      = lerXML( $xml_curso );
    
if ( $curso->erro == 0 ){
    
    $curso              = $curso->curso;
    $titulo_curso       = $curso->titulo;
    $url_curso          = $curso->url;
    $distancia_curso    = $curso->distancia;
    $banner_curso       = $curso->banner;
    $resumo_curso       = $curso->resumo;
    $descricao_curso    = $curso->descricao;
    $metatags_curso     = $curso->metatags;
    $variaveis_curso    = $curso->variaveis;
    $planos_curso       = $curso->planos;
    $turmas_curso       = $curso->turmas->turma;
    $perfis_curso       = $curso->perfis->perfil;
    $curso_tema         = $curso->tema;
    
    if ( isset( $variaveis_curso->classe ) ){
        $classe_curso = $variaveis_curso->classe;
    }else{
        $classe_curso = 'vermelho'; 
    }
    
    if ( isset( $curso->fotos ) ){
        $fotos_curso = $curso->fotos->foto;
    }
    
    if ( isset( $curso->planos->plano ) == false ){
        exit("Erro ao acessar planos do curso");        
    }else{
        
        $plano_fisico = $curso->planos->xpath("plano[@empresarial='0']");

        if ( count( $plano_fisico ) >= 1 ){
            $plano_fisico = $plano_fisico[0];   
        }else{
            unset($plano_fisico);   
        }
        
        $plano_juridico = $curso->planos->xpath("plano[@empresarial='1']");
        if ( count( $plano_juridico ) >= 1 ){
            $plano_juridico = $plano_juridico[0];   
        }else{
            unset($plano_juridico); 
        }
        
        if ( isset( $plano_fisico ) == false ){
            exit("O Curso não tem planos para pessoa física");      
        }
            
    }
    
}else{
    exit("Erro ao acessar os dados do curso");      
}

?>

<div class="main-container">
    <section class="page-title page-title-4 image-bg overlay parallax">
      <div class="background-image-holder"> <img alt="Background Image" class="background-image" src="<?php echo $url_api;  echo $banner_curso; ?>"> </div>
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <h1 class="titulo-curso"><?php echo $titulo_curso; ?></h1>
          </div>
        </div>
      </div>
    </section>
    
 <style type="text/css">
  .descricao-matricula{
    background-color: #f53b57;

  }

  .descricao-matricula h3{
    font-size: 18px;
    color: #fff;
  }

  .horario_mentoria strong{
    display: block;
    color: red;
    margin-top: 20px;
  }


  .horario_mentoria b{
    color: #000;
  }
  .cupomField button{
    border: none;
    padding: 5px 25px;
    margin-top: 5px;
  }
  .cupomField input{
    margin-bottom: 0px;
    text-transform: uppercase;
  }
  .cupomField::-webkit-input-placeholder {
      text-transform: none;
  }
  .cupomField:-moz-placeholder {
      text-transform: none;
  }
  .cupomField::-moz-placeholder {
      text-transform: none;
  }
  .cupomField:-ms-input-placeholder {
      text-transform: none;
  }
  .cupomField p {
    margin: 0px;
    font-weight: 700;
    font-size: 12px;
  }
  .cupomField .valid {
    color: green;
  }
  .cupomField .invalid {
    color: #a00000;
  }
</style>

<?php if ($curso_tema == 6): ?>
   <section class="descricao-matricula">
        
        <div class="container">
          <h3> 
            <?php
             if($turmas_curso === NULL ){
                    
                    //Se  não tiver turma não faz nada

                }else if ( count( $turmas_curso ) > 0 ){
                      foreach( $turmas_curso as $turma ){
                          echo    $turma->descricao;
                      }
                  }                  

              ?>
              
            </h3>

        </div>

  </section>
<?php endif ?>

    <section>
        <div class="container investimento_<?php echo $classe_curso; ?>">
           <div class="row mb0">
              <div class="col-sm-6">
                 <h2 class="number"><?php the_field('titulo_investimentoTexto'); ?></h2>
                 <?php
                 // Gambearra para Black Friday 2018
                 /*
                 $gambi_desconto_prazo = 20;
                 $gambi_desconto_vista = 30;

                 if ( $ID == 4 ){
                    $gambi_valor_cheio = 1790;
                 }else if ( $ID == 5 ){
                    $gambi_valor_cheio = 2590;
                 }else if ( $ID == 6 ){
                   $gambi_valor_cheio = 1590;
                 }else if ( $ID == 7 ){
                   $gambi_valor_cheio = 590;
                 }
                 */

                 //CUPOM DESC

                 // if(isset($_POST['ID_cupom'])) {
                 //    if ( isset( $gambi_valor_cheio ) ){
                 //      $gambi_valor_cheio = ($gambi_valor_cheio - ($gambi_valor_cheio * 0.1));
                 //    } else {
                 //      $plano_fisico[0] = ($plano_fisico[0] - ($plano_fisico[0] * 0.1));
                 //    }
                 // }
                 if ( isset( $gambi_valor_cheio ) ){
                     //echo 'de <b>'.formataMoeda( $gambi_valor_cheio, 'tela' ).'</b> por:<BR />';
                     echo '<b style="font-size:18px;" class="cor">R$ '.formataMoeda( $plano_fisico, 'tela' ).'</b>';
                 }else{
                     echo '<b style="font-size:18px;" class="cor">R$ '.formataMoeda( $plano_fisico, 'tela' ).'</b>';
                 }
                 ?>
                 <BR />
                 
                 <span id="precos_<?php echo $ID; ?>_1">
                     <?php
                     if ( $plano_fisico['desconto1'] > 0 ){
                        if ( isset( $gambi_desconto_vista ) ){
                            $desconto_avista_tela = $gambi_desconto_vista;
                        }else{
                            $desconto_avista_tela = $plano_fisico['desconto1'];
                        }
                         echo 'à vista '.$desconto_avista_tela.'% de desconto: <b>'.formataMoeda( ( $plano_fisico - ($plano_fisico * ( $plano_fisico['desconto1'] / 100 )) ), 'tela' ).'</b><BR />';
                     }
                     
                     if ( $plano_fisico['parcelas'] > 1 ){

                        if ( isset( $gambi_desconto_prazo ) ){
                            echo 'a prazo '.$gambi_desconto_prazo.'%</b> de desconto';
                        }else{
                            echo    'a prazo ';
                        }
                        echo    ', seguem os planos abaixo<BR />
                                <p style="margin-bottom: 12px; font-size:9pt;">';
                        
                        for ( $i=2; $i <= $plano_fisico['parcelas']; $i++ ){
                            echo '- '.$i.' x R$ '.formataMoeda( ($plano_fisico/$i), 'tela' ).' ';
                            if ($i==7){
                                echo '<BR />';  
                            }
                        }
                            
                        echo    '</p>';
                     }
                     ?>
                 </span>
                 
                 <span id="precos_<?php echo $ID; ?>_2" style="display:none;">
                     <?php
                     if ( $plano_fisico['desconto2'] > 0 ){
                         echo 'à vista '.$plano_fisico['desconto2'].'% de desconto: <b>'.formataMoeda( ( $plano_fisico - ($plano_fisico * ( $plano_fisico['desconto2'] / 100 )) ), 'tela' ).'</b><BR />';
                     
                     
                         if ( $plano_fisico['parcelas'] > 1 ){
                            
                            if ( $plano_fisico['desconto3'] > 0 ){
                                echo    'a prazo, '.$plano_fisico['desconto3'].'% de desconto'; 
                            }else{
                                echo    'a prazo, seguem os planos abaixo';
                            }
                            
                            echo    '<BR /><p style="margin-bottom: 12px; font-size:9pt;">';
                            
                            for ( $i=2; $i <= $plano_fisico['parcelas']; $i++ ){
                                if ( $plano_fisico['desconto3'] > 0 ){
                                    echo '- '.$i.' x R$ '.formataMoeda( (($plano_fisico - ($plano_fisico * ( $plano_fisico['desconto3'] / 100 )))/$i), 'tela' ).' ';
                                }else{
                                    echo '- '.$i.' x R$ '.formataMoeda( (($plano_fisico - ($plano_fisico * ( $plano_fisico['desconto2'] / 100 )))/$i), 'tela' ).' ';    
                                }
                                if ($i==7){
                                    echo '<BR />';  
                                }
                            }
                                
                            echo    '</p>';
                         }
                     
                     }
                     ?>
                 </span>
                 
                 <?php
                 if ( $plano_fisico['desconto2'] > 0 ){
                     echo   '<p style="margin-bottom:6px;">Obs.: Para mais de uma pessoa, '.$plano_fisico['desconto2'].'% de desconto à vista';
                     
                     if ( $plano_fisico['desconto3'] > 0 ){
                        echo    ' ou '.$plano_fisico['desconto3'].'% de desconto a prazo';
                     }
                     
                     echo   '</p>
                            <a href="javascript:" style="font-size:11px;" onclick="precos_'.$ID.'_1.style.display=\'block\'; precos_'.$ID.'_2.style.display=\'none\'">Investimento para 1 pessoa</a> | 
                            <a href="javascript:" style="font-size:11px;" onclick="precos_'.$ID.'_1.style.display=\'none\'; precos_'.$ID.'_2.style.display=\'block\'">Investimento para 2 ou mais pessoas</a>';
                 }
                 ?>
                <!-- CUPOM DESC -->
                 <?php /*<form name="checkCupom" id="checkCupom" action="" method="post">
                  <div class="cupomField">
                    <label>Cupom</label>
                    <input type="text" name="ID_cupom" id="cupomInput" placeholder="Insira seu cupom de desconto aqui">
                    <div id="cupomValidation">
                      <?php if(isset($_POST['ID_cupom'])) { ?>
                        <p class="valid">Cupom válido!</p>
                      <?php } ?>
                    </div>
                    <button type="submit" class="vermelho" id="cupomValidator">Validar cupom</button>
                  </div>
                  <input type="hidden" name="id_curso" value="<?php echo $ID; ?>" />
                </form>*/ ?>
              </div>
              
              <script src="<?php echo $urlWP; ?>/matricula/datas.js"></script>
              <form name="inscricao" id="inscricao" action="<?php echo $url_site_wp; ?>/matricula/cadastro" method="post" onsubmit="return checaData(this);">
              <input type="hidden" name="ID" value="<?php echo $ID ?>" />
              <input type="hidden" name="ID_plano" value="" lang="Plano" />
              <input type="hidden" name="participantes" value="0" lang="Participantes" />
              <div id="hiddenCoupon">
                
              </div>

              
              <div class="col-sm-3">
                <?php
                // Curso presencial
                if ( $distancia_curso == 0 ){
                    if ($curso_tema == 6){
                    echo '<h2 class="number">Inscreva-se</h2>
                         <b class="cor">Dias e Horários Disponíveis para Mentorias</b>
                         <table border="0" cellpadding="1" cellspacing="0" width="100%">';

                if($turmas_curso === NULL ){
                    
                    //Se  não tiver turma não faz nada

                }else if ( count( $turmas_curso ) > 0 ){
                        foreach( $turmas_curso as $turma ){
                 

                            echo    '<TR>
                                       <TD style="display: none">

                                            <label class="radio"><input type="radio" name="ID_data" value="'.$turma['codigo'].'" id="data_'.$turma['codigo'].'" onclick="verDiasTurma(\''.$turma['codigo'].'\')" checked /><span></span></label>
                                       </TD>
                                       <TD class="horario_mentoria">
                                          <label for="data_'.$turma['codigo'].'" style="cursor:pointer;" onclick="verDiasTurma(\''.$turma['codigo'].'\')">
                                          <strong>  Segunda á Sexta </strong>
                                       
                                          <b>manhãs:</b> 8h às 10h / 10:30 às 12:30 <br/>
                                          <b>tardes:</b> 13h às 15h / 15h30 às 17h:30 
                                          </label>
                                       </TD>
                                    </TR>';
                        }
                    }


                    }else{
                    echo '<h2 class="number">Inscreva-se</h2>
                         <b class="cor">Datas dos próximos grupos</b>
                         <table border="0" cellpadding="1" cellspacing="0" width="100%">';

                if($turmas_curso === NULL ){
                    
                    //Se  não tiver turma não faz nada

                }else if ( count( $turmas_curso ) > 0 ){
                        foreach( $turmas_curso as $turma ){
                            
                            echo    '<TR>
                                       <TD>
                                            <label class="radio"><input type="radio" name="ID_data" value="'.$turma['codigo'].'" id="data_'.$turma['codigo'].'" onclick="verDiasTurma(\''.$turma['codigo'].'\')" /><span></span></label>
                                       </TD>
                                       <TD>
                                          <label for="data_'.$turma['codigo'].'" style="cursor:pointer;" onclick="verDiasTurma(\''.$turma['codigo'].'\')">
                                          <b style="font-size:10px;">'.$turma->horario.'</b><BR />
                                          de <b>'.formataData( $turma->inicio, 'tela' ).'</b> à <b>'.formataData( $turma->fim, 'tela' ).'</b>
                                          </label>
                                       </TD>
                                    </TR>';
                        }
                    }

                    }

                    
                    echo '</table>';
                    if($curso_tema==6){
                    echo '      <BR />

                            </div>
                            <div class="col-sm-3">
                                <h4 class="number">Após a confirmação de pagamento você receberá acesso a agenda para selecionar os horários de sua mentoria.</h4>';
                    }else{
                    echo '      <BR />
                            </div>
                            <div class="col-sm-3">
                                <h4 class="number">Veja os dias das aulas</h4>
                                <div class="dias_turma visivel">Escolha uma turma para ver os dias</div>';
                          }      
                if($turmas_curso === NULL ){
                    
                    //Se  não tiver turma não faz nada

                }else if ( count( $turmas_curso ) > 0 ){
                  if($curso_tema != 6){
                        foreach( $turmas_curso as $turma ){
                            echo    '<div class="dias_turma" id="dias_turma_'.$turma['codigo'].'">'.$turma->descricao.'</div>';
                        }
                    }
                  }
                    
                
                
                // Curso à distância
                }else{
                    
                    if ( isset($turmas_curso) && count( $turmas_curso ) > 0 ){
                        $turma = $turmas_curso[0];
                        echo    '<input type="hidden" name="ID_data" value="'.$turma['codigo'].'" />
                                <input type="hidden" name="curso_distancia" value="1" />';  
                    }else{
                        echo    '<input type="hidden" name="ID_data" value="0" />
                                <input type="hidden" name="curso_distancia" value="1" />';      
                    }
                    
                }
                ?>
                <BR />
              </div>
           </div>
        </div>
        <div class="container investimento_<?php echo $classe_curso; ?>">
           <div class="row mb0">
           
              <div class="col-sm-6">
                <p class="mb0"><b class="cor"><?php the_field('titulo_in_company'); ?></b></p>
                 <p class="mb0"><a href="<?php echo $url_site; ?>site/incompany/?cod=ic&curso=<?php echo $ID ?>"><?php the_field('chamada_proposta_in_company'); ?></a></p>
                 <br>
              </div>
              
              
              <?php
              if ( $distancia_curso == 0 ){
              ?>
              <div class="col-sm-3">
                <p class="mb0"><b class="cor"><?php the_field('titulo_pessoa_fisica'); ?></b></p><br>
                 <p class="mb0"><select id="participantes_fisica" style="border:solid 1px #666; width: 60px">
                    <?php
                    for ($i=1; $i<=$plano_fisico['participantes']; $i++){
                        echo '<option value="'.$i.'">'.$i.'</option>';  
                    }
                    ?>
                 </select>
                 pessoa(s)</p>
                 <input type="button" class="<?php echo $classe_curso; ?>" value="Inscrever-se" style="width:120px; border:solid 1px #666;" onclick="inscreverSe('fisica',<?php echo $plano_fisico['codigo'] ?>)" />
              </div>
              <?php
              }else{
              ?>
              <div class="col-sm-3">
                <p class="mb0"><b class="cor"><?php the_field('titulo_curso_a_distancia'); ?></b></p><br>
                 <p class="mb0"><input id="participantes_fisica" type="hidden" value="1" />
                 <input type="button" class="<?php echo $classe_curso; ?>" value="Adquirir o curso" style="width:220px; border:solid 1px #666;" onclick="inscreverSe('fisica',<?php echo $plano_fisico['codigo'] ?>)" />
              </div>      
              <?php
              }
              ?>
              
              <?php
              if ( isset($plano_juridico) ){
              ?>
                  <div class="col-sm-3">
                     <p class="mb0"><b class="cor"><?php the_field('titulo_empresas'); ?></b></p>
                     <p class="mb0"><?php the_field('legenda_empresas'); ?></p>
                     <p class="mb0"><select id="participantes_juridica" style="border:solid 1px #666; width: 60px">
                        <?php
                        for ($i=1; $i<=$plano_juridico['participantes']; $i++){
                            echo '<option value="'.$i.'">'.$i.'</option>';  
                        }
                        ?>
                     </select>
                     pessoa(s)</p>
                     <input type="button" class="<?php echo $classe_curso; ?>" value="Inscrever-se" style="width:120px; border:solid 1px #666;" onclick="inscreverSe('juridica',<?php echo $plano_juridico['codigo'] ?>)" />
                     
                  </div>
              <?php
              }
              ?>
              
              <input type="submit" value="Enviar" id="enviar_form" style="display:none;" />
              
              </div></div></section>
    
        </form>
    
        <section class="pb64 pb-xs-40 investimento_<?php echo $classe_curso; ?>">
           <div class="container">
              <div class="row">
              <h4 style="text-align: center;"><?php the_field('titulo_formas_de_pagamento'); ?></h4><p style="text-align:center"><?php the_field('sub_titulo_formas_de_pagamento'); ?></p>
                <?php if (have_rows('formas_de_pagamento')): ?>
                  <?php while(have_rows('formas_de_pagamento')): the_row(); ?>
                    <div class="col-sm-3">
                      <div class="feature feature-2 filled text-center">
                        <div class="text-center">
                          <i class="<?php the_sub_field('icone_formas_de_pagamento'); ?> cor" style="background: none; border:0;"></i>
                          <h5 class="uppercase"><?php the_sub_field('titulo_formas_de_pagamento'); ?></h5>
                        </div>
                        <p><?php the_sub_field('texto_formas_de_pagamento'); ?></p>
                      </div>
                    </div>
                  <?php endwhile ?>
                <?php endif ?>
                 
                 
              </div>
           </div>
        </section>
    
    
</div>
<script type="text/javascript">
// $(document).ready(function(){
//     $("#cupomValidator").click(function(){

//         $( "#cupomValidation" ).empty();
//         var value = $("#cupomInput").val();
//         var cupomValue = value.toUpperCase();

//         if(cupomValue === "M10"){
//           var appendCupom = '<input type="hidden" name="ID_cupom" value="'+ cupomValue +'"/>'
//           var appendValidation = '<p class="valid">Cupom válido!</p>'
//           $('#cupomValidation').append(appendValidation);
//           $('#hiddenCoupon').append(appendCupom);
//           location.reload();
//         } else {
//           var appendValidation = '<p class="invalid">Cupom inválido!</p>'
//           $('#cupomValidation').append(appendValidation);
//           return false;
//         }
//     });
// });
</script>

<?php get_footer(); ?>