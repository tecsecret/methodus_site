<?php
/**
* Template Name: Página Teste Leitura
*
* @package WordPress
* @author Mais Code Tecnologia
* @since First Version
*/
?>
   <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Teste</title>
</head>

<body>


<script type="text/javascript">
//#########################################################//
//                         TESTES                          //
//#########################################################//


function iniciar()
{
	document.getElementById( "camada_01" ).style.display='none'; 
	document.getElementById( "camada_02" ).style.display='block'; 

	relogio();
}

relogio_i 	= 0;
fim			= false;

function relogio()
{

	if (fim == false)
	{
	
		div_relogio = document.getElementById( "tempo" );
		div_relogio.innerHTML = "<b>Tempo:</b> "+ relogio_i;
		relogio_i = relogio_i + 1
		setTimeout("relogio();", 1000);
	
	}
	else
	{
		
		tempo_utilizado = relogio_i;
		
	}
	
}


acertos = 0;
function finaliza()
{

	fim = true;

	document.getElementById( "camada_02" ).style.display='none'; 
	document.getElementById( "camada_03" ).style.display='block'; 
	
	document.location="#ancora_questoes";
	
	
}

function finaliza_questoes()
{
	
	for (i=0; i<4; i++)
	{
		if ( questoes.pergunta_1[i].checked == true && questoes.pergunta_1[i].value == "1" )
		{
			acertos = acertos + 1
		}
	}
	
	for (i=0; i<4; i++)
	{
		if ( questoes.pergunta_2[i].checked == true && questoes.pergunta_2[i].value == "1" )
		{
			acertos = acertos + 1
		}
	}
	
	for (i=0; i<4; i++)
	{
		if ( questoes.pergunta_3[i].checked == true && questoes.pergunta_3[i].value == "1" )
		{
			acertos = acertos + 1
		}
	}
	
	for (i=0; i<4; i++)
	{
		if ( questoes.pergunta_4[i].checked == true && questoes.pergunta_4[i].value == "1" )
		{
			acertos = acertos + 1
		}
	}
	
	for (i=0; i<4; i++)
	{
		if ( questoes.pergunta_5[i].checked == true && questoes.pergunta_5[i].value == "1" )
		{
			acertos = acertos + 1
		}
	}
	
	for (i=0; i<4; i++)
	{
		if ( questoes.pergunta_6[i].checked == true && questoes.pergunta_6[i].value == "1" )
		{
			acertos = acertos + 1
		}
	}
	
	for (i=0; i<4; i++)
	{
		if ( questoes.pergunta_7[i].checked == true && questoes.pergunta_7[i].value == "1" )
		{
			acertos = acertos + 1
		}
	}
	
	for (i=0; i<4; i++)
	{
		if ( questoes.pergunta_8[i].checked == true && questoes.pergunta_8[i].value == "1" )
		{
			acertos = acertos + 1
		}
	}
	
	for (i=0; i<4; i++)
	{
		if ( questoes.pergunta_9[i].checked == true && questoes.pergunta_9[i].value == "1" )
		{
			acertos = acertos + 1
		}
	}
	
	for (i=0; i<4; i++)
	{
		if ( questoes.pergunta_10[i].checked == true && questoes.pergunta_10[i].value == "1" )
		{
			acertos = acertos + 1
		}
	}
	
	document.getElementById( "resultado_1" ).innerHTML = "Tempo de Leitura: <b>"+ formata_tempo(tempo_utilizado) +" seg</b>";
	
	PLM = Math.round( (977 * 60) / (tempo_utilizado) );
	document.getElementById( "resultado_2" ).innerHTML = "P.L.M. <font style='font-size:9px'>(Palavras lidas por Min.)</font>: <b>"+ PLM +"</b>";
	
	PC = Math.round( (acertos * 100) / 10 );
	document.getElementById( "resultado_3" ).innerHTML = "P.C. <font style='font-size:9px'>(Percentual de Compreensão)</font>: <b>"+ PC +" %</b>";
	
	PCM = Math.round( (PLM * PC) / 100 );
	document.getElementById( "resultado_4" ).innerHTML = "P.C.M. <font style='font-size:9px'>(Palavras compreendidas por Min.)</font>: <b>"+ PCM +"</b>";
	
	livro = Math.round( ( 300 / ( PLM / 350 ) ) / 60 );
	document.getElementById( "resultado_5" ).innerHTML = "Livro de 300 páginas em <b>"+ livro +"</b> horas";
	
	document.getElementById( "camada_03" ).style.display='none'; 
	document.getElementById( "camada_04" ).style.display='block'; 

}

// função para calcular segundos em min e seg
function formata_tempo(total)
{
	hora 			= parseInt(total/3600);
	minuto 			= parseInt((total - (hora*3600))/60);
	segundo 		= total - ((hora*3600)+(minuto*60));
	
	if (hora == 0)
		hora = "";
	else
		hora = hora+"h ";
		
	if (segundo >= 0 && segundo <=9)
		segundo = "0"+segundo;
		
	if (minuto >= 0 && minuto <=9)
		minuto = ""+minuto+" min ";
	else
		minuto = minuto+" min ";
		
	CALCULA_TEMPO 	= hora+""+minuto+""+segundo;	
		
	return CALCULA_TEMPO
}

	
</script>



<table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">

<TR id="camada_01" style="display:block">
	<TD>
    	Clique em iniciar para começar a contagem do tempo para ler o texto. Ao terminar de ler, clique em "finalizar leitura" para terminar a contagem do tempo, a seguir, preencha as questões corretamente para ver os resultados.
    	<BR /><BR />
    	<center>
        	<input type="button" value="Iniciar" class="input_botao_01" style="width:250px;" onclick="iniciar();" />
    	</center>
    </TD>
</TR>

<TR id="camada_02" style="display:none;">
	<TD>
    
    	<div id="tempo" style="float:right; padding:10px; background-color:#CCCCCC; position:fixed; top:2px; right:2px;"><b>Tempo:</b> 60</div>

        <b>Na Fronteira da Mente</b>
        <BR /><BR />
        Nunca as pesquisas avançaram tanto na compreensão do cérebro humano como nesta virada de milênio. Veja, por exemplo, como os cientistas estão trabalhando em conjunto para entender o funcionamento da memória.
        <BR /><BR />
        O ano final do século XX trará um marco muito importante na compreensão do cérebro humano, ainda considerando um dos maiores enigmas da ciência. É o ano em que várias linhas de pesquisa independentes convergirão em torno de um programa realmente unificado de estudo daquela que já foi descrita como a estrutura mais complexa do universo conhecido. O resultado final deverá ser uma revelação da natureza mais profunda da psique humana.
        <BR /><BR />
        O filósofo seiscentista René Descartes pôs a bola em movimento quando resumiu sua própria consciência com a frase: "Penso, logo existo". De lá para cá, o estudo do que ocorre em nossas cabeças tem sido personagem de uma história cheia de altos e baixos, dominada pelas escolas de psicologia do início do século XX e pelas abordagens em grande medida desacreditadas de Sigmund Freud e Carl Jung. Mas, nos últimos tempos, especialmente na década de 90 (a chamada "Década do Cérebro"), as pesquisas envolvendo o cérebro humano vêm passando por uma  transformação radical. 
        <BR /><BR />
        Qualquer discussão sobre o cérebro precisa partir de alguns fatos relativos à sua fenomenal complexidade. O cérebro pesa entre 1 e 2 quilos - embora tamanho não guarde nenhuma relação com inteligência - e é composto por cerca de 100 bilhões de células nervosas, ou neurônios. Cada uma dessas células eletricamente excitáveis se liga a até 10.000 outras por meio de uma série de conexões chamadas sinapses. Cada sinapse pode recorrer a dúzias de mensageiros químicos, chamados neurotransmissores, para enviar um impulso mervoso de uma célula a outra. Se você começasse a contar todas as conexões presentes apenas no córtex cerebral (a camada externa do cérebro, que é altamente desenvolvida nos seres humanso), levaria 32 milhões de anos, contando uma sinapse por segundo. Outra maneira de visualizar o cérebro é olhar para o céu numa noite estrelada. Você possui mais conexões entre seus dois ouvidos do que as estrelas visíveis, mesmo com o uso dos telescópios mais potentes. 
        <BR /><BR />
        Em 1999, diferentes ramos de pesquisas cerebrais trabalharam em conjunto. Tome-se o caso da memória, uma das funções cerebrais menos compreendidas, mas cuja importância é crucial. Como se explica o fato de que conseguimos nos lembrar de eventos da nossa infância, mas esquecemos onde deixamos a chave do carro? Por que nos lembramos de coisas que aconteceram anos atrás, se, de lá para cá, todas as substâncias químicas e os neurotransmissores presentes no cérebro já devem ter sido substituídos várias vezes? A deterioração de nossas memórias à medida que envelhecemos é um processo inevitável? Se perdermos nossa memória, perderemos, literalmente, nossa identidade. Mais pessoas sofrerão perda grave de memória à medida que aumenta, numa população em processo de envelhecimento, a incidência de deterioração cerebral associada a demências senis, como o mal de Alzheimer. 
        <BR /><BR />
        Cientistas estão pesquisando a memória de várias maneiras. Uma das abordagens utiliza a última geração de aparelhos capazes de gerar imagens digitalizadas do cérebro em funcionamento, em tempo real e com grande nível de detalhamento. Esse método é capaz de mostrar quais partes do cérebro estão envolvidas no armazenamento de memórias de curto ou longo prazo. Uma equipe de cientistas americanos anunciou em 1998 que já identificou a parte do cérebro responsável por decidir se uma experiência deve ser codificada como memória, tarefa desempenhada em frações de segundo. "É a primeira vez que se consegue olhar dentro do cérebro de uma pessoa e prever se ela vai ou não se lembrar do que está vivendo naquele momento", disse o doutor Randy Buckner, da Universidade Washington, em St. Louis.
        <BR /><BR />
        Cientistas britânicos estão estudando a formação das memórias em nível molecular. Eles prevêem que, nos próximos anos, pode ser possível incrementar o processo de formação de memórias criando "drogas inteligentes" para ajudar as vítimas da deterioração da memória. O professor Steven Rose, diretor do grupo de pesquisas cerebrais e comportamentais da Open Universitiy, em Milton Keynes, pode estar à beira de compreender com precisão como as memórias se formam. Seu grupo descobriu que moléculas especiais mantêm as sinapses unidas, de uma maneira não muito diferente daquela como funciona uma tira de velcro. "Quando são formadas memórias novas e duradouras, as moléculas de 'velcro' se descolam brevemente para que as junções possam se separar; novas moléculas de 'velcro' são formadas e as religam num desenho novo, que forma um  traço de memória e que pode ser reativada novamente quando nos lembramos do que aprendemos", diz Rose. 
        <BR /><BR />
        Nos próximos anos, cientistas como Buckner e Rose, que sempre operaram relativamente isolados uns dos outros, poderão trabalhar em cooperação mais estreita. Em breve testemunharemos os primeiros frutos dessa fertilização cruzada, com aparelhos de digitalização de imagens crebrais cada vez mais sofisticados mostrando a formação da memória enquanto ela ocorre, capacitando os cientistas moleculares a encaixar seus resultados experimentais nas imagens formadas pelos aparelhos no momento em que um cérebro humano vivo captura uma memória. 
        <BR /><BR />
        Esses trabalhos indicam que funções cerebrais como a memória não são atributos amorfos do intelecto humano, como se acreditava anteriormente. Elas se tornam compreensives, algo físico e, em última instância, algo que pode ser manipulado em benefício humano. O novo alvo será a própria consciência, mas ninguém nutre ilusões quanto ao grau de dificuldade implícito em seu estudo. Apesar disso, algumas das mais brilhantes cabeças da comunidade científica internacional já estão começando a falar seriamente sobre a perspectiva de que algum dia seja possível abordar a consciência da mesma maneira que agora se começa a compreender a memória. Quando esse dia chegar, talvez já nos primeiros anos do século XXI, ele marcará um avanço verdadeiramente histórico no estudo da natureza humana.
        <BR /><BR />
        Texto extraído da Revista Exame<BR />
        Artigo de Steve Cannor, editor de ciências do jornal "The Independent", de Londres. 

        
        <BR /><BR />
        <center><input id="botao_finaliza" type="button" value="Finalizar leitura" class="input_botao_01" onclick="finaliza()" /></center>
        
        
	</TD>
</TR>
<TR id="camada_03" style="display:none;">
	<form name="questoes">
	<TD>
    	
        <a name="ancora_questoes"></a>
        Responda o questionário:
        <BR /><BR />
        
        <b>1-) O cérebro é composto por quantas células nervosas?</b>
        <BR /><BR />
        a) <input type="radio" name="pergunta_1" value="1" />  100 bilhões<BR />
        b) <input type="radio" name="pergunta_1" value="0" />  100 trilhões<BR />
        c) <input type="radio" name="pergunta_1" value="0" />  150 bilhões<BR />
        d) <input type="radio" name="pergunta_1" value="0" />  200 trilhões<BR />
        <BR /><BR />
        
        <b>2-) Qual o nome dos mensageiros químicos do cérebro?</b>
        <BR /><BR />
        a) <input type="radio" name="pergunta_2" value="0" />  neuromoleculares<BR />
        b) <input type="radio" name="pergunta_2" value="0" />  transcelulares<BR />
        c) <input type="radio" name="pergunta_2" value="0" />  transcodificadores<BR />
        d) <input type="radio" name="pergunta_2" value="1" />  neurotransmissores<BR />
        <BR /><BR />
        
        <b>3-) Apenas para contar as conexõex no córtex cerebral levaríamos quantos anos contando uma sinapse por segundo?</b>
        <BR /><BR />
        a) <input type="radio" name="pergunta_3" value="0" />  62 mil anos<BR />
        b) <input type="radio" name="pergunta_3" value="0" />  104 mil anos<BR />
        c) <input type="radio" name="pergunta_3" value="1" />  32 milhões de anos<BR />
        d) <input type="radio" name="pergunta_3" value="0" />  12 trilhões de anos<BR />
        <BR /><BR />
        
        <b>4-) As "drogas inteligentes" atuarão para ajudar os pacientes que apresentam:</b>
        <BR /><BR />
        a) <input type="radio" name="pergunta_4" value="0" />  falta de concentração<BR />
        b) <input type="radio" name="pergunta_4" value="1" />  deteriorização da memória<BR />
        c) <input type="radio" name="pergunta_4" value="0" />  insanidade mental<BR />
        d) <input type="radio" name="pergunta_4" value="0" />  atenção<BR />
        <BR /><BR />
        
        <b>5-) Com recursos de alta tecnologia os cientistas estão estudando a formação da memória em nível:</b>
        <BR /><BR />
        a) <input type="radio" name="pergunta_5" value="0" />  celular<BR />
        b) <input type="radio" name="pergunta_5" value="1" />  molecular<BR />
        c) <input type="radio" name="pergunta_5" value="0" />  neuronal<BR />
        d) <input type="radio" name="pergunta_5" value="0" />  localizacional<BR />
        <BR /><BR />
        
        <b>6-) O cérebro pesa entre:</b>
        <BR /><BR />
        a) <input type="radio" name="pergunta_6" value="0" />  3 a 4 kg<BR />
        b) <input type="radio" name="pergunta_6" value="0" />  3,5 a 4 kg<BR />
        c) <input type="radio" name="pergunta_6" value="0" />  2 a 4 kg<BR />
        d) <input type="radio" name="pergunta_6" value="1" />  1 a 2 kg<BR />
        <BR /><BR />
        
        <b>7-) As funções cerebrais como a memória não são atributos:</b>
        <BR /><BR />
        a) <input type="radio" name="pergunta_7" value="0" />  dinâmicos<BR />
        b) <input type="radio" name="pergunta_7" value="0" />  estruturais<BR />
        c) <input type="radio" name="pergunta_7" value="1" />  amorfos<BR />
        d) <input type="radio" name="pergunta_7" value="0" />  conectados<BR />
        <BR /><BR />
        
        <b>8-) No final do XX qual era a estrutura mais complexa do universo conhecido?</b>
        <BR /><BR />
        a) <input type="radio" name="pergunta_8" value="0" />  mente humana<BR />
        b) <input type="radio" name="pergunta_8" value="1" />  cérebro<BR />
        c) <input type="radio" name="pergunta_8" value="0" />  galáxias<BR />
        d) <input type="radio" name="pergunta_8" value="0" />  inteligência humana<BR />
        <BR /><BR />
        
        <b>9-) Qual o nome do autor deste artigo?</b>
        <BR /><BR />
        a) <input type="radio" name="pergunta_9" value="1" />  Steve Connor<BR />
        b) <input type="radio" name="pergunta_9" value="0" />  Stephen Rose<BR />
        c) <input type="radio" name="pergunta_9" value="0" />  Daniel Goleman<BR />
        d) <input type="radio" name="pergunta_9" value="0" />  Randy Buckner<BR />
        <BR /><BR />
        
        <b>10-) Qual o nome da revista que consta esta matéria?</b>
        <BR /><BR />
        a) <input type="radio" name="pergunta_10" value="0" />  Veja<BR />
        b) <input type="radio" name="pergunta_10" value="0" />  Isto É<BR />
        c) <input type="radio" name="pergunta_10" value="0" />  Você S.A.<BR />
        d) <input type="radio" name="pergunta_10" value="1" />  Exame<BR />

        <BR /><BR />
        <center><input id="botao_finaliza" type="button" value="Finalizar questionário" class="input_botao_01" onclick="finaliza_questoes()" /></center>
        
	</TD>
    </form>
</TR>  

<TR id="camada_04" style="display:none;">
	<TD>
    	
        <center><b>AVALIAÇÃO DA LEITURA</b></center>
        
        <BR />
        
        <table border="0" cellpadding="5" cellspacing="0" align="center" width="100%">
        	<TR>
            	<TD align="center"><b>Seu Resultado</b></TD>
                <TD align="center" rowspan="6">&nbsp;</TD>
                <TD align="center" bgcolor="#FFFFCC"><b>Leitor Dinâmico</b></TD>
            </TR>
            <TR>
            	<TD><div id="resultado_1">Tempo de Leitura: <b>0</b> segundos</div></TD>
                <TD bgcolor="#FFFFCC">Tempo de Leitura: <b>1 min 40 seg</b></TD>
            </TR>
            <TR>
            	<TD><div id="resultado_2">P.L.M. <font style="font-size:9px">(Palavras lidas por Min.)</font>: <b>0</b></div></TD>
                <TD bgcolor="#FFFFCC">P.L.M.: <b>586</b></TD>
            </TR>
            <TR>
            	<TD><div id="resultado_3">P.C. <font style="font-size:9px">(Percentual de Compreensão)</font>: <b>0 %</b></div></TD>
                <TD bgcolor="#FFFFCC">P.C.: <b>90 %</b></TD>
            </TR>
            <TR>
            	<TD><div id="resultado_4">P.C.M. <font style="font-size:9px">(Palavras compreendidas por Min.)</font>: <b>0</b></div></TD>
                <TD bgcolor="#FFFFCC">P.C.M.: <b>527</b></TD>
            </TR>
            <TR>
            	<TD><div id="resultado_5">Livro de 300 páginas em <b>0</b> horas</div></TD>
                <TD bgcolor="#FFFFCC">Livro de 300 páginas em <b>3</b> horas</TD>
            </TR>
        </table>
        
        
    </TD>
</TR> 

</table>

</body>
</html>
