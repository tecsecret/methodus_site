<?php
/**
* Template Name: Página Depoimentos
*
* @package WordPress
* @author Mais Code Tecnologia
* @since First Version
*/ 
get_header(); 
$url_site = get_option('link_site'); 
$url_api  =  get_option('link_api'); 
$url_amigavel   = "https://api.methodus.com.br/"
// URLs amigáveis

?> 

<script>
    ID_tema         = '<?php echo formataVar( 'ID_tema', 'GET' ) ?>';
    pesquisa        = '';
    paginacao_tipo  = 'depoimentos';
</script>


<script src="<?php bloginfo('template_url'); ?>/js/scripts_site/paginacao_depoimento.js"></script>
<div class="main-container">
    <section>

        
        <div class="container">
            <div class="row">
                <div class="col-md-9 mb-xs-24" id="conteudo_paginado">

                </div>
                <div class="col-md-3 hidden-sm">
                    
                    <div class="widget">
                        <h6 class="title">Depoimentos em:</h6>
                        <hr>
                        
                        <ul class="link-list">
                            <?php
                            // Temas
                            $xml_temas  = executaPagina( 'api/', array( 'a'=>'temas' ) );
                            $temas      = lerXML( $xml_temas );
                                
                            if ( $temas->erro == 0 ){
                                foreach( $temas->temas->tema as $tema ){
                                    echo '<li><a href="?ID_tema='.$tema['codigo'].'">'.$tema.'</a></li>';
                                }
                            }
                            ?>
                        </ul>
                    </div>
                    
                </div>
                
            </div>
        </div>
    
    </section>
</div>


<div class="text-center">
    <ul class="pagination">
        <?php
         get_template_part('theme-settings/paginacao', 'paginacao');
        ?>
    </ul>
</div>


<?php get_footer('paginacao'); ?>