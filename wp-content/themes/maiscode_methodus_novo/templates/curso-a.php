<?php

// URL utilizada para caminhos de arquivos (URLs amigáveis)
$url_site = get_option('link_site'); 
$url_api  =  get_option('link_api'); 
$url_amigavel_wp = home_url(); 
// URLs amigáveis
$url_amigavel_on  = 1;
$url_amigavel   =  get_option('link_site_amigavel');

$pagina_curso = 1;

$urlWP = get_template_directory_uri();
// $urlWP = site_url();
// $urlWP = 'https://www.methodus.com.br/';
  
// Verificar
$ID = get_field('id_do_curso');
$ID_Depoimento_Curso = get_field('id_depoimento_curso');
$ebookWp = get_field('ebook');
$xml_curso  = executaPagina( 'api/', array( 'a'=>'cursos', 'metodo'=>'ver', 'ID'=>$ID ) );
$curso      = lerXML( $xml_curso );
    

// var_dump($curso);
    
if ( $curso->erro == 0 ){
    
    $curso              = $curso->curso;
    $status             = $curso->status;
    $titulo_curso       = $curso->titulo;
    $url_curso          = $curso->url;
    $distancia_curso    = $curso->distancia;
    $banner_curso       = $curso->banner;
    $resumo_curso       = $curso->resumo;
    $descricao_curso    = $curso->descricao;
    $variaveis_curso    = $curso->variaveis; 
    $planos_curso       = $curso->planos;
    $turmas_curso       = $curso->turmas->turma;
    $perfis_curso       = $curso->perfis->perfil;
    $ID_tema            = $curso->tema;
    
    $seo_metatags       = $curso->metatags;
    $seo_title          = $titulo_curso.' - Methodus';
    $seo_imagem         = 'https://'.$_SERVER['HTTP_HOST'].''.$banner_curso;
    
    if ( isset( $variaveis_curso->classe ) ){
        $classe_curso = $variaveis_curso->classe;
    }else{
        $classe_curso = 'vermelho'; 
    }
    
    if ( isset( $curso->fotos ) ){
        $fotos_curso = $curso->fotos->foto;
    }

    if ( $status != 1 ){
        exit("Erro ao acessar os dados do curso 1");
    }
    
}else{
    exit("Erro ao acessar os dados do curso");      
}

?>

<div class="main-container page-curso">

    <section class="page-title page-title-4 overlay parallax">
        <div class="background-image-holder fadeIn" style="transform: translate3d(0px, 95.5px, 0px); background: url('<?php echo $url_api;  echo $banner_curso; ?>'); top: -120px;"> 
            <img alt="Background Image" class="background-image" src="<?php echo $url_api; echo $banner_curso; ?>" style="display: none;"> 
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="titulo-curso">
                        <?php 
                            if ( $distancia_curso != '1' ){ 
                                $str_adicional_presencial = ' - Presencial'; 
                            }else{
                                $str_adicional_presencial = '';
                            }

                            if ( isset( $variaveis_curso->titulo_pagina ) ){
                                echo $variaveis_curso->titulo_pagina; 
                                echo '<span>('.$titulo_curso.''.$str_adicional_presencial.')</span>';
                            }else{
                                echo $titulo_curso; 
                                echo '<span>('.$str_adicional_presencial.')</span>';
                            }
                            
                         ?>
                    </h1>
                </div>
            </div>
        </div>
    </section>


        <?php
    if ( isset( $variaveis_curso->ebook_download ) ){
        $temp_ebook = explode( '|', $variaveis_curso->ebook_download );

        if ( count( $temp_ebook ) >= 4 ){

            $titulo_ebook           = $temp_ebook[0];
            $descricao_ebook        = $temp_ebook[1];
            $destino_ebook          = $temp_ebook[2];
            $identificador_ebook    = $temp_ebook[3];
            $urlWP = get_template_directory_uri();
            ?>


            <section class="bg-primary <?php echo $classe_curso; ?> caixa_ebook ">
                <div class="container-fluid bg_ebook">
                    <div class="container">                       
                 
                        <script src="<?php echo $urlWP; ?>/js/scripts_site/ebook.js?ver=1"></script>
                        <form action="<?php echo $url_api; ?>/api/" method="post" class="form-email formAjax" id="form_ebook" data-validacao="checaEbook" data-resultado="sucessoEbook">
                            <input type="hidden" name="destino" value="<?php echo $ebookWp ?>" />
                            <input type="hidden" name="a" value="aluno" />
                            <input type="hidden" name="metodo" value="cadastro" />
                            <input type="hidden" name="origem" value="Ebook_<?php echo $identificador_ebook; ?>" />
                            <input type="hidden" name="curso" value="<?php echo $ID; ?>" />
                    
                            <div class="row">
                                <div class="col-xs-12 col-md-8">
                                    <img src="<?php echo $urlWP ?>/img/ebook_<?php echo $classe_curso; ?>.png" width="80" />
                                    <div class="chamada-ebook"><?php echo $descricao_ebook ?></div>
                                </div>
                                <div class="col-xs-12 col-md-4">
                                    <h5 class="mb0 inline-block p0-xs">
                                        <?php echo $titulo_ebook ?>
                                    </h5>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <input type="text" class="validate-required cada-input" name="nome" placeholder="Nome" lang="Nome">
                                        </div>
                                        <div class="col-sm-12 form-ebook">
                                            <input type="text" class="validate-required cada-input" name="email" placeholder="E-mail" lang="E-mail">
                                            <?php
                                            $i = 0;
                                            if( have_rows('formulario') ):
                                                while ( have_rows('formulario') ) : the_row(); $i++?>	
                                                    <div class="radio_cursos">	
                                                        <label for="ebook-<?php echo $i; ?>" class="radio">
                                                            <input type="radio" class="validate-required" id="ebook-<?php echo $i; ?>" name="conheceu" value="<?php the_sub_field('opcoes'); ?>" lang="opções">
                                                            <span></span>
                                                        </label>					
                                                        <label for="ebook-<?php echo $i; ?>"><?php the_sub_field('opcoes'); ?></label><br>
                                                    </div>
                                                <?php endwhile;
                                            endif;
                                            ?>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <button class="btn <?php echo $classe_curso; ?>" type="submit">Download</button>
                                        </div>
                                    </div>
                                </div>
                            </div>                            
                        </form>
                    </div>
                </div>
            </section>
        <?php
        }
    }
    ?>


<section class="prox-turma">
    <div class="container">
        <?php if($ID_tema != 6) : ?>
             <h2 class="number">Próximas Turmas</h2>
         <?php endif ?>
        <div class="row">

        <div class="prox-turmas-cards">
        
            <?php

            
            if ( $distancia_curso == '1' ){ 
        
                echo ''.$resumo_curso.'<BR /><BR /><a href="#investir" class="inner-link"><button class="'.$classe_curso.'" type="submit">Ver investimento</button></a>';
                //echo ''.$resumo_curso.'<BR /><BR /><a href="'.$url_site.'/matricula/?ID='.$ID.'" class="inner-link"><button class="'.$classe_curso.'" type="submit">Ver investimento</button></a>';
                // Adquira o curso à distância
                            
            }else{
            	if($ID_tema == 6){

                $titulo_prox_mentoria = get_field('titulo_proximas_mentorias'); 
                $dia_da_semana = get_field('dias_da_semana'); 
                $texto_horario = get_field('horario'); 

                    echo '<div class="col-xs-12 col-md-6 text-center">';
            	       	echo '<h2 class="number">'.$titulo_prox_mentoria.'</h2>';

                    echo '</div>';

                  
                        echo    ' <div class="col-xs-12 col-md-6 cada-turma turma-mentoria"> <p><span class="texto-'.$classe_curso.'"><b>'.$dia_da_semana.' </b></span> <br> <br>
                            '.$texto_horario.'</p>

                                <a href="#investir" class="inner-link">
                                    <button class="'.$classe_curso.' curso_mentoria" type="submit">Ver investimento e calendário de mentorias</button>
                                </a>
                                </div>';
       


            	}else{
                
                
                
                $quantidade_vagas = 15;
               

              // if ( count( $turmas_curso ) > 0 ){
              //       foreach( $turmas_curso as $turma ){
              //           echo    '<p><span class="texto-'.$classe_curso.'"><b>'.formataData( $turma->inicio, 'tela' ).' à '.formataData( $turma->fim, 'tela' ).' </b></span> <br>
              //                   <span class="texto-escuro">('.$turma->horario.')</span></p>';
              //           $quantidade_vagas = $turma->quantidade_vagas;
              //       }
              //   }
                
              if($turmas_curso === NULL ){
                    
                    //Se  não tiver turma não faz nada

                }else if ( count( $turmas_curso ) > 0 ){

            

                  foreach( $turmas_curso as $turma ){
                        echo    ' <div class="col-xs-12 col-md-4 cada-turma"> <p><span class="texto-'.$classe_curso.'"><b>'.formataData( $turma->inicio, 'tela' ).' à '.formataData( $turma->fim, 'tela' ).' </b></span> <br>
                                <span class="texto-escuro">('.$turma->horario.')
                                </span></p>

                                <a href="#investir" class="inner-link">
                                    <button class="'.$classe_curso.'" type="submit">Ver investimento e calendário de aulas</button>
                                </a>
                                </div>';
                        $quantidade_vagas = $turma->quantidade_vagas;
                    }

                }
            
			}
                
  
            }
            
            ?>
            
        </div>

        

        </div>
    </div>
</section>


<section class="depoimento-page-curso">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="number">Depoimentos</h2>
            <?php
            // Listar depoimento
            $xml_depoimentos    = executaPagina( 'api/', array( 'a'=>'depoimentos', 'metodo'=>'listar', 'ID'=>$$ID_Depoimento_Curso  ) );


            // var_dump($xml_depoimentos);
            $array_depoimentos  = lerXML( $xml_depoimentos );
            // var_dump($array_depoimentos);

            if ( $array_depoimentos->erro == 0 ){
                // $urlWP = get_template_directory_uri();

                $depoimentos = $array_depoimentos->depoimentos->depoimento; 

              
                $conta_depoimentos = 0;

                      //get_site_url(null, '/wp-content/themes/maiscode_methodus', 'http');

                foreach( $depoimentos as $depoimento ){
                    $conta_depoimentos++;

                    echo '<div class="curso_depoimento" id="depoimento_'.$conta_depoimentos.'">';
                    
                    if ($url_amigavel_on){ 
                        $link_depoimentos   = $url_amigavel.'/depoimentos.html?ID_tema='.$ID_tema; 
                        // $link_depoimento    = $url_amigavel_wp.'/'.encodarURL( $depoimento->nome ).'/'.$depoimento['codigo'].'-depoimento.html';  
                         $link_depoimento    = $url_amigavel_wp.'/depoimento/'.$depoimento['codigo'];
                    }else{ 
                        $link_depoimentos   = $url_amigavel.'/depoimentos/?ID='.$ID_tema; 
                        $link_depoimento    = $url_amigavel.'/depoimentos/detalhes.php?ID='.$depoimento['codigo'].''; 
                    }
                    
                    echo    '
                            <img class="img-depoimentos-cursos" alt="'.$depoimento->nome.'" src="'.$url_api.''.$depoimento->foto.'">';

                   echo    '<h4> <i>'.$depoimento->nome.', ';
                            
                    if ( $depoimento->formacao != '' )
                        echo    $depoimento->formacao;
                    
                    if ( $depoimento->formacao != '' && $depoimento->empresa != '' )
                        echo    ', ';
                    
                    if ( $depoimento->empresa != '' )
                        echo    $depoimento->empresa;
                  echo '</i></h4>';


                     echo ' <b>'.$depoimento->resumo.'</b>  <p><BR />"'.substr( $depoimento->descricao, 0, 560 ).'"';
                    if ( strlen($depoimento->descricao) > 560 ){

                        echo '<a href="'.$link_depoimento.'">...continuar lendo</a>';
                    }
                    

                    echo    '
                            </p>
                        </div>';
                }

                echo '<script>iniciaDepoimentosCursos();</script>';

              echo '<div class="curso_depoimentos '.$classe_curso.'">
            
                      <div class="botao_proximo '.$classe_curso.'" onclick="depoimentoAlterar(1)"></div>
                      <div class="botao_anterior '.$classe_curso.'" onclick="depoimentoAlterar(-1)"></div>
    
                   </div>';
            }
            ?>
        
        </div>
        

        </div>
    </div>
</section>

<?php 
    if ($classe_curso == 'verde') {
        $bg_color = "linear-gradient(rgba(10,76,34,.98),rgba(10,76,34,.98))";
        
    } elseif ($classe_curso == 'vermelho') {
        $bg_color = "linear-gradient(rgba(160,0,0,.98),rgba(160,0,0,.98))";
    } else {
        $bg_color = "linear-gradient(rgba(255,161,0,.98),rgba(255,161,0,.98))";
    }    
?>

<section class="bg-primary forma-pgto" style="background: <?php echo $bg_color; ?>,url(<?php echo esc_url( get_template_directory_uri() ); ?>/img/section-home-bg.png) repeat-x !important">
    <div class="container">
        <div class="row">
        <div class="col-sm-12 text-center">
            <h3 class="mb0  p32 p0-xs">
                <?php
                if ( $distancia_curso == '1' ){
                    echo 'Quer saber como adquirir o curso à distância?&nbsp;';
                    $texto_botao_investimento = 'Ver Formas de Pagamentos'; //'Realizar inscrição'; //'Ver investimento';
                }else{
                	if($ID_tema==6){
					echo 'Quer saber como fazer uma mentoria?&nbsp; ';
                    $texto_botao_investimento = 'Ver Formas de Pagamentos'; //'Realizar inscrição'; //'Ver investimento e calendário de aulas'; 
                	}else{
                    echo 'Quer saber como fazer parte da próxima turma?&nbsp; ';
                    $texto_botao_investimento = 'Ver Formas de Pagamentos'; //'Realizar inscrição'; //'Ver investimento e calendário de aulas'; 
                	}
                }
                ?>
            </h3>
            <!-- <a class="btn btn-lg btn-white mb8 mt-xs-24 inner-link btn-<?php echo $classe_curso; ?>" href="<?php echo $url_site; ?>/matricula/?ID=<?php echo $ID; ?>"><?php echo $texto_botao_investimento ?></a>  -->
            <div class="btn-pgto">
                    <a class="btn btn-lg btn-white mb8 mt-xs-24 inner-link btn-<?php echo $classe_curso; ?>" href="#investir"><?php echo $texto_botao_investimento ?></a> 
            </div>


        </div>
        </div>
    </div>
</section>

<section class="image-bg overlay parallax pq-fazer-curso">
     <div class="background-image-holder features fadeIn" style="transform: translate3d(0px, 443.156px, 0px); background: url(&quot;<?php echo $url_api;  echo $banner_curso; ?>&quot;);"> <img alt="Background" class="background-image" src="<?php echo $url_api;  echo $banner_curso; ?>" style="display: none;"> </div>


    <div class="container">

    <?php if($ID_tema == 6) : ?>
       
         <div class="row">
            <div class="col-sm-12 text-center mentoria">
                <h2 class="mb16">Por que fazer <?php echo $titulo_curso; ?>?</h2>
                <!-- <p class="lead mb64"> </p> -->
            </div>
         </div>
     <?php endif; ?>
        
        <?php
        if ( isset( $variaveis_curso->motivos ) ){
            echo $variaveis_curso->motivos;
        }
        ?>
        
    </div>
    <div id="lp-curso" class="mb32"></div>
</section>


    <section class="conteudo-curso">
        <div class="container">
            <div class="row">

            <div class="col-sm-6 col-md-7">
                <h2>Como é o <?php echo $titulo_curso; ?> da Methodus</h2>
                <div class="descricao-curso">
                     <?php echo $descricao_curso; ?>
                    
                </div>
                <hr>

                <div class="contato-curso">
                 <?php if( get_field('contato_sobre')) { ?>
                     <strong><?php the_field('contato_sobre'); ?></strong>
                 <?php } ?>
                  
                   <p>

                      <br>
                        <i class="fa fa-phone">&nbsp;</i><?php echo get_option('telefone'); ?>
                        <br>
                        <i class="fa fa-whatsapp" >&nbsp;</i> <?php echo get_option('whatsapp'); ?>
                        <br>
                        <i class="ti-email">&nbsp;</i><?php echo get_option('email'); ?>
                    </p>

                </div>
   
            </div>

                <div class="col-sm-6 col-md-4 col-md-offset-1">



                    <?php
                    // Formulário para ver investimento
                     get_template_part('formulario');

                    ?>
                
<?php  /*
                    <div class="box-contato" id="investir">

                        <h2 class="titulo-box" style="background: <?php the_field('cor_curso'); ?>;"><?php the_field('titulo_curto'); ?></h2>
                        <div class="interno-box">
                        </div>

                        <h4 class="dados-box" style="background: <?php the_field('cor_curso'); ?>;"><strong><?php the_field('texto_preencha_seus_dados'); ?></strong></h4>
                        <div class="interno-box">
                            <script src="<?php bloginfo('template_url'); ?>/js/scripts_site/investimento.js"></script>
                            <form action="<?php echo $url_api; ?>/api/"  method="post" class="form-email" id="investimento" data-validacao="checaCurso" data-resultado="sucessoCurso" data-curso="<?php the_field('id_do_curso'); ?>">
                                <input type="hidden" name="a" value="aluno">
                                <input type="hidden" name="metodo" value="cadastro"> 
                                <input type="hidden" name="origem" value="Investimento">
                                <input type="hidden" name="curso" value="<?php the_field('id_do_curso'); ?>">
                                <input type="text" class="validate-required field-error" name="nome" placeholder="Nome" lang="Nome">
                                <input type="text" class="validate-required validate-email field-error" name="email" placeholder="Email" lang="E-mail">
                                <!-- <p class="pl20"><strong>Quem é você?</strong></p> -->

                                <br>
                                <button type="submit" class="btn_pg_curso" style="background: <?php the_field('cor_curso'); ?>;"><?php the_field('texto_veja_investimento'); ?></button>
                                <!-- <button class="vermelho" type="submit">
                        Realizar Inscrição Online        </button> -->
                            </form>

                            <div class="fb-page fb_iframe_widget" data-href="https://www.facebook.com/cursomethodus/" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-height="720" fb-xfbml-state="rendered" fb-iframe-plugin-query="adapt_container_width=true&amp;app_id=1302272369795939&amp;container_width=340&amp;height=720&amp;hide_cover=false&amp;href=https%3A%2F%2Fwww.facebook.com%2Fcursomethodus%2F&amp;locale=pt_BR&amp;sdk=joey&amp;show_facepile=true&amp;small_header=false&amp;tabs=timeline"><span style="vertical-align: bottom; width: 340px; height: 720px;"><iframe name="f18bd715b60fd8" width="1000px" height="720px" title="fb:page Facebook Social Plugin" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" allow="encrypted-media" src="https://www.facebook.com/v2.11/plugins/page.php?adapt_container_width=true&amp;app_id=1302272369795939&amp;channel=https%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter.php%3Fversion%3D44%23cb%3Df1813178e3d996%26domain%3Dwww.methodus.com.br%26origin%3Dhttps%253A%252F%252Fwww.methodus.com.br%252Ff33a0b0868dbb98%26relation%3Dparent.parent&amp;container_width=340&amp;height=720&amp;hide_cover=false&amp;href=https%3A%2F%2Fwww.facebook.com%2Fcursomethodus%2F&amp;locale=pt_BR&amp;sdk=joey&amp;show_facepile=true&amp;small_header=false&amp;tabs=timeline" style="border: none; visibility: visible; width: 340px; height: 720px;" class=""></iframe></span></div>

                        </div>
                     </div>

                     */?>
                </div>


            </div>
        </div>
    </section>



    <section class="curso-videos" style="padding-bottom: 0">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 mb24">
                    <h4 class=" mb16"><?php the_field('titulo_videos'); ?></h4>
                </div>
            </div>
            <!--end of row-->
            <div class="row">
                <?php if (have_rows('videos_turma')): ?>
                    <?php while (have_rows('videos_turma')) : the_row(); ?>
                        <div class="col-sm-3 text-center">
                            <div class="local-video-container">
                                <div class="background-image-holder" style="background: url(&quot;<?php the_sub_field('imagem_fundo_video'); ?>&quot;); "> <img alt="Background Image" class="background-image" src="<?php the_sub_field('imagem_fundo_video'); ?>" style="display: none;"> </div>
                                
                               <video controls="">
                                  <source src="<?php the_sub_field('link_video_da_ultima_turma'); ?>" type="video/mp4">                                
                                </video> 
                                
                                <div class="play-button" style="opacity: 0;"></div>
                            </div>
                            <!--end of local inline video-->
                        </div>
                    <?php endwhile ?>
                <?php endif ?>
                
                
                <!--end of embed video container-->
            </div>
        </div>
        <!--end of row-->
        <!--end of container-->
    </section>

<?php 
    /* Antiga Sessão de Video e Fotos desativado


    <?php if ($ID == 5) { ?>
        <section style="padding-bottom: 0">
        <div class="container">
            <div class="row">
            <div class="col-sm-12 text-center mb24">
                <h4 class=" mb16">Vídeos e fotos das últimas turmas do <?php echo $titulo_curso; ?></h4>
            </div>
            </div>
            <!--end of row-->
            <div class="row">
            <div class="col-sm-3 text-center">
                <div class="local-video-container">
                <div class="background-image-holder"> <img alt="Background Image" class="background-image" src="<?php echo $url_site; ?>/img/videos/video1.jpg"> </div>
                <video controls>
                    <source src="<?php echo $url_site; ?>/cursos/videos/1-eleuzia.mp4" type="video/mp4">
                </video>
                <div class="play-button"></div>
                </div>
                <!--end of local inline video--> 
            </div>
            <div class="col-sm-3 text-center">
                <div class="local-video-container">
                <div class="background-image-holder"> <img alt="Background Image" class="background-image" src="<?php echo $url_site; ?>/img/videos/video2.jpg"> </div>
                <video controls>
                    <source src="<?php echo $url_site; ?>/cursos/videos/2-vera.mp4" type="video/mp4">
                </video>
                <div class="play-button"></div>
                </div>
                <!--end of local inline video--> 
            </div>
            <div class="col-sm-3 text-center">
                <div class="local-video-container">
                <div class="background-image-holder"> <img alt="Background Image" class="background-image" src="<?php echo $url_site; ?>/img/videos/video3.jpg"> </div>
                <video controls>
                    <source src="<?php echo $url_site; ?>/cursos/videos/3-ricardo.mp4" type="video/mp4">
                </video>
                <div class="play-button"></div>
                </div>
                <!--end of local inline video--> 
            </div>
            <div class="col-sm-3 text-center">
                <div class="local-video-container">
                <div class="background-image-holder"> <img alt="Background Image" class="background-image" src="<?php echo $url_site; ?>/img/videos/video4.jpg"> </div>
                <video controls>
                    <source src="<?php echo $url_site; ?>/cursos/videos/4.mp4" type="video/mp4">
                </video>
                <div class="play-button"></div>
                </div>
                <!--end of local inline video--> 
            </div>
            <!--end of embed video container--> 
            </div>
        </div>
        <!--end of row--> 
        <!--end of container--> 
        </section>
    <?php } ?>

    <?php if (isset($fotos_curso) && 1==2) { // Desativado em 22/11/17 ?>
        <section>
        <div class="container">
            
            <?php if ($ID != 5) { ?>
            <div class="row">
            <div class="col-sm-12 text-center mb24">
                <h4 class="uppercase mb16">Fotos das últimas turmas do <?php echo $titulo_curso; ?></h4>
            </div>
            </div>
            <?php } ?>
            
            <div class="row">
            <div class="col-sm-12">
                <div class="lightbox-grid square-thumbs" data-gallery-title="Galleria" style="margin-bottom: -140px">
                <ul>
                    <?php
                    foreach( $fotos_curso as $foto ){
                        echo '<li> <a href="'. $foto->arquivo .'" data-lightbox="true">
                            <div class="background-image-holder"> <img alt="image" class="background-image" src="'. $foto->arquivo .'" /> </div>
                            </a> </li>';
                    }
                    ?>
                </ul>
                </div>
                <!--end of lightbox gallery--> 
            </div>
            </div>
            <!--end of row--> 
        </div>
        <!--end of container--> 
        </section>
    <?php } ?>



    */
 ?>

    
    <?php /* get_template_part( 'depoimentos/carrossel', 'Depoimentos' );*/ ?>

</div>


<?php get_footer(); ?>