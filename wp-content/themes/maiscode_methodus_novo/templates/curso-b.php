<!-- TOPO -->
<?php $category = get_the_category();
  $id = $category[0]->term_id;
  $parent = $category[0]->parent;

  $curseCat = get_terms( 'category', array(
    'orderby'     => 'name',
    'hide_empty'  => 0,
    'pad_counts'  => true,
    'term_taxonomy_id' => $id
  ) ); 


  //ar_dump($curseCat[0]->slug);
  //var_dump($curseCat);
  // GET THE COLOR FROM ACF
  foreach ( $curseCat as $curse ) :
    $color = get_field('cor_padrao_cat', $curse);
    $gradient = get_field('background_gradient', $curse);
    $gradientB = get_field('background_gradient_2', $curse);
  endforeach; ?>

<style type="text/css">
  .courseColor {
    color: <?php echo $color; ?>;
  }
  .courseBackgroundColor {
    background-color: <?php echo $color; ?>;
  }
  #secStoryModB .storyContent svg path {
    fill: <?php echo $color; ?>;
  }
  .sliderCard svg path {
    fill: <?php echo $color; ?>;
  }
  #secTopModB:before {
    content: '';
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    background: <?php echo $gradient; ?>;
  }
  #secCallBannerModB .bgTransformation:before {
    content: '';
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    background: <?php echo $gradientB; ?>;
  }
  .loadd{
    display: none;
    opacity: .5;
    background: #000;
    width: 100%;
    position: absolute;
    top: 0px;
    height: 100vh;
    z-index: 10000000;
  }
  div.wpcf7 .ajax-loader{
    visibility: visible;
    display: none;
    width: 26px;
    height: 26px;
    border: none;
    padding: 0;
    margin: auto;
    vertical-align: middle;
    background-size: cover;
  }
  .sliderCardDepo .depo-prev
  {
    height: 36px;
    width: 220px;
    z-index: 100000;
    object-fit: contain;
  }
  .sliderCardDepo .depo-next{
    width: 235px;
    height: 36px;
    object-fit: contain;
  }
</style>

<section id="secTopModB" style="background-image: url('<?php the_field('imagem_de_fundo_modb') ?>')">
  <div class="container">
    <div class="row">
      <div class="col-md-7 col-md-offset-5 col-sm-7 col-sm-offset-5 topContent">
        <h2><?php the_field('titulo_modb'); ?></h2>
        <p><?php the_field('subtitulo_modb'); ?></p>
        <img src="<?php the_field('imagem_do_icone'); ?>">
      </div>
    </div>
  </div>
</section>

<!-- VIDEO -->
<?php if(get_field('titulo_videos_modb')){ ?>
<section id="secVideoModB">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h3 class="courseColor"><?php the_field('titulo_videos_modb'); ?></h3>
        <h5 class="courseColor"><?php the_field('subtitulo_videos_modb'); ?></h5>
      </div>
      <?php if (have_rows('videos_modb')): ?>
        <?php $cont = 0; ?>
        <?php while (have_rows('videos_modb')) : the_row(); ?>
          <?php if ($cont == 0){
            $colorLi = '#a00000';
          } else {
            $colorLi = '#0df205';
          }?>
          <div class="col-md-6 col-sm-6 videoContent text-center">
            <h5 class="courseColor"><?php the_sub_field('titulo_videomodb'); ?></h5>

            <div class="local-video-container">
              <div class="background-image-holder" style="background: url(&quot;<?php the_sub_field('imagem_do_video_modb'); ?>&quot;); "> 
                <img alt="Background Image" class="background-image" src="<?php the_sub_field('imagem_do_video_modb'); ?>" style="display: none;"> 
              </div>
              <video controls="">
                <source src="<?php the_sub_field('link_do_video_modb'); ?>" type="video/mp4">                                
              </video> 
              <div class="play-button" style="opacity: 0;"></div>
            </div>
            <ul class="videoDescription">
              <?php if (have_rows('descricoes_modb')): ?>
                <?php while (have_rows('descricoes_modb')) : the_row(); ?>
                  <li style="color: <?php echo $colorLi ?>"><span><?php the_sub_field('descricao_modb'); ?></span></li>
                <?php endwhile;
              endif; ?>
            </ul>
          </div>
        <?php $cont++; endwhile;
      endif; ?>
    </div>
  </div>
</section>
 <?php } ?>

<!-- BANNER CHAMADA -->
<section id="secCallBannerModB" class="courseBackgroundColor">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2><?php the_field('titulo_banner_trans_modb'); ?></h2>
        <?php if (have_rows('lista_tranformacao_modb')): ?>
          <ul>
            <?php while (have_rows('lista_tranformacao_modb')) : the_row(); ?>
              <li>
                <?php the_sub_field('texto_lista_tranformacap'); ?>
              </li>
            <?php endwhile; ?>
          </ul>
        <?php endif; ?>
        <p><?php the_field('texto_destaque_trans'); ?></p>
      </div>
    </div>
  </div>
  <?php if(get_field('texto_do_banner_trans')){ ?>
  <div class="bgTransformation" style="background-image: url('<?php the_field('imagem_do_banner_trans') ?>')">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h2><?php the_field('texto_do_banner_trans') ?></h2>
        </div>
      </div>
    </div>
  </div>
<?php } ?>
</section>

<!-- HISTORIA -->
<section id="secStoryModB">
  <div class="container">
  	<div class="row">
      <div class="col-12">
      	<div class="storyContent">
      	<?php the_field('texto_antes_modulo'); ?>
      </div>
      </div>
  </div>
    <div class="row">
      <div class="col-md-12">
        <h2 class="courseColor">&#x25cf; <?php the_field('titulo_texto_moduloa_modb') ?> &#x25cf;</h2>
        <div class="storyContent">
          <svg class="quoteStart" width="71" height="56" viewBox="0 0 71 56" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M16.2565 0L26.072 7.67124C21.4225 12.4444 18.3229 16.962 16.7731 21.2237C15.2232 25.3151 14.4483 29.4916 14.4483 33.7534L8.24908 29.4064H27.1052V56H0.5V37.3333C0.5 30.344 1.61931 23.8661 3.85793 17.8996C6.26876 11.7626 10.4016 5.79605 16.2565 0ZM59.6513 0L69.4668 7.67124C64.8173 12.4444 61.7177 16.962 60.1679 21.2237C58.6181 25.3151 57.8432 29.4916 57.8432 33.7534L51.6439 29.4064H70.5V56H43.8948V37.3333C43.8948 30.344 45.0141 23.8661 47.2528 17.8996C49.6636 11.7626 53.7964 5.79605 59.6513 0Z"/>
          </svg>
          <?php if (have_rows('texto_do_modulo_modb')): ?>
            <?php while (have_rows('texto_do_modulo_modb')) : the_row(); ?>
              <?php the_sub_field('texto_hist_modb'); ?>

              <?php if ( get_sub_field('adicionar_box_destaque_modb') == 'opt02' ) { ?>
                <div class="boxContrast">
                  <?php if( get_sub_field('complemento_opcional_modb') ): ?>
                    <span class="courseColor"><?php the_sub_field('complemento_opcional_modb'); ?></span>
                  <?php endif; ?>
                  <h2><?php the_sub_field('texto_destaque_hist_modb'); ?></h2>
                </div>
              <?php } ?>
            <?php endwhile; ?>
          <?php endif; ?>
          <svg class="quoteEnd" width="73" height="58" viewBox="0 0 73 58" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M56.2934 0L46.1974 7.94521C50.9797 12.8889 54.1679 17.5677 55.762 21.9817C57.3561 26.2192 58.1531 30.5449 58.1531 34.9589L64.5295 30.4566H45.1347V58H72.5V38.6667C72.5 31.4277 71.3487 24.7184 69.0461 18.5388C66.5664 12.1827 62.3155 6.00305 56.2934 0ZM11.6587 0L1.56272 7.94521C6.34501 12.8889 9.5332 17.5677 11.1273 21.9817C12.7214 26.2192 13.5184 30.5449 13.5184 34.9589L19.8948 30.4566H0.5V58H27.8653V38.6667C27.8653 31.4277 26.714 24.7184 24.4114 18.5388C21.9317 12.1827 17.6808 6.00305 11.6587 0Z"/>
          </svg>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="secReminder">
  <div class="reminderBanner courseBackgroundColor">
    <img class="small" src='<?php echo esc_url( get_template_directory_uri() ); ?>/img/small.png'>
    <div class="">
      <div class="row">
        <div class="col-md-12">
          <h2><?php the_field('titulo_reminder_modb') ?></h2>
          <h3><?php the_field('subtitulo_reminder_modb') ?></h3>
          <a id="reminder" href="<?php the_field('link_cta_reminder_modb'); ?>"><?php the_field('nome_cta_reminder_modb') ?></a>
        </div>
      </div>
    </div>
    <img class="big" src='<?php echo esc_url( get_template_directory_uri() ); ?>/img/big.png'>
  </div>
</section>

<!-- POR QUE A METHODUS -->
<section id="secWhyMethodus">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2 class="courseColor"><?php the_field('titulo_metho_modb') ?></h2>
        <div class="methodusContent">
          <?php if (have_rows('texto_do_modulo_methodus_modb')): ?>
            <?php while (have_rows('texto_do_modulo_methodus_modb')) : the_row(); ?>
              <?php if( get_sub_field('titulo_opcional_methodus_modb') ): ?>
                <h3 class="courseColor">&#x25cf; <?php the_sub_field('titulo_opcional_methodus_modb'); ?> &#x25cf;</h3>
              <?php endif; ?>
                <?php the_sub_field('texto_methodus_modb') ?>
              <?php if ( get_sub_field('imagem_methodus_choice_modb') == 'opt04' ) { ?>
                <div class="boxMethodusImg">
                  <img src="<?php the_sub_field('imagem_methodus_modb'); ?>">
                  <span class="courseColor"><?php the_sub_field('rodape_da_imagem_methodus_modb'); ?></span>
                </div>
              <?php } ?>
            <?php endwhile; ?>
          <?php endif; ?>
        </div>
        <div class="differenceTitle">
          <h2 class="courseColor"><?php the_field('titulo_diferenca_modb') ?></h2>
          <h3 class="courseColor"><?php the_field('frase_citacao_modb') ?></h3>
        </div>
        <div class="differenceContent">
          <?php if (have_rows('texto_diferenca_modb')): ?>
            <?php while (have_rows('texto_diferenca_modb')) : the_row(); ?>
              <?php the_sub_field('texto_dif_modb'); ?>
              <?php if ( get_sub_field('adicionar_lista_choice_modb') == 'opt06' ) { ?>
                <?php if (have_rows('lista_dif_modb')): ?>
                  <ul>
                    <?php while (have_rows('lista_dif_modb')) : the_row(); ?>
                      <li><span><?php the_sub_field('item_da_lista_dif_modb'); ?></span></li>
                    <?php endwhile; ?>
                  </ul>
                <?php endif; ?>
              <?php } ?>
            <?php endwhile; ?>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- VANTAGENS -->
<section id="secAdvantages">
  <div class="bannerSatisfaction courseBackgroundColor">
    <h2><?php the_field('titulo_satisfacao_modb') ?></h2>
    <h3><?php the_field('subtitulo_satisfacao_modb') ?></h3>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <?php if (have_rows('videos_satisfaction_modb')): ?>
          <div class="advantagesVideos">
          <?php while (have_rows('videos_satisfaction_modb')) : the_row(); ?>
            <div class="col-md-4 col-sm-6 text-center">
              <div class="local-video-container">
                <div class="background-image-holder" style="background: url(&quot;<?php the_sub_field('imagem_do_video_satisfaction'); ?>&quot;); "> <img alt="Background Image" class="background-image" src="<?php the_sub_field('imagem_do_video_satisfaction'); ?>" style="display: none;"> 
                </div>
                <video controls="">
                  <source src="<?php the_sub_field('link_do_video_satisfaction'); ?>" type="video/mp4">                                
                </video> 
                <div class="play-button" style="opacity: 0;"></div>
              </div>
            </div>
          <?php endwhile ?>
          </div>
        <?php endif ?>
        <?php if(get_field('texto_destaque_videos_modb')){ ?><span class="courseColor"><?php the_field('texto_destaque_videos_modb') ?></span><?php } ?>
        <?php if(get_field('titulo_vantagens_modb')){ ?><h2 class="courseColor">&#x25cf; <?php the_field('titulo_vantagens_modb') ?> &#x25cf;</h2><?php } ?>
        <div class="advantagesBox">
          <?php if (have_rows('vantagens_modb')): ?>
            <?php while (have_rows('vantagens_modb')) : the_row(); ?>
              <h3 class="courseColor">&#x25cf; <?php the_sub_field('vantagem_satis_modb'); ?></h3>
              <?php if( get_sub_field('complemento_da_vantagem_modb') ): ?>
                <p><?php the_sub_field('complemento_da_vantagem_modb'); ?></p>
              <?php endif; ?>
              <?php if ( get_sub_field('adicionar_texto_separador') == 'opt08' ) { ?>
                <span><?php the_sub_field('texto_separador_modb'); ?></span>
              <?php } ?>
            <?php endwhile; ?>
          <?php endif; ?>
        </div>
      </div>
    </div>

<section id="secTestimony">
    <div class="row">
      <div class="col-md-12">
        <?php if (have_rows('depoimentos_slider_modb_copiar')): ?>
            <div class="row sliderCardDepo">
              <?php while (have_rows('depoimentos_slider_modb_copiar')) : the_row(); ?>
                <div class="col-md-12">
                  <div class="boxTestimony">
                    <img class="avatarImg" src="<?php the_sub_field('foto_depoimento_modb'); ?>">
                    <h3 class="courseColor"><?php the_sub_field('nome_depoimento_modb'); ?></h3>
                    <span><?php the_sub_field('formacao_depoimento_modb'); ?></span>
                    <div class="testimonyText">
                      <svg class="quoteSlickStart" width="39" height="33" viewBox="0 0 39 33" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M9.0535 0.201172L14.3819 4.58474C11.8579 7.31229 10.1753 9.89371 9.33395 12.329C8.49262 14.6669 8.07196 17.0535 8.07196 19.4888L4.70664 17.0048H14.9428V32.2012H0.5V21.5345C0.5 17.5406 1.10763 13.8389 2.32288 10.4295C3.63161 6.92264 5.87515 3.5132 9.0535 0.201172ZM32.6107 0.201172L37.9391 4.58474C35.4151 7.31229 33.7325 9.89371 32.8911 12.329C32.0498 14.6669 31.6292 17.0535 31.6292 19.4888L28.2638 17.0048H38.5V32.2012H24.0572V21.5345C24.0572 17.5406 24.6648 13.8389 25.8801 10.4295C27.1888 6.92264 29.4324 3.5132 32.6107 0.201172Z"/>
                      </svg>
                      <?php the_sub_field('depoimento_modb'); ?>
                      <svg class="quoteSlickEnd" width="39" height="31" viewBox="0 0 39 31" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M29.9465 0.201172L24.6181 4.31076C27.1421 6.86784 28.8247 9.28793 29.6661 11.571C30.5074 13.7628 30.928 16.0003 30.928 18.2834L34.2934 15.9546H24.0572V30.2012H38.5V20.2012C38.5 16.4569 37.8924 12.9866 36.6771 9.79022C35.3684 6.50255 33.1248 3.3062 29.9465 0.201172ZM6.3893 0.201172L1.06088 4.31076C3.58487 6.86784 5.26752 9.28793 6.10885 11.571C6.95018 13.7628 7.37085 16.0003 7.37085 18.2834L10.7362 15.9546H0.5V30.2012H14.9428V20.2012C14.9428 16.4569 14.3352 12.9866 13.1199 9.79022C11.8112 6.50255 9.56765 3.3062 6.3893 0.201172Z"/>
                      </svg>
                    </div>
                  </div>
                </div>
              <?php endwhile; ?>
            </div>

        <?php endif; ?>
      </div>
    </div>
  </section>
  </div>
</section>

<section id="secBannerAdvantages" class="courseBackgroundColor">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2><?php the_field('titulo_do_banner_adv_modb') ?></h2>
        <div class="advantagesBox">
          <?php if (have_rows('vantagens_de_fazer_o_curso_modb')): ?>
            <?php while (have_rows('vantagens_de_fazer_o_curso_modb')) : the_row(); ?>
              <h3><?php the_sub_field('vantagem_do_curso_modb'); ?></h3>
            <?php endwhile; ?>
          <?php endif; ?>
          <a id="advantage" href="<?php the_field('link_cta_adv_modb'); ?>"><?php the_field('nome_cta_adv_modb') ?></a>
        </div>
      </div>
    </div>
  </div>
</section>
<?php if(get_field('titulo_logistica_modb')){ ?>
<section id="secLogistic">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2 class="courseColor"><?php the_field('titulo_logistica_modb') ?></h2>
        <h3 class="courseColor">&#x25cf; <?php the_field('subtitulo_logistica_modb') ?> &#x25cf;</h3>
        <div class="boxLogistic">
          <?php if (have_rows('lista_logistica_modb')): ?>
            <ul>
              <?php while (have_rows('lista_logistica_modb')) : the_row(); ?>
                <li><span><?php the_sub_field('texto_logistica_modb'); ?></span></li>
              <?php endwhile; ?>
            </ul>
          <?php endif; ?>
          <h3 class="courseColor"><?php the_field('texto_investimento_modb') ?></h3>
          <?php if (have_rows('lista_investimento_modb')): ?>
            <div class="boxInvestiment">
              <ul>
                <?php while (have_rows('lista_investimento_modb')) : the_row(); ?>
                  <li><span><?php the_sub_field('texto_investimento_modb'); ?></span></li>
                <?php endwhile; ?>
              </ul>
            </div>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</section>
<?php } ?>
<!-- Modal Cupom -->
<div class="modal fade" id="cupomModal" tabindex="-1" role="dialog" aria-labelledby="cupomModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <?php echo do_shortcode('[contact-form-7 id="6273" title="Formulário cupom de desconto"]') ?>
      </div>
    </div>
  </div>
</div>

<!-- PREÇOS -->
<section id="secPrices" class="courseBackgroundColor" style="background-color: <?php the_field('box_precos_color') ?>">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="pricesTop featurePrice">
          <?php the_field('box_precos') ?>
        </div>
        <a id="price_a" href="<?php the_field('link_cta_price_modb'); ?>"><?php the_field('nome_cta_price_modb') ?></a>
      </div>
    </div>
  </div>
</section>
<!-- proximas turmas -->
<section id="secPrices" class="courseBackgroundColor">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
		<span><?php the_field('texto_chamada_price_modb') ?></span>
        <div class="boxWorkload">
          <p><?php the_field('carga_horaria_price_modb') ?></p>
        </div>
        <h2><?php the_field('titulo_turmas_price_modb') ?></h2>
        <?php if (have_rows('box_de_turmas_price_modb')): ?>
          <?php while (have_rows('box_de_turmas_price_modb')) : the_row(); ?>
            <div class="col-md-6 col-sm-12">
              <div class="classesDate">
                <p><?php the_sub_field('data_price_modb'); ?></p>
                <small><?php the_sub_field('dia_da_semana_horario_price_modb'); ?></small>
              </div>
            </div>
          <?php endwhile; ?>
        <?php endif; ?>

      </div>
    </div>
	<div class="row">
		<div class="col-md-12">
			        <div>
          <a id="price_b" href="<?php the_field('link_cta_2_price_modb'); ?>"><?php the_field('nome_cta_2_price_modb') ?></a>
        </div>
		</div>
	</div>
  </div>
</section>

<section id="secTestimony">
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1 col-sm-12">
        <h2 class="courseColor"><?php the_field('titulo_depoimento_modb') ?></h2>
        <?php if (have_rows('depoimentos_slider_modb')): ?>
          
            <div class="row sliderCard">
              <?php while (have_rows('depoimentos_slider_modb')) : the_row(); ?>
                <div class="col-md-12">
                  <div class="boxTestimony">
                    <img class="avatarImg" src="<?php the_sub_field('foto_depoimento_modb'); ?>">
                    <h3 class="courseColor"><?php the_sub_field('nome_depoimento_modb'); ?></h3>
                    <span><?php the_sub_field('formacao_depoimento_modb'); ?></span>
                    <div class="testimonyText">
                      <svg class="quoteSlickStart" width="39" height="33" viewBox="0 0 39 33" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M9.0535 0.201172L14.3819 4.58474C11.8579 7.31229 10.1753 9.89371 9.33395 12.329C8.49262 14.6669 8.07196 17.0535 8.07196 19.4888L4.70664 17.0048H14.9428V32.2012H0.5V21.5345C0.5 17.5406 1.10763 13.8389 2.32288 10.4295C3.63161 6.92264 5.87515 3.5132 9.0535 0.201172ZM32.6107 0.201172L37.9391 4.58474C35.4151 7.31229 33.7325 9.89371 32.8911 12.329C32.0498 14.6669 31.6292 17.0535 31.6292 19.4888L28.2638 17.0048H38.5V32.2012H24.0572V21.5345C24.0572 17.5406 24.6648 13.8389 25.8801 10.4295C27.1888 6.92264 29.4324 3.5132 32.6107 0.201172Z"/>
                      </svg>
                      <?php the_sub_field('depoimento_modb'); ?>
                      <svg class="quoteSlickEnd" width="39" height="31" viewBox="0 0 39 31" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M29.9465 0.201172L24.6181 4.31076C27.1421 6.86784 28.8247 9.28793 29.6661 11.571C30.5074 13.7628 30.928 16.0003 30.928 18.2834L34.2934 15.9546H24.0572V30.2012H38.5V20.2012C38.5 16.4569 37.8924 12.9866 36.6771 9.79022C35.3684 6.50255 33.1248 3.3062 29.9465 0.201172ZM6.3893 0.201172L1.06088 4.31076C3.58487 6.86784 5.26752 9.28793 6.10885 11.571C6.95018 13.7628 7.37085 16.0003 7.37085 18.2834L10.7362 15.9546H0.5V30.2012H14.9428V20.2012C14.9428 16.4569 14.3352 12.9866 13.1199 9.79022C11.8112 6.50255 9.56765 3.3062 6.3893 0.201172Z"/>
                      </svg>
                    </div>
                  </div>
                </div>
              <?php endwhile; ?>
            </div>
          
        <?php endif; ?>
      </div>
    </div>
  </div>
</section>

<section id="secFeaturedBanner" class="courseBackgroundColor">
  <div class="row">
    <div class="col-md-12">
      <div class="featuredList">
        <h2><?php the_field('titulo_featured_modb') ?></h2>
        <?php if (have_rows('lista_featured_modb')): ?>
          <ul>
            <?php while (have_rows('lista_featured_modb')) : the_row(); ?>
              <li><span><?php the_sub_field('vantagem_featured_modb'); ?></span></li>
            <?php endwhile; ?>
          </ul>
        <?php endif; ?>
      </div>
      <div class="featuredCta">
        <h2><?php the_field('titulo_destaque_feat_modb') ?></h2>
        <h3><?php the_field('subtitulo_featured_modb') ?></h3>
        <div id="formRegistration" class="row">
          <div class="col-md-8 col-md-offset-2">
            <form>
              <input type="hidden" name="curso" value="<?php echo $ID_curso; ?>" />
              <div class="btt2linhas">
              <a href="https://methodus.com.br/matricula?ID=<?php the_field('id_do_curso') ?>" class="mainClass" type="submit">
                <?php the_field('nome_cta_featured') ?>
                </a>
                </div>
            </form>
          </div>
        </div>
        <?php /*<a type="button" href="#"><?php the_field('nome_cta_featured') ?></a>*/ ?>
      </div>
    </div>
  </div>
</section>
<?php /*data-toggle="modal" data-target="#courseModal"
<!-- Modal -->
<div class="modal fade" id="courseModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
      </div>
    </div>
  </div>
</div>*/ ?>

<?php function scriptModuloB() { ?>

<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script> 
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>

<script type="text/javascript">
$(document).ready(function(){
  $('.sliderCard').slick({
    dots: false,
    slidesToShow: 1,
    infinite: false,
    autoplay: false,
    fade: true,
    arrows: true,
    prevArrow: "<img class='a-left control-c prev slick-prev' src='<?php echo esc_url( get_template_directory_uri() ); ?>/img/arrowPrev.png'>",
    nextArrow: "<img class='a-right control-c next slick-next' src='<?php echo esc_url( get_template_directory_uri() ); ?>/img/arrowNext.png'>",
    responsive: [
      {
        breakpoint: 780,
        settings: {
          slidesToShow: 1,
          arrows: false,
        }
      },        
    ]
  });
  $('.sliderCardDepo').slick({
    dots: false,
    slidesToShow: 1,
    infinite: true,
    autoplay: true,
    fade: true,
    arrows: true,
    prevArrow: "<img class='a-left control-c prev slick-prev depo-prev' src='<?php echo esc_url( get_template_directory_uri() ); ?>/img/arrowPrev.png'>",
    nextArrow: "<img class='a-right control-c next slick-next depo-next' src='<?php echo esc_url( get_template_directory_uri() ); ?>/img/arrowNext.png'>",
    responsive: [
      {
        breakpoint: 780,
        settings: {
          slidesToShow: 1,
          arrows: false,
        }
      },        
    ]
  });
 });

$('.wpcf7-submit').click(function(){
  $('#cupomModal').modal('hide');
  //$(".loadd").show();
  //var tam = $( document ).height();
  //$(".loadd").height(tam);
  exibe_carregamento( true );
});

var wpcf7Elm = document.querySelector( '.wpcf7' );
 
wpcf7Elm.addEventListener( 'wpcf7submit', function( event ) {
  if ( $('#coursePref').val() == '' ){
    return false;
  } else {
    desabilitarModal();
    exibe_carregamento( false );
    alert('Mensagem Enviada! Cheque seu e-mail para ver o cupom de desconto.');
  }
}, false );

function desabilitarModal(){
    var timestamp = new Date().getTime();
    localStorage.setItem("methodus_modal_desconto", timestamp);
  }
  $(window).one('scroll',function() {
    //if (localStorage.getItem("methodus_modal_desconto") == null) {
    $('#cupomModal').modal('show');
  //}
});

$(document).ready(function () {
  $("#coursePref").change(function () {
    var val = $(this).val();
    if (val == "Oratória") {
      $( ".oratory" ).show();
      $( ".memory" ).hide();
      $( ".time" ).hide();
    } else if (val == "Leitura Dinâmica / Memória") {
      $( ".memory" ).show();
      $( ".oratory" ).hide();
      $( ".time" ).hide();
    } else if (val == "Administração do Tempo") {
      $( ".time" ).show();
      $( ".oratory" ).hide();
      $( ".memory" ).hide();
    }
  });
});

// banner reminder
$(document).ready(function (){
  $("#reminder").click(function (){
      $('html, body').animate({
        scrollTop: $("#formRegistration").offset().top
      }, 1500);
  });
});
$(document).ready(function (){
  $("#advantage").click(function (){
      $('html, body').animate({
        scrollTop: $("#formRegistration").offset().top
      }, 1500);
  });
});
$(document).ready(function (){
  $("#price_a").click(function (){
      $('html, body').animate({
        scrollTop: $("#formRegistration").offset().top
      }, 1500);
  });
});
$(document).ready(function (){
  $("#price_b").click(function (){
      $('html, body').animate({
        scrollTop: $("#formRegistration").offset().top
      }, 1500);
  });
});
</script>
<?php } add_action('wp_footer','scriptModuloB', 1250); ?>
