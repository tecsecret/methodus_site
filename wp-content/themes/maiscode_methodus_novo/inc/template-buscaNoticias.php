<section id="artigos-pagina">
  
<?php if (have_posts()) :  ?>
  <div class="col-md-9 mb-xs-24" id="conteudo_paginado">

    <?php  while (have_posts()) : the_post();?>

      <?php 

      $tipo_post = get_post_type();     

         $image = wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() )); 
                            $btn = get_field('btn_mais_noticia');
       ?> 

           <div class="container-artigo">
                    <?php if ($image != ""): ?>
                        <div class="imagem-noticia">                                    
                            <a href="<?php the_permalink(); ?>"><img alt="Post Image" src="<?php echo $image; ?>"></a>
                        </div>
                    <?php endif ?> 
                    <div class="conteudo-noticia conteudo-artigo">
                        <a href="<?php the_permalink(); ?>">
                            <h4><?php the_title(); ?></h4>
                            <?php the_excerpt(); ?>
                            <div class="footer-artigo footer-not">
                                <span>Publicado em: <?php the_time('d/m/Y'); ?></span>
                                <a href="<?php the_permalink(); ?>">Ler mais<i class="fas fa-arrow-right"></i></a>
                            </div>
                        </a>
                    </div>
                </div>    


  <?php endwhile;  ?>
  <div class="text-center">

    <?php paginacao($limit); ?>

  </div>
</div>

<div class="col-md-3 hidden-sm widget-methodus">
  <div class="busca-widget">
    <form action="<?php bloginfo('siteurl'); ?>" id="searchform" method="get">
      <div class="input-artigo">
        <input type="hidden" name="tipo" value="noticias" id="noticia">
        <input placeholder="Pesquisar por" type="text" name="s" value="<?php the_search_query(); ?>" />
        
          <input type="submit" class="btn btn-input btn-sm" value="Buscar" id="searchsubmit" />
        
      </div>
    </form>
  </div>



  <h6 class="title">Categorias de Artigos</h6>

  <?php
  $args = array(
   'taxonomy' => 'categoria_noticias',
   'orderby' => 'date',
   'order'   => 'DESC',
   'hide_empty' => false
 );

  $cats = get_terms($args);
  $cont = 1;

  ?>

  <ul class="link-list">
    <?php
    foreach($cats as $cat) { 
      ?>

      <li><a href="<?php echo get_term_link($cat->term_id); ?>"><?php echo $cat->name; ?></a></li>

      <?php 
    }
    ?>
  </ul>

  <h6 class="title">Artigos mais acessados </h6>
  <ul class="link-list">



    <?php 
    $args = array(
      'post_type' => 'artigos',
      'post_status' => 'publish',
      'posts_per_page' => '20',
      'meta_key' => 'post_views_count',
      'order' => 'DESC'
    );
    $my_posts = new WP_Query( $args );

    if ( $my_posts->have_posts() ) : 
      ?>

      <?php while ( $my_posts->have_posts() ) : $my_posts->the_post(); 

        ?>


        <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>

      <?php endwhile; ?>
    <?php endif; ?>

  </ul>

</div>


<?php

else:

 ?>
 <div class="col-md-9 mb-xs-24" id="conteudo_paginado">
  <h3> Nenhuma notícia encontrada </h3> 
</div>  

<div class="col-md-3 hidden-sm widget-methodus">
  <h6 class="title">PESQUISAR NOS Noticias</h6>
  <div class="busca-widget">

    <form action="<?php bloginfo('siteurl'); ?>" id="searchform" method="get">
      <div class="input-artigo">
        <input type="hidden" name="tipo" value="noticias" id="noticia">
        <input placeholder="Pesquisar por" type="text" name="s" value="<?php the_search_query(); ?>" />
        <div class="text-center">
          <input type="submit" class="btn btn-input btn-sm" value="Buscar" id="searchsubmit" />
        </div>
      </div>
    </form>

  </div>



  <h6 class="title">Categorias de noticias</h6>

  <?php
  $args = array(
   'taxonomy' => 'categoria_noticias',
   'orderby' => 'date',
   'order'   => 'DESC',
   'hide_empty' => false
 );

  $cats = get_terms($args);
  $cont = 1;

  ?>

  <ul class="link-list">
    <?php
    foreach($cats as $cat) { 
      ?>

      <li><a href="<?php echo get_term_link($cat->term_id); ?>"><?php echo $cat->name; ?></a></li>

      <?php 
    }
    ?>
  </ul>

  <h6 class="title">Artigos mais acessados </h6>
  <ul class="link-list">



    <?php 
    $args = array(
      'post_type' => 'artigos',
      'post_status' => 'publish',
      'posts_per_page' => '20',
      'meta_key' => 'post_views_count',
      'order' => 'DESC'
    );
    $my_posts = new WP_Query( $args );

    if ( $my_posts->have_posts() ) : 
      ?>

      <?php while ( $my_posts->have_posts() ) : $my_posts->the_post(); 

        ?>


        <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>

      <?php endwhile; ?>
    <?php endif; ?>

  </ul>


</div>
<?php endif;?>
</section>