<section id="artigos-pagina">
<?php if (have_posts()) :  ?>
  <div class="col-md-9 mb-xs-24" id="conteudo_paginado">

    <?php  while (have_posts()) : the_post();?>

      <?php 

      $tipo_post = get_post_type();

        $image = wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() )); 
                      $site = get_field('site_empresa');
                      //$sobre = get_field('breve_descricao_da_empresa');
                      $telefone = get_field('telefone_empresa');
                      $email = get_field('e-mail_empresa');
       ?>       

      <div class="post-snippet mb64" style="clear:both;">
        <div class="post-title">
          <h4 class="inline-block"><?php the_title(); ?></h4>
          <span class="label">
            <a href="<?php echo $site; ?>" target="_blank"><?php echo $site; ?></a>
          </span> 
        </div>
        <hr>
        <p> 
          <img class="mb24 thumb_pequeno" src="<?php echo $image; ?>"><?php the_excerpt(); ?><br><?php echo $telefone; ?><br>
          <a href="<?php echo $email; ?>"><?php echo $email; ?></a>
        </p>
      </div>

  <?php endwhile;  ?>
  <div class="text-center">

    <?php paginacao($limit); ?>

  </div>
</div>

<div class="col-md-3 hidden-sm widget-methodus">

  <div class="busca-widget">
    <form action="<?php bloginfo('siteurl'); ?>" id="searchform" method="get">
      <div class="input-artigo">
        <input type="hidden" name="tipo" value="empresas" id="empresa">
        <input placeholder="Pesquisar por" type="text" name="s" value="<?php the_search_query(); ?>" />
         <input type="submit" class="btn btn-input btn-sm" value="Buscar" id="searchsubmit" />

      </div>
    </form>
  </div>



  <h6 class="title">Categorias de ARTIGOS</h6>

  <?php
  $args = array(
   'taxonomy' => 'categoria_empresas',
   'orderby' => 'date',
   'order'   => 'DESC',
   'hide_empty' => false
 );

  $cats = get_terms($args);
  $cont = 1;

  ?>

  <ul class="link-list">
    <?php
    foreach($cats as $cat) { 
      ?>

      <li><a href="<?php echo get_term_link($cat->term_id); ?>"><?php echo $cat->name; ?></a></li>

      <?php 
    }
    ?>
  </ul>

  <h6 class="title">Artigos mais acessados </h6>
  <ul class="link-list">



    <?php 
    $args = array(
      'post_type' => 'empresas',
      'post_status' => 'publish',
      'posts_per_page' => '20',
      'meta_key' => 'post_views_count',
      'order' => 'DESC'
    );
    $my_posts = new WP_Query( $args );

    if ( $my_posts->have_posts() ) : 
      ?>

      <?php while ( $my_posts->have_posts() ) : $my_posts->the_post(); 

        ?>


        <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>

      <?php endwhile; ?>
    <?php endif; ?>

  </ul>

</div>


<?php else: ?>
 <div class="col-md-9 mb-xs-24" id="conteudo_paginado">
  <h3> Nenhuma Notícia Encontrada </h3> 
</div>  

<div class="col-md-3 hidden-sm widget-methodus">

  <div class="busca-widget">
    <form action="<?php bloginfo('siteurl'); ?>" id="searchform" method="get">
      <div class="input-artigo">
        <input type="hidden" name="tipo" value="empresas" id="empresa">
        <input placeholder="Pesquisar por" type="text" name="s" value="<?php the_search_query(); ?>" />
      
          <input type="submit" class="btn btn-input btn-sm" value="Buscar" id="searchsubmit" />

      </div>
    </form>

  </div>



  <h6 class="title">Categorias de ARTIGOS</h6>

  <?php
  $args = array(
   'taxonomy' => 'categoria_empresas',
   'orderby' => 'date',
   'order'   => 'DESC',
   'hide_empty' => false
 );

  $cats = get_terms($args);
  $cont = 1;

  ?>

  <ul class="link-list">
    <?php
    foreach($cats as $cat) { 
      ?>

      <li><a href="<?php echo get_term_link($cat->term_id); ?>"><?php echo $cat->name; ?></a></li>

      <?php 
    }
    ?>
  </ul>

  <h6 class="title">Artigos mais acessados </h6>
  <ul class="link-list">



    <?php 
    $args = array(
      'post_type' => 'artigos',
      'post_status' => 'publish',
      'posts_per_page' => '20',
      'meta_key' => 'post_views_count',
      'order' => 'DESC'
    );
    $my_posts = new WP_Query( $args );

    if ( $my_posts->have_posts() ) : 
      ?>

      <?php while ( $my_posts->have_posts() ) : $my_posts->the_post(); 

        ?>


        <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>

      <?php endwhile; ?>
    <?php endif; ?>

  </ul>


</div>
<?php endif;?>
</section>