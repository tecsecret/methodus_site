<section id="artigos-pagina">
  
<?php 
$my_form_id = get_query_var('my_tipo_post');


if (have_posts()) :  ?>
  <div class="col-md-9 mb-xs-24" id="conteudo_paginado">

    <?php  while (have_posts()) : the_post();?>

      <?php 

      $tipo_post = get_post_type();
      $endereco = get_field('link_link');
       ?> 

       <div class="post-snippet mb64" style="clear:both;">
        <div class="post-title">
          <h4 class="inline-block"><?php the_title(); ?></h4>
          <span class="label">
            <a href="<?php echo $endereco; ?>" target="_blank"><?php echo $endereco; ?></a>
          </span> 
        </div>
        <hr>
        <p> 
          <?php the_excerpt(); ?>
        </p>
       </div>



  <?php endwhile;  ?>
  <div class="text-center">

    <?php paginacao($limit); ?>

  </div>
</div>

<div class="col-md-3 hidden-sm widget-methodus">
  <div class="busca-widget">
    <form action="<?php bloginfo('siteurl'); ?>" id="searchform" method="get">
      <div class="input-artigo">
        <input type="hidden" name="tipo" value="links" id="link">
        <input placeholder="Pesquisar por" type="text" name="s" value="<?php the_search_query(); ?>" />
        
          <input type="submit" class="btn btn-input btn-sm" value="Buscar" id="searchsubmit" />
        
      </div>
    </form>
  </div>



  <h6 class="title">Categorias de ARTIGOS</h6>

  <?php
  $args = array(
   'taxonomy' => 'categoria_links',
   'orderby' => 'date',
   'order'   => 'DESC',
   'hide_empty' => false
 );

  $cats = get_terms($args);
  $cont = 1;

  ?>

  <ul class="link-list">
    <?php
    foreach($cats as $cat) { 
      ?>

      <li><a href="<?php echo get_term_link($cat->term_id); ?>"><?php echo $cat->name; ?></a></li>

      <?php 
    }
    ?>
  </ul>

  <h6 class="title">Artigos mais acessados </h6>
  <ul class="link-list">



    <?php 
    $args = array(
      'post_type' => 'artigos',
      'post_status' => 'publish',
      'posts_per_page' => '20',
      'meta_key' => 'post_views_count',
      'order' => 'DESC'
    );
    $my_posts = new WP_Query( $args );

    if ( $my_posts->have_posts() ) : 
      ?>

      <?php while ( $my_posts->have_posts() ) : $my_posts->the_post(); 

        ?>


        <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>

      <?php endwhile; ?>
    <?php endif; ?>

  </ul>

</div>


<?php

else:

 ?>
 <div class="col-md-9 mb-xs-24" id="conteudo_paginado">
  <h3> Nenhum link Encontrado </h3> 
</div>  

<div class="col-md-3 hidden-sm widget-methodus">

  <div class="busca-widget">

    <form action="<?php bloginfo('siteurl'); ?>" id="searchform" method="get">
      <div class="input-artigo">
        <input type="hidden" name="tipo" value="links" id="link">
        <input placeholder="Pesquisar por" type="text" name="s" value="<?php the_search_query(); ?>" />
        
          <input type="submit" class="btn btn-input btn-sm" value="Buscar" id="searchsubmit" />
        
      </div>
    </form>

  </div>



  <h6 class="title">Categorias de links</h6>

  <?php
  $args = array(
   'taxonomy' => 'categoria_links',
   'orderby' => 'date',
   'order'   => 'DESC',
   'hide_empty' => false
 );

  $cats = get_terms($args);
  $cont = 1;

  ?>

  <ul class="link-list">
    <?php
    foreach($cats as $cat) { 
      ?>

      <li><a href="<?php echo get_term_link($cat->term_id); ?>"><?php echo $cat->name; ?></a></li>

      <?php 
    }
    ?>
  </ul>

  <h6 class="title">Artigos mais acessados </h6>
  <ul class="link-list">



    <?php 
    $args = array(
      'post_type' => 'artigos',
      'post_status' => 'publish',
      'posts_per_page' => '20',
      'meta_key' => 'post_views_count',
      'order' => 'DESC'
    );
    $my_posts = new WP_Query( $args );

    if ( $my_posts->have_posts() ) : 
      ?>

      <?php while ( $my_posts->have_posts() ) : $my_posts->the_post(); 

        ?>


        <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>

      <?php endwhile; ?>
    <?php endif; ?>

  </ul>


</div>
<?php endif;?>

</section>