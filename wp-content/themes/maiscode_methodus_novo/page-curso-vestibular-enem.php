<?php
/**
* Template Name: Curso Online vestibular do Enem
* @package WordPress
* @author Revolution Tech
* @since First Version
*/

$url_site = get_option('link_site'); 
$url_api  =  get_option('link_api'); 
$url_amigavel_wp = home_url(); 
// URLs amigáveis
$url_amigavel_on  = 1;
$url_amigavel   =  get_option('link_site_amigavel');

$pagina_curso = 1;

$urlWP = get_template_directory_uri();
// $urlWP = site_url();
// $urlWP = 'https://www.methodus.com.br/';
  
// Verificar
$ID = get_field('id_do_curso');
$ID_Depoimento_Curso = get_field('id_depoimento_curso');
$ebookWp = get_field('ebook');
$xml_curso  = executaPagina( 'api/', array( 'a'=>'cursos', 'metodo'=>'ver', 'ID'=>$ID ) );
$curso      = lerXML( $xml_curso );
    

// var_dump($curso);
    
if ( $curso->erro == 0 ){
    
    $curso              = $curso->curso;
    $status             = $curso->status;
    $titulo_curso       = $curso->titulo;
    $url_curso          = $curso->url;
    $distancia_curso    = $curso->distancia;
    $banner_curso       = $curso->banner;
    $resumo_curso       = $curso->resumo;
    $descricao_curso    = $curso->descricao;
    $variaveis_curso    = $curso->variaveis; 
    $planos_curso       = $curso->planos;
    $turmas_curso       = $curso->turmas->turma;
    $perfis_curso       = $curso->perfis->perfil;
    $ID_tema            = $curso->tema;
    
    $seo_metatags       = $curso->metatags;
    $seo_title          = $titulo_curso.' - Methodus';
    $seo_imagem         = 'https://'.$_SERVER['HTTP_HOST'].''.$banner_curso;
    
    if ( isset( $variaveis_curso->classe ) ){
        $classe_curso = $variaveis_curso->classe;
    }else{
        $classe_curso = 'Verde'; 
    }
    
    if ( isset( $curso->fotos ) ){
        $fotos_curso = $curso->fotos->foto;
    }

    if ( $status != 1 ){
        exit("Erro ao acessar os dados do curso 1");
    }
    
}else{
    exit("Erro ao acessar os dados do curso");      
}

?>


<?php get_header(); ?>

<style type="text/css">
    .caixa_ebook img{
    width: 170px;
  }
        .btn_top{
            color: #fff !important;
        }
        .btn_top:hover{
            background-color: #fff !important;
            color: #e38902 !important;
        }
        .btn_pg_curso{
            border: 2px solid #e38902 !important;;
        }
        .btn_pg_curso:hover{
            background-color: #fff !important;
            color: #e38902 !important;
        }
        .btn_chamada:hover{
            color: #e38902 !important;
        }
    </style><!-- TOPO -->

<style type="text/css">
  .courseColor {
    color: #214822;
  }
  .courseBackgroundColor {
    background-color: #214822;
  }
  #secStoryModB .storyContent svg path {
    fill: #214822;
  }
  .sliderCard svg path {
    fill: #214822;
  }
  #secTopModB:before {
    content: '';
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    background: linear-gradient(270deg, rgba(33,72,34,0.7) 23.16%, rgba(52, 50, 50, 0) 118.16%);
  }
  #secCallBannerModB .bgTransformation:before {
    content: '';
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    background: linear-gradient(179.64deg, rgba(96, 94, 94, 0) -36.93%, rgba(160, 0, 0, 0.6) 85.54%);
  }
  .loadd{
    display: none;
    opacity: .5;
    background: #000;
    width: 100%;
    position: absolute;
    top: 0px;
    height: 100vh;
    z-index: 10000000;
  }
 
  .sliderCardDepo .depo-prev
  {
    height: 36px;
    width: 220px;
    z-index: 100000;
    object-fit: contain;
  }
  h2 {
	  padding-top: 30px;
	  padding-bottom:30px;
  }
  .sliderCardDepo .depo-next{
    width: 235px;
    height: 36px;
    object-fit: contain;
  }
</style>



<section id="secTopModB" style="background-image: url('https://methodus.com.br/wp-content/uploads/2021/04/vestibular1.png')">
  <div class="container">
    <div class="row">
      <div class="col-md-7 col-md-offset-5 col-sm-7 col-sm-offset-5 topContent" >
        <h2>O Mestre dos Estudos</h2>
		<h3 style="color:#FFF;">Estude Menos e Aprenda 7 vezes Melhor</h3>
        <h4 style="color:#FFF;">Vestibulares • Concursos Públicos • Enem</h4>
        <img src="https://methodus.com.br/wp-content/uploads/2020/08/curso-de-oratoria-lp-1.png">
      </div>
    </div>
  </div>
</section>

        <?php
    if ( isset( $variaveis_curso->ebook_download ) ){
        $temp_ebook = explode( '|', $variaveis_curso->ebook_download );

        if ( count( $temp_ebook ) >= 4 ){

            $titulo_ebook           = $temp_ebook[0];
            $descricao_ebook        = $temp_ebook[1];
            $destino_ebook          = $temp_ebook[2];
            $identificador_ebook    = $temp_ebook[3];
            $urlWP = get_template_directory_uri();
            ?>


            <section class="caixa_ebook ">
                <div class="container-fluid bg_ebook">
                    <div class="container">                       
                 
                        <script src="<?php echo $urlWP; ?>/js/scripts_site/ebook.js?ver=1"></script>
                        <form action="<?php echo $url_api; ?>/api/" method="post" class="form-email formAjax" id="form_ebook" data-validacao="checaEbook" data-resultado="sucessoEbook">
                            <input type="hidden" name="destino" value="<?php echo $ebookWp ?>" />
                            <input type="hidden" name="a" value="aluno" />
                            <input type="hidden" name="metodo" value="cadastro" />
                            <input type="hidden" name="origem" value="Ebook_<?php echo $identificador_ebook; ?>" />
                            <input type="hidden" name="curso" value="<?php echo $ID; ?>" />
                    
                            <div class="row">
                                <div class="col-xs-12 col-md-8">
                                    <img src="<?php echo $urlWP ?>/img/ebook_vestibular.jpg" width="80" />
                                    <div class="chamada-ebook"><strong>NOVO INFOGRÁFICO</strong> <br> <span style="font-size: small;"> Vestibulares • Concursos Públicos • Enem </span><br> OS 5 ACERTOS DECISIVOS </div>
                                </div>
                                <div class="col-xs-12 col-md-4">
                                    <h5 class="mb0 inline-block p0-xs">
                                        <?php echo $titulo_ebook ?>
                                    </h5>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <input type="text" class="validate-required cada-input" name="nome" placeholder="Nome" lang="Nome">
                                        </div>
                                        <div class="col-sm-12 form-ebook">
                                            <input type="text" class="validate-required cada-input" name="email" placeholder="E-mail" lang="E-mail">
                                            <?php
                                            $i = 0;
                                            if( have_rows('formulario') ):
                                                while ( have_rows('formulario') ) : the_row(); $i++?> 
                                                    <div class="radio_cursos">  
                                                        <label for="ebook-<?php echo $i; ?>" class="radio">
                                                            <input type="radio" class="validate-required" id="ebook-<?php echo $i; ?>" name="conheceu" value="<?php the_sub_field('opcoes'); ?>" lang="opções">
                                                            <span></span>
                                                        </label>          
                                                        <label for="ebook-<?php echo $i; ?>"><?php the_sub_field('opcoes'); ?></label><br>
                                                    </div>
                                                <?php endwhile;
                                            endif;
                                            ?>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <button class="btn <?php echo $classe_curso; ?>" type="submit">Download</button>
                                        </div>
                                    </div>
                                </div>
                            </div>                            
                        </form>
                    </div>
                </div>
            </section>
        <?php
        }
    }
    ?>
<!-- BANNER CHAMADA -->
<section id="secCallBannerModB" class="courseBackgroundColor">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2 style="padding-bottom:0;">Técnicas COMPROVADAS de estudo somente para 1 tipo de estudante:</h2>
		<h3 style="color:#FFF; text-align:center; padding:0 !important";"> Quem não passou e não quer ter a dor de não passar de novo.</strong></h3>		
      </div>
    </div>
  </div>
    
</section>

<!-- HISTORIA -->
<section id="secStoryModB" style="padding-top: 10px !important;">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="storyContent">
			<h2> Leia abaixo sobre o Curso que, sim, vai tirar um peso enorme da sua vida!</h2>
			<h3>  “Com este método, você vai com certeza ter a segurança de estar fazendo a coisa certa na hora de estudar! E vai ser tão assertivo que nem vai acreditar quando descobrir o quanto vai acertar na próxima prova.” </h3>
			<h4 class="align-center; font-size:small; text-align:right">Prof. Alcides Schotten.</h4>	
		<h2> Quem Desenvolveu este curso?</h2>
		<h4 class="align-center">Apresentamos a vocês, Prof. Alcides Schotten, o Mestre dos Estudos.</h4>
		<h4> Para você entender quais os diferencias reais, vamos falar primeiro sobre o criador da Methodus, o professor Alcides Schotten. O professor Alcides é professor e filósofo, ou seja, ele vai além do básico e aprofunda-se muito na mente humana. Estudioso por natureza, aprimora-se todos os anos, buscando na neurociência e na neuropedagogia, o entendimento de como o cérebro funciona e reage. E assim, usa toda sua base e conhecimento de professor e filósofo, para criar os estímulos perfeitos para que o cérebro aprenda de verdade. Não é simplesmente aprender por uns dias. A metodologia é feita para transformar você para sempre. E de maneira fácil. </h4>
		<img class="aligncenter wp-image-6366" src="https://methodus.com.br/wp-content/uploads/2019/11/curso-de-administracao-do-tempo-8.jpg" alt="" width="100%" height="auto" style="position:static;"/>
		<h2>Não seria maravilhoso aprender o que fazer para estudar e esta ser a última vez que você faz uma prova?</h2>
		<h3>Nós temos a fórmula! E com certeza funciona para você!</h3>
		<h4>Por isso, quero te mostrar um pouco de como é o caminho, para que você consiga visualizar e, com toda a certeza do mundo, ir em frente para a sua última prova.</h4>
		<h4>São técnicas de estudos, de memorização (para estudar e não esquecer), de análise do que é importante estudar e muito mais! </h4>
		<h2>E qual é este caminho?</h2>
		<h4>O caminho é simples e direto. Basta você seguir – a Methodus desenvolve cursos de aprimoramento pessoal e acadêmico há mais de 23 anos. E tem a segurança de saber todas as armadilhas e todos os atalhos para que você consiga passar nas provas.
E por isso, desenvolveu este método, agora online, mais direto e mais eficaz, para que você consiga aprender o que fazer na hora de estudar e gastar muito menos tempo e aprender muito melhor.</h4>
              </div>
      </div>
  </div>
  </div>
  </section>
  
  <!-- POR QUE A METHODUS -->
<section id="secWhyMethodus">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2 class="courseColor">O resultado? É menos esforço e mais chances de passar!</h2>
        <div class="methodusContent">
                                 

			<img class="aligncenter wp-image-6362" src="hhttps://methodus.com.br/wp-content/uploads/2021/04/vestibular2.jpg" alt="" width="100%" height="auto" />
            <h4 class="">A nota de corte nunca mais te assustará! Você vai passar confiante por ela!</h4>
                            <h4 class="">Veja só estes depoimentos sobre a Methodus:</h4>
							<div style="text-align:center;">
				<div class="col-3" style="display:inline; padding:15px;"><div class="local-video-container">
				<div class="background-image-holder" style="background: url(&quot;https://methodus.com.br/wp-content/uploads/2020/08/curso-de-oratoria-lp-2.png&quot;); "> 
				
            </div>
            <video controls="">
               <source src="https://methodus.com.br/wp-content/uploads/2021/05/nicoly.mp4" type="video/mp4">                                
              </video> 
              <div class="play-button" style="opacity: 0;"></div>
            </div></div>
			<div class="col-3" style="display:inline; padding:15px;"><div class="local-video-container">
				<div class="background-image-holder" style="background: url(&quot;https://methodus.com.br/wp-content/uploads/2020/08/curso-de-oratoria-lp-2.png&quot;); "> 
				
            </div>
            <video controls="">
               <source src="https://methodus.com.br/wp-content/uploads/2021/05/thaisa.mp4" type="video/mp4">                                
              </video> 
              <div class="play-button" style="opacity: 0;"></div>
            </div></div>
						
							</div>
        </div>

        <div class="differenceContent">
                    <h3 class="courseColor"><strong>Não vai ser incrível ver seu nome na lista de aprovados? Então vamos começar! Agora!</strong></h3>
										
						<p>Vou te contar aqui embaixo quais são os segredos para passar e como trilhar o caminho seguro para o sucesso!</p>
						<p>É um caminho que exige esforço, claro, mas é uma vez só. </p>
						<p>Todos os outros caminhos que tentar, você vai chegar na prova sem estar no seu melhor.</p>

						<p>Com a segurança do Curso “Estude Menos e Aprenda 7 vezes Melhor”, o resultado é:</p>
						<h4>você vai aprender 7 vezes melhor. E aí, vai deixar os outros candidatos para trás.</h4>
						
						<p>Com as Estratégias de como estudar para concurso, vestibulares ou Enem criadas pelo Mestre dos Estudos, você vai conhecer melhores métodos de estudo, vai estudar de forma mais eficiente, vai aprender a elaborar mapas mentais, perder a preguiça de ler, perder a preguiça de estudar e muito mais!</p>
					<h3 class="courseColor"><strong>Vamos ver as etapas deste caminho? Você vai ver que sim, é para você!</strong></h3>
						
					<h3 class="courseColor"><strong>NO MÁXIMO EM 1 SEMANA VOCÊ ESTARÁ PRONTO PARA INICIAR E SEU ESTUDO VAI RENDER MUITO MAIS!</strong></h3>
					<img class="aligncenter wp-image-6366" src="https://methodus.com.br/wp-content/uploads/2021/04/vestibular3.jpg" alt="" width="100%" height="auto" />
					<h3>PASSO 1: AUTOCONHECIMENTO: SÓ ASSIM O ESTUDO VAI RENDER!</h3>
					<h4>A)	Você precisa se autoconhecer e ver se é a carreira certa. Pra não ter que fazer outra prova depois!</h3>
					<h4 class="courseColor">B)	Descubra quais das 9 inteligências você possui mais desenvolvidas – isso guiará o seu método de vida e de estudo para você se sair sempre melhor.</h4>
					<h4>C)	Descubra se você é visual, auditivo ou sinestésico – isso vai te dizer qual o jeito que você tem que estudar – Assim não estuda do jeito errado e o estudo passa a ser mais fácil e sem sofrimento!</h4>
					<h4 class="courseColor">D)	Veja como é confortável para você dividir seu tempo para atingir os objetivos de curto, médio e o longo prazo.</h4>
					<h4>E)	Como se automotivar e não desanimar (mais de 57% dos concorrentes desanimam no meio do caminho ou na reta final)!</h4>
					<img class="aligncenter wp-image-6366" src="https://methodus.com.br/wp-content/uploads/2021/04/vestibular4.jpg" alt="" width="100%" height="auto" />
					<h3>PASSO 2: PASSO A PASSO: AS TÉCNICAS DE ESTUDO PARA A APROVAÇÃO</h3>
					<h4>A)	Como estudar Sozinho e como estudar no Cursinho?</h4>
					<h4 class="courseColor">B)	Presencial e Online – técnicas para cada meio.</h4>
					<h4>C)	3 práticas decisivas: Teoria + Exercícios + Revisões –> Guia de como fazer correto!</h4>
					<h4 class="courseColor">D)	Como se sair bem nas provas –> Simulados de Altíssimo Desempenho.</h4>
					<h4>E)	Conheça os Perfis de Candidatos – Descubra o seu!</h4>
					<h4 class="courseColor">F)	Estratégias de Otimização de Desempenho</h4>
					<img class="aligncenter wp-image-6366" src="https://methodus.com.br/wp-content/uploads/2021/04/vestibular5.jpg" alt="" width="100%" height="auto" />
					<h3>PASSO 3: CORPO + CÉREBRO + LOGÍSTICA: O PODER DO FÍSICO, DA RAZÃO E DA EMOÇÃO!</h3>
					<h4>A)	Nutrição</h4>
					<h4 class="courseColor">B) Atividade Física</h4>
					<h4>C)	Sono + Memória</h4>
					<h4 class="courseColor">D)	Lógica (Cérebro Lógico)</h4>
					<h4>E)	Emoções e Motivações (Cérebro Límbico)</h4>
					<h4 class="courseColor">F)	Logística (sua Rotina Empoderada)</h4>
					<p><br><br><br></p>
	</div>
	</div>
	</div>
	</div>
</section>

<!-- SOBRE O CURSO -->
<section id="secWhyMethodus" style="padding-bottom:0 !important">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
	  <h2 class="courseColor">O que o curso faz é juntar autoconhecimento e técnicas de estudo.</h2>
        <div class="methodusContent">
			<p><br></p>
            <h4><strong>O Autoconhecimento deixa você forte e seguro! É o Auto Empoderamento! </strong></h4>
			<h4><strong>E as técnicas comprovadas de estudo melhoram muito a sua performance. É Certeiro!</strong></h4>
			<h4><strong>São dicas muito, muito poderosas de como estudar para o Enem, dicas de como estudar para concursos e como estudar para vestibulares.</strong></h4>
            <h2>Resultados:</h2>
			<h4 class="">Menos esforço para aprender, pois você aprenderá do seu jeito. Assim, você vai perder a preguiça de estudar.</h4>
			<h4 class="">Sendo do seu jeito e do jeito que seu cérebro se sente mais confortável e rende mais, você fica mais ativo sem perceber!</h4>
			<h4 class="">Sendo do seu jeito e do jeito que seu cérebro se sente mais confortável e rende mais, você fica mais ativo sem perceber!</h4>
			<h4 class="">Você vai conseguir chegar na prova sabendo muito mais e sem estar tão esgotado. E fará a prova muito mais descansado e tranquilo!</h4>
			<h4 class="courseColor"><strong>Perfeito para Vestibulares de todas as áreas, principalmente os mais difíceis, com altas notas de corte, como:</strong></h4>
			<ul class="h4">
				<li> ● Vestibular de Medicina</li>
				<li> ● Vestibular de Direito</li>
				<li> ● Vestibular de Administração</li>
				<li> ● Vestibular de Engenharia</li>
			</ul>	
			<h4 class="courseColor"><strong>Para Concursos Públicos, sinta-se preparado para os mais complicados de passar:</strong></h4>
			<ul class="h4">
				<li> ● Concurso Receita Federal</li>
				<li> ● Concurso Magistratura</li>
				<li> ● Concurso Promotoria</li>
			</ul>
				
			<h3 class="courseColor">E para quem está começando no mundo as provas, é perfeito para o ENEM.</h3>
			
</div>
</div>
</div>
</div>
</section>


<!-- Lembrete  -->
<section id="secReminder" style="padding:0">
  <div class="reminderBanner " style="padding:0">
    <div class="">
      <div class="row">
        <div class="col-md-12">
          <h2 style="color:#214822;">Acredite: você vai passar!</h2>
		  <img class="aligncenter wp-image-6362" style="position:static;" src="https://methodus.com.br/wp-content/uploads/2021/04/vestibular6.jpg" alt="" width="50%" height="auto" />
        </div>
      </div>
    </div>
  </div>
</section>

<section id="secPrices" class="" style="">
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1 col-sm-12" style="align:center">
        <div class="pricesTop featurePrice">
          <h2 style= "color:#e38902">E você quer saber o valor de investimento no curso?</h2>
			<h3>É muito menos do que o que você vai pagar de cursinho se não passar de novo. É garantido!</h3>
			<h3>Muito menos do que a dor de mais um ano estudando sem conseguir passar!</h3>
			<h3 style="color: #214822;"><strong>Por isso vale a pena! </strong></h3>
			<img class="aligncenter wp-image-6362" src="https://methodus.com.br/wp-content/uploads/2021/04/vertibular6.png" alt="" width="100%" height="auto" />
			<h3>Porque o cursinho é caro e a dor de não passar não tem preço.</h3>
			<h3 style="color: #214822;"><strong>E mais: Você tem ainda 7 dias grátis! Se não gostar, devolvemos integralmente o valor da sua compra.</strong></h3>
			<h3>Você deve estar pensando: se um bom cursinho custa em torno de 18 Mil Reais por ano, quanto você acha que custaria o curso: Estude Menos e Aprenda 7 vezes Melhor.</h3>
			<h3>Metade disso? R$ 9 Mil?</h3>
			<h3>Não!</h3>
			<h3>Então menos metade? R$ 3.500?</h3>
			<h3>Também não. Muito Menos! E em até 10 vezes.</h3>
			<a data-toggle="" data-target="#cursoVejaInvestimento" href="https://go.hotmart.com/U52432598P">Clique e veja agora! Você vai se surpreender!</a>
</section>
<section id="secPrices" class="courseBackgroundColor">
  <div class="row">
    <div class="col-md-12">
      <div class="featuredList">
        <h2>BÔNUS!</h2>
		<h4 style="color: #FFF;">E além de todo o curso, separamos para você 3 Bônus extras!</h4>
		<h4 style="color: #FFF;">São 3 e-books que tratam especificamente de técnicas e segredos comprovados de como passar nas provas que o Mestre dos Estudos preparou para você!</h4>
		<h4 style="color: #FFF;">Clicando agora e comprando o Curso “Estude Menos e Aprenda 7 vezes Melhor”, você receberá:</h4>
		<div style="display: flex;flex-wrap: wrap;align-items: stretch;flex-direction: row; margin:60px">
		<div class="col-md-4 col-sm-12">
		<div class="boxWorkload" style="height: 100%; margin:0;">
			<h3 style="color: #214822;">E-book • O Passo a Passo para Aprovação</h3>
			<h5>Ele traz resumidamente as principais Dúvidas e Queixas dos estudantes e explica o caminho que precisa ser seguido em cada situação.</h5>
			<h5></h5>
        </div>
		</div>
		
		<div class="col-md-4 col-sm-12">
        <div class="boxWorkload" style="height: 100%; margin:0;">
			<h3 style="color: #214822;">E-book • As 7 teses científicas da Leitura Dinâmica</h3>
			<h5>De maneira clara e objetiva, este e-book te mostra como o cérebro funciona em relação à leitura. E te prova que no caso da Leitura Dinâmica: a pressa é amiga da perfeição e como a leitura, por ser dinâmica, é menos cansativa e, claro, toma muito menos tempo para o aprendizado de grandes volumes de informação.</h5>
        </div>
		</div>
		<div class="col-md-4 col-sm-12">
        <div class="boxWorkload" style="height: 100%; margin:0;">
			<h3 style="color: #214822;">E-book • Descubra seu Estilo de Aprendizagem: visual, auditivo ou sinestésico</h3>
			<h5>Aqui um super segredo que em nenhum lugar te ensinam: COMO VOCÊ TEM QUE ESTUDAR PARA SOFRER MENOS E APRENDER MAIS.</h5>
			<h5>Neste e-book você vai identificar com facilidade e clareza qual é o seu tipo e vai ver o que precisa fazer. Assim, vai parar de sofrer estudando de um jeito que é cansativo, desesperador e que você não aprende. Estudando do jeito certo para o seu tipo, você sofre menos e aprende muito mais fácil.</h5>
        </div>
		</div>
		</div>
		<div class="col-md-12 col-sm-12 d-block">
		<div class="boxWorkload" style="padding-inline:75px">
			<h3 style="color: #214822;">GARANTIA 7 DIAS</h3>
			<h5>VOCÊ PODE COMPRAR O CURSO E SE NÃO GOSTAR, DEVOLVEMOS INTEGRALMENTE O VALOR EM 7 DIAS! SEM PERGUNTAS OU MOTIVOS!</h5>
        </div>
		</div>
		<div class="col-12 d-block">
			<h3 style="color:#fff">Tudo isso com um valor especial para você! </h3>
              <a data-toggle="" data-target="#cursoVejaInvestimento" href="https://go.hotmart.com/U52432598P">Clique agora e descubra!</a>
        </div>
  
              </div>

    </div>
  </div>
</section>



<!-- Modal  veja investimento -->
<div id="cursoVejaInvestimento" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <?php echo do_shortcode( '[contact-form-7 id="6289" title="VEJA INVESTIMENTOS E PRÓXIMOS GRUPOS!"]'); ?>

      </div>
    </div>

  </div>
</div>


  <script type='text/javascript' async src='https://d335luupugsy2.cloudfront.net/js/loader-scripts/67cc1fec-89be-4715-b86e-5eda535a7d57-loader.js'></script><script type='text/javascript' src='https://methodus.com.br/wp-content/themes/maiscode_methodus_novo/js/main.js?ver=1.0.0' id='jquery-js'></script>
<script type='text/javascript' id='contact-form-7-js-extra'>
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/methodus.com.br\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script type='text/javascript' src='https://methodus.com.br/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.3' id='contact-form-7-js'></script>
<script type='text/javascript' src='https://methodus.com.br/wp-includes/js/wp-embed.min.js?ver=5.5.3' id='wp-embed-js'></script>

<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script> 
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>

<script type="text/javascript">
$(document).ready(function(){

  $('.sliderCard').slick({
    dots: false,
    slidesToShow: 1,
    infinite: false,
    autoplay: false,
    fade: true,
    arrows: true,
    prevArrow: "<img class='a-left control-c prev slick-prev' src='https://methodus.com.br/wp-content/themes/maiscode_methodus_novo/img/arrowPrev.png'>",
    nextArrow: "<img class='a-right control-c next slick-next' src='https://methodus.com.br/wp-content/themes/maiscode_methodus_novo/img/arrowNext.png'>",
    responsive: [
      {
        breakpoint: 780,
        settings: {
          slidesToShow: 1,
          arrows: false,
        }
      },        
    ]
  });

  $('.sliderCardDepo').slick({
    dots: false,
    slidesToShow: 1,
    infinite: true,
    autoplay: true,
    fade: true,
    arrows: true,
    prevArrow: "<img class='a-left control-c prev slick-prev depo-prev' src='https://methodus.com.br/wp-content/themes/maiscode_methodus_novo/img/arrowPrev.png'>",
    nextArrow: "<img class='a-right control-c next slick-next depo-next' src='https://methodus.com.br/wp-content/themes/maiscode_methodus_novo/img/arrowNext.png'>",
    responsive: [
      {
        breakpoint: 780,
        settings: {
          slidesToShow: 1,
          arrows: false,
        }
      },        
    ]
  });


 });


</script>

<?php get_footer(); ?>