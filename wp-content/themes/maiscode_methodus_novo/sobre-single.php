<?php
/**
* Template Name: Single Post Sobre
* Template Post Type: post, page, sobres
*
* @package WordPress
* @author Mais Code Tecnologia
* @since First Version
*/
get_header(); 
?>

<?php $page = new WP_Query( 'page_id=211' ); ?>
<?php if( $page->have_posts() ) : $page->the_post(); ?>
	<h1><?php the_title(); ?></h1>
	<?php the_excerpt(); ?> 
<?php endif; ?>

<?php get_footer(); ?>