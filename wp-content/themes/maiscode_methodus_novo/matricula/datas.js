// JavaScript Document

function checaData(form){
	
	var erro = "";
	
	erro+= checa_nulo( form.ID_plano );
	
	if ( form.ID_data.type == 'hidden' ){
		erro+= checa_nulo( form.ID_data );
	}else{
		erro+= checa_opcoes( form.ID_data, 'Turma' );	
	}
	
	if ( form.participantes.value <= 0 ){
		erro+= '- Escolha quantos participantes irão se inscrever<BR />';	
	}
	
	if ( erro != "" ){
		notifica( erro, 'atencao', 10 );
		return false;
	}else{
		exibe_carregamento( true );
		return true;
	}
	
	return(false);	
}

function inscreverSe(tipo, plano){
	
	form = document.getElementById('inscricao');
	
	form.participantes.value 	= $('#participantes_'+tipo).val();
	form.ID_plano.value 		= plano;
	
	$('#enviar_form').click();
	
}