// JavaScript Document
function escolheMeio( meio ){
	
	exibe_carregamento( true );
	
	forma 			= $('#forma').val();
	ID_inscricao	= $('#forma').attr('data-inscricao');
	distancia		= $('#forma').attr('data-distancia');
	cupom 			= $('#forma').attr('data-cupom');
	percent 		= $('#forma').attr('data-cupom');
	qtd				= $('#forma').attr('data-qtd');
	valor 			= $('#forma').children('option:selected').data('valor');

	//discont 		= 0;
	
	// if(qtd >= 1){
	// 	if(forma == 1){
	// 		discont = 30;
	// 	} else {
	// 		discont = 20;
	// 	}
	// } else if (qtd > 1){
	// 	if(forma == 1){
	// 		discont = 35;
	// 	} else {
	// 		discont = 25;
	// 	}
	// }
	
	if ( forma == '' ){
		exibe_carregamento( false );
		notifica( 'Escolha uma opção de parcelamento', 'atencao', 10 );
	}else{

		if ( meio == 6 && forma > 1 && distancia != '1' ){ //if ( meio == 6 && forma > 6 && distancia != '1' ){
			exibe_carregamento( false );
			//notifica( 'Pagamentos por boleto só podem ser parcelados em até 6 vezes', 'atencao', 10 );
			notifica( 'Pagamentos por boleto só podem ser pagos à vista', 'atencao', 10 );

		}else if ( meio == 6 && forma > 1 && distancia == '1' ){
			exibe_carregamento( false );
			notifica( 'Pagamentos por boleto só podem ser pagos à vista', 'atencao', 10 );

		}else{
		
			//chamar aqui o AJAX para salvar a forma, meio e valor do pagamento, se tudo der certo, ir para página de conclusão, senão, exibir qual o erro
			carregaAjax( "POST", "https://api.methodus.com.br/api/", "a=matricula&metodo=meio&ID_inscricao="+ID_inscricao+"&forma="+forma+"&meio="+meio, function(retorno){
				
				var retorno = JSON.parse(retorno);
				if (cupom == '1') {
					if ( retorno.erro == 0 ){
					encaminhar_pagina(url_site+'/matricula/pagamento?cupom=1&bf_discount='+valor+'', {'ID_inscricao':retorno.ID_inscricao}, 'post');
					} else {
						exibe_carregamento( false );
						notifica( retorno.mensagem, 'erro', 10 );
					}
				} else {
					if ( retorno.erro == 0 ){
						encaminhar_pagina(url_site+'/matricula/pagamento?bf_discount='+valor+'', {'ID_inscricao':retorno.ID_inscricao}, 'post');
					} else {
						exibe_carregamento( false );
						notifica( retorno.mensagem, 'erro', 10 );
					}
				}
				
				
			});

		}
		
	}
}