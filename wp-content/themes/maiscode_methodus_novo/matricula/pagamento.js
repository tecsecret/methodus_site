// JavaScript Document

$(document).ready(function() { 
	
	setTimeout(function(){
		$('html, body').animate({scrollTop: $('#formPagamento').offset().top-100 }, 500);
	},500);
	
});


function checaPagamento(formData, jqForm, options){
	
	var form = jqForm[0];
	var erro = "";
	
	erro+= checa_opcoes(form.meio,"Meio de pagamento");
	//alert( retorna_opcao(form.meio) );
	
	erro+= checa_nulo( form.cartao_nome );
	erro+= checa_nulo( form.cartao_numero );
	erro+= checa_tamanho(form.cartao_numero,13)
	erro+= checa_caracter( form.cartao_numero, "0123456789" );
	erro+= checa_nulo( form.cartao_seguranca );
	erro+= checa_tamanho(form.cartao_seguranca,3)
	erro+= checa_caracter( form.cartao_seguranca, "0123456789" );
	erro+= checa_nulo( form.cartao_validade );
	erro+= checa_tamanho(form.cartao_validade,5)
	erro+= checa_caracter( form.cartao_validade, "0123456789/" );
	
	
	if ( erro != "" ){
		notifica( erro, 'atencao', 10 );
		return false;
	}else{
		exibe_carregamento( true );
		return true;
	}
	
	return(false);	
}

function pagamentoSucesso(data){
	if (data.erro == 0){
		exibe_carregamento( false );
		notifica( data.mensagem, 'sucesso', 100 );
		$('#formPagamento').hide('fast');
		dataLayer.push({'event' : 'matricula_MKT'});
		//encaminhar_pagina(url_site+'/matriculas/conclusao.php', {'ID_inscricao': data.ID_inscricao }, 'get' );
	}else{
		exibe_carregamento( false );
		notifica( data.mensagem, 'erro', 10 );
	}
}