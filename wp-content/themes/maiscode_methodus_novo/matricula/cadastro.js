// JavaScript Document

function checaDados(formData, jqForm, options){
	
	var form = jqForm[0];
	var erro = "";
	
	numero_participantes = form.participantes.value;
	str_emails_participantes = '';
	
	for ( contador=1; contador<=parseInt(numero_participantes); contador++ ){
		erro+= checa_nulo( form.elements['nome_'+contador] );
		erro+= checa_nulo( form.elements['nascimento_'+contador] );
		erro+= checa_data( form.elements['nascimento_'+contador] )
		erro+= checa_caracter( form.elements['nascimento_'+contador], '0123456789/' );
		erro+= checa_tamanho( form.elements['cpf_'+contador], 14 );
		erro+= checa_caracter( form.elements['cpf_'+contador], '0123456789.-' );
		erro+= checa_cpf( form.elements['cpf_'+contador] );
		//erro+= checa_tamanho( form.elements['rg_'+contador], 7 );
		erro+= checa_tamanho( form.elements['tel_'+contador], 14 );
		erro+= checa_caracter( form.elements['tel_'+contador], '0123456789()- ' );
		erro+= checa_nulo( form.elements['email_'+contador] );
		erro+= checa_email( form.elements['email_'+contador] );
		erro+= checa_nulo( form.elements['endereco_'+contador] );
		erro+= checa_nulo( form.elements['cidade_'+contador] );
		erro+= checa_tamanho( form.elements['cep_'+contador], 9 );
		erro+= checa_caracter( form.elements['cep_'+contador], '0123456789-' );
		erro+= checa_nulo( form.elements['estado_'+contador] );
		
		// Verificar aluno com mesmo email
		if ( str_emails_participantes.indexOf( form.elements['email_'+contador].value ) >= 0 ){
			erro+= '- Não é permitido usar o mesmo e-mail para mais de um participante.';	
		}
		
		str_emails_participantes = str_emails_participantes +'|'+ form.elements['email_'+contador].value ;
		
		if ( erro != "" ){
			break;	
		}
	}
	
	if ( erro == "" && form.razao ){
		erro+= checa_nulo( form.razao );
		erro+= checa_tamanho( form.cnpj, 18 );
		erro+= checa_caracter( form.cnpj, '0123456789.-/' );
		erro+= checa_cnpj( form.cnpj );
		erro+= checa_nulo( form.endereco_empresa );
		erro+= checa_nulo( form.cidade_empresa );
		erro+= checa_tamanho( form.cep_empresa, 9 );
		erro+= checa_caracter( form.cep_empresa, '0123456789-' );
		erro+= checa_nulo( form.estado_empresa );
	}
	
	if ( erro != "" ){
		notifica( erro, 'atencao', 10 );
		return false;
	}else{
		exibe_carregamento( true );
		return true;
	}
	
	return(false);	
}

function sucessoDados(data){
	if (data.erro == 0){
		exibe_carregamento( true );
		encaminhar_pagina(url_site+'/matricula/inscricao', {'ID_inscricao':data.ID_inscricao}, 'post');
		
	}else{
		exibe_carregamento( false );
		notifica( data.mensagem, 'erro', 5 );
	}
}