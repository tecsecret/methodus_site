<?php
include_once '../../api/biblioteca/funcoes.php';
include_once '../biblioteca/variaveis.php';


$ID				= formataVar( 'ID', 'POST' );
$ID_data		= formataVar( 'ID_data', 'POST' );
$ID_plano		= formataVar( 'ID_plano', 'POST' );
$participantes	= formataVar( 'participantes', 'POST' );

// Dados do curso
$xml_curso 	= executaPagina( 'api/', array( 'a'=>'cursos', 'metodo'=>'ver', 'ID'=>$ID ) );
$curso 		= lerXML( $xml_curso );
	
if ( $curso->erro == 0 ){
	
	$curso				= $curso->curso;
	$titulo_curso		= $curso->titulo;
	$url_curso			= $curso->url;
	$distancia_curso	= $curso->distancia;
	$banner_curso		= $curso->banner;
	$resumo_curso		= $curso->resumo;
	$descricao_curso	= $curso->descricao;
	$metatags_curso		= $curso->metatags;
	$variaveis_curso	= $curso->variaveis;
	$planos_curso		= $curso->planos;
	$turmas_curso		= $curso->turmas->turma;
	$perfis_curso		= $curso->perfis->perfil;
	
	if ( isset( $variaveis_curso->classe ) ){
		$classe_curso = $variaveis_curso->classe;
	}else{
		$classe_curso = 'vermelho';	
	}
	
	if ( isset( $curso->planos->plano ) == false ){
		exit("Erro ao acessar planos do curso");		
	}else{
			
		$plano_escolhido = $curso->planos->xpath("plano[@codigo='".$ID_plano."']");
		if ( count( $plano_escolhido ) >= 1 ){
			$plano_escolhido = $plano_escolhido[0];	
		}else{
			exit("Erro com o plano escolhido");		
		}
			
	}
	
	if ( isset( $curso->turmas->turma ) == false ){
		exit("Erro ao acessar turmas do curso");		
	}else{
			
		$data_escolhida = $curso->turmas->xpath("turma[@codigo='".$ID_data."']");
		if ( count( $data_escolhida ) >= 1 ){
			$data_escolhida = $data_escolhida[0];	
		}else{
			exit("Erro com a turma escolhida");		
		}
			
	}
	
	// verificar se é plano empresarial
	if ( $plano_escolhido['empresarial'] == '1' ){
		$plano_empresarial = true;	
	}else{
		$plano_empresarial = false;		
	}
	
}else{
	exit("Erro ao acessar os dados do curso");		
}

/* CABEÇALHO */
include_once '../biblioteca/cabecalho.php';
?>

<div class="main-container">
    <section class="page-title page-title-4 image-bg overlay parallax">
      <div class="background-image-holder"> <img alt="Background Image" class="background-image" src="<?php echo $banner_curso; ?>"> </div>
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <h1 class="titulo-curso"><?php echo $titulo_curso; ?></h1>
          </div>
        </div>
      </div>
    </section>
    
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="text-center">
                        <hr>
                        <p>
							<?php
							if ( $distancia_curso == 0 ){
								echo ( $data_escolhida->horario . ' / de ' . formataData( $data_escolhida->inicio, 'tela' ) . ' à ' . formataData( $data_escolhida->fim, 'tela' ) ) ;
							}else{
								echo 'Seu Acesso será Permanente pela Área do Aluno.								';
							}
							?>
                        </p>
                    </div>
                    
                    <script src="<?php echo $url_site; ?>/matricula/cadastro.js"></script>
                    <form action="<?php echo $url_api; ?>" method="post" class="customer-details mb80 mb-xs-40 formAjax" data-validacao="checaDados" data-resultado="sucessoDados">
                        <input type="hidden" name="a" value="matricula" />
                        <input type="hidden" name="metodo" value="inscricao" />
                        <input type="hidden" name="participantes" value="<?php echo $participantes; ?>" />
                        <input type="hidden" name="ID_curso" value="<?php echo $ID; ?>" />
                        <input type="hidden" name="ID_data" value="<?php echo $ID_data; ?>" />
                        <input type="hidden" name="ID_plano" value="<?php echo $ID_plano; ?>" />
                        
                        <?php
						for ( $i=1; $i <= $participantes; $i++ ){
						?>
                            <div class="text-center" style="clear:both;">
                                <hr />
                                <h4 class="uppercase">Participante <?php echo $i; ?></h4>
                            </div>
                            <div class="input-with-label col-sm-6 text-left">
                                <span>Nome Completo</span>
                                <input type="text" placeholder="Nome completo" lang="Nome do participante <?php echo $i; ?>" maxlength="150" name="nome_<?php echo $i; ?>" />
                            </div>
                            <div class="input-with-label col-sm-6 text-left">
                                <span>Data de Nascimento</span>
                                <input type="text" placeholder="Data de Nascimento" lang="Data de Nascimento do participante <?php echo $i; ?>" maxlength="10" name="nascimento_<?php echo $i; ?>" oninput="mascara(this,mascaraData);" />
                            </div>
                            <div class="input-with-label col-sm-6 text-left">
                                <span>CPF</span>
                                <input type="text" placeholder="CPF" lang="CPF do participante <?php echo $i; ?>" maxlength="14" name="cpf_<?php echo $i; ?>" oninput="mascara(this,mascaraCpf);" />
                            </div>
                            <!-- <div class="input-with-label col-sm-6 text-left">
                                <span>RG</span>
                                <input type="text" placeholder="RG" lang="RG do participante <?php echo $i; ?>" maxlength="20" name="rg_<?php echo $i; ?>" />
                            </div> -->
                            <div class="input-with-label col-sm-6 text-left">
                                <span>Whatsapp</span>
                                <input type="text" placeholder="DDD + Celular" lang="Whatsapp do participante <?php echo $i; ?>" maxlength="15" name="tel_<?php echo $i; ?>" oninput="mascara(this,mascaraTelefone);" />
                            </div>
                            <div class="input-with-label col-sm-6 text-left">
                                <span>E-mail</span>
                                <input type="text" placeholder="E-mail" lang="E-mail do participante <?php echo $i; ?>" maxlength="150" name="email_<?php echo $i; ?>" />
                            </div>
                            <div class="input-with-label col-sm-12 text-left">
                                <span>Dados de endereço</span>
                                <input type="text" placeholder="Endereço Completo" lang="Endereço do participante <?php echo $i; ?>" maxlength="150" name="endereco_<?php echo $i; ?>" />
                            </div>
                            <div class="input-with-label col-sm-6 text-left">
                                <span>Cidade</span>
                                <input type="text" placeholder="Cidade" lang="Cidade do participante <?php echo $i; ?>" maxlength="150" name="cidade_<?php echo $i; ?>" />
                            </div>
                            <div class="input-with-label col-sm-3 text-left">
                                <span>CEP</span>
                                <input type="text" placeholder="CEP" lang="CEP do participante <?php echo $i; ?>" maxlength="9" name="cep_<?php echo $i; ?>" oninput="mascara(this,mascaraCep);" />
                            </div>
                            <div class="input-with-label col-sm-3 text-left">
                                <span>Estado</span>
                                <select name="estado_<?php echo $i; ?>" lang="Estado do participante <?php echo $i; ?>">
                                  <option value="">Selecione</option>
                                  
                                    <?php
									if ( isset( $estados ) == false )
                                    	$estados = lerXML( '../../facix2/_xml/estados.xml' );
                                    
                                    foreach( $estados as $estado ){
                                        echo '<option value="'.$estado.'">'.$estado.'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            
                        <?php
						}
						
						// Se é plano empresarial, exibir formulário para dadso da empresa
						if ( $plano_empresarial == true ){
						?>
                        	<div class="text-center" style="clear:both;">
                                <hr />
                                <h4 class="uppercase">Dados da empresa</h4>
                            </div>
                            <div class="input-with-label col-sm-6 text-left">
                                <span>Razão Social</span>
                                <input type="text" placeholder="Razão Social" lang="Razão Social" maxlength="150" name="razao" />
                            </div>
                            <div class="input-with-label col-sm-6 text-left">
                                <span>CNPJ</span>
                                <input type="text" placeholder="CNPJ" lang="CNPJ" maxlength="19" name="cnpj" oninput="mascara(this,mascaraCnpj);" />
                            </div>
                            <div class="input-with-label col-sm-12 text-left">
                                <span>Dados de endereço</span>
                                <input type="text" placeholder="Endereço Completo" lang="Endereço da empresa" maxlength="150" name="endereco_empresa" />
                            </div>
                            <div class="input-with-label col-sm-6 text-left">
                                <span>Cidade</span>
                                <input type="text" placeholder="Cidade" lang="Cidade da empresa" maxlength="150" name="cidade_empresa" />
                            </div>
                            <div class="input-with-label col-sm-3 text-left">
                                <span>CEP</span>
                                <input type="text" placeholder="CEP" lang="CEP da empresa" maxlength="9" name="cep_empresa" oninput="mascara(this,mascaraCep);" />
                            </div>
                            <div class="input-with-label col-sm-3 text-left">
                                <span>Estado</span>
                                <select name="estado_empresa" lang="Estado da empresa">
                                  <option value="">Selecione</option>
                                  
                                    <?php
									if ( isset( $estados ) == false )
                                    	$estados = lerXML( '../../facix2/_xml/estados.xml' );
										
                                    foreach( $estados as $estado ){
                                        echo '<option value="'.$estado.'">'.$estado.'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        <?php
						}
						?>
                        <input type="submit" class="pull-right" value="Submeter Inscrição" />
                    </form>
                </div>
            </div>
        </div>
    </section>
    
</div>


<?php
/* RODAPÉ */	
include_once '../biblioteca/rodape.php';
?>
