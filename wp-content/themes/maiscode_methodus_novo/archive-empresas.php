<?php
/**
* Template Name: Página Empresas
*
* @package WordPress
* @author Mais Code Tecnologia
* @since First Version
*/ 
get_header();?>

<section id="artigos-pagina">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-9 mb-xs-24" id="conteudo_paginado">
            	<div class="post-title">
            		<h1 class="inline-block"><?php the_field('empresas_recomendadas',401); ?></h1>
            	</div>
            	<hr>
                <?php 

                $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

                $limit = 10;
                $offset = $limit * ($paged - 1);
                //var_dump($paged);
                $args = array(
                    'post_type'      => 'empresas',
                    //'posts_per_page' => $limit,
                    'offset'         => $offset,
                    'orderby'        => 'date',
                    'order'          => 'ASC',
                );

                $postsList = new WP_Query($args);

                ?>
            	<?php if ($postsList->have_posts()): ?>

                    <?php while($postsList->have_posts()) : $postsList->the_post(); 
                    	$image = wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() )); 
                    	$site = get_field('site_empresa');
                    	//$sobre = get_field('breve_descricao_da_empresa');
                    	$telefone = get_field('telefone_empresa');
                    	$email = get_field('e-mail_empresa');
                    ?>
	                    <div class="post-snippet mb64" style="clear:both;">
	                    	<div class="post-title">
	                    		<h4 class="inline-block"><?php the_title(); ?></h4>
	                    		<span class="label">
	                    			<a href="<?php echo $site; ?>" target="_blank"><?php echo $site; ?></a>
	                    		</span>	
	                    	</div>
	                    	<hr>
	                    	<p>	
	                    		<img class="mb24 thumb_pequeno" src="<?php echo $image; ?>"><?php the_excerpt(); ?><br><?php echo $telefone; ?><br>
	                    		<a href="<?php echo $email; ?>"><?php echo $email; ?></a>
	                    	</p>
	                    </div>
                    <?php endwhile ?>
                <?php endif ?>
                <?php paginacao($limit); ?>
            </div>

            <div class="col-md-3 hidden-sm widget-methodus">
                <div class="busca-widget">
                <form action="<?php bloginfo('siteurl'); ?>" id="searchform" method="get">
                        <div class="input-artigo">
                            <input type="hidden" name="tipo" value="empresas" id="empresa">
                            <input placeholder="Pesquisar por" type="text" name="s" value="<?php the_search_query(); ?>" />                            
                            <input type="submit" class="btn btn-input btn-sm" value="Buscar" id="searchsubmit" />
                            
                        </div>
                    </form>


                      <h6 class="title"><?php the_field('texto_mais_acessados',326); ?></h6>
                      
                      <ul class="link-list">



                        <?php 
                        $args = array(
                            'post_type' => 'artigos',
                            'post_status' => 'publish',
                            'posts_per_page' => '20',
                            'meta_key' => 'post_views_count',
                            'order' => 'DESC'
                        );
                        $my_posts = new WP_Query( $args );

                        if ( $my_posts->have_posts() ) : 
                            ?>

                            <?php while ( $my_posts->have_posts() ) : $my_posts->the_post(); 

                              ?>

                              <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>

                          <?php endwhile; ?>
                      <?php endif; ?>

                  </ul>


                </div>

        </div>
    </div>
</section>


<?php get_footer(); ?>