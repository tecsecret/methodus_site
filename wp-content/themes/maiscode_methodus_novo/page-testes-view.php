<?php
/**
* Template Name: Página Teste Single
*
* @package WordPress
* @author Mais Code Tecnologia
* @since First Version
*/
get_header();



echo		'<section>
				<div class="container">';

// Conteúdo principal
echo 	'<div class="post-title">
			<h1 class="inline-block">Testes on-line</h1>
		</div>
		<hr>';
	
?>
    
    <div class="row">
		<div class="col-sm-6 col-md-6">
            
            <h3>TESTE DE LEITURA</h3>
            
            <p>
                Avalie sua capacidade de compreender e reter as informações.
                <BR /><a class="btn btn-sm" href="javascript:" onclick="janela('leitura/','800','600','yes','yes');">Iniciar Teste</a>
            </p>
            
     	</div>
    	<div class="col-sm-6 col-md-6">
                
            <h3>TESTES DE MEMORIZAÇÃO</h3>
            
            <p>
                <strong>Memorizar Palavras</strong><BR />
                Memorizar as palavras em seus respectivos lugares.<BR />
                Tempo: 1 Minuto.
                <BR /><a class="btn btn-sm" href="javascript:" onclick="janela('memoria-palavras/','500','600','yes','yes');">Iniciar Teste</a>
            </p>
            
            <p>
                <strong>Memorizar Números e Palavras</strong><BR />
                Memorizar as palavras com os números respectivos.<BR />
                Tempo: 1 Minuto.
                <BR /><a class="btn btn-sm" href="javascript:" onclick="janela('memoria-relacao/','500','600','yes','yes');">Iniciar Teste</a>
            </p>
            
            <p>
                <strong>Memorizar Sequências Numéricas</strong><BR />
                Memorizar os números em seus respectivos lugares.<BR />
                Tempo: 1 Minuto.
                <BR /><a class="btn btn-sm" href="javascript:" onclick="janela('memoria-numeros/','500','250','yes','yes');">Iniciar Teste</a>
            </p>
		
        </div>
 	</div>

<?php


echo 	'		</div>
    		</section>';

?>

<?php get_footer(); ?>