<?php

$category = get_the_category();
  $id = $category[0]->term_id;
  $parent = $category[0]->parent;


$url_api  =  get_option('link_api'); 

$ID = get_field('id_do_curso');

if ( isset($ID_curso)==false && $ID ){
	$ID_curso = $ID;
}elseif ( isset($ID_curso)==false && isset($ID)==false ){
	exit("Erro: Curso não encontrado.");
}

// Se dados do curso já não estão setados, buscar dados do curso
if ( isset($curso) == false ){
	
	// Dados do curso
	$xml_curso 	= executaPagina( 'api/', array( 'a'=>'cursos', 'metodo'=>'ver', 'ID'=>$ID_curso ) );
	$curso 		= lerXML( $xml_curso );
		
	if ( $curso->erro == 0 ){
		
		$curso				= $curso->curso;
		$titulo_curso		= $curso->titulo;
		$variaveis_curso	= $curso->variaveis;
		$turmas_curso		= $curso->turmas->turma;
		$perfis_curso		= $curso->perfis->perfil;
		$distancia_curso	= $curso->distancia;
		$id_tema			= $curso->tema;
		
		if ( isset( $variaveis_curso->classe ) ){
			$classe_curso = $variaveis_curso->classe;
		}else{
			$classe_curso = 'vermelho';	
		}
		
	}else{
		exit("Erro ao acessar os dados do curso");		
	}

}

$urlWP = get_template_directory_uri();
?>



  <div class="box-contato" id="investir">
    <?php /*<h2 class="titulo-box <?php echo $classe_curso; ?>"><?php echo str_replace('Curso de ','',$titulo_curso); ?></h2>*/ ?>
    <div class="interno-box proximas-turmas" style="display: none;">
    <?php 
	if ( $distancia_curso == '1' ){ 
		
		echo 	'<video width="100%" autoplay loop>
				  	<source src="'.$url_site.'/img/Projetor.mp4" type="video/mp4">
					Seu navegador não suporta esse vídeo
				</video><BR /><BR />';
	
	}else{
		
		if ( isset($pagina_curso)==false ){

		
			   if($turmas_curso === NULL ){
                    
                    //Se  não tiver turma não faz nada

                }else if ( count( $turmas_curso ) > 0 ){
				if($id_tema==6){
					echo '<span class="texto-escuro"> Próximas Mentorias</span><br>';

					echo '<p>Individuais ou grupos até 3 pessoas</p>';

						echo 	'<p><span class="texto-escuro">Segunda à sexta – 8h às 18h</span><BR/>
								<span class="texto-escuro">Sábados e domingos – 8h às 18h</span></p>';
								
				}else{
					echo '<span class="texto-escuro"> Próximas Turmas</span><br>';

					echo '<p>Grupos com '.$turmas_curso[0]->quantidade_vagas.' alunos</p>';
					foreach( $turmas_curso as $turma ){
						echo 	'<p><span class="texto-'.$classe_curso.'"><b>'.formataData( $turma->inicio, 'tela' ).' à '.formataData( $turma->fim, 'tela' ).' </b></span> <br>
								<span class="texto-escuro">('.$turma->horario.')</span></p>';
					}
				}
			}
			
		}
            
	}
	?>
    </div>
    
    <h4><strong>Preencha aqui seus dados</strong></h4>
    <div class="interno-box">
      <script src="<?php echo $urlWP; ?>/js/scripts_site/investimento.js?ver=1"></script>
      <form action="<?php echo $url_api; ?>/api/" method="post" class="form-email formAjax" id="investimento" data-validacao="checaCurso" data-resultado="sucessoCurso" data-curso="<?php echo $ID_curso; ?>">
        <input type="hidden" name="a" value="aluno" />
        <input type="hidden" name="metodo" value="cadastro" />
        <input type="hidden" name="origem" value="Investimento" />
        <input type="hidden" name="curso" value="<?php echo $ID_curso; ?>" />
        <input type="text" class="validate-required" name="nome" placeholder="Nome" lang="Nome">
        <input type="text" class="validate-required validate-email" name="email" placeholder="Email" lang="E-mail">
		<?php
		$i = 0;
		/*if( have_rows('formulario', $id_post) ):
			while ( have_rows('formulario', $id_post) ) : the_row(); $i++ ?>	
				<div class="radio_cursos">	
					<label for="<?php echo $i; ?>" class="radio">
						<input type="radio" class="validate-required" id="<?php echo $i; ?>" name="conheceu" value="<?php the_sub_field('opcoes', $id_post); ?>" lang="opções">
						<span></span>
					</label>					
					<label for="<?php echo $i; ?>"><?php the_sub_field('opcoes', $id_post); ?></label><br>
				</div>
			<?php endwhile;
		endif;*/
		?>
        <!-- <p class="pl20"><strong>Quem é você?</strong></p> -->
          
		<?php
		/*if ( count( $perfis_curso ) > 0 ){
			foreach( $perfis_curso as $perfil ){
				echo '<label class="radio"><input type="radio" name="perfil" value="'.$perfil['codigo'].'" /><span></span> '.$perfil.'</label>';
			}
		}*/
		?>

        <br />
        <button class="mainClass" type="submit"><?php the_field('nome_cta_featured') ?></button>
        <!-- <button class="<?php echo $classe_curso; ?>" type="submit">
        <?php
		if ( $distancia_curso == '1' ){
			echo 'Adquira agora: Livro + CD';
		}else{
			echo 'Realizar Inscrição Online';	
		}
		?>
        </button> -->
	  </form>



    </div>
  </div>
