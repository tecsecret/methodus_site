<?php /*
if ( isset($identificador_form_generico) == false ){
    $identificador_form_generico = 'popup'; 
}
 $url_api          = get_option('link_api');
?>
<style type="text/css">
  .hidden {
    display: none;
  }
</style>
<section id="secao_generico_<?php echo $identificador_form_generico; ?>" class="hidden" style="background: linear-gradient(90deg, #304DA5 47.4%, rgba(48, 77, 165, 0.14) 100%), url(<?php echo esc_url( get_template_directory_uri() ); ?>/img/home-newsletter.png);">
<div class="container">
  <div class="row">  
    <div class="col-lg-6 text-center">
      <ul class="accordion accordion-1 mb0">
        <li class="active">
          <div class="title"> <span style="font-size: 24px">Quero manter-me atualizado</span> </div>
          <div class="content">
            <div id="bio_ep_content">
              <form action="<?= $url_api; ?>/api/" id="generico_<?php echo $identificador_form_generico; ?>" method="post" class="formAjax" data-validacao="checaGenerico" data-resultado="sucessoGenerico">
              <input type="hidden" name="a" value="aluno" />
              <input type="hidden" name="metodo" value="cadastro" />
              <input type="hidden" name="origem" value="Newsletter" />
              <div id="leftpopup">
                <input type="text" name="nome" placeholder="Nome" lang="Nome">
                <input type="text" name="email" placeholder="Email" lang="E-mail">
              </div>
              <div id="rightpopup">
                <?php
                  $xml_cursos     = executaPagina( 'api/', array( 'a'=>'cursos', 'metodo'=>'listar' ) );
                  $lista_cursos   = lerXML( $xml_cursos );
                  
                  if ( $lista_cursos->erro == 0 ){
                      $lista_cursos = $lista_cursos->cursos->curso;
                      $cont = 0;
                      foreach ($lista_cursos as $lista_curso){
                        $cont++;
                        $classGrup = ($cont > 3) ? 'group-label' : '';
                          echo    '<div class="radio_cursos '.$classGrup.' ">
                                      <label class="radio"><input type="radio" name="curso" value="'.$lista_curso['codigo'].'" id="radio_curso_'.$identificador_form_generico.'_'.$lista_curso['codigo'].'" /><span></span></label> 
                                      <label for="radio_curso_'.$identificador_form_generico.'_'.$lista_curso['codigo'].'" style="cursor:pointer;">'.$lista_curso->titulo.'</label>
                                  </div>';
                      }
                  }
                  ?>
              </div>
              <!--<a href="#YOURURLHERE" class="bio_btn" style="margin-bottom: 20px; margin-top: 0px">QUERO MANTER-ME ATUALIZADO</a> -->
              <input type="button" class="bio_btn" style="margin-bottom: 20px; margin-top: 0px" value="QUERO MANTER-ME ATUALIZADO" onclick="$('#generico_<?php echo $identificador_form_generico; ?>').submit();" />
              </form>
            </div>
          </div>
        </li>
      </ul>
    </div>    
  </div>
</div>

</section> */ ?>