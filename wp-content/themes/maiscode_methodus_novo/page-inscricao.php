<?php
/**
* Template Name: Página Inscricao
* 
*
* @package WordPress
* @author Mais Code Tecnologia
* @since First Version
*/

get_header();
$ID_cupom = $_POST['ID_cupom'];

$singleInCash = 30;
$singleInPeriod = 20;

$groupInCash = 35;
$groupInPeriod = 25;

$urlWP = get_template_directory_uri();

// URL utilizada para caminhos de arquivos (URLs amigáveis)
$url_site = get_option('link_site'); 
$url_site_wp     =  site_url();
$url_api      =  get_option('link_api');

// URLs amigáveis
$url_amigavel_on  = 1;
$url_amigavel    =  get_option('link_site_amigavel');
 
//$ID_inscricao   = formataVar( 'ID_inscricao', '*' ); //, 'POST' ); // para teste, vou permitir enviar ID da inscrição por GET

// Variável estática para testar checkout 
$ID_inscricao = $_POST["ID_inscricao"];

// Dados da inscrição
$xml_inscricao  = executaPagina( 'api/', array( 'a'=>'matricula', 'metodo'=>'ver', 'ID'=>$ID_inscricao ) );
$inscricao      = lerXML( $xml_inscricao );

$ID_curso = '';
    
if ( $inscricao->erro == 0 ){
    
    $inscricao          = $inscricao->inscricao;
    $ID_curso           = $inscricao->ID_curso;
    $ID_plano           = $inscricao->ID_plano;
    $ID_data            = $inscricao->ID_data;
    $qtd_inscritos      = count( $inscricao->participantes->participante );
    
}else{
    exit("Erro ao acessar os dados da inscrição");  
}

// Dados do curso
$xml_curso  = executaPagina( 'api/', array( 'a'=>'cursos', 'metodo'=>'ver', 'ID'=>(int)$ID_curso) );
$curso      = lerXML( $xml_curso );
    
if ( $curso->erro == 0 ){
    
    $curso              = $curso->curso;
    $titulo_curso       = $curso->titulo;
    $url_curso          = $curso->url;
    $distancia_curso    = $curso->distancia;
    $banner_curso       = $curso->banner;
    $resumo_curso       = $curso->resumo;
    $descricao_curso    = $curso->descricao;
    $metatags_curso     = $curso->metatags;
    $variaveis_curso    = $curso->variaveis;
    $planos_curso       = $curso->planos;
    //var_dump($planos_curso);
    $turmas_curso       = $curso->turmas->turma;
    $perfis_curso       = $curso->perfis->perfil;
    
    if ( isset( $variaveis_curso->classe ) ){
        $classe_curso = $variaveis_curso->classe;
    }else{
        $classe_curso = 'vermelho'; 
    }
    
    if ( isset( $curso->planos->plano ) == false ){
        exit("Erro ao acessar planos do curso");        
    }else{
            
        $plano_escolhido = $curso->planos->xpath("plano[@codigo='".$ID_plano."']");
        if ( count( $plano_escolhido ) >= 1 ){
            $plano_escolhido = $plano_escolhido[0]; 
        }else{
            exit("Erro com o plano escolhido");     
        }
            
    }
    
    if ( isset( $curso->turmas->turma ) == false ){
        exit("Erro ao acessar turmas do curso");        
    }else{
            
        $data_escolhida = $curso->turmas->xpath("turma[@codigo='".$ID_data."']");
        if ( count( $data_escolhida ) >= 1 ){
            $data_escolhida = $data_escolhida[0];   
        }else{
            exit("Erro com a turma escolhida");     
        }
            
    }
    
    // verificar se é plano empresarial
    if ( $plano_escolhido['empresarial'] == '1' ){
        $plano_empresarial = true;  
    }else{
        $plano_empresarial = false;     
    }
    
}else{
    exit("Erro ao acessar os dados do curso..");      
}

// Verificar preço a vista e a prazo
if ( $qtd_inscritos <= 1 || $plano_escolhido['desconto2'] <= 0 ){

    $investimento_aprazo = ( $plano_escolhido - ($plano_escolhido * ( $singleInPeriod / 100 )) );

    //BLACK FRIDAY
    //$investimento_aprazo = $plano_escolhido;
    
    /*if ( $plano_escolhido['desconto1'] > 0 ){
       $investimento_avista = ( $plano_escolhido - ($plano_escolhido * ( $plano_escolhido['desconto1'] / 100 )) );*/

      if ( $plano_escolhido['desconto1'] > 0 ){
       $investimento_avista = ( $plano_escolhido - ($plano_escolhido * ( $singleInCash / 100 )) );

    }else{
        $investimento_avista = $investimento_aprazo;    
    }
    if (isset($_POST['ID_cupom'])) {
      $descCupom = 0.1;
      $investimento_avista= ($investimento_avista - ($investimento_avista * $descCupom)) *$qtd_inscritos;
      $investimento_aprazo= ($investimento_aprazo - ($investimento_aprazo * $descCupom)) *$qtd_inscritos;
    } else {
      $investimento_avista = $investimento_avista * $qtd_inscritos;
      $investimento_aprazo = $investimento_aprazo * $qtd_inscritos;
    }
    
}else{
    
    //$investimento_avista = ( $plano_escolhido - ($plano_escolhido * ( $plano_escolhido['desconto2'] / 100 )) );

    //BLACK FRIDAY
    $investimento_avista = ( $plano_escolhido - ($plano_escolhido * ( $groupInCash / 100 )) );
    
    if ( $plano_escolhido['desconto3'] > 0 ){
        //BLACK FRIDAY
        $investimento_aprazo = ( $plano_escolhido - ($plano_escolhido * ( $groupInPeriod / 100 )) );
        //$investimento_aprazo = ( $plano_escolhido - ($plano_escolhido * ( $plano_escolhido['desconto3'] / 100 )) );
    }else{
        $investimento_aprazo = ( $plano_escolhido - ($plano_escolhido * ( $groupInPeriod / 100 )) );
        //$investimento_aprazo = $investimento_avista;    
    }

   if (isset($_POST['ID_cupom'])) {
      $descCupom = 0.1;
      $investimento_avista= ($investimento_avista - ($investimento_avista * $descCupom)) *$qtd_inscritos;
      $investimento_aprazo= ($investimento_aprazo - ($investimento_aprazo * $descCupom)) *$qtd_inscritos;
   } else {
      $investimento_avista = $investimento_avista * $qtd_inscritos;
      $investimento_aprazo = $investimento_aprazo * $qtd_inscritos;
   }
}

// Verificar limite de parcelamento
$parcelamento = $plano_escolhido['parcelas'];


?>


<div class="main-container">

    <section class="page-title page-title-4 image-bg overlay parallax">
      <div class="background-image-holder"> <img alt="Background Image" class="background-image" src="<?php echo $urlWP; echo $banner_curso; ?>"> </div>
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <h1 class="titulo-curso"><?php echo $titulo_curso; ?></h1>
          </div>
        </div>
      </div>
    </section>
    
    <section>
        <div class="container">
           <div class="row mb0">
           
              <div class="col-sm-12">
                 <h2 class="number"><?php the_field('titulo_inscrição'); ?></h2>
                 <hr>
                 <?php if($ID_curso == 5) { ?>
                  <form name="checkCupom" id="checkCupom" action="" method="post">
                    <div class="cupomField">
                      <label>Adicione um cupom de desconto</label>
                      <input type="text" name="ID_cupom" id="cupomInput" placeholder="Insira seu cupom de desconto aqui">
                      <div id="cupomValidation">
                        <?php if(isset($_POST['ID_cupom'])) { ?>
                          <p class="valid">Cupom válido!</p>
                        <?php } ?>
                      </div>
                      <button type="submit" class="vermelho" id="cupomValidator">Validar cupom</button>
                    </div>
                    <input type="hidden" name="ID_inscricao" value="<?php echo $ID_inscricao; ?>" />
                  </form>
                 <?php } ?>

                 <?php //BLACK FRIDAY ?>
                <form name="blackFriday" id="blackFriday" action="" method="post">
                  <input type="hidden" name="cupom_percent" value="" />
                </form>

                 <strong><?php the_field('curso'); ?></strong> <?php echo $titulo_curso; ?><BR />
                 <strong><?php the_field('grupo'); ?></strong> <?php echo ( $data_escolhida->horario . ' / de ' . formataData( $data_escolhida->inicio, 'tela' ) . ' à ' . formataData( $data_escolhida->fim, 'tela' ) ) ;?><BR />
                 <strong><?php the_field('quantidade_de_inscricoes'); ?></strong> <?php echo $qtd_inscritos; ?><BR />
                 <p class="mb0">Número de inscrição: <span class="texto-<?php echo $classe_curso; ?>"><?php echo $ID_inscricao; ?></span></p>
                 <p class="mb0"><?php the_field('investimento_a_vista'); ?><strong>R$ <?php echo formataMoeda( $investimento_avista, 'tela' ) ?></strong></p>
                 <p class="mb0"><?php the_field('investimento_a_prazo'); ?><strong>R$ <?php echo formataMoeda( $investimento_aprazo, 'tela' ) ?></strong></p>
              </div>

           </div>
        </div>
    </section>
    <?php if(isset($_POST['ID_cupom'])) {
      $cupomData = true;
    } else {
      $cupomData = false;
    }?>
    <script src="<?php echo $urlWP; ?>/matricula/meio.js?ver=<?php echo rand(111,999)?>"></script>
    <section>
       <div class="container">
          <div class="row">
            <h3><?php the_field('titulo_parcelamento'); ?></h3><hr>
            <div class="input-with-label col-sm-3 text-left">
                <span><?php the_field('label_parcelamento'); ?></span>
                <select id="forma" lang="Parcelamento" 
                data-qtd="<?php echo $qtd_inscritos; ?>" 
                data-inscricao="<?php echo $ID_inscricao; ?>" 
                data-distancia="<?php echo $distancia_curso; ?>" 
                data-cupom="<?php echo $cupomData; ?>">
                    <option value="">Selecione uma opção</option>

                    <option value="1" data-valor="<?php echo $investimento_avista ?>">à vista por R$  <?php echo formataMoeda( $investimento_avista, 'tela' ) ?></option>
                    <?php
                    for($i=2;$i<=$parcelamento;$i++){
                        echo '<option value="'.$i.'" data-valor="'.($investimento_aprazo/$i).'">Em '.$i.'x de R$ '.formataMoeda( ($investimento_aprazo/$i), 'tela' ).'</option>';
                    }
                    ?>
                </select>
            </div>
          </div>
       </div>
    </section>
    
    <section class="pb64 pb-xs-40">
       <div class="container">
          <div class="row">
          
          <h3>Selecione sua forma de pagamento</h3><hr>
             <div class="col-sm-3">
                <div class="feature feature-2 filled text-center">
                   <div class="text-center meio_botao borda-<?php echo $classe_curso; ?>" onclick="escolheMeio('');">
                      <i class="ti ti-credit-card icon-sm <?php echo $classe_curso; ?>"></i>
                      <h5 class="uppercase">CARTÃO DE CRÉDITO</h5>
                   </div>
                   <p>Para pagamentos à vista ou em até 12 vezes.<br>VISA, MasterCard ou American Express.</p>
                </div>
             </div>
             <div class="col-sm-3">
                <div class="feature feature-2 filled text-center">
                   <div class="text-center meio_botao borda-<?php echo $classe_curso; ?>" onclick="escolheMeio('6');">
                      <i class="fa fa-barcode icon-sm <?php echo $classe_curso; ?>"></i>
                      <h5 class="uppercase">Boleto Bancário</h5>
                   </div>
                   <p>
                        <!-- Para pagamento à vista ou primeira parcela  <br>As demais parcelas serão acertadas na primeira aula através de cartão de crédito ou 
              cheques pré-datados em até 11 vezes.&nbsp; -->
              Para pagamento à vista 
            <?php
            //if ( $distancia_curso == 0 ){
            //  echo ' ou parcelado em até 6 vezes';
            //}
            ?>
                   </p>
                </div>
             </div>
             <div class="col-sm-3">
                <div class="feature feature-2 filled text-center">
                   <div class="text-center meio_botao borda-<?php echo $classe_curso; ?>" onclick="escolheMeio('4');">
                      <i class="ti ti-money icon-sm <?php echo $classe_curso; ?>"></i>
                      <h5 class="uppercase">Depósito bancário</h5>
                   </div>
                   <p>Para pagamento à vista ou primeira parcela  <br>As demais parcelas serão acertadas na primeira aula através de cartão de crédito ou cheques pré-datados em até 11 vezes.&nbsp;<br>Banco Bradesco - Agência 3114-3 - Conta 169361-1<br>Methodus Consultoria<br></p>
                </div>
             </div>
             <div class="col-sm-3">
                <div class="feature feature-2 filled text-center">
                   <div class="text-center meio_botao borda-<?php echo $classe_curso; ?>" onclick="escolheMeio('0');">
                      <i class="fa fa-pencil-square-o icon-sm <?php echo $classe_curso; ?>"></i>
                      <h5 class="uppercase">Cheques pré-datados</h5>
                   </div>
                   <p>Parcelamento em até 12 vezes na unidade Methodus.<br></p>
                </div>
             </div>
          </div>
       </div>
    </section>
    
    <?php /*

    <section class="pb64 pb-xs-40">
       <div class="container">
          <div class="row">
          <h3><?php the_field('titulo_formas_de_pagamento'); ?></h3><hr>
          <?php if (have_rows('formas_de_pagamento')): ?>
            <?php while(have_rows('formas_de_pagamento')): the_row(); ?>
              <div class="col-sm-3">
                <div class="feature feature-2 filled text-center">
                  <div class="text-center borda-<?php echo $classe_curso; ?>">
                    <i class="<?php the_sub_field('icone_formas_de_pagamento'); ?> <?php echo $classe_curso; ?>" style="background: none; border:0;"></i>
                    <h5 class="uppercase"><?php the_sub_field('titulo_formas_de_pagamento'); ?></h5>
                  </div>
                  <p><?php the_sub_field('texto_formas_de_pagamento'); ?></p>
                </div>
              </div>
            <?php endwhile ?>
          <?php endif ?>
          </div>
       </div>
    </section>
    */
     ?>
    
</div>
<style type="text/css">
  .cupomField {
      width: 30%;
  }
  .cupomField button{
    border: none;
    padding: 5px 25px;
    margin-top: 5px;
    height: 45px;
    line-height: 38px;
    width: 110px;
  }
  .cupomField input{
    margin-bottom: 0px;
    text-transform: uppercase;
  }
  .cupomField::-webkit-input-placeholder {
      text-transform: none;
  }
  .cupomField:-moz-placeholder {
      text-transform: none;
  }
  .cupomField::-moz-placeholder {
      text-transform: none;
  }
  .cupomField:-ms-input-placeholder {
      text-transform: none;
  }
  .cupomField p {
    margin: 0px;
    font-weight: 700;
    font-size: 12px;
  }
  .cupomField .valid {
    color: green;
  }
  .cupomField .invalid {
    color: #a00000;
  }
</style>
<script type="text/javascript">
$(document).ready(function(){
  $("#cupomValidator").click(function(){

    $( "#cupomValidation" ).empty();
    var value = $("#cupomInput").val();
    var cupomValue = value.toUpperCase();

    if(cupomValue === "M10"){
      var appendCupom = '<input type="hidden" name="ID_cupom" value="'+ cupomValue +'"/>'
      var appendValidation = '<p class="valid">Cupom válido!</p>'
      $('#cupomValidation').append(appendValidation);
      $('#hiddenCoupon').append(appendCupom);
      location.reload();
    } else {
      var appendValidation = '<p class="invalid">Cupom inválido!</p>'
      $('#cupomValidation').append(appendValidation);
      return false;
    }
  });
});

</script>

<?php get_footer();